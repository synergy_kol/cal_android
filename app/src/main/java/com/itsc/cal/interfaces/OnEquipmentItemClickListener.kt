package com.itsc.cal.interfaces

/**
 * Created by Sandeep on 07-08-2018.
 */
interface OnEquipmentItemClickListener {
    fun onAddItemClick(equipment_option_id: String, equipment_id: String,position:Int )
    fun onRemovedItemClick(equipment_option_id: String, equipment_id: String,position:Int )
}