package com.itsc.cal.interfaces;

import java.util.ArrayList;

import com.google.android.gms.maps.model.PolylineOptions;

public interface AsyncInterface
{
	/*public void loginSuccessCallBack(ArrayList<RouteModel> routeList);
	public void loginFailureCallBack(String message);
	
	public void forgotPasswordSuccessCallBack(String message);
	public void forgotPasswordErrorCallBack(String message);
	
	
	public void myTeamSuccessCallBack(ArrayList<MyTeamModel> teamList, boolean viewAll);
	public void myTeamFailureCallBack(String message, boolean viewAll);
	
	public void outletLocationSuccessCallBack(ArrayList<OutLetModel> outLetList);
	public void outletLocationFailureCallBack(String message);
	
	public void refreshMyteamSuccessCallBack(ArrayList<MyTeamModel> teamList);*/
	
	public void viewNearestRoute(ArrayList<PolylineOptions> polylineOptionsList);
	public void viewNearestRouteErrorCallBack();
	
}
