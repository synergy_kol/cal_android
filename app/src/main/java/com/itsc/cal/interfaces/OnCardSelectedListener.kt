package com.itsc.cal.interfaces

import android.view.View
import androidx.appcompat.widget.AppCompatButton
import com.itsc.cal.model.CardListResponse

interface OnCardSelectedListener {
    fun onSelectCard(view:AppCompatButton, mCardListResponse: CardListResponse?)
}