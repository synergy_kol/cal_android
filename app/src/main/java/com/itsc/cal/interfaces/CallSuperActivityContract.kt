package com.itsc.cal.interfaces

interface CallSuperActivityContract {
    fun goBack() {}
    fun editProfile() {}
    fun saveProfile(){}
}