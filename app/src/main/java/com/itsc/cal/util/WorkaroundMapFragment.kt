package com.itsc.cal.util


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.gms.maps.SupportMapFragment


class WorkaroundMapFragment : SupportMapFragment() {
    private var mListener: OnTouchListener? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout: View? = super.onCreateView(inflater, parent, savedInstanceState)
        val frameLayout =
            TouchableWrapper(this.requireContext())
        frameLayout.setBackgroundColor(resources.getColor(android.R.color.transparent))
        (layout as ViewGroup?)!!.addView(
            frameLayout,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        return layout
    }

    fun setListener(listener: OnTouchListener?) {
        mListener = listener
    }

    interface OnTouchListener {
        fun onTouch()
    }

    inner class TouchableWrapper(context: Context) : FrameLayout(context) {
        override fun dispatchTouchEvent(event: MotionEvent): Boolean {

            try {
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> mListener!!.onTouch()
                    MotionEvent.ACTION_UP -> mListener!!.onTouch()
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
            return super.dispatchTouchEvent(event)
        }
    }
}