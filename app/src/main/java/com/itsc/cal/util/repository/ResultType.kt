package com.itsc.cal.util.repository

enum class ResultType {
    OK, FAIL
}