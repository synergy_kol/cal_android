package com.itsc.cal.util.repository

interface SuperRepositoryCallback<in T> {
    fun success(result: T) {}
    fun notAuthorised() {}
    fun noContent() {}
    fun error(result: com.itsc.cal.util.model.Result) {}
}