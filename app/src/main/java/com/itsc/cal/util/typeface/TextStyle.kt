package com.itsc.cal.util.typeface

enum class TextStyle {
    LIGHT,
    NORMAL,
    MEDIUM,
    BOLD
}