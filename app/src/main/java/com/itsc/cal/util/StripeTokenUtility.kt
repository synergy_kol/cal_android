package com.itsc.cal.util

import android.app.ProgressDialog
import android.content.Context
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.stripe.android.ApiResultCallback
import com.stripe.android.Stripe
import com.stripe.android.model.Card
import com.stripe.android.model.Token


val PUBLIC_kEY =
    "pk_test_51HQXnUJkoYtFflHH1PqSF1x55FxEbaEDXIzduqv6cbJSNr3qBzsAY0kpQDpfDNY4agB9pCNFGOjlG6TFXAp2DlMx00ZOX6P7ci"
val Secret_KEY =
    "sk_test_51HQXnUJkoYtFflHHtXDXdNB5111TXKJrBpFCJIQikTnMrL7IHxLJyYXxeuvLXju3bTCYSTcEH5grkgqGNwLEp0NC00WWZHGHAV"

fun Context.generateStripeToken(
    cardNumber: String,
    cardExpMonth: Int,
    cardExpYear: Int,
    cardCVV: String, onSuccess: (token: String) -> Unit
) {


    val pd = showProgressDialog("Processing Card")

    val card = Card.create(
        cardNumber,
        cardExpMonth,
        cardExpYear,
        cardCVV
    )

    val isValid = card.validateCard()
    Log.d("Stripe", "Token: $isValid")
    Log.d("Stripe", "Token: $card")
    if (isValid) {
        val stripe = Stripe(this, PUBLIC_kEY)
        stripe.createCardToken(card, callback = object : ApiResultCallback<Token> {

            override fun onSuccess(result: Token) {
                Log.d("Stripe", "Token: $result")
                pd.dismiss()
                onSuccess(result.id)
            }

            override fun onError(e: Exception) {
                Log.d("Stripe", "Token: $e")
                pd.dismiss()
            }
        })
    } else {
        pd.dismiss()

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Invalid Card")
        builder.setMessage("Please add a valid card for success full payment.")
        builder.setPositiveButton("OK") { dialogInterface, which ->

        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()

    }
}


fun Context.showProgressDialog(message: String): ProgressDialog {
    val pd = ProgressDialog(this)
    pd.setCancelable(false)
    pd.setMessage(message)
    pd.show()
    return pd
}