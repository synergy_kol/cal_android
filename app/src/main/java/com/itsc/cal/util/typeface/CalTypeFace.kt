package com.itsc.cal.util.typeface

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.TypefaceSpan
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import com.itsc.cal.R


class CalTypeFace(
    private val context: Context,
    private val textToSet: String
) {
    private var textColor: Int = 0
    private var textStyle: TextStyle =
        TextStyle.NORMAL

    fun textColor(textColor: Int): CalTypeFace {
        this.textColor = textColor
        return this
    }

    fun typeFace(textStyle: TextStyle): CalTypeFace {
        this.textStyle = textStyle
        return this
    }

    fun getSpannable(textView: AppCompatTextView? = null): ArrayList<Spannable> {
        val list: ArrayList<String> = ArrayList()
        val listToReturn: ArrayList<Spannable> = ArrayList()
        if (
            textToSet.matches(
                Regex(
                    "^[a-zA-Z ]*$"
                )
            )
        ) {
            list.add(textToSet)
        } else {
            val splitList = textToSet.toCharArray()
            var isSpecialChar = false
            splitList.forEachIndexed { index, ch ->
                if (
                    ch.toString().matches(
                        Regex(
                            "^[a-zA-Z ]*$"
                        )
                    )
                ) {
                    if (index != 0 && !isSpecialChar) {
                        list[list.size - 1] = "${list[list.size - 1]}$ch"
                    }else{
                        list.add(ch.toString())
                    }
                    isSpecialChar = false
                } else {
                    if (index != 0 && isSpecialChar) {
                        list[list.size - 1] = "${list[list.size - 1]}$ch"
                    }else{
                        list.add(ch.toString())
                    }
                    isSpecialChar = true
                }
            }
        }

        list.forEach {
            val spannable: Spannable = SpannableString(it)
            spannable.setSpan(
                TypeFaceSpanMaster(it),
                0,
                spannable.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            listToReturn.add(spannable)
        }

        textView?.let {
            listToReturn.forEachIndexed { index, spannable ->
                if (index == 0)
                    it.text = spannable
                else
                    it.append(spannable)
            }
        }

        return listToReturn
    }

    inner class TypeFaceSpanMaster(private val textToOperate: String) : TypefaceSpan(null) {
        override fun updateDrawState(ds: TextPaint) {
            setUp(ds)
        }

        override fun updateMeasureState(paint: TextPaint) {
            setUp(paint)
        }

        private fun setUp(paint: TextPaint) {
            if (
                textToOperate.matches(
                    Regex(
                        "^[a-zA-Z ]*$"
                    )
                )
            ) {
                paint.typeface = when (textStyle) {
                    TextStyle.LIGHT -> {
                        ResourcesCompat.getFont(
                            context,
                            R.font.poppins_light
                        )
                    }
                    TextStyle.NORMAL -> {
                        ResourcesCompat.getFont(
                            context,
                            R.font.poppins_regular
                        )
                    }
                    TextStyle.MEDIUM -> {
                        ResourcesCompat.getFont(
                            context,
                            R.font.poppins_medium
                        )
                    }
                    TextStyle.BOLD -> {
                        ResourcesCompat.getFont(
                            context,
                            R.font.poppins_bold
                        )
                    }
                }
            } else {
                paint.typeface = when (textStyle) {
                    TextStyle.LIGHT -> {
                        ResourcesCompat.getFont(
                            context,
                            R.font.poppins_light
                        )
                    }
                    TextStyle.NORMAL -> {
                        ResourcesCompat.getFont(
                            context,
                            R.font.poppins_regular
                        )
                    }
                    TextStyle.MEDIUM -> {
                        ResourcesCompat.getFont(
                            context,
                            R.font.poppins_medium
                        )
                    }
                    TextStyle.BOLD -> {
                        ResourcesCompat.getFont(
                            context,
                            R.font.poppins_bold
                        )
                    }
                }
            }

            if (textColor == 0) {
                paint.color = ResourcesCompat.getColor(
                    context.resources,
                    R.color.textBlack,
                    null
                )
            } else {
                paint.color = textColor
            }
        }
    }
}