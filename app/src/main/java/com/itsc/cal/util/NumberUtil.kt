package com.itsc.cal.util

object NumberUtil {
    fun roundToTwoDecimal(number: Float): String {
        return "%.0f".format(number)
    }
}