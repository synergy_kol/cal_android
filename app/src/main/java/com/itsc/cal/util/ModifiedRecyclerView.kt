package com.itsc.cal.util

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView

class ModifiedRecyclerView(context: Context, attributeSet: AttributeSet?, defStyle: Int) :
    RecyclerView(context, attributeSet, defStyle) {

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attributeSet: AttributeSet) : this(context, attributeSet, 0)

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        val eventConsumed = super.onInterceptTouchEvent(event)

        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> if (scrollState == SCROLL_STATE_SETTLING) {
                parent.requestDisallowInterceptTouchEvent(false)
                // only if it touched the top or the bottom.
                if (!canScrollVertically(-1) || !canScrollVertically(1)) {
                    // stop scroll to enable child view to get the touch event
                    stopScroll()
                    // do not consume the event
                    return false
                }
            }
        }

        return eventConsumed
    }
}