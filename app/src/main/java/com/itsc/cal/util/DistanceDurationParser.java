package com.itsc.cal.util;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

public class DistanceDurationParser
{
	private ArrayList<LatLng> list;
	private String distance,duration;
	private String JSON_ROUTE="routes";
	private String JSON_LEGS="legs";
	private String JSON_DISTANCE="distance";
	private String JSON_TEXT="text";
	private String JSON_DURATION="duration";
	private String JSON_STEPS="steps";
	private String JSON_START_LOCATION="start_location";
	private String JSON_END_LOCATION="end_location";
	private String JSON_LAT="lat";
	private String JSON_LONG="lng";
	private String JSON_STATUS="status";
	private String status;
	
	public DistanceDurationParser(String json)
	{
		list=new ArrayList<LatLng>();
		parser(json);
	}
	
	private void parser(String json)
	{
		try
		{
			JSONObject jsonobject=new JSONObject(json);
			status=jsonobject.getString(JSON_STATUS);
			if(status.equalsIgnoreCase("OK"))
			{
				JSONArray route=jsonobject.getJSONArray(JSON_ROUTE);
				for(int length=0;route.length()>length;length++)
				{
					JSONObject object=(JSONObject) route.get(0);
					JSONArray legs=object.getJSONArray(JSON_LEGS);
					
					for(int leglength=0;legs.length()>leglength;leglength++)
					{
						JSONObject legobject=(JSONObject) legs.get(leglength);
						distance=(legobject.getJSONObject(JSON_DISTANCE)).getString(JSON_TEXT);
						duration=(legobject.getJSONObject(JSON_DURATION)).getString(JSON_TEXT);
						JSONArray steps=legobject.getJSONArray(JSON_STEPS);
						
						for(int stepslength=0;steps.length()>stepslength;stepslength++)
						{
							JSONObject point=(JSONObject) steps.get(stepslength);
							JSONObject spoint=point.getJSONObject(JSON_START_LOCATION);
							JSONObject epoint=point.getJSONObject(JSON_END_LOCATION);
							LatLng startpoint=new LatLng(spoint.getDouble(JSON_LAT),spoint.getDouble(JSON_LONG));
							LatLng endpoint=new LatLng(epoint.getDouble(JSON_LAT),epoint.getDouble(JSON_LONG));
							list.add(startpoint);
							list.add(endpoint);
						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			
		}
		
	}
	
	public String getDistance()
	{
		return this.distance;
	}
	
	public String getDuration()
	{
		return this.duration;		
	}
	
	public ArrayList<LatLng> getPoints()
	{
		return this.list;
	}
	
	public String getStatus()
	{
		return this.status;
	}

}
