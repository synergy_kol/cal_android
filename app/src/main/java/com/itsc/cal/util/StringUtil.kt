package com.itsc.cal.util

import kotlin.random.Random

class StringUtil {
    companion object {
        private const val ALLOWED_CHARACTERS =
            "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"

        fun getRandomString(sizeOfRandomString: Int = Random.nextInt(5, 120)): String {
            val sb = StringBuilder(sizeOfRandomString)
            for (i in 0 until sizeOfRandomString) {
                sb.append(ALLOWED_CHARACTERS[Random.nextInt(ALLOWED_CHARACTERS.length)])
                if (Random.nextInt(0, 5) == 0)
                    sb.append(" ")
            }
            return sb.toString()
        }
    }
}