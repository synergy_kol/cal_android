@file:Suppress("unused")

package com.itsc.cal.util

import androidx.lifecycle.ViewModel
import com.itsc.cal.repository.Repository

open class SuperViewModel(private val repository: Repository) : ViewModel() {

    private var activityPaused = false
    private var shouldNotifyWhileActivityPaused = true

    fun notifyDuringPaused(shouldNotifyWhileActivityPaused: Boolean) {
        this.shouldNotifyWhileActivityPaused = shouldNotifyWhileActivityPaused
    }

    fun activityPaused() {
        activityPaused = true
    }

    fun activityResumed() {
        activityPaused = false
    }

    fun canPushData(): Boolean {
        if (shouldNotifyWhileActivityPaused)
            return true
        return !activityPaused
    }

    fun giveRepository(): Repository {
        return repository
    }
}