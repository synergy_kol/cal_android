package com.itsc.cal.util;

import android.graphics.drawable.Drawable;

public interface DropDownItem {
    String getTitle();
    Drawable getItemIcon();
    String getId();
}
