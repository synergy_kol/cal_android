package com.itsc.cal.model

data class EquipmentListResponse(
    var equipment_id: String,
    var name: String,
    var is_active:String,
    var type:String,
    var equipment_option:ArrayList<EquipmentOptionResponse>
)