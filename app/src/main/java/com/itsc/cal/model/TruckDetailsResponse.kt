package com.itsc.cal.model

data class TruckDetailsResponse(
    var truck_id: String?="",
    var truck_vin_number: String?="",
    var truck_reg_number:String?="",
    var truck_model:String?="",
    var truck_type_name:String?="",
    var truckimage:String?="",
    var selected:String?="",
    var truck_empty_weight:String,
    var truck_color:String,
    var truck_weight_metric:String,
    var images:ArrayList<Images>
)