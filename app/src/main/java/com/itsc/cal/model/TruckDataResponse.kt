package com.itsc.cal.model

data class TruckDataResponse(
    var truck_type_id: String,
    var name: String

)