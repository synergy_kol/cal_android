package com.itsc.cal.model


data class TruckLocation(

	val lng: String? = null,

	val job_id: String? = null,

	val created_at: String? = null,

	val id: String? = null,

	val lat: String? = null,

	val status: String? = null
)
