package com.itsc.cal.model

data class PostJobRequest(
    var source_lat: String,
    var Equipment_value:String,
    var Weight:String,
    var TruckType:String,
    var Source:String,
    var priority:String,
    var pickup_time:String,
    var pickup_date:String,
    var NoOfTrucks:String,
    var MaterialType:String,
    var LodeType:String,
    var JobEstimatePrice:String,
    var FreightCharges:String,
    var Equipment:String,
    var card_id:String,
    var Destination:String,
    var delivery_time:String,
    var delivery_date:String,
    var destination_long:String,
    var destination_lat:String,
    var source_long:String,
    var builderId:String,
    var stripeToken:String,
    var payment_type:String,
    var jobPayableAmount:String

)