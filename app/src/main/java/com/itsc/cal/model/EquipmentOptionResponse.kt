package com.itsc.cal.model

data class EquipmentOptionResponse(
    var equipment_id: String,
    var equipment_option_id: String,
    var option_title:String,
    var is_active:String

)