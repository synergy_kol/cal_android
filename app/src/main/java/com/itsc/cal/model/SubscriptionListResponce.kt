package com.itsc.cal.model

import java.io.Serializable

data class SubscriptionListResponce(
    var title: String? = null,
    var id: String? = null,
    var amount: String? = null,
    var sale_tax: String? = null,
    var tax_amount: String? = null,
    var user_taken_status: String? = null,
    var details:detailsResponse
)

data class detailsResponse(
    var id: String? = null,
    var subscription_date: String? = null,
    var subscription_end_date: String? = null
)
