package com.itsc.cal.model

data class NotificationResponce (

    var stripe_check:stripeCheck,
    var notification_count:notificationCount,
    var notification_list:ArrayList<notificationList>
)
data class stripeCheck(
    var stripe_account_id:String
)

data class notificationCount(
    var count:String
)
data class notificationList(
    var notification_id:String,
    var user_id:String,
    var alert_type:String,
    var notification_title:String,
    var notification_description:String,
    var view_status:String,
    var created_ts:String
)
