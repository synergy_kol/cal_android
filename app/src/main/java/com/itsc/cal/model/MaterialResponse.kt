package com.itsc.cal.model

data class MaterialResponse(
    var material_id: String,
    var name: String,
    var is_active:String,
    var type:String,
    var material_option:ArrayList<MaterialOptionResponse>?=null
)