package com.itsc.cal.model

data class EquipementResponse(
    var equipment_id: String,
    var name: String

)