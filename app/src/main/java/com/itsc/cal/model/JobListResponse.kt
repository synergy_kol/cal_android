package com.itsc.cal.model

data class JobListResponse(
    var job_id: String,
    var user_master_id: String,
    var supplier_id:String,
    var LodeType:String,
    var Source:String,
    var Destination:String,
    var pickup_date:String,
    var delivery_date:String,
    var pickup_time:String,
    var delivery_time:String,
    var source_lat:String,
    var source_long:String,
    var self_job:String,
    var destination_lat:String,
    var destination_long:String,
    var priority:String,
    var MaterialType:String,
    var Weight:String,
    var TruckType:String,
    var FreightCharges:String,
    var Equipment:String,
    var job_status:String,
    var status:String?="",
    var Equipment_value:String,
    var NoOfTrucks:String,
    var JobEstimatePrice:String,
    var material_name:String,
    var truck_name:String,
    var filter:String="",
    var supplier_otp:String,
    var builder_otp:String?="",
    var equipment_name:String?="",
    var job_type:String?="",
    var spec_sheet:String?="",
    var lib_information:String?="",
    var material_cont:String?="",
    var material_option_name:String?="",
    var materials_available:String?="",
    var equipment_list:ArrayList<EquipementDetailsResponse>?=null,
    var truck_details:ArrayList<BuilderTruckDetailsResponse>?=null


)