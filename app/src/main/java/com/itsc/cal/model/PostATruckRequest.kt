package com.itsc.cal.model

import java.io.File

data class PostATruckRequest(
    var user_master_id: String,
    var truck_vin_number:String,
    var truck_reg_number:String,
    var truck_make:String,
    var truck_body_style:String,
    var truck_model:String,
    var truck_color:String,
    var truck_cabin_type:String,
    var truck_empty_weight:String,
    var truck_carring_capacity:String,
    var truck_weight_metric:String,
    var truck_type:String,
    var truck_image:File?=null,
    var front:File?=null,
    var back:File?=null,
    var left:File?=null,
    var right:File?=null,
    var usdot:File?=null

)