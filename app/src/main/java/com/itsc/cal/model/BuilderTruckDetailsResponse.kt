package com.itsc.cal.model

data class BuilderTruckDetailsResponse(
    var truck_id: String,
    var empty_truck_weight: String,
    var empty_truck_slip:String,
    var loaded_truck_weight:String,
    var loaded_truck_slip:String,
    var truck_reg_number:String


)