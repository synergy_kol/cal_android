package com.itsc.cal.model

data class NearByConductorListResponse(
    var user_master_id: String,
    var user_role:String,
    var email:String,
    var fname:String,
    var lanme:String,
    var mobile:String,
    var address:String,
    var lat:String,
    var lon:String,
    var company_name:String,
    var company_registration:String,
    var builder_image:String,
    var license_number:String,
    var license:String,
    var trucks:ArrayList<TruckListResponse>
)