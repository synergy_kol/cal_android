package com.itsc.cal.model

import android.graphics.drawable.Drawable
import com.itsc.cal.util.DropDownItem
import java.io.Serializable

data class StateResponce (
    var state_id: String? = null,
    var state_name: String? = null,
    var country_id: String? = null
): DropDownItem, Serializable {
    override fun getTitle(): String? {
        return state_name
    }
    override fun getItemIcon(): Drawable? {
        return null
    }

    override fun getId(): String? {
        return state_id
    }
}
