package com.itsc.cal.model

data class MaterialEquipmentResponse(
    var material_list:ArrayList<MaterialResponse>,
    var equipment_list:ArrayList<EquipmentListResponse>
)