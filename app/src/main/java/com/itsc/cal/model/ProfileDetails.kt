package com.itsc.cal.model

data class ProfileDetails(
    var user_master_id: String,
    var user_role: String,
    var email: String,
    var password: String,
    var password_token: String,
    var registration_source: String,
    var fname: String,
    var lanme: String,
    var mobile: String,
    var address: String,
    var country: String,
    var state: String,
    var pincode: String,
    var city: String,
    var lat: String,
    var lon: String,
    var dob: String,
    var cal_registration: String,
    var profile_image: String,
    var license_number: String,
    var company_name: String,
    var company_registration: String,
    var license: String,
    var created_ts: String,
    var login_status: String,
    var otp: String,
    var otp_time: String,
    var verification_code: String,
    var vcerification_device: String,
    var verification_time: String,
    var verification_status: String,
    var user_type: String,
    var numberof_truck: String,
    var sos_verification: String,
    var stripe_account_id: String,
    var stripe_publish_Key: String,
    var stripe_token_type: String,
    var stripe_refresh_token: String,
    var stripe_access_token: String,
    var payout_after_day: String,
    var is_active: String,
    var updated_ts: String,
    var days_ago: String,
    var total_job: String,
    var avarage_rating: String,
    var full_name: String
)
data class CompleteProfileInfo(
    var user_master_id:String?="",
    var dob:String?="",
    var company_legal_name:String?="",
    var company_dba:String?="",
    var phone_2:String?="",
    var email_2:String?="",
    var company_ein:String?="",
    var driver_license:String?="",
    var driver_license_exp:String?="",
    var street_address:String?="",
    var zip:String?="",
    var is_billing_address:String?=""
)
data class BillingInfo(
    var street_address:String?="",
    var zip:String?="",
    var city: String?="",
    var state:String?="",
    var state_name:String?="",
    var city_name:String?=""
)

data class ProfileResponse(
    var profile:ProfileDetails?=null,
    var complete_profile_info:CompleteProfileInfo?=null,
    var billing_info:BillingInfo?=null
)