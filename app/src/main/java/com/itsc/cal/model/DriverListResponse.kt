package com.itsc.cal.model

data class DriverListResponse(
    var driver_name: String,
    var driver_id: String,
    var driverimage:String,
    var driver_number:String,
    var driver_licence:String,
    var driverlicenceimage:String,
    var email:String,
    var selected:String?="0"

)