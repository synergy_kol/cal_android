package com.itsc.cal.model

data class HaulofSubscriptionStatusResponce(
    var id: String? = null,
    var user_id: String? = null,
    var subscription_type: String? = null,
    var subscription_amount: String? = null,
    var subscription_date: String? = null,
    var subscription_status: Int? = 0,
    var subscription_end_date: String? = null,
    var charge_id: String? = null,
    var payment_tnx_id: String? = null,
    var payment_status: String? = null,
    var payment_receipt_url: String? = null

)
