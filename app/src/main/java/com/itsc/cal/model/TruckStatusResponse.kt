package com.itsc.cal.model

data class TruckStatusResponse(
    var truck_id: String? = "",
    var truck_vin_number: String? = "",
    var truck_reg_number: String? = "",
    var truck_model: String? = "",
    var truck_type_name: String? = "",
    var truck_image: String? = "",
    var selected: String? = "",
    var driver_job_status: String? = "",
    var Source: String? = "",
    var Destination: String? = "",
    var pickup_date: String? = "",
    var delivery_date: String? = "",
    var pickup_time: String? = "",
    var delivery_time: String? = "",
    var source_lat: String? = "",
    var source_long: String? = "",
    var destination_lat: String? = "",
    var job_id: String? = "",
    var name: String? = "",
    var destination_long: String? = "",
    var driver_licence: String? = ""
)