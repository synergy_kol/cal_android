package com.itsc.cal.model

data class BannerResponce (
    var banner_id: String? = null,
    var banner_title: String? = null,
    var tips:String?=null,
    var type:String?=null,
    var banner_image: String? = null
)
