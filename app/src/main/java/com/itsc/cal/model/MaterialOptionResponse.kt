package com.itsc.cal.model

data class MaterialOptionResponse(
    var material_id : String,
    var material_option_id: String,
    var option_title:String,
    var is_active:String

)