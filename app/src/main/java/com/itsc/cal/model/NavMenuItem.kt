package com.itsc.cal.model

import com.itsc.cal.enums.Menus

data class NavMenuItem(
    var image: Int,
    var name: String,
    var selected: Boolean = false,
    var innerItems: ArrayList<NavMenuInnerItem> = ArrayList(),
    var menu: Menus
)