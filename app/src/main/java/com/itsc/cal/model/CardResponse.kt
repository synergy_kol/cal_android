package com.itsc.cal.model

data class CardResponse(
    var card_id: String,
    var user_master_id: String,
    var number:String,
    var expiry_month:String,
    var expiry_year:String,
    var card_name:String,
    var cvv_number:String

)