package com.itsc.cal.model

data class ExportHaulOffResponse(
    var all_user_export_request:ArrayList<HauljobListResponse>?=null,
    var self_export_request:ArrayList<HauljobListResponse>?=null

)