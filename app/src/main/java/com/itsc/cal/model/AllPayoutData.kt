package com.itsc.cal.model


data class AllPayoutData(

	val updated_ts: String? = null,

	val transfer_group: String? = null,

	val is_active: String? = null,

	val tnx_id: String? = null,

	val job_payout_id: String? = null,

	val destination: String? = null,

	val transfer_id: String? = null,

	val transfer_status: String? = null,

	val cal_commission: String? = null,

	val transfer_amount: String? = null,

	val created_ts: String? = null,

	val job_id: String? = null,

	val destination_payment: String? = null,

	val fleet_id: String? = null
)
