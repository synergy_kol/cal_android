package com.itsc.cal.model

data class DashboardStatus(
    var driver_no: Int? = null,
    var truck_no: Int? = null,
    var certificate_status: Int? = null

)
