package com.itsc.cal.model

data class TruckListResponse(
    var truck_id: String?="",
    var truck_vin_number: String?="",
    var truck_reg_number:String?="",
    var truck_model:String?="",
    var truck_type_name:String?="",
    var truckimage:String?="",
    var selected:String?=""
)