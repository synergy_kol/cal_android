package  com.itsc.cal.model

data class SOSResponce(
    var sos_verification:String="0",
    var document_list:ArrayList<SOSDocumentResponse>,
    var completed_steps:String = "0"
)
