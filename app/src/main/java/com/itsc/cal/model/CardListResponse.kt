package com.itsc.cal.model

data class CardListResponse(
    var user_master_id: String,
    var card_id:String,
    var number:String,
    var expiry_month:String,
    var expiry_year:String,
    var card_name:String,
    var is_default:String,
    var is_active:String,
    var cvv:String,
    var cvv_number:String,
    var isSelected:Boolean? =false

)