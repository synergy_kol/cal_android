package com.itsc.cal.model

import com.itsc.cal.enums.Menus

data class DashboardGridItem(
    var name: String,
    var image: Int,
    var menu: Menus
)