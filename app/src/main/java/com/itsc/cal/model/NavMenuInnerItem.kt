package com.itsc.cal.model

import com.itsc.cal.enums.Menus

data class NavMenuInnerItem(
    var name: String,
    var selected: Boolean = false,
    var menu: Menus
)