package com.itsc.cal.model

data class VehicleImage (
    var imageFileString:String,
    var isAddImage:Boolean
)
