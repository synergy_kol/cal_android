package com.itsc.cal.model

data class ImportJobPostStep2(
    var haul_request_id: String,
    var MaterialType:String,
    var material_other:String,
    var MaterialoptionType:String,
    var cubic_yards:String,
    var tons:String,
    var equipment_type:String,
    var equipment_other:String,
    var equipment_any:String,
    var spreading_equipment_type:String,
    var spreading_equipment_other:String,
    var additional_notes:String
   )