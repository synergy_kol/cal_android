package com.itsc.cal.model

data class ImportHaulOffResponse(
    var all_user_import_request:ArrayList<HauljobListResponse>?=null,
    var self_import_request:ArrayList<HauljobListResponse>?=null

)