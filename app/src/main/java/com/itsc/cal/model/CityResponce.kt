package com.itsc.cal.model

import android.graphics.drawable.Drawable
import com.google.gson.annotations.SerializedName
import com.itsc.cal.util.DropDownItem
import java.io.Serializable

data class CityResponce (
    @field:SerializedName("id")
    var city_id: String? = null,
    var city_name: String? = null

): DropDownItem, Serializable {
    override fun getTitle(): String? {
        return city_name
    }


    override fun getItemIcon(): Drawable? {
        return null
    }

    override fun getId(): String? {
        return city_id
    }
}
