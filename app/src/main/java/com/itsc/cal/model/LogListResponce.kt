package com.itsc.cal.model

data class LogListResponce (
    var id: String? = null,
    var user_master_id: String? = null,
    var source:String?=null,
    var action:String?=null,
    var lat: String? = null,
    var lng: String? = null,
    var created_at:String?=null
)
