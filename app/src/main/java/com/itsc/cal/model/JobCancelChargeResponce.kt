package com.itsc.cal.model

data class JobCancelChargeResponce (
    var amount: String? = null,
    var sales_tax: String? = null,
    var pay_amount:String?=null

)
