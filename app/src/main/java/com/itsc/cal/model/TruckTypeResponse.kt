package com.itsc.cal.model

data class TruckTypeResponse(
    var truck_type_id: String="",
    var name: String="",
    var selected:String?="0"

)