package  com.itsc.cal.model

import com.itsc.cal.enums.Result

data class StatusReply(
    var error_code: Int = 0,
    var message: String = "",
    var result: Result? = null
) {
    fun isResultOk(): Boolean {
        if (result == null)
            result = if (error_code == 0) Result.OK else Result.FAIL

        return result == Result.OK
    }
}