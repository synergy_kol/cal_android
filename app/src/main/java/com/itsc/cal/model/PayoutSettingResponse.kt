package com.itsc.cal.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PayoutSettingResponse(

	var routing: String? = null,
	var account_type: String? = null,
	var user_master_id: String? = null,
	var card_no: String? = null,
	var response: String? = null,
	var name: String? = null,
	var created_at: String? = null,
	var connect_type: String? = null,
	var id: String? = null,
	var is_default: String? = null,
	var account: String? = null,
	var status: String? = null,
    var completed_steps:String?=null
):Parcelable
