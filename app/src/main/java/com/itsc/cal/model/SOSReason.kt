package com.itsc.cal.model

data class SOSReason(
    var sos_reason_id: String,
    var sos_reason: String

)