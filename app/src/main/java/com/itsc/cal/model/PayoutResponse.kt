package com.itsc.cal.model

data class PayoutResponse(
    var job_id: String,
    var JobEstimatePrice:String,
    var cal_commission:String,
    var payment_amount:String,
    var round_payment_amount:String,
    var fleet_id:String,
    var fleet_email:String,
    var fleet_phone:String,
    var job_invoice:String
)