package com.itsc.cal.model

data class ReviewResponse(
    var review_id: String,
    var comment: String,
    var rating:String,
    var sender_id:String,
    var receiver_id:String,
    var job_id:String


)