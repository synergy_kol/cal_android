package com.itsc.cal.model

data class EquipementDetailsResponse(
    var equipment_id: String,
    var equipment_option_id: String,
    var equipment_name: String,
    var equipment_option_name: String?=""

)