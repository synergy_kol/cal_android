package com.itsc.cal.model

data class SOSDocumentResponse(
    var document_id: String?="",
    var document_type: String?="",
    var document_image:String?="",
    var document_status:String?="",
    var document_title:String?="",
    var isMandatory:String?=""
)