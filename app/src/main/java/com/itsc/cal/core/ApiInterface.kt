package com.itsc.cal.core

import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("index.php/user/userlogin")
    fun login(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("source") source: String,
        @Field("device_token") device_token: String,
        @Field("device_type") device_type: String,
        @Field("lat") lat: String,
        @Field("lng") lng: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("index.php/user/submit_register")
    fun signUp(
        @Field("fname") fname: String,
        @Field("lanme") lanme: String,
        @Field("company_name") company_name: String,
        @Field("company_registration") company_registration: String,
        @Field("mobile") mobile: String,
        @Field("email") email: String,
        @Field("registration_source") registration_source: String,
        @Field("password") password: String,
        @Field("user_role") user_role: String,
        @Field("license") license: String,
        @Field("user_type") user_type: String,
        @Field("numberof_truck") numberof_truck: String,
        @Field("latitude") latitude: String,
        @Field("longitude") longitude: String,
        @Field("state") state: String,
        @Field("city") city: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/resendotp")
    fun reSendOTP(
        @Field("user_master_id") user_master_id: String,
        @Field("email") email: String,
        @Field("mobile") mobile: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/send_verification_code")
    fun sendTwoStepVerificationCode(
        @Field("user_master_id") user_master_id: String,
        @Field("email") email: String,
        @Field("mobile") mobile: String,
        @Field("device_token") device_token: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/verifyuserstep")
    fun verifyOTPTwoStepVerification(
        @Field("user_master_id") user_master_id: String,
        @Field("otp") otp: String,
        @Field("device_token") device_token: String,
        @Field("is_trusted") is_trusted: String
    ): Observable<Response<ResponseBody>>


    @FormUrlEncoded
    @POST("user/verifyuser")
    fun verifyOTP(
        @Field("user_master_id") user_master_id: String,
        @Field("otp") otp: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/password_reset_code")
    fun forgotPassword(
        @Field("cred") cred: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/check_reset_pwd_code")
    fun verifyOTPPasswordToken(
        @Field("user_master_id") user_master_id: String,
        @Field("password_token") password_token: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/password_reset_update")
    fun changePassword(
        @Field("password_token") password_token: String,
        @Field("password") password: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/update_password")
    fun updatePassword(
        @Field("user_master_id") user_master_id: String,
        @Field("password") password: String,
        @Field("new_password") new_password: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/get_master_data")
    fun getMasterData(
        @Field("table") table: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/submit_job")
    fun postJob(
        @Field("user_master_id") user_master_id: String,
        @Field("LodeType") LodeType: String,
        @Field("Source") Source: String,
        @Field("Destination") Destination: String,
        @Field("MaterialType") MaterialType: String,
        @Field("Weight") Weight: String,
        @Field("TruckType") TruckType: String,
        @Field("FreightCharges") FreightCharges: String,
        @Field("priority") priority: String,
        @Field("NoOfTrucks") NoOfTrucks: String,
        @Field("JobEstimatePrice") JobEstimatePrice: String,
        @Field("pickup_date") pickup_date: String,
        @Field("delivery_date") delivery_date: String,
        @Field("Equipment") Equipment: String,
        @Field("pickup_time") pickup_time: String,
        @Field("delivery_time") delivery_time: String,
        @Field("source_lat") source_lat: String,
        @Field("source_long") source_long: String,
        @Field("destination_lat") destination_lat: String,
        @Field("destination_long") destination_long: String,
        @Field("Equipment_value") Equipment_value: String,
        @Field("builder_id") builder_id: String,
        @Field("stripeToken") stripeToken: String,
        @Field("card_id") card_id: String,
        @Field("payment_type") payment_type: String,
        @Field("jobPayableAmount") jobPayableAmount: String

    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/my-posted-job")
    fun getJobList(
        @Field("user_master_id") user_master_id: String,
        @Field("filter") filter: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/haul_off_export")
    fun exportHaultOfJobLising(
        @Field("user_master_id") user_master_id: String,
        @Field("type") type: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/haul_off_import")
    fun importHaultOfJobLising(
        @Field("user_master_id") user_master_id: String,
        @Field("type") type: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/my_jobs")
    fun getBuilderJobList(
        @Field("user_master_id") user_master_id: String,
        @Field("filter") filter: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/manage_job")
    fun getJobListTruckList(
        @Field("user_master_id") user_master_id: String,
        @Field("job_status") job_status: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/builder_assign_job")
    fun getJobListBuilderList(
        @Field("builder_id") builder_id: String,
        @Field("supplier_id") supplier_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/view_document")
    fun viewDocument(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/create_request")
    fun createRequestHaultOf(
        @Field("user_master_id") user_master_id: String,
        @Field("request_type") request_type: String
    ): Observable<Response<ResponseBody>>

    // @POST("fleet/submit_sos")
    //  https://itsc.fitser.com/fleet/submit_sos_v1
    @POST("fleet/submit_sos_v1")
    @Multipart
    fun submitSOS(
        @Part("user_master_id") user_master_id: RequestBody,
        @Part("sos_document_type") sos_document_type: RequestBody,
        @Part sos_document_file: MultipartBody.Part?
    ): Observable<Response<ResponseBody>>

    @POST("fleet/update_sos")
    @Multipart
    fun updateSOS(
        @Part("user_master_id") user_master_id: RequestBody,
        @Part("document_id") document_id: RequestBody,
        @Part sos_document_file: MultipartBody.Part?
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/manage_builders")
    fun getBuilderListing(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/manage_suppliers")
    fun getSupplierList(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @POST("dashboard/submit_builder")
    @Multipart
    fun addBuilder(
        @Part("user_master_id") user_master_id: RequestBody,
        @Part("user_role") user_role: RequestBody,
        @Part("email") email: RequestBody,
        @Part("first_name") first_name: RequestBody,
        @Part("last_name") last_name: RequestBody,
        @Part("phone") phone: RequestBody,
        @Part("address") address: RequestBody,
        @Part("latitute") latitute: RequestBody,
        @Part("longitute") longitute: RequestBody,
        @Part("company_name") company_name: RequestBody,
        @Part builder_photo: MultipartBody.Part?
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/accept_job")
    fun applyJobs(
        @Field("user_master_id") user_master_id: String,
        @Field("job_id") job_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/hide_job")
    fun hideJobs(
        @Field("user_master_id") user_master_id: String,
        @Field("job_id") job_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/show_job")
    fun showJobs(
        @Field("user_master_id") user_master_id: String,
        @Field("job_id") job_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/cancle_job")
    fun cancelJobs(
        @Field("user_master_id") user_master_id: String,
        @Field("job_id") job_id: String,
        @Field("cancel_reason") cancel_reason: String,
        @Field("amount") amount: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/cancleJob")
    fun cancelJobsBuilderSupplier(
        @Field("user_master_id") user_master_id: String,
        @Field("job_id") job_id: String,
        @Field("cancel_reason") cancel_reason: String,
        @Field("stripeToken") stripeToken: String,
        @Field("role_id") role_id: String,
        @Field("amount") amount: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/truck_type")
    fun truckTypeList(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @POST("fleet/submit_truck")
    @Multipart
    fun addTruck(
        @Part("user_master_id") user_master_id: RequestBody,
        @Part("truck_vin_number") truck_vin_number: RequestBody,
        @Part("truck_reg_number") truck_reg_number: RequestBody,
        @Part("truck_make") truck_make: RequestBody,
        @Part("truck_body_style") truck_body_style: RequestBody,
        @Part("truck_model") truck_model: RequestBody,
        @Part("truck_color") truck_color: RequestBody,
        @Part("truck_cabin_type") truck_cabin_type: RequestBody,
        @Part("truck_empty_weight") truck_empty_weight: RequestBody,
        @Part("truck_carring_capacity") truck_carring_capacity: RequestBody,
        @Part("truck_weight_metric") truck_weight_metric: RequestBody,
        @Part("truck_type") truck_type: RequestBody,
        @Part truck_image: MultipartBody.Part?,
        @Part front: MultipartBody.Part?,
        @Part back: MultipartBody.Part?,
        @Part left: MultipartBody.Part?,
        @Part right: MultipartBody.Part?,
        @Part usdot: MultipartBody.Part?
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/posted_trucks")
    fun truckList(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/manage_drivers")
    fun driverList(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @POST("fleet/configure_job")
    @Multipart
    fun jobConfiguration(
        @Part("user_master_id") user_master_id: RequestBody,
        @Part("job_id") job_id: RequestBody,
        @Part("driver_id[]") driver_id: ArrayList<RequestBody>,
        @Part("truck_id[]") truck_id: ArrayList<RequestBody>
    ): Observable<Response<ResponseBody>>

    @POST("fleet/get_job_by_token")
    @FormUrlEncoded
    fun getJobByToken(
        @Field("token_id") tokenId: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/truck_details")
    fun truckDetails(
        @Field("truck_id") truck_id: String
    ): Observable<Response<ResponseBody>>

    @POST("fleet/submit_driver")
    @Multipart
    fun addDriver(
        @Part("user_master_id") user_master_id: RequestBody,
        @Part("driver_name") driver_name: RequestBody,
        @Part("email") email: RequestBody,
        @Part("driver_number") driver_number: RequestBody,
        @Part("driver_licence") driver_licence: RequestBody,
        @Part driver_licence_photo: MultipartBody.Part?,
        @Part driver_photo: MultipartBody.Part?
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/add_submit_card")
    fun saveCard(
        @Field("user_master_id") user_master_id: String,
        @Field("number") number: String,
        @Field("expiry_month") expiry_month: String,
        @Field("expiry_year") expiry_year: String,
        @Field("card_name") card_name: String,
        @Field("cvv_number") cvv_number: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/my-cards")
    fun getCard(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/make_default")
    fun markAsDefault(
        @Field("user_master_id") user_master_id: String,
        @Field("card_id") card_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/remove_card")
    fun deleteCard(
        @Field("card_id") card_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/edit_submit_card")
    fun editCard(
        @Field("user_master_id") user_master_id: String,
        @Field("number") number: String,
        @Field("expiry_month") expiry_month: String,
        @Field("expiry_year") expiry_year: String,
        @Field("card_name") card_name: String,
        @Field("card_id") card_id: String,
        @Field("cvv_number") cvv_number: String
    ): Observable<Response<ResponseBody>>


    @POST("fleet/driver_strat_job")
    @Multipart
    fun startJob(
        @Part("token_id") token_id: RequestBody,
        @Part("truck_weight") truck_weight: RequestBody,
        @Part truck_image: MultipartBody.Part?,
        @Part challan: MultipartBody.Part?
    ): Observable<Response<ResponseBody>>

    @POST("fleet/proceed_job")
    @Multipart
    fun emptyTruckAPI(
        @Part("token_id") token_id: RequestBody,
        @Part("truck_weight") truck_weight: RequestBody,
        @Part truck_image: MultipartBody.Part?
    ): Observable<Response<ResponseBody>>

    @POST("fleet/driver_complete_job")
    @Multipart
    fun endTrip(
        @Part("token_id") token_id: RequestBody,
        @Part("receiver_name") receiver_name: RequestBody,
        @Part("receiver_nric") receiver_nric: RequestBody,
        @Part("driver_job_status") driver_job_status: RequestBody,
        @Part signature: MultipartBody.Part?,
        @Part final_receipt: MultipartBody.Part?
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/delay_reason")
    fun marksAsDelay(
        @Field("token_id") token_id: String,
        @Field("delay_reason") delay_reason: String,
        @Field("delay_time") delay_time: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/get_fleet_by_lat_lon")
    fun getNearByConductor(
        @Field("lat") lat: String,
        @Field("long") long: String,
        @Field("distance") distance: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("/fleet/job_truck_details")
    fun getTruckStatus(
        @Field("job_id") job_id: String,
        @Field("builder_id") builder_id: String,
        @Field("supplier_id") supplier_id: String,
        @Field("fleet_id") fleet_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/default_card")
    fun getDefaultCard(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/material_equipment_list")
    fun getMeterialEquipmentList(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/equipment_material_list")
    fun getEquipementOption(
        @Field("type") type: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/submit_review")
    fun submitReview(
        @Field("comment") comment: String,
        @Field("rating") rating: String,
        @Field("sender_id") sender_id: String,
        @Field("receiver_id") receiver_id: String,
        @Field("job_id") job_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/get_review")
    fun getReview(
        @Field("sender_id") sender_id: String,
        @Field("receiver_id") receiver_id: String,
        @Field("job_id") job_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/sos_reasons")
    fun getSOSReason(
        @Field("type") type: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/send_sos")
    fun submitSOSReason(
        @Field("reason") reason: String,
        @Field("token_id") token_id: String,
        @Field("driver_id") driver_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/my-profile")
    fun getProfileData(
        @Field("user_master_id") userId: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/search_truck")
    fun searchTruck(
        @Field("truck_type") truck_type: String,
        @Field("fleet_type") fleet_type: String
    ): Observable<Response<ResponseBody>>

    @POST("dashboard/update_profile")
    @Multipart
    fun updateProfile(
        @Part("fname") firstName: RequestBody? = null,
        @Part("lanme") lastName: RequestBody? = null,
        @Part("mobile") mobile: RequestBody? = null,
        @Part("address") address: RequestBody? = null,
        @Part("country") country: RequestBody? = null,
        @Part("state") state: RequestBody? = null,
        @Part("pincode") pinCode: RequestBody? = null,
        @Part("city") city: RequestBody? = null,
        @Part("dob") dob: RequestBody? = null,
        @Part("cal_registration") calRegistration: RequestBody? = null,
        @Part("user_master_id") userId: RequestBody,
        @Part("company_name") companyName: RequestBody? = null,
        @Part("company_registration") companyRegistration: RequestBody? = null,
        @Part profile_image: MultipartBody.Part? = null
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/job_payout_details")
    fun payoutDetails(
        @Field("job_id") job_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/haulof_request_details")
    fun importExportJobDetails(
        @Field("haul_request_id") haul_request_id: String,
        @Field("user_master_id") userId: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/notification")
    fun getNotification(
        @Field("user_master_id") userId: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/noti_status_change")
    fun notificationRead(
        @Field("user_master_id") user_master_id: String,
        @Field("read_status") read_status: String
    ): Observable<Response<ResponseBody>>

    @POST("dashboard/submit_haul_job")
    @Multipart
    fun postHaulOffJob(
        @Part("MaterialType") MaterialType: RequestBody,
        @Part("MaterialoptionType") MaterialoptionType: RequestBody,
        @Part("spec_sheet") spec_sheet: RequestBody,
        @Part("lib_information") lib_information: RequestBody,
        @Part("material_cont") material_cont: RequestBody,
        @Part("materials_available") materials_available: RequestBody,
        @Part("Source") Source: RequestBody,
        @Part("Destination") Destination: RequestBody,
        @Part("haul_paymemt_amt") haul_paymemt_amt: RequestBody,
        @Part("payment_type") payment_type: RequestBody,
        @Part("stripeToken") stripeToken: RequestBody,
        @Part("user_master_id") user_master_id: RequestBody,
        @Part("source_lat") source_lat: RequestBody,
        @Part("source_long") source_long: RequestBody,
        @Part("destination_lat") destination_lat: RequestBody,
        @Part("destination_long") destination_long: RequestBody,
        @Part("TruckType") TruckType: RequestBody,
        @Part("NoOfTrucks") NoOfTrucks: RequestBody,

        @Part("pickup_date") pickup_date: RequestBody,
        @Part("delivery_date") delivery_date: RequestBody,
        @Part("pickup_time") pickup_time: RequestBody,
        @Part("delivery_time") delivery_time: RequestBody,

        @Part("haul_request_mapid") haul_request_mapid: RequestBody,
        @Part("Equipment") Equipment: RequestBody,

        @Part("exporter_id") exporter_id: RequestBody,
        @Part("importer_id") importer_id: RequestBody,
        @Part("specs_sheet") specs_sheet: RequestBody,
        @Part("export_request_id") export_request_id: RequestBody,
        @Part("import_request_id") import_request_id: RequestBody


    ): Observable<Response<ResponseBody>>

    @POST("dashboard/submit_haul_stepone")
    @Multipart
    fun postExportStep1(
        @Part("haul_request_id") haul_request_id: RequestBody,
        @Part("mapLat") mapLat: RequestBody,
        @Part("mapLng") mapLng: RequestBody,
        @Part("address") address: RequestBody,
        @Part("point_of_contact") point_of_contact: RequestBody,
        @Part("main_number") main_number: RequestBody,
        @Part("email") email: RequestBody,
        @Part materialImage: ArrayList<MultipartBody.Part?>?

    ): Observable<Response<ResponseBody>>


    @POST("dashboard/submit_haul_importstepone")
    @Multipart
    fun postImportStep1(
        @Part("haul_request_id") haul_request_id: RequestBody,
        @Part("mapLat") mapLat: RequestBody,
        @Part("mapLng") mapLng: RequestBody,
        @Part("address") address: RequestBody,
        @Part("point_of_contact") point_of_contact: RequestBody,
        @Part("main_number") main_number: RequestBody,
        @Part("email") email: RequestBody

    ): Observable<Response<ResponseBody>>


    @POST("dashboard/submit_haul_steptwo")
    @Multipart
    fun postExportStep2(
        @Part("haul_request_id") haul_request_id: RequestBody? = null,
        @Part("MaterialType") MaterialType: RequestBody? = null,
        @Part("material_other") material_other: RequestBody? = null,
        @Part("MaterialoptionType") MaterialoptionType: RequestBody? = null,
        @Part("cubic_yards") cubic_yards: RequestBody? = null,
        @Part("tons") tons: RequestBody? = null,
        @Part specsDocuments: MultipartBody.Part? = null
    ): Observable<Response<ResponseBody>>

    @POST("dashboard/submit_haul_stepthree")
    @Multipart
    fun postExportStep3(
        @Part("haul_request_id") haul_request_id: RequestBody? = null,
        @Part("equipment_type") equipment_type: RequestBody? = null,
        @Part("equipment_other") equipment_other: RequestBody? = null,
        @Part("equipment_any") equipment_any: RequestBody? = null,
        @Part("additional_notes") additional_notes: RequestBody? = null
    ): Observable<Response<ResponseBody>>


    @POST("dashboard/submit_haul_importsteptwo")
    @Multipart
    fun postImportStep2(
        @Part("haul_request_id") haul_request_id: RequestBody? = null,
        @Part("MaterialType") MaterialType: RequestBody? = null,
        @Part("material_other") material_other: RequestBody? = null,
        @Part("MaterialoptionType") MaterialoptionType: RequestBody? = null,
        @Part("cubic_yards") cubic_yards: RequestBody? = null,
        @Part("tons") tons: RequestBody? = null,
        @Part("equipment_type") equipment_type: RequestBody? = null,
        @Part("equipment_other") equipment_other: RequestBody? = null,
        @Part("equipment_any") equipment_any: RequestBody? = null,
        @Part("spreading_equipment_type") spreading_equipment_type: RequestBody? = null,
        @Part("spreading_equipment_other") spreading_equipment_other: RequestBody? = null,
        @Part("additional_notes") additional_notes: RequestBody? = null
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/payment_percentage")
    fun getPaymentPercentage(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/import_haul_request_send")
    fun sendImportRequest(
        @Field("importer_request_id") importer_request_id: String,
        @Field("exporter_request_id") exporter_request_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/export_haul_request_send")
    fun sendExportRequest(
        @Field("importer_request_id") importer_request_id: String,
        @Field("exporter_request_id") exporter_request_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/accept_haul_request")
    fun requestAcceptByExporter(
        @Field("request_map_id") request_map_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/reject_haul_request")
    fun requestReject(
        @Field("request_map_id") request_map_id: String
    ): Observable<Response<ResponseBody>>


    @FormUrlEncoded
    @POST("home/getStateList")
    fun getStateList(
        @Field("source") source: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("home/getCityList")
    fun getCityList(
        @Field("state_id") state_id: String
    ): Observable<Response<ResponseBody>>


    @FormUrlEncoded
    @POST("user/getBanners")
    fun getBanner(
        @Field("user_role") user_role: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/deviceLogout")
    fun logout(
        @Field("user_id") user_id: String,
        @Field("lat") lat: String,
        @Field("lng") lng: String,
        @Field("device_type") device_type: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/checkDashboardStatus")
    fun fleetcheckDashboardStatus(
        @Field("user_master_id") user_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/getSubscriptionListDetails")
    fun getSubscriptionListDetails(
        @Field("city_id") city_id: String,
        @Field("user_master_id") user_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/getCertificationDetails")
    fun getCertificationDetails(
        @Field("city_id") city_id: String,
        @Field("user_master_id") user_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/userSubscription")
    fun userSubscription(
        @Field("city_id") city_id: String,
        @Field("user_master_id") user_master_id: String,
        @Field("stripeToken") stripeToken: String,
        @Field("subscribe_id") subscribe_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/fleetCertification")
    fun fleetCertification(
        @Field("city_id") city_id: String,
        @Field("user_master_id") user_master_id: String,
        @Field("stripeToken") stripeToken: String,
        @Field("id") certification_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/getHaulofSubscriptionStatus")
    fun getHaulofSubscriptionStatus(
        @Field("user_master_id") user_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/cancelHaulofSubscription")
    fun cancelHaulofSubscription(
        @Field("user_master_id") user_id: String,
        @Field("role_id") role_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/getCancelJobDetails")
    fun getCancelJobDetails(
        @Field("job_id") job_id: String,
        @Field("user_master_id") user_id: String,
        @Field("city_id") city_id: String,
        @Field("user_type") user_type: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/getLogDetails")
    fun getLogDetails(
        @Field("user_master_id") user_master_id: String

    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("user/getNotificationsList")
    fun getNotificationList(
        @Field("user_master_id") user_master_id: String,
        @Field("msg_type") msg_type: String
    ): Observable<Response<ResponseBody>>


    @FormUrlEncoded
    @POST("fleet/connectStripe")
    fun payoutAddCard(
        @Field("user_master_id") user_master_id: String,
        @Field("connect_type") connect_type: String,
        @Field("cardnumber") cardnumber: String,
        @Field("exp_m") exp_m: String,
        @Field("exp_y") exp_y: String,
        @Field("cvc") cvc: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/connectStripe")
    fun payoutAddBankAccount(
        @Field("user_master_id") user_master_id: String,
        @Field("connect_type") connect_type: String,
        @Field("account_number") account_number: String,
        @Field("routing_number") routing_number: String,
        @Field("account_holder_type") account_holder_type: String,
        @Field("account_holder_name") account_holder_name: String,
        @Field("currency") currency: String,
        @Field("country") country: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/paymentSettings")
    fun getPaymentSettings(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("fleet/getPayoutDetails")
    fun getAllPayoutDetails(
        @Field("user_master_id") user_master_id: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/storeCurrentLocation")
    fun updateDriverCurrentLocation(
        @Field("job_id") job_id: String,
        @Field("lat") lat: String,
        @Field("lng") lng: String
    ): Observable<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("dashboard/getCurrentLocation")
    fun getDriverCurrentLocation(
        @Field("job_id") job_id: String
    ): Observable<Response<ResponseBody>>


    @FormUrlEncoded
    @POST("api/completeProfileInformations")
    fun completeInformation(
        @Field("user_master_id") userId: String,
        @Field("dob") dob: String,
        @Field("company_legal_name") companyLegalName: String,
        @Field("company_dba") companyDba: String,
        @Field("phone_2") phone2: String,
        @Field("email_2") email2: String,
        @Field("company_ein") companyEin: String,
        @Field("driver_license") driverLicense: String,
        @Field("driver_license_exp") driverLicenseExp: String,
        @Field("street_address") street_address: String,
        @Field("zip") zip: String,
        @Field("is_billing_address") is_billing_address: String,
        @Field("b_street_address") bStreetAddress: String,
        @Field("city") city: String,
        @Field("state") state: String,
        @Field("b_zip") b_zip: String
        ): Observable<Response<ResponseBody>>
}
