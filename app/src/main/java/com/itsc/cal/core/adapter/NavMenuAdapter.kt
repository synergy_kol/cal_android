package com.itsc.cal.core.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.databinding.NavMenuItemBinding
import com.itsc.cal.model.NavMenuInnerItem
import com.itsc.cal.model.NavMenuItem

class NavMenuAdapter(
    private val list: ArrayList<NavMenuItem>,
    private val context: Context,
    private val clicked: (NavMenuItem) -> Unit,
    private val clickedInnerItem: (NavMenuInnerItem) -> Unit
) :
    RecyclerView.Adapter<NavMenuAdapter.Holder>() {

    inner class Holder(private val binding: NavMenuItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private var haveChildList = false
        private lateinit var innerAdapter: NavMenuInnerAdapter

        fun bind(navMenuItem: NavMenuItem) {
            binding.textViewMenuName.text = navMenuItem.name
            binding.appcompatImageViewLogo.setImageDrawable(
                ResourcesCompat.getDrawable(
                    context.resources,
                    navMenuItem.image,
                    null
                )
            )
            binding.appcompatImageViewLogo.setColorFilter(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.white,
                    null
                )
            )
            binding.recyclerView.layoutManager = LinearLayoutManager(context)
            innerAdapter = NavMenuInnerAdapter(
                navMenuItem.innerItems,
                context
            ) {
                clickedMe(navMenuItem)
                clickedInnerItem(it)
            }
            haveChildList = navMenuItem.innerItems.size > 0
            binding.recyclerView.adapter = innerAdapter

            binding.constraintLayoutContainer.setOnClickListener {
                clickedMe(navMenuItem)

                notifyDataSetChanged()
            }

            if (navMenuItem.selected)
                select()
            else
                unSelect()
        }

        private fun clickedMe(navMenuItem: NavMenuItem) {
            clicked(navMenuItem)
            list.forEachIndexed { index, item ->
                item.selected = index == adapterPosition
            }

            notifyDataSetChanged()
        }

        private fun select() {
            binding.constraintLayoutContainer.setBackgroundColor(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.white,
                    null
                )
            )
            binding.appcompatImageViewLogo.setColorFilter(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.color_blue,
                    null
                )
            )
            binding.textViewMenuName.setTextColor(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.color_blue,
                    null
                )
            )
        }

        private fun unSelect() {
            try {
                binding.constraintLayoutContainer.setBackgroundColor(
                    ResourcesCompat.getColor(
                        context.resources,
                        android.R.color.transparent,
                        null
                    )
                )
                binding.appcompatImageViewLogo.setColorFilter(
                    ResourcesCompat.getColor(
                        context.resources,
                        R.color.white,
                        null
                    )
                )
                binding.textViewMenuName.setTextColor(
                    ResourcesCompat.getColor(
                        context.resources,
                        R.color.white,
                        null
                    )
                )
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                if (haveChildList)
                    innerAdapter.unSelectAll()
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            NavMenuItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }
}