package com.itsc.cal.core

import android.annotation.SuppressLint
import android.content.*
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.PopupWindow
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.hold1.keyboardheightprovider.KeyboardHeightProvider
import com.itsc.cal.R
import com.itsc.cal.core.KeywordsAndConstants.END_SELF
import com.itsc.cal.core.adapter.NavMenuAdapter
import com.itsc.cal.databinding.ActivityCalSuperBinding
import com.itsc.cal.databinding.OptionsPopUpBinding
import com.itsc.cal.enums.Menus
import com.itsc.cal.interfaces.CallSuperActivityContract
import com.itsc.cal.model.NavMenuItem
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.builder_my_job_list_with_haul_job.MyPostedJobBuilder
import com.itsc.cal.ui.builders_details.BuilderDetailsActivity
import com.itsc.cal.ui.card_list.CardListActivity
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.dashboard_change_password.DashboardChangePasswordActivity
import com.itsc.cal.ui.job.PostJobActivity
import com.itsc.cal.ui.job.my_joblist.MyPostedJob
import com.itsc.cal.ui.job_list_truck_fleet.JobListTruckFleetActivity
import com.itsc.cal.ui.login.LoginActivity
import com.itsc.cal.ui.manage_driver.ManageDriverActivity
import com.itsc.cal.ui.my_booking.MyBookingActivity
import com.itsc.cal.ui.near_by_contractor_list.NearByContractorList
import com.itsc.cal.ui.paymentsettings.PayoutDetailsActivity
import com.itsc.cal.ui.paymentsettings.PayoutSettingsActivity
import com.itsc.cal.ui.post_a_truck.PostATruckActivity
import com.itsc.cal.ui.post_hault_of_job_list.PostHaultOfJobList
import com.itsc.cal.ui.profile.ProfileActivity
import com.itsc.cal.ui.profile_builder.ProfileBuilderActivity
import com.itsc.cal.ui.search_contractor.SearchContractorActivity
import com.itsc.cal.ui.search_job.SearchJobActivity
import com.itsc.cal.ui.search_material_supplier.SearchMaterialSupplierActivity
import com.itsc.cal.ui.search_truck.SearchTruck
import com.itsc.cal.ui.sos_document_submited.SOSDocumentSubmitedActivity
import com.itsc.cal.ui.sos_message_log.SOSMessageLogActivity
import com.itsc.cal.ui.suscription.SubsciptionListActivity
import com.itsc.cal.ui.track_list.TruckListActivity
import com.itsc.cal.ui.track_status.TrackStatusActivity
import com.itsc.cal.ui.webview.WebView
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.activity.SuperActivity
import com.itsc.cal.widget.spinner.Spinner
import com.itsc.cal.widget.spinner.SpinnerData
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

@SuppressLint("Registered")
open class CalSuperActivity(
    private val withNav: Boolean = false,
    private val trulyInfinite: Boolean = false
) :
    SuperActivity(), CallSuperActivityContract, KodeinAware {
    override val kodein: Kodein by kodein()
    private val repository: Repository by instance()
    private lateinit var superBinding: ActivityCalSuperBinding
    private lateinit var keyboardHeightProvider: KeyboardHeightProvider
    private var backButtonEnabled = false
    private var backHandledByChild = false

    private lateinit var methodForKeyBoardMovement: ((moveDirection: KBMoved) -> Unit)

    enum class KBMoved {
        UP, DOWN
    }

    private val endSelfBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            finish()
        }
    }

    private fun registerForEndSelf() {
        registerReceiver(
            endSelfBroadcastReceiver,
            IntentFilter(
                END_SELF
            )
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(endSelfBroadcastReceiver)
    }

    protected fun endAllActivities() {
        sendBroadcast(
            Intent(
                END_SELF
            )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cal_super)
        UiUtil.makeFullScreen(this)

        superBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_cal_super
        )
        superBinding.context = this

        if (trulyInfinite) {
            infiniteView()
        } else {
            ViewCompat.setOnApplyWindowInsetsListener(superBinding.content.container) { _, insets ->
                superBinding.content.constraintLayoutInnerContainer.setMarginTop(insets.systemWindowInsetTop)
                insets.consumeSystemWindowInsets()
            }
        }

        cleanView()

        setUpKeyBoardHeightListener()

        registerForEndSelf()

        if (withNav) {
            superBinding.drawerLayout.setDrawerLockMode(
                DrawerLayout.LOCK_MODE_UNLOCKED
            )
        } else {
            superBinding.drawerLayout.setDrawerLockMode(
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED
            )
        }
        superBinding.content.appcompatImageViewHelp.setOnClickListener {
            callWebView()
        }
        superBinding.content.appcompatImageViewLogout.setOnClickListener {
            navLogout()
        }
    }

    private fun callWebView() {
        Log.i("CallWebView", "Callwebview")
        if (repository.getUserData()?.user_role.equals("2")) {
            startActivity(
                Intent(
                    this,
                    WebView::class.java
                ).putExtra(
                    WebView.URL,
                    "${KeywordsAndConstants.BASE_URL}index.php/home/faq/2"
                ).putExtra(
                    WebView.TITLE,
                    "Help"
                )
            )
        } else if (repository.getUserData()?.user_role.equals("3")) {

            startActivity(
                Intent(
                    this,
                    WebView::class.java
                ).putExtra(
                    WebView.URL,
                    "${KeywordsAndConstants.BASE_URL}index.php/home/faq/3"
                ).putExtra(
                    WebView.TITLE,
                    "Help"
                )
            )

        } else if (repository.getUserData()?.user_role.equals("4")) {
            startActivity(
                Intent(
                    this,
                    WebView::class.java
                ).putExtra(
                    WebView.URL,
                    "${KeywordsAndConstants.BASE_URL}index.php/home/faq/4"
                ).putExtra(
                    WebView.TITLE,
                    "Help"
                )
            )
        } else {
            startActivity(
                Intent(
                    this,
                    WebView::class.java
                ).putExtra(
                    WebView.URL,
                    "${KeywordsAndConstants.BASE_URL}index.php/home/faq/5"
                ).putExtra(
                    WebView.TITLE,
                    "Help"
                )
            )
        }
    }

    private fun infiniteView() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(superBinding.content.constraintLayoutInnerContainer)

        constraintSet.connect(
            superBinding.content.frameContainer.id,
            ConstraintSet.TOP,
            superBinding.content.constraintLayoutInnerContainer.id,
            ConstraintSet.TOP
        )

        constraintSet.applyTo(
            superBinding.content.constraintLayoutInnerContainer
        )

        val params =
            superBinding.content.frameContainer.layoutParams as ConstraintLayout.LayoutParams
        params.setMargins(
            0, 0, 0, 0
        )
        superBinding.content.frameContainer.layoutParams = params
    }

    private fun cleanView() {
        superBinding.content.appcompatImageViewBack.visibility = View.GONE
        superBinding.content.textViewTitle.visibility = View.GONE
        superBinding.content.appcompatImageViewLogo.visibility = View.GONE
        superBinding.content.viewLine.visibility = View.GONE
        superBinding.content.appcompatImageViewMenu.visibility = View.GONE
        superBinding.content.appcompatImageViewHelp.visibility = View.GONE
        superBinding.content.appcompatImageViewLogout.visibility = View.GONE
        superBinding.content.appcompatImageViewOptions.visibility = View.GONE
        superBinding.content.appcompatImageViewReload.visibility = View.GONE
        superBinding.content.appcompatImageViewNotification.visibility = View.GONE
        superBinding.content.buttonSaveProfile.visibility = View.GONE
        superBinding.content.buttonEditProfile.visibility = View.GONE
    }

    protected fun getContainer(): NestedScrollView {
        return superBinding.content.frameContainer
    }

    private fun setView(view: View) {
        getContainer().addView(view)
    }

    protected fun <T : ViewDataBinding> setLayout(@Suppress("SameParameterValue") layout: Int): T {
        val binding: T = DataBindingUtil.inflate(
            layoutInflater,
            layout,
            null,
            false
        )
        setView(binding.root)
        return binding
    }

    protected fun enableBackButton() {
        backButtonEnabled = true
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
    }

    protected fun hideBackButton() {
        backButtonEnabled = false
        superBinding.content.appcompatImageViewBack.visibility = View.INVISIBLE
    }

    protected fun showHelpButton() {
        superBinding.content.appcompatImageViewHelp.visibility = View.VISIBLE
    }

    protected fun showLogoutButton() {
        superBinding.content.appcompatImageViewLogout.visibility = View.VISIBLE
    }

    protected fun hideLogoutButton() {
        superBinding.content.appcompatImageViewLogout.visibility = View.GONE
    }

    protected fun setPageTitle(title: String) {
        if (!backButtonEnabled) {
            superBinding.content.appcompatImageViewBack.visibility = View.INVISIBLE
        }
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.textViewTitle.text = title
    }

    override fun goBack() {
        onBackPressed()
    }

    override fun onBackPressed() {
        if (backHandledByChild)
            goBack()
        else
            finish()
    }

    protected fun willHandleBackNavigation() {
        backHandledByChild = true
    }

    protected fun registerForKBMovement(methodForKBMovement: (moveDirection: KBMoved) -> Unit) {
        this.methodForKeyBoardMovement = methodForKBMovement
    }

    private fun setUpKeyBoardHeightListener() {
        keyboardHeightProvider = KeyboardHeightProvider(this)
        keyboardHeightProvider.addKeyboardListener(
            object : KeyboardHeightProvider.KeyboardListener {
                override fun onHeightChanged(height: Int) {
                    if (height == 0)
                        superBinding.content.filler.visibility = View.GONE
                    else
                        superBinding.content.filler.visibility = View.VISIBLE
                    val params = superBinding.content.filler.layoutParams
                    params.height = if (height == 0) 1 else height
                    superBinding.content.filler.layoutParams = params
                }
            }
        )
    }

    override fun onResume() {
        super.onResume()
        keyboardHeightProvider.onResume()

    }

    override fun onPause() {
        super.onPause()
        keyboardHeightProvider.onPause()
    }

    protected fun setUpDrawer(menuItems: ArrayList<NavMenuItem>) {
        if (!withNav)
            return

        superBinding.navLayout.textViewUserName.text =
            repository.getUserData()!!.fname + " " + repository.getUserData()!!.lanme
        superBinding.navLayout.userImage.setImage(
            repository.getUserData()?.profile_image ?: "https://www.google.com/nimg.jpg",
            isCircularImage = true,
            needPlaceHolderImageForName = repository.getUserData()!!.fname!!.split("")[0].toUpperCase()
        )
        superBinding.navLayout.userImage.registerForOnClick {
            if (repository.getUserData()!!.user_role.equals("2")) {
                startActivity(
                    Intent(this, ProfileBuilderActivity::class.java).putExtra(
                        KeywordsAndConstants.IS_OWN_PROFILE,
                        true
                    )
                )
            } else {
                startActivity(
                    Intent(this, ProfileActivity::class.java).putExtra(
                        KeywordsAndConstants.IS_OWN_PROFILE,
                        true
                    )
                )
            }
        }
        superBinding.navLayout.textViewUserName.setOnClickListener {
            if (repository.getUserData()!!.user_role.equals("2")) {
                startActivity(
                    Intent(this, ProfileBuilderActivity::class.java).putExtra(
                        KeywordsAndConstants.IS_OWN_PROFILE,
                        true
                    )
                )
            } else {
                startActivity(
                    Intent(this, ProfileActivity::class.java).putExtra(
                        KeywordsAndConstants.IS_OWN_PROFILE,
                        true
                    )
                )
            }
        }
        superBinding.navLayout.spinnerActivity.apply {

            addItems(
                arrayListOf(
                    SpinnerData(
                        "Active",
                        "Active"
                    ),
                    SpinnerData(
                        "Non Active",
                        "Non Active"
                    )
                ),
                true,
                object : Spinner.OnItemSelectedListener {
                    override fun <T> selected(item: SpinnerData<T>) {
                    }
                }
            )
        }
        setUpNavMenuItems(menuItems)
    }

    fun openDrawer() {
        if (!withNav)
            return

        superBinding.drawerLayout.openDrawer(GravityCompat.START)
    }

    fun closeDrawer() {
        if (!withNav)
            return

        superBinding.drawerLayout.closeDrawer(GravityCompat.START)
    }

    fun showAppLogoWithBackButton() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewLogo.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewHelp.visibility = View.GONE
        superBinding.content.viewLine.visibility = View.VISIBLE
    }
    fun showAppLogoWithOutBackButton() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.INVISIBLE
        superBinding.content.appcompatImageViewLogo.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewHelp.visibility = View.GONE
        superBinding.content.viewLine.visibility = View.VISIBLE
    }

    fun showDashboardActionbar() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.INVISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewMenu.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewHelp.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewOptions.visibility = View.VISIBLE
    }

    fun showDashboardActionbarWithBackButton() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewHelp.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewOptions.visibility = View.VISIBLE
        enableBackButton()
    }

    fun showActionbarWithBackButtonAndCall() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewHelp.visibility = View.VISIBLE
        enableBackButton()
    }

    fun showBuilderDetailsActionbar() {
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewHelp.visibility = View.VISIBLE
    }

    fun showTrackStatusActionbar() {
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewReload.visibility = View.GONE
        superBinding.content.appcompatImageViewNotification.visibility = View.GONE
        superBinding.content.appcompatImageViewHelp.visibility = View.GONE

    }

    fun showAlertActionbar() {
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewNotification.visibility = View.GONE
        superBinding.content.appcompatImageViewHelp.visibility = View.GONE
    }

    private fun setUpNavMenuItems(menuItems: ArrayList<NavMenuItem>) {
        superBinding.navLayout.recyclerViewNavItems.apply {
            layoutManager = LinearLayoutManager(this@CalSuperActivity)
            adapter = NavMenuAdapter(
                menuItems,
                this@CalSuperActivity,
                {
                    clickedMenu(it.menu)
                },
                {
                    clickedMenu(it.menu)
                }
            )
        }
    }

    private fun clickedMenu(menu: Menus) {
        closeDrawer()
        @Suppress("NON_EXHAUSTIVE_WHEN")
        when (menu) {
            Menus.NEAR_BY_CONTRACTOR -> {
                navNearByContractor()
            }
            Menus.MANAGE_BUILDERS -> {
                navManageBuilders()
            }
            Menus.TRACK_STATUS -> {
                if (repository.getUserData()!!.user_role.equals("4")) {
                    if ((this as Dashboard).certificate_status == 1) {
                        if ((this as Dashboard).driver_no >= 1 && (this as Dashboard).truck_no >= 1) {
                            navTrackStatus()
                        } else if ((this as Dashboard).driver_no == 0 && (this as Dashboard).truck_no == 0) {
                            showMessageInDialog("Please provide at least one drive and at least one truck.")
                        } else if ((this as Dashboard).driver_no == 0) {
                            showMessageInDialog("Please provide at least one drive.")
                        } else if ((this as Dashboard).truck_no == 0) {
                            showMessageInDialog("Please provide at least one truck.")
                        }
                    } else {
                        showMessageWithOneButton(
                            message = "Pay certification free",
                            cancellable = true,
                            buttonText = "OK",
                            callback = object : DialogUtil.CallBack {
                                override fun buttonClicked() {
                                    super.buttonClicked()
                                    navSubscriptionList()
                                }
                            }
                        )
                    }

                } else {
                    navTrackStatus()
                }
            }
            Menus.PAYOUT_SETTINGS -> {
                navPayoutSettings()
            }
            Menus.PAYOUT_DETAILS -> {
                navPayoutDetails()
            }
            Menus.MANAGE_CARD -> {
                navManageCard()
            }
            Menus.SOS_ADDITIONAL_REQUEST -> {
                navSOSAdditionalRequest()
            }
            Menus.SEARCH_CONTRACTOR -> {
                navSearchContractor()
            }
            Menus.SEARCH_MATERIAL_SUPPLIER -> {
                navSearchMaterialSupplier()
            }
            Menus.SOS_MESSAGE_LOG -> {
                navSOSMessageLogRequest()
            }
            Menus.ABOUT_US -> {
                navAboutUs()
            }
            Menus.ALERTS -> {
                navAlerts()
            }
            Menus.SUBSCRIPTION_LIST -> {
                navSubscriptionList()
            }
            Menus.CONTACT_US -> {
                navContactUs()
            }
            Menus.LOGOUT -> {
                navLogout()
            }
            Menus.POST_A_JOB -> {
                navPostAJob()
            }
            Menus.MY_POSTED_JOBS -> {
                navMyPostedJob()
            }
            Menus.SEARCH_TRUCK -> {
                navSearchTruck()
            }
            Menus.MY_JOBS -> {
                navMyJobs()
            }
            Menus.MANAGE_DRIVER -> {
                if ((this as Dashboard).certificate_status == 1) {
                    navManagerDriver()
                } else {
                    showMessageWithOneButton(
                        message = "Pay certification free",
                        cancellable = true,
                        buttonText = "OK",
                        callback = object : DialogUtil.CallBack {
                            override fun buttonClicked() {
                                super.buttonClicked()
                                navSubscriptionList()
                            }
                        }
                    )
                }

            }
            Menus.POST_TRUCK -> {
                navPostATruck()
            }

            Menus.MANAGE_TRUCK -> {
                if ((this as Dashboard).certificate_status == 1) {
                    navManageTruck()
                } else {
                    showMessageWithOneButton(
                        message = "Pay certification free",
                        cancellable = true,
                        buttonText = "OK",
                        callback = object : DialogUtil.CallBack {
                            override fun buttonClicked() {
                                super.buttonClicked()
                                navSubscriptionList()
                            }
                        }
                    )
                }
            }
            Menus.MY_BOOKINGS -> {
                if ((this as Dashboard).certificate_status == 1) {
                    if ((this as Dashboard).driver_no >= 1 && (this as Dashboard).truck_no >= 1) {
                        navMyBookings()
                    } else if ((this as Dashboard).driver_no == 0 && (this as Dashboard).truck_no == 0) {
                        showMessageInDialog("Please provide at least one drive and at least one truck.")
                    } else if ((this as Dashboard).driver_no == 0) {
                        showMessageInDialog("Please provide at least one drive.")
                    } else if ((this as Dashboard).truck_no == 0) {
                        showMessageInDialog("Please provide at least one truck.")
                    }
                } else {
                    showMessageWithOneButton(
                        message = "Pay certification free",
                        cancellable = true,
                        buttonText = "OK",
                        callback = object : DialogUtil.CallBack {
                            override fun buttonClicked() {
                                super.buttonClicked()
                                navSubscriptionList()
                            }
                        }
                    )
                }
            }
            Menus.SEARCH_FOR_JOBS -> {
                navSearchJob()
            }
            Menus.JOB_LIST_TRUCK_FLEET -> {
                if ((this as Dashboard).certificate_status == 1) {
                    if ((this as Dashboard).driver_no >= 1 && (this as Dashboard).truck_no >= 1) {
                        navJobListTruckFleet()
                    } else if ((this as Dashboard).driver_no == 0 && (this as Dashboard).truck_no == 0) {
                        showMessageInDialog("Please provide at least one drive and at least one truck.")
                    } else if ((this as Dashboard).driver_no == 0) {
                        showMessageInDialog("Please provide at least one drive.")
                    } else if ((this as Dashboard).truck_no == 0) {
                        showMessageInDialog("Please provide at least one truck.")
                    }
                } else {
                    showMessageWithOneButton(
                        message = "Pay certification free",
                        cancellable = true,
                        buttonText = "OK",
                        callback = object : DialogUtil.CallBack {
                            override fun buttonClicked() {
                                super.buttonClicked()
                                navSubscriptionList()
                            }
                        }
                    )
                }
            }
            Menus.TRACKING_TRUCKS_LIVE -> {
                navTrackingTrucksLive()
            }
            Menus.HAUL_OFF -> {
                (this as Dashboard).subscription_status
                if (subscription_status == 0) {
                    showMessageWithOneButton(
                        message = "Pay Haul-OFF Subscription free",
                        cancellable = true,
                        buttonText = "OK",
                        callback = object : DialogUtil.CallBack {
                            override fun buttonClicked() {
                                super.buttonClicked()
                                navSubscriptionList()
                            }
                        }
                    )
                } else if (subscription_status == 1) {
                    navHaulOff()
                } else if (subscription_status == 2) {
                    showMessageWithOneButton(
                        message = "Your subscription Haul-OFF subscription plan expired",
                        cancellable = true,
                        buttonText = "OK",
                        callback = object : DialogUtil.CallBack {
                            override fun buttonClicked() {
                                super.buttonClicked()
                                navSubscriptionList()
                            }
                        }
                    )
                }

            }
        }
    }

    fun showAppLogoWithBackGetStartedButton() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewLogo.visibility = View.VISIBLE
        superBinding.content.constraintLayoutInnerContainer.setBackgroundResource(R.color.colorPrimary)
        superBinding.content.appcompatImageViewHelp.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.VISIBLE
    }

    fun showAProfileActionbar() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.textViewTitle.setTextColor(resources.getColor(R.color.white))
        superBinding.content.textViewTitle.text = "Profile"
        superBinding.content.constraintLayoutInnerContainer.setBackgroundResource(R.color.color_blue)
        superBinding.content.viewLine.visibility = View.GONE
    }

    fun showAProfileEditActionbar() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.textViewTitle.setTextColor(resources.getColor(R.color.white))
        superBinding.content.textViewTitle.text = "Profile"
        superBinding.content.constraintLayoutInnerContainer.setBackgroundResource(R.color.color_blue)
        superBinding.content.buttonEditProfile.visibility = View.VISIBLE
        superBinding.content.buttonSaveProfile.visibility = View.GONE
        superBinding.content.viewLine.visibility = View.GONE
        superBinding.content.appcompatImageViewBack.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    fun showAProfileSaveActionbar() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.textViewTitle.setTextColor(resources.getColor(R.color.white))
        superBinding.content.textViewTitle.text = "Profile"
        superBinding.content.constraintLayoutInnerContainer.setBackgroundResource(R.color.color_blue)
        superBinding.content.buttonEditProfile.visibility = View.GONE
        superBinding.content.buttonSaveProfile.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.GONE
    }


    fun showASOSDocumentSubmitedActionbar() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.textViewTitle.setTextColor(resources.getColor(R.color.white))
        superBinding.content.textViewTitle.text = "Required Documents"
        superBinding.content.constraintLayoutInnerContainer.setBackgroundResource(R.color.color_blue)
        superBinding.content.appcompatImageViewHelp.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewBack.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        superBinding.content.appcompatImageViewHelp.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        superBinding.content.appcompatImageViewLogout.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        superBinding.content.viewLine.visibility = View.GONE
    }

    fun showContactUsActionbar() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.VISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewMenu.visibility = View.INVISIBLE
        superBinding.content.appcompatImageViewHelp.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewLogout.visibility = View.GONE
        superBinding.content.appcompatImageViewOptions.visibility = View.VISIBLE
    }

    fun showPostATruckActionbar() {
        cleanView()
        superBinding.content.appcompatImageViewBack.visibility = View.INVISIBLE
        superBinding.content.textViewTitle.visibility = View.VISIBLE
        superBinding.content.viewLine.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewMenu.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewHelp.visibility = View.VISIBLE
        superBinding.content.appcompatImageViewOptions.visibility = View.VISIBLE
    }

    fun showOptions() {
        val popupWindow = PopupWindow(this)
        val popUpBinding = OptionsPopUpBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        popupWindow.contentView = popUpBinding.root
        popupWindow.isOutsideTouchable = true
        popupWindow.setBackgroundDrawable(
            ColorDrawable(
                ResourcesCompat.getColor(
                    resources,
                    android.R.color.transparent,
                    null
                )
            )
        )

        popUpBinding.textViewChangePassword.setOnClickListener {
            startActivity(Intent(this, DashboardChangePasswordActivity::class.java))
        }

        popupWindow.showAsDropDown(
            superBinding.content.appcompatImageViewOptions
        )
        popUpBinding.textViewCallNow.setOnClickListener {
            dialANumber("088820808")
        }
        popUpBinding.textViewRateUs.setOnClickListener {
            showsInPlayStore()
        }

        popUpBinding.textViewShare.setOnClickListener {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.apply {
                type = "text/plain"
                putExtra(
                    Intent.EXTRA_TEXT,
                    "https://play.google.com/store/apps/details?id=$packageName"
                )
            }
            startActivity(Intent.createChooser(shareIntent, "Share"))
        }
    }

    private fun showsInPlayStore() {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$packageName")
                )
            )
        } catch (exception: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                )
            )
        }

        finish()
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //functions for nav drawer


    private fun dialANumber(phone: String?) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phone")
        startActivity(intent)

    }

    //truck
    fun navHome() {}
    fun navSOSAdditionalRequest() {
        startActivity(Intent(this, SOSDocumentSubmitedActivity::class.java).putExtra("from", "menu"))
    }

    fun navSOSMessageLogRequest() {
        startActivity(Intent(this, SOSMessageLogActivity::class.java))
    }

    fun navPostATruck() {
        startActivity(Intent(this, PostATruckActivity::class.java))
    }

    fun navManageTruck() {
        startActivity(Intent(this, TruckListActivity::class.java))
    }

    fun navPayoutSettings() {
        startActivity(Intent(this, PayoutSettingsActivity::class.java))
    }
    fun navPayoutDetails() {
        startActivity(Intent(this, PayoutDetailsActivity::class.java))
    }

    fun navMyBookings() {
        startActivity(
            Intent(
                this,
                MyBookingActivity::class.java
            )
        )
    }

    fun navSubscriptionList() {
        startActivity(
            Intent(
                this,
                SubsciptionListActivity::class.java
            )
        )
    }

    fun navSearchJob() {
        startActivity(
            Intent(
                this,
                SearchJobActivity::class.java
            )
        )
    }

    fun navJobListTruckFleet() {
        startActivity(
            Intent(
                this,
                JobListTruckFleetActivity::class.java
            )
        )
    }

    fun navManageAccount() {}
    fun navTruckStatus() {
        startActivity(
            Intent(
                this,
                TrackStatusActivity::class.java
            ).putExtra(KeywordsAndConstants.isJobListing, false)
        )
    }

    fun navManagerDriver() {
        startActivity(
            Intent(
                this,
                ManageDriverActivity::class.java
            )
        )
    }

    fun navAlerts() {

    }

    fun navAboutUs() {
        startActivity(
            Intent(
                this,
                WebView::class.java
            ).putExtra(
                WebView.URL,
                "${KeywordsAndConstants.BASE_URL}home/cms/AU"
            ).putExtra(
                WebView.TITLE,
                "About Us"
            )
        )
    }

    fun navContactUs() {
        startActivity(
            Intent(
                this,
                WebView::class.java
            ).putExtra(
                WebView.URL,
                "${KeywordsAndConstants.BASE_URL}home/cms/CU"
            ).putExtra(
                WebView.TITLE,
                "Contact Us"
            )
        )

    }

    fun navLogout() {
        showMessageWithTwoButton(
            message = "Do you want to logout?",
            buttonOneText = "Yes",
            buttonTwoText = "No",
            callback = object : DialogUtil.MultiButtonCallBack {
                override fun buttonOneClicked() {
                    super.buttonOneClicked()

                    repository.logout()
                    repository.clearAllData()
                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@CalSuperActivity,
                                LoginActivity::class.java
                            )
                        )
                    )
                    finish()
                }

                override fun buttonTwoClicked() {
                    super.buttonTwoClicked()
                }
            }
        )

    }

    //manufacturer
    fun navNearByContractor() {
        startActivity(
            Intent(
                this,
                NearByContractorList::class.java
            )
        )
    }

    fun navSearchContractor() {
        startActivity(
            Intent(
                this,
                SearchContractorActivity::class.java
            )
        )
    }

    fun navSearchMaterialSupplier() {
        startActivity(
            Intent(
                this,
                SearchMaterialSupplierActivity::class.java
            )
        )
    }

    fun navPostAJob() {
        startActivity(
            Intent(
                this,
                PostJobActivity::class.java
            )
        )
    }

    fun navMyPostedJob() {
        startActivity(
            Intent(
                this,
                MyPostedJob::class.java
            )
        )
    }

    fun navSearchTruck() {
        startActivity(
            Intent(
                this,
                SearchTruck::class.java
            )
        )
    }

    fun navTrackingTrucksLive() {
        val intent = Intent(this, TrackStatusActivity::class.java)
        startActivity(intent)
    }


    fun navManageBuilders() {
        startActivity(
            Intent(
                this,
                BuilderDetailsActivity::class.java
            ).putExtra(KeywordsAndConstants.isPostJob, false)
        )

    }

    fun navTrackStatus() {
        startActivity(
            Intent(
                this,
                TrackStatusActivity::class.java
            ).putExtra(KeywordsAndConstants.isJobListing, false)
        )
    }

    fun navManageCard() {
        startActivity(Intent(this, CardListActivity::class.java))
    }

    //builder
    fun navMyJobs() {
        /* startActivity(
             Intent(
                 this,
                 BuilderMyJobsList::class.java
             ).putExtra(KeywordsAndConstants.IS_BUILDER_JOBS, true)
         )*/
        startActivity(Intent(this, MyPostedJobBuilder::class.java))
    }

    fun navTrackingTrucks() {}
    fun navHaulOff() {
        val intent = Intent(this, PostHaultOfJobList::class.java)
        startActivity(intent)
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
}
