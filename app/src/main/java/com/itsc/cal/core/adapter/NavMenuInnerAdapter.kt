package com.itsc.cal.core.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.databinding.NavMenuInnerItemBinding
import com.itsc.cal.model.NavMenuInnerItem

class NavMenuInnerAdapter(
    private val list: ArrayList<NavMenuInnerItem>,
    private val context: Context,
    private val clickedItem: (NavMenuInnerItem) -> Unit
) :
    RecyclerView.Adapter<NavMenuInnerAdapter.Holder>() {

    inner class Holder(private val binding: NavMenuInnerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(navMenuInnerItem: NavMenuInnerItem) {
            binding.textViewMenuName.text = navMenuInnerItem.name
            binding.viewBullet.setBackgroundColor(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.white,
                    null
                )
            )

            binding.constraintLayoutContainer.setOnClickListener {
                list.forEachIndexed { index, item ->
                    item.selected = index == adapterPosition
                }

                notifyDataSetChanged()

                clickedItem(navMenuInnerItem)
            }

            if (navMenuInnerItem.selected)
                select()
            else
                unSelect()
        }

        private fun select() {
            binding.constraintLayoutContainer.setBackgroundColor(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.white,
                    null
                )
            )
            binding.viewBullet.setBackgroundColor(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.color_blue,
                    null
                )
            )
            binding.textViewMenuName.setTextColor(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.color_blue,
                    null
                )
            )
        }

        private fun unSelect() {
            try {
                binding.constraintLayoutContainer.setBackgroundColor(
                    ResourcesCompat.getColor(
                        context.resources,
                        android.R.color.transparent,
                        null
                    )
                )
                binding.viewBullet.setBackgroundColor(
                    ResourcesCompat.getColor(
                        context.resources,
                        R.color.white,
                        null
                    )
                )
                binding.textViewMenuName.setTextColor(
                    ResourcesCompat.getColor(
                        context.resources,
                        R.color.white,
                        null
                    )
                )
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            NavMenuInnerItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }

    fun unSelectAll() {
        list.forEachIndexed { _, item ->
            item.selected = false
        }

        notifyDataSetChanged()
    }
}