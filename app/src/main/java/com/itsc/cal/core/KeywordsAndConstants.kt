package com.itsc.cal.core

object KeywordsAndConstants {

    const val BASE_URL = "https://itsc.fitser.com/"

    const val SHARED_PREF_DB = "calSharedPref"

    const val END_SELF = "endSelf"

    const val LOG_TAG = "cal_log"
    const val DELAY_FOR_ADVERTISEMENT = 5000L
    const val WAIT_INTERVAL_SPLASH = 5000L
    const val EMAIL = "email"
    const val PASSWORD = "password"
    const val DEVICE_FCM_TOKEN = "fcmToken"
    const val GOOGLE_MAP_DIRECTION_API_BASE_URL = "https://maps.googleapis.com/maps/api/directions/"

    const val AUTOCOMPLETE_PLACE_REQUEST_CODE = 1
    const val EDIT_PROFILE_REQUEST_CODE = 101

    const val USER_DATA = "userData"
    const val DATA = "data"
    const val IS_FORM_DASHBOARD = "is_from_dashboard"
    const val REQUEST_CHECK_SETTINGS = 101
    const val FILTER_TYPE = "filter_type"
    const val DESTINATION_DATA = "destination_data"
    const val ACCEPT_JOB="1"
    const val IMPORT_DATA_MODEL = "import_data_model"
    const val IMPORT_EXPORT = "import_export"
    const val EMPTY_WEIGHT = "empty_weight"
    const val isFromBooking = "isFromBooking"
    const val isFromForgotPassword = "isfromForgotPassword"
    const val isOTP = "isOTP"
    const val isPostJob = "isPostJobs"
    const val isJobListing = "isJobListing"
    const val isEditCard = "isEditCard"
    const val SIGNUP_TYPE = "signup_type"
    const val IS_BUILDER_JOBS = "is_builder_jobs"
    const val INDEPENDENT_TRUCK = "independent_truck"
    const val MATERIAL_MANUFACTURER = "material_manufacturer"
    const val CONTRATOR_BUILDER = "contractor_builder"
    const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
    const val IS_OWN_PROFILE = "is_own_profile"
    const val REQUEST_CODE_PICK_IMAGE = 3000

    const val TRUCK_ID_FOR_FIREBASE = "truckIdForFirebase"
    const val JOB_ID_FOR_FIREBASE = "jobIdForFirebase"
    const val POLYLINE = "polyLine"
    const val MAY_RE_DRAW_POLY_LINES = "reDrawPolyLines"

    const val LAST_TRUCK_LOCATION = "lastTruckLocation"

    const val END_JOB_MIN_DISTANCE_MTS = 500
    const val START_JOB_MIN_DISTANCE_MTS = 500
    const val USER_ROLE="userRole"
    const val DATE_FORMAT="dd/MM/yyyy"
}