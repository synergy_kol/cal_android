package com.itsc.cal.application

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.itsc.cal.BuildConfig
import com.itsc.cal.core.KeywordsAndConstants.LOG_TAG
import com.itsc.cal.di.NetworkModule
import com.itsc.cal.di.SharedPrefModule
import com.itsc.cal.repository.Repository
import com.sagar.android.logutilmaster.LogUtil
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class ApplicationClass : Application(), KodeinAware {

    private val logUtil: LogUtil by instance<LogUtil>()

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        logUtil.logV("^^^ CAL App started. ^^^")
    }

    override val kodein = Kodein.lazy {

        import(androidXModule(this@ApplicationClass))

        bind() from singleton {
            LogUtil(
                LogUtil.Builder()
                    .setCustomLogTag(LOG_TAG)
                    .setShouldHideLogInReleaseMode(false, BuildConfig.DEBUG)
            )
        }

        bind() from singleton { NetworkModule(instance()).apiInterface }

        bind() from singleton { SharedPrefModule(instance()).pref }

        bind() from singleton {
            Repository(
                instance(),
                instance(),
                instance(),
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.login.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.signup.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.verification.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.forgot_password.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.forgot_change_password.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.job.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.two_step_verification.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.job.fragment.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.dashboard_change_password.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.sos_document_submited.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.upload_sos_id_proof.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.builders_details.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.manage_builders.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.job_list_truck_fleet.fragment.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.post_a_truck.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.post_a_truck_image.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.track_list.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.arrange_trip.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.truck_details.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.manage_driver.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.add_driver.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.driver_access_point.ViewModelProvider(

                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.builder_my_jobs_list.ViewModelProvider(

                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.payment_method.ViewModelProvider(

                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.card_list.ViewModelProvider(

                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.driver_truck_live.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.end_trip.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.near_by_contractor_list.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.track_status.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.my_booking.fragment.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.submit_review.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.search_truck.ViewModelProvider(
                instance()
            )
        }


        bind() from provider {
            com.itsc.cal.ui.search_contractor.ViewModelProvider(

                instance()
            )
        }


        bind() from provider {
            com.itsc.cal.ui.profile.ViewModelProvider(

                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.profile_builder.ViewModelProvider(

                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.job_details.ViewModelProvider(

                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.edit_profile.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.dashboard.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.alert.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.driver_job_details.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.post_a_haul_off.ViewModelProvider(
                instance()
            )
        }


        bind() from provider {
            com.itsc.cal.ui.builder_my_job_list_with_haul_job.fragment.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.post_hault_of_job_list.fragment.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.post_hault_of_job_list.post_export_job.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.post_hault_of_job_list.post_export_job_two.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.post_hault_of_job_list.post_export_job_three.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.post_hault_of_job_list.import_export_job_details.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.post_hault_of_job_list.matches_jobs.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.suscription.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.subscription_details.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.cancel_policy_details.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.sos_message_log.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.sos_section.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.log_listing.ViewModelProvider(
                instance()
            )
        }

        bind() from provider {
            com.itsc.cal.ui.search_material_supplier.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.paymentsettings.ViewModelProvider(
                instance()
            )
        }
        bind() from provider {
            com.itsc.cal.ui.tracking_truck_live.ViewModelProvider(
                instance()
            )
        }
    }
}
