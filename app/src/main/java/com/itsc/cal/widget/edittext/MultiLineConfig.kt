package com.itsc.cal.widget.edittext

data class MultiLineConfig(
    val minLines: Int = 3,
    val maxLines: Int = 6
)