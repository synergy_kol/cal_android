package com.itsc.cal.widget.spinner

data class SpinnerData<T>(
    val toShow: String,
    val data: T
)