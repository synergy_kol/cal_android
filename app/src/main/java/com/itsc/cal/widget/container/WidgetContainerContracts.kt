package com.itsc.cal.widget.container

import androidx.constraintlayout.solver.widgets.WidgetContainer

interface WidgetContainerContracts {
    fun setErrorText(errorText: String): WidgetContainer
    fun showError(errorText: String? = null)
    fun hideError()
}