package com.itsc.cal.widget.spinner

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.itsc.cal.R
import com.itsc.cal.databinding.SpinnerBinding

class Spinner @JvmOverloads constructor(
    context: Context? = null,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    ConstraintLayout(context!!, attrs, defStyleAttr) {

    private lateinit var binding: SpinnerBinding
    private lateinit var contextForLaterUse: Context
    private lateinit var adapter: Any

    init {
        context?.let {
            binding = SpinnerBinding.inflate(
                it.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater,
                this,
                true
            )
            contextForLaterUse = it
        }
    }

    fun <T> addItems(
        list: ArrayList<SpinnerData<T>>,
        withDot: Boolean = false,
        listener: OnItemSelectedListener
    ): Spinner {
        adapter = SpinnerAdapter(
            contextForLaterUse,
            withDot,
            list
        )
        binding.spinner.adapter = adapter as SpinnerAdapter<*>
        binding.spinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    parent?.let {
                        @Suppress("UNCHECKED_CAST")
                        listener.selected(
                            it.getItemAtPosition(position) as SpinnerData<T>
                        )
                    }
                }
            }
        return this
    }

    interface OnItemSelectedListener {
        fun <T> selected(item: SpinnerData<T>)
    }

    fun <T> select(toShowString: String? = null, data: T? = null): Spinner {
        var indexToChange = -1
        data?.let {
            (adapter as SpinnerAdapter<*>)
                .getList()
                .forEachIndexed { index, item ->
                    if (item.data == it) {
                        indexToChange = index
                    }
                }
        } ?: run {
            toShowString?.let {
                (adapter as SpinnerAdapter<*>)
                    .getList()
                    .forEachIndexed { index, spinnerData ->
                        if (spinnerData.toShow == it && indexToChange == -1) {
                            indexToChange = index
                        }
                    }
            }
        }

        if (indexToChange != -1) {
            binding.spinner.setSelection(indexToChange)
        }

        return this
    }

    fun editable(isEditable: Boolean = true): Spinner {
        if (isEditable)
            binding.cover.visibility = View.GONE
        else
            binding.cover.visibility = View.VISIBLE

        return this
    }

    fun noBg() {
        binding.spinner.setBackgroundColor(
            ResourcesCompat.getColor(
                resources,
                android.R.color.transparent,
                null
            )
        )
    }

    fun setBg() {
     /*   binding.spinner.setBackgroundColor(
            ResourcesCompat.getColor(
                resources,
                android.R.color.transparent,
                null
            )
        )*/
        binding.spinner.setBackground(ContextCompat.getDrawable(context, R.drawable.spinner_bg));
    }


}