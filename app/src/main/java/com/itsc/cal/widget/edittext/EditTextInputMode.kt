package com.itsc.cal.widget.edittext

enum class EditTextInputMode {
    INPUT_TEXT,
    INPUT_TEXT_MULTI_LINE,
    INPUT_TEXT_CAPS,
    DATE,
    NUMBER,
    DECIMAL,
    PASSWORD,
    NUMERIC_PASSWORD,
    EMAIL
}