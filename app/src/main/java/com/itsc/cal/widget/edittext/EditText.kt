package com.itsc.cal.widget.edittext

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import com.jakewharton.rxbinding3.widget.textChanges
import com.itsc.cal.databinding.EdittextBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.*
import java.util.concurrent.TimeUnit


@Suppress("unused")
class EditText @JvmOverloads constructor(
    context: Context? = null,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    ConstraintLayout(context!!, attrs, defStyleAttr) {

    interface TextWatcher {
        fun textChanged(text: String)
    }

    private lateinit var binding: EdittextBinding
    private lateinit var contextForLaterUse: Context
    private var timeMills: Long = 0L

    fun getEditText() = binding.editText

    fun getTimeMills() = timeMills

    init {
        context?.let {
            binding = EdittextBinding.inflate(
                it.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater,
                this,
                true
            )
            contextForLaterUse = it
        }
    }

    @SuppressLint("CheckResult")
    fun watchForTextChange(textWatcher: TextWatcher): EditText {
        binding.editText
            .textChanges()
            .debounce(1000, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { charSeq ->
                textWatcher.textChanged(charSeq.toString())
            }

        return this
    }

    fun getText(): String {
        return binding.editText.text.toString().trim()
    }

    fun setText(text: String): EditText {
        binding.editText.setText(text)
        return this
    }

    fun hint(hint: String): EditText {
        binding.editText.hint = hint
        return this
    }

    fun inputMode(
        editTextInputMode: EditTextInputMode,
        multiLineConfig: MultiLineConfig = MultiLineConfig()
    ): EditText {
        when (editTextInputMode) {
            EditTextInputMode.INPUT_TEXT -> {
                inputText()
            }
            EditTextInputMode.INPUT_TEXT_CAPS -> {
                inputTextCaps()
            }
            EditTextInputMode.INPUT_TEXT_MULTI_LINE -> {
                inputTextMultiLine(multiLineConfig)
            }
            EditTextInputMode.DATE -> {
                inputDate()
            }
            EditTextInputMode.NUMBER -> {
                inputNumber()
            }
            EditTextInputMode.DECIMAL -> {
                inputDecimalNumber()
            }
            EditTextInputMode.PASSWORD -> {
                inputPassword()
            }
            EditTextInputMode.NUMERIC_PASSWORD -> {
                inputNumericPassword()
            }
            EditTextInputMode.EMAIL -> {
                inputEmail()
            }
        }
        return this
    }

    fun editable(isEditable: Boolean = true): EditText {
        if (isEditable)
            binding.cover.visibility = View.GONE
        else
            binding.cover.visibility = View.VISIBLE

        return this
    }

    fun drawableStart(drawable: Drawable): EditText {
        binding.editText.setCompoundDrawablesWithIntrinsicBounds(
            drawable, null, null, null
        )

        return this
    }

    fun backgroundDrawable(drawable: Drawable): EditText {
        binding.editText.background = drawable

        return this
    }

    fun gravityCenter(): EditText {
        binding.editText.gravity = Gravity.CENTER
        return this
    }

    fun setColor(textColor: Int): EditText {
        binding.editText.setTextColor(textColor)
        return this
    }

    private fun inputText(): EditText {
        binding.editText.apply {
            inputType = InputType.TYPE_CLASS_TEXT
            transformationMethod = null
            isSingleLine = true
        }
        return this
    }

    private fun inputTextCaps(): EditText {
        binding.editText.apply {
            inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            transformationMethod = null
            isSingleLine = true
        }
        return this
    }

    fun typeFace(fontResEditText: Int? = null, fontResHeading: Int? = null): EditText {
        fontResEditText?.let {
            binding.editText.typeface = ResourcesCompat.getFont(
                context,
                it
            )
        }
        return this
    }

    private fun inputTextMultiLine(multiLineConfig: MultiLineConfig = MultiLineConfig()): EditText {
        binding.editText.apply {
            inputType = InputType.TYPE_CLASS_TEXT
            transformationMethod = null
            isSingleLine = false
            imeOptions = EditorInfo.IME_FLAG_NO_ENTER_ACTION
            setLines(multiLineConfig.minLines)
            minLines = multiLineConfig.minLines
            maxLines = multiLineConfig.maxLines
        }
        return this
    }

    @SuppressLint("SetTextI18n")
    private fun inputDate(): EditText {
        inputText()
        binding.editText.apply {
            isClickable = true
            isFocusable = false
        }
        binding.editText.setOnClickListener {
            DatePickerDialog(
                contextForLaterUse,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    binding.editText.setText(
                        "$year-${monthOfYear + 1}-$dayOfMonth"
                    )
                    val cal = Calendar.getInstance()
                    cal.apply {
                        this.set(Calendar.YEAR, year)
                        this.set(Calendar.MONTH, monthOfYear)
                        this.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    }
                    timeMills = cal.timeInMillis
                },
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            )
                .show()
        }
        return this
    }

    private fun inputNumber(): EditText {
        binding.editText.apply {
            inputType = InputType.TYPE_CLASS_NUMBER
            transformationMethod = null
            isSingleLine = true
        }
        return this
    }

    private fun inputDecimalNumber(): EditText {
        binding.editText.apply {
            inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            transformationMethod = null
            isSingleLine = true
        }
        return this
    }

     fun setMaxLength(maxLength:Int){
        binding.editText.setFilters(arrayOf<InputFilter>(LengthFilter(maxLength)))
    }

    private fun inputPassword(): EditText {
        binding.editText.apply {
            inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD
            transformationMethod = PasswordTransformationMethod.getInstance()
        }
        return this
    }

    private fun inputNumericPassword(): EditText {
        binding.editText.apply {
            inputType = InputType.TYPE_CLASS_NUMBER  or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            transformationMethod = PasswordTransformationMethod.getInstance()
        }
        return this
    }

    private fun inputEmail(): EditText {
        binding.editText.apply {
            inputType =
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS
            transformationMethod = null
            isSingleLine = true
        }
        return this
    }
}