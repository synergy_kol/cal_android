package com.itsc.cal.widget.edittext

import android.graphics.drawable.Drawable

interface EditTextContracts {
    fun getText(): String
    fun setText(text: String): EditText
    fun hint(hint: String): EditText
    fun inputMode(
        editTextInputMode: EditTextInputMode,
        multiLineConfig: MultiLineConfig = MultiLineConfig()
    ): EditText

    fun editable(isEditable: Boolean = true): EditText
    fun watchTextChange(textWatcher: EditText.TextWatcher): EditText
    fun getTimeMills(): Long
    fun drawableStart(drawable: Drawable): EditText
    fun gravityCenter(): EditText
    fun setColor(textColor: Int): EditText
    fun typeFace(fontResEditText: Int? = null, fontResHeading: Int? = null): EditText
    fun getEditText(): android.widget.EditText
}