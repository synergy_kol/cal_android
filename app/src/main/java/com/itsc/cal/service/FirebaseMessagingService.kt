package com.itsc.cal.service

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.itsc.cal.R
import com.itsc.cal.core.KeywordsAndConstants.DEVICE_FCM_TOKEN
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.dashboard.Dashboard

import com.sagar.android.logutilmaster.LogUtil
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class FirebaseMessagingService : FirebaseMessagingService(), KodeinAware {

    override val kodein: Kodein by kodein()

    private val repository: Repository by instance()
    private val logUtil: LogUtil by instance()

    override fun onNewToken(token: String) {
        super.onNewToken(token)

        logUtil.logI("got new token :$token")
        Log.i("Token:",token)
        repository.getSharedPref()
            .edit()
            .putString(
                DEVICE_FCM_TOKEN,
                token
            )
            .apply()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
/*
        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            logUtil.logV(
                """
                    notification body :
                    ${it.body}
                """.trimIndent()
            )
        }

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            logUtil.logV(
                """
                    data payload :
                    ${remoteMessage.data}
                """.trimIndent()
            )
            try {
                val map: Map<String, String> = remoteMessage.data
                logUtil.logV(
                    """
                    data payload json :
                    ${JSONObject(map)}
                """.trimIndent()
                )
                repository.gotPushNotification(JSONObject(map))
            } catch (ex: Exception) {
                logUtil.logV("error $ex")
            }
        }*/

        remoteMessage?.notification?.let {
            Log.d("MESSAGE", "Message Notification Body: ${it.body}")
            val title = it.title
            val message = it.body
            val mIntent = Intent()
            val resultIntent = Intent(this, Dashboard::class.java)
            resultIntent.addCategory(Intent.CATEGORY_DEFAULT)
            sendBroadcast(mIntent)
            showNotification(resultIntent,title,message)

        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun showNotification(resultIntent : Intent = Intent(), title : String?, message : String?){

        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val CHANNEL_ID = "fenicia.android_channel_01"
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val name = "fenicia.android_channel"
            val Description = "Fenicia Notification Channel"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.description = Description
            mChannel.enableLights(true)
            mChannel.lightColor = Color.RED
            mChannel.enableVibration(true)
            mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            mChannel.setShowBadge(false)
            mNotificationManager.createNotificationChannel(mChannel)
        }
        val drawable = ContextCompat.getDrawable(this, R.mipmap.ic_launcher_foreground);
        val bitmap = (drawable as BitmapDrawable).bitmap
        val mBuilder = NotificationCompat.Builder(this,CHANNEL_ID)
        mBuilder.setSmallIcon(R.mipmap.ic_launcher_foreground)
        mBuilder.setLargeIcon(bitmap)
        mBuilder.setContentTitle(title)
        mBuilder.setContentText(message)
        /* resultIntent.addCategory(Intent.CATEGORY_DEFAULT)
         sendBroadcast(resultIntent)*/
        //resultIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
        val pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        mBuilder.setContentIntent(pendingIntent)
        mBuilder.setAutoCancel(true)
        val notification = mBuilder.build()
        notification.flags = Notification.FLAG_AUTO_CANCEL
        notification.defaults = Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE or Notification.DEFAULT_SOUND
        // notificationID allows you to update the notification later on.
        mNotificationManager.notify(1, notification)
    }
}