package com.itsc.cal.service

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.IBinder
import android.os.Looper
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.repository.Repository
import com.sagar.android.logutilmaster.LogUtil
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class LocationMonitorForegroundService : Service(), KodeinAware {

    override val kodein: Kodein by kodein()
    private val repository: Repository by instance()
    private val logUtil: LogUtil by instance()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()

        createNotificationChannel()

        startForeground(
            124,
            getNotificationForLocationMonitoring()
        )

        startLocationMonitoring()
    }

    override fun onDestroy() {
        super.onDestroy()

        fusedLocationProviderClient.removeLocationUpdates(
            locationCallback
        )

        logUtil.logV("location monitoring service destroyed")
    }

    @SuppressLint("MissingPermission")
    private fun startLocationMonitoring() {
        val locationRequest = LocationRequest()
        locationRequest.apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = 1000 * 60
            smallestDisplacement = 10F
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult?) {
                super.onLocationResult(result)

                result?.let {
                    it.locations.forEach { location: Location ->
                        logUtil.logV(
                            """
                                  latitude : ${location.latitude}
                                  longitude : ${location.longitude}
                                  accuracy : ${location.accuracy}
                              """.trimIndent()
                        )
                        checkIfGoingOutOfPath(location)
                       // sendLocationToServer(location.latitude, location.longitude)
                    }
                }
            }
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    companion object {
        const val CHANNEL_ID_HIGH_IMPORTANCE = "notification_app_channel_high_importance"
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "High Importance"
            val descriptionText = "this is the high importance notification channel"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID_HIGH_IMPORTANCE, name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun getNotificationForLocationMonitoring(): Notification? {
        val builder = NotificationCompat.Builder(this, CHANNEL_ID_HIGH_IMPORTANCE)
            .setSmallIcon(com.itsc.cal.R.mipmap.ic_launcher)
            .setContentTitle("Cal")
            .setContentText("Trip on progress.")
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setOngoing(true)
            .setAutoCancel(false)

        return builder.build()
    }

    data class LocationForFirebase(
        var dateTime: String,
        var jobID: String,
        var lat: String,
        var long: String
    )

    private fun sendLocationToServer(latitude: Double, longitude: Double) {
        val truckId =
            repository.getSharedPref().getString(KeywordsAndConstants.TRUCK_ID_FOR_FIREBASE, "0")
        val jobId =
            repository.getSharedPref().getString(KeywordsAndConstants.JOB_ID_FOR_FIREBASE, "0")

        if (truckId == "0" || jobId == "0")
            return

        val key = "${truckId}_$jobId"

        logUtil.logV("key is $key")

        val ref = Firebase.database.reference.child("Location")

        var innerKey = ""

        val cal = Calendar.getInstance()

        ref.addListenerForSingleValueEvent(
            object : com.google.firebase.database.ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(snapshot: com.google.firebase.database.DataSnapshot) {
                    if (snapshot.hasChild(key)) {
                        logUtil.logV("has the child")
                        snapshot.child(key).children.forEach { children ->
                            innerKey = children.key!!
                            logUtil.logV(
                                "inner key is $innerKey"
                            )
                        }

                        ref
                            .child(key)
                            .child(innerKey)
                            .setValue(
                                LocationForFirebase(
                                    "${cal.get(Calendar.DATE)}/${cal.get(Calendar.MONTH) + 1}/${cal.get(
                                        Calendar.YEAR
                                    )} ${cal.get(Calendar.HOUR_OF_DAY)}:${cal.get(Calendar.MINUTE)}:${cal.get(
                                        Calendar.SECOND
                                    )}",
                                    jobId!!,
                                    latitude.toString(),
                                    longitude.toString()
                                )
                            )
                    } else {
                        logUtil.logV("does not have the child")

                        ref.child(key).push().setValue(
                            LocationForFirebase(
                                "${cal.get(Calendar.DATE)}/${cal.get(Calendar.MONTH) + 1}/${cal.get(
                                    Calendar.YEAR
                                )} ${cal.get(Calendar.HOUR_OF_DAY)}:${cal.get(Calendar.MINUTE)}:${cal.get(
                                    Calendar.SECOND
                                )}",
                                jobId!!,
                                latitude.toString(),
                                longitude.toString()
                            )
                        )
                    }
                }
            }
        )
    }

    private fun checkIfGoingOutOfPath(location: Location) {
        sendBroadcast(
            Intent(KeywordsAndConstants.MAY_RE_DRAW_POLY_LINES)
                .putExtra(
                    "lat",
                    location.latitude
                )
                .putExtra(
                    "long",
                    location.longitude
                )
        )
    }
}