package com.itsc.cal.enums

enum class UserType {
    TRUCK,
    MANUFACTURER,
    BUILDER
}