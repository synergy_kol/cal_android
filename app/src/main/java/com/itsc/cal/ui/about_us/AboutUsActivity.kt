package com.itsc.cal.ui.about_us

import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityAboutUsBinding

class AboutUsActivity : CalSuperActivity() {
    private lateinit var binding: ActivityAboutUsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_about_us)
        enableBackButton()
        setPageTitle("About Us")
        showContactUsActionbar()
    }
}