package  com.itsc.cal.ui.search_material_supplier

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveSupplierListing: MediatorLiveData<Event<ArrayList<BuilderListResponse>>> =
        MediatorLiveData()
    val mediatorLiveSupplierListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveSupplierListing.addSource(
            repository.mutableLiveDataSupplierList
        ) { t -> mediatorLiveSupplierListing.postValue(t) }
        mediatorLiveSupplierListingError.addSource(
            repository.mutableLiveDataSupplierListError
        ) { t -> mediatorLiveSupplierListingError.postValue(t) }

    }

    fun getSupplierList() = repository.getSupplierList()


}