package com.itsc.cal.ui.trucks.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.itsc.cal.R
import com.itsc.cal.databinding.FragmentTruckListBinding
import com.itsc.cal.ui.trucks.TruckList
import com.itsc.cal.ui.trucks.adapter.TruckListAdapter

class TruckListFragment:Fragment() {
    private lateinit var binding: FragmentTruckListBinding
    private lateinit var mTruckListAdapter: TruckListAdapter
    private lateinit var baseActivity: TruckList
    companion object{
        fun newInstance(): TruckListFragment {
            val mTruckListFragment = TruckListFragment()
            return mTruckListFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_truck_list,container,false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        baseActivity  = activity as TruckList
        setTruckListAdapter()
    }


    private fun setTruckListAdapter() {
     /*   mTruckListAdapter = TruckListAdapter(baseActivity, object : OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                when(view.id){

                }
            }

        })
        val layoutManager = LinearLayoutManager(baseActivity, LinearLayoutManager.VERTICAL,false)
        binding.rvTruckList.layoutManager = layoutManager
        binding.rvTruckList.adapter = mTruckListAdapter*/
    }
}