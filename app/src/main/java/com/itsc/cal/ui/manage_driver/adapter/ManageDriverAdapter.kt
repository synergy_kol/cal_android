package com.itsc.cal.ui.manage_driver.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ChildManageDriverBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.DriverListResponse

class ManageDriverAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener,val arrayList:ArrayList<DriverListResponse>):
    RecyclerView.Adapter<ManageDriverAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ChildManageDriverBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))
    }

    inner class ViewHolder(val binding: ChildManageDriverBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(driverListResponse: DriverListResponse) {
            binding.textDriverName.text  = driverListResponse.driver_name
            binding.textNumber.text = driverListResponse.driver_licence
            binding.textTruckNumber.text = driverListResponse.driver_number
            binding.imgProfileImage.setImage(  imageUrl = "${KeywordsAndConstants.BASE_URL}${driverListResponse.driverimage}",
                isCircularImage = true,
                needPlaceHolderImageForName = driverListResponse.driver_name.substring(0,1))

            binding.btnButtonDetails.setOnClickListener {
                onItemClickListener.onItemClick(it,position = adapterPosition)
            }
        }

    }

}