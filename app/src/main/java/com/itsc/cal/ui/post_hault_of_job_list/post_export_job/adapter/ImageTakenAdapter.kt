package com.itsc.cal.ui.post_hault_of_job_list.post_export_job.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.`interface`.ImageOnItemClickListener
import com.itsc.cal.databinding.ChildPictureListBinding
import com.itsc.cal.model.VehicleImage
import java.io.File

class ImageTakenAdapter(
    val mContext: Context,
    val arrayList: ArrayList<VehicleImage>,
    val onItemClickListener: ImageOnItemClickListener
) :
    RecyclerView.Adapter<ImageTakenAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ChildPictureListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))


    }

    inner class ViewHolder(val binding: ChildPictureListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(vehicleImage: VehicleImage) {
            if (vehicleImage.isAddImage) {
                binding.imgVehicle.visibility = View.INVISIBLE
                binding.imgVehicle.setImage(imageDrawable = mContext.resources.getDrawable(R.drawable.ic_camera))
                binding.imgVehicle.scaleType = ImageView.ScaleType.FIT_CENTER
            } else {
                binding.imgVehicle.visibility = View.VISIBLE
                binding.imgVehicle.scaleType = ImageView.ScaleType.CENTER_CROP
                binding.imgVehicle.setImage(File(vehicleImage.imageFileString))
            }
            binding.imgVehicle.registerForOnClick {
                if (vehicleImage.isAddImage) {
                    onItemClickListener.onItemClick(adapterPosition)
                }
            }
            binding.llBgImage.setOnClickListener {
                if (vehicleImage.isAddImage) {
                    onItemClickListener.onItemClick(adapterPosition)
                }
            }

        }


    }

}