package  com.itsc.cal.ui.sos_section

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveNotificationRead: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveNotificationReadError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveNotificationRead.addSource(
            repository.mutableLiveDataNotificationRead
        ) { t -> mediatorLiveNotificationRead.postValue(t) }
        mediatorLiveNotificationReadError.addSource(
            repository.mutableLiveDataNotificationReadError
        ) { t -> mediatorLiveNotificationReadError.postValue(t) }

    }

    fun notificationRead(){
        repository.notificationRead()
    }
}
