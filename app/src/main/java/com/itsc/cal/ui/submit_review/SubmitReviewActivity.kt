package com.itsc.cal.ui.submit_review

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySubmitReviewBinding
import com.itsc.cal.model.JobListResponse
import com.itsc.cal.model.ReviewResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class SubmitReviewActivity : CalSuperActivity(), KodeinAware {
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var binding: ActivitySubmitReviewBinding
    private lateinit var jobListResponse: JobListResponse
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_submit_review)
        setPageTitle("Review")
        enableBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        showHelpButton()
        invitView()
        bindingViewModel()
    }

    private fun invitView() {
        intent.let {
            jobListResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            getReview(jobListResponse)
        }
        binding.buttonRate.setOnClickListener {
            apicalled()
        }
    }

    private fun getReview(jobListResponse: JobListResponse) {
        showProgress()
        viewModel.getReview(
            sender_id = viewModel.giveRepository().getUserData()!!.user_master_id,
            receiver_id = jobListResponse.supplier_id,
            job_id = jobListResponse.job_id
        )
    }

    private fun bindingViewModel() {
        viewModel.mediatorLiveSubmitReview.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveSubmitReviewError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveGetReview.observe(
            this,
            Observer<Event<ReviewResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        getReviewData(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveGetReviewError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )
    }

    private fun getReviewData(response: ReviewResponse) {
        if (response.rating != null && response.rating != "") {
            binding.ratingBar.rating = response.rating.toFloat()
            binding.editTextTypeYourReview.setText(response.comment.toString())
            binding.buttonRate.visibility = View.GONE
            binding.editTextTypeYourReview.isEnabled = false
            binding.ratingBar.isEnabled = false

        }else{
            binding.buttonRate.visibility = View.VISIBLE
            binding.editTextTypeYourReview.isEnabled = true
            binding.ratingBar.isEnabled = true
        }

    }

    private fun apicalled() {

        if (binding.ratingBar.rating == 0f) {
            showMessageInDialog("Please provide user rating.")
            return
        }
        if (binding.editTextTypeYourReview.text.toString().isEmpty()) {
            showMessageInDialog("Please provide user review.")
            return
        }
        showProgress()
        viewModel.submitReview(
            comment = binding.editTextTypeYourReview.text.toString(),
            job_id = jobListResponse.job_id,
            rating = binding.ratingBar.toString(),
            receiver_id = jobListResponse.supplier_id,
            sender_id = viewModel.giveRepository().getUserData()!!.user_master_id
        )
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@SubmitReviewActivity,
                                Dashboard::class.java
                            )
                        )
                    )
                    finish()

                }
            }
        )

    }
}