package  com.itsc.cal.ui.driver_truck_live

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.SOSReason
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataStartJob: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataStartJobError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveDataMarksAsDelay: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataMarksAsDelayError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveEndTrip: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveEndTripError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveSubmitSOSReason: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveSubmitSOSReasonError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveSOSReasonListing: MediatorLiveData<Event<ArrayList<SOSReason>>> =
        MediatorLiveData()
    val mediatorLiveSOSReasonListingError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveUpdCurrLocation: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveUpdCurrLocationError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDataStartJob.addSource(
            repository.mutableLiveStartJob
        ) { t -> mediatorLiveDataStartJob.postValue(t) }
        mediatorLiveDataStartJobError.addSource(
            repository.mutableLiveStartJobError
        ) { t -> mediatorLiveDataStartJobError.postValue(t) }

        mediatorLiveDataMarksAsDelay.addSource(
            repository.mutableLiveDataMarksAsDelay
        ) { t -> mediatorLiveDataMarksAsDelay.postValue(t) }
        mediatorLiveDataMarksAsDelayError.addSource(
            repository.mutableLiveDataMarksAsDelayError
        ) { t -> mediatorLiveDataMarksAsDelayError.postValue(t) }


        mediatorLiveEndTrip.addSource(
            repository.mutableLiveDataEndTrip
        ) { t -> mediatorLiveEndTrip.postValue(t) }
        mediatorLiveEndTripError.addSource(
            repository.mutableLiveDataEndTripError
        ) { t -> mediatorLiveEndTripError.postValue(t) }

        mediatorLiveSubmitSOSReason.addSource(
            repository.mutableLiveDataSubmitSOSReason
        ) { t -> mediatorLiveSubmitSOSReason.postValue(t) }
        mediatorLiveSubmitSOSReasonError.addSource(
            repository.mutableLiveDataSubmitSOSReasonError
        ) { t -> mediatorLiveSubmitSOSReasonError.postValue(t) }

        mediatorLiveSOSReasonListing.addSource(
            repository.mutableLiveDataSOSReason
        ) { t -> mediatorLiveSOSReasonListing.postValue(t) }
        mediatorLiveSOSReasonListingError.addSource(
            repository.mutableLiveDataSOSReasonError
        ) { t -> mediatorLiveSOSReasonListingError.postValue(t) }

        mediatorLiveUpdCurrLocation.addSource(
            repository.mutableLiveDataUpdCurrLocation
        ) { t -> mediatorLiveUpdCurrLocation.postValue(t) }
        mediatorLiveUpdCurrLocationError.addSource(
            repository.mutableLiveDataUpdCurrLocationError
        ) { t -> mediatorLiveUpdCurrLocationError.postValue(t) }
    }

    fun startJob(challan: File?, token: String,truckImage:File?,truckWeight:String) =
        repository.startJob(challan = challan, token = token,truckImage = truckImage,truckWeight = truckWeight)

    fun endTrip(
        token_id: String,
        driver_job_status: String
    ) {
        repository.endTrip(
            token_id = token_id,
            driver_job_status = driver_job_status
        )
    }

    fun marksAsDelay(delayReason: String, token: String, delayTime: String) =
        repository.marksAsDelay(token = token, delayReason = delayReason, delayTime = delayTime)

    fun submitSOSReason(driver_id: String, token_id: String, reason: String) =
        repository.submitSOSReason(driver_id = driver_id,token_id = token_id,reason = reason)

    fun getSOSReasonList()=repository.getSOSReason()

    fun updateDriverCurrentLocation(jobId: String, lat:String, lng:String) =
        repository.updateDriverCurrentLocation(jobId, lat, lng)
}
