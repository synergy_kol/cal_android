package com.itsc.cal.ui.trucks.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.itsc.cal.ui.trucks.fragment.TruckListFragment

class TruckListPagerAdapter(val fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> {
                fragment =  TruckListFragment.newInstance()
            }
            1 -> {
                fragment = TruckListFragment.newInstance()
            }

        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title = ""
        when (position) {
            0 -> {
                title = "TOP RESULT"
            }
            1 -> {
                title = "USER RATING"
            }

        }
        return title
    }
}