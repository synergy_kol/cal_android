package com.itsc.cal.ui.post_a_truck_driver_details

import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityPostTruckDriverBinding


class PostTruckDriverActivity : CalSuperActivity() {
    private lateinit var binding: ActivityPostTruckDriverBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_post_truck_driver)
        val metrics = resources.displayMetrics
        val screenWidth = (metrics.widthPixels * 0.80).toInt()
        val screenHeight = (metrics.heightPixels * 0.90).toInt()
        getWindow().setLayout(screenWidth, screenHeight)

    }
}