package com.itsc.cal.ui.job_list_truck_fleet

import android.os.Bundle
import android.os.Handler
import androidx.viewpager.widget.ViewPager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityJobListTruckFleetBinding
import com.itsc.cal.ui.job_list_truck_fleet.adapter.ReservationViewPagerAdapter
import com.itsc.cal.ui.job_list_truck_fleet.fragment.PostedJobTruckListFragment

class JobListTruckFleetActivity : CalSuperActivity() {

    private lateinit var binding: ActivityJobListTruckFleetBinding
    private val pagerFragmentList = listOf(
        PostedJobTruckListFragment.newInstance("1"),
        PostedJobTruckListFragment.newInstance("2"),
        PostedJobTruckListFragment.newInstance("3")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_job_list_truck_fleet)
        enableBackButton()
        setPageTitle("Job listing")
        showHelpButton()
        setPagerAdapter()
    }

    private fun setPagerAdapter() {
        val mBookingPagerAdapter =
            ReservationViewPagerAdapter(supportFragmentManager, pagerFragmentList)
        binding.viewPager.adapter = mBookingPagerAdapter
        binding.viewPager.offscreenPageLimit = 1
        binding.tabs.setViewPager(binding.viewPager)
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                val myReservationFragment =
                    mBookingPagerAdapter.fragmentList[position] as? PostedJobTruckListFragment
                myReservationFragment?.getJobListing((position + 1).toString())
            }

        })

        Handler().postDelayed(
            {
                val myReservationFragment =
                    mBookingPagerAdapter.fragmentList[0] as? PostedJobTruckListFragment
                myReservationFragment?.getJobListing((0 + 1).toString())
            },
            800
        )

    }
}