package  com.itsc.cal.ui.profile_builder

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.ProfileDetails
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataProfileDetails: MediatorLiveData<Event<ProfileDetails>> =
        MediatorLiveData()
    val mediatorLiveDataProfileDetailsError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDataProfileDetails.addSource(
            repository.mutableLiveDataProfileDetail
        ) { t -> mediatorLiveDataProfileDetails.postValue(t) }
        mediatorLiveDataProfileDetailsError.addSource(
            repository.mutableLiveDataProfileDetailError
        ) { t -> mediatorLiveDataProfileDetailsError.postValue(t) }
    }

    fun getProfileDetails(userId:String) = repository.getProfileData(userId = userId)
}