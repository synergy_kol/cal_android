package  com.itsc.cal.ui.submit_review

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveSubmitReview: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveSubmitReviewError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveGetReview: MediatorLiveData<Event<ReviewResponse>> =
        MediatorLiveData()
    val mediatorLiveGetReviewError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveSubmitReview.addSource(
            repository.mutableLiveDataSubmitReview
        ) { t -> mediatorLiveSubmitReview.postValue(t) }
        mediatorLiveSubmitReviewError.addSource(
            repository.mutableLiveDataSubmitReviewError
        ) { t -> mediatorLiveSubmitReviewError.postValue(t) }


        mediatorLiveGetReview.addSource(
            repository.mutableLiveDataGetReview
        ) { t -> mediatorLiveGetReview.postValue(t) }
        mediatorLiveGetReviewError.addSource(
            repository.mutableLiveDataGetReviewError
        ) { t -> mediatorLiveGetReviewError.postValue(t) }

    }

    fun submitReview(
        comment: String,
        sender_id: String,
        receiver_id: String,
        rating: String,
        job_id: String
    ) =
        repository.submitReview(
            comment = comment,
            sender_id = sender_id,
            receiver_id = receiver_id,
            rating = rating,
            job_id = job_id
        )

    fun getReview(

        sender_id: String,
        receiver_id: String,
        job_id: String
    ) =
        repository.getReView(
            sender_id = sender_id,
            receiver_id = receiver_id,
            job_id = job_id
        )



}