package com.itsc.cal.ui.builder_my_job_list_with_haul_job.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.application.ApplicationClass
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.FragmentPostedJobBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse
import com.itsc.cal.ui.builder_my_job_list_with_haul_job.MyPostedJobBuilder
import com.itsc.cal.ui.builder_my_job_list_with_haul_job.adapter.PostedJobBuilderAdapter
import com.itsc.cal.ui.cancel_policy_details.CancelPolicyDetailsActivity
import com.itsc.cal.ui.job_details.JobDetails
import com.itsc.cal.ui.submit_review.SubmitReviewActivity
import com.itsc.cal.ui.track_status.TrackStatusActivity
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class PostedJobBuilderFragment : Fragment(), KodeinAware {

    private lateinit var binding: FragmentPostedJobBinding
    private lateinit var mPostedJobAdapter: PostedJobBuilderAdapter
    private lateinit var baseActivity: MyPostedJobBuilder
    override lateinit var kodein: Kodein
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var filter = ""

    companion object {
        fun newInstance(filter: String): PostedJobBuilderFragment {
            val mPostedJobFragment = PostedJobBuilderFragment()
            val bundle = Bundle()
            bundle.putString("filter", filter)
            mPostedJobFragment.arguments = bundle
            return mPostedJobFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_posted_job, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        baseActivity = activity as MyPostedJobBuilder
        kodein = (baseActivity.applicationContext as ApplicationClass).kodein
        viewModel = androidx.lifecycle.ViewModelProvider(
            this,
            viewModelProvider
        )
            .get(
                ViewModel::class.java
            )


        bindToViewModel()

    }

    private fun getFilterId(): String {
        return arguments?.getString("filter") ?: ""
    }

    public fun getJobListing(filter: String) {
        binding.textviewNoDataFound.visibility = View.GONE
        (baseActivity as MyPostedJobBuilder).showProgress()
        this.filter = filter
        viewModel.getJobList(filter)
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveJobListing.observe(
            baseActivity,
            Observer<Event<ArrayList<JobListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        val listToPush: ArrayList<JobListResponse> = ArrayList()
                        it.getContent()!!.forEach { job ->
                            if (job.filter == filter) {
                                listToPush.add(job)
                            }
                        }
                        (baseActivity as MyPostedJobBuilder).hideProgress()
                        setPostedJobAdapter(listToPush)
                    }

                }
            }
        )
        viewModel.mediatorLiveJobListingError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent() && filter != "") {
                        (baseActivity as MyPostedJobBuilder).hideProgress()
                        if (it.getContent()!!.getErrorCode() == filter.toInt()) {
                            binding.rvPostedJob.visibility = View.GONE
                            binding.textviewNoDataFound.visibility = View.VISIBLE
                        }
                    }

                }
            }
        )


    }

    override fun onResume() {
        super.onResume()

    }

    private fun setPostedJobAdapter(arrayList: ArrayList<JobListResponse>) {
        if (arrayList.size > 0) {
            binding.textviewNoDataFound.visibility = View.GONE
            binding.rvPostedJob.visibility = View.VISIBLE
            binding.imgTraView.visibility = View.GONE
            mPostedJobAdapter = PostedJobBuilderAdapter(baseActivity, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    when (view.id) {
                        R.id.cvPostedJob -> {
                            val intent = Intent(baseActivity, JobDetails::class.java)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                (baseActivity).toJson(arrayList.get(position))
                            )
                            intent.putExtra(KeywordsAndConstants.isFromBooking,false)
                            startActivity(intent)
                        }
                        R.id.btn_tracking->{
                            val intent = Intent(baseActivity, TrackStatusActivity::class.java)
                            intent.putExtra(KeywordsAndConstants.isJobListing,true)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                (baseActivity).toJson(arrayList.get(position))
                            )
                            startActivity(intent)
                        }
                        R.id.btn_review->{

                            val intent = Intent(baseActivity, SubmitReviewActivity::class.java)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                (baseActivity).toJson(arrayList.get(position))
                            )
                            startActivity(intent)
                        }
                        R.id.btn_cancel->{
                            val intent = Intent(baseActivity, CancelPolicyDetailsActivity::class.java)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                (baseActivity).toJson(arrayList.get(position))
                            )
                            startActivity(intent)
                        }
                    }
                }

            }, arrayList)
            val layoutManager =
                LinearLayoutManager(baseActivity, LinearLayoutManager.VERTICAL, false)
            binding.rvPostedJob.layoutManager = layoutManager
            binding.rvPostedJob.adapter = mPostedJobAdapter
        } else {
            binding.textviewNoDataFound.visibility = View.VISIBLE
            binding.rvPostedJob.visibility = View.GONE
            binding.imgTraView.visibility = View.VISIBLE
        }

    }

    private fun moveToJobDetails() {

    }

}