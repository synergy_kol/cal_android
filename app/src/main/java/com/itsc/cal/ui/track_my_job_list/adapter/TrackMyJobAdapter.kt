package com.itsc.cal.ui.track_my_job_list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildPostedJobTrackBinding
import com.itsc.cal.interfaces.OnItemClickListener

class TrackMyJobAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener):
    RecyclerView.Adapter<TrackMyJobAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(ChildPostedJobTrackBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return 12
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData()


    }

    inner class ViewHolder(val binding: ChildPostedJobTrackBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData() {
            binding.cvPostedJob.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
        }

    }

}