package com.itsc.cal.ui.sos_message_log

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySosMessageLogBinding
import com.itsc.cal.model.*
import com.itsc.cal.ui.alert.AlertActivity
import com.itsc.cal.ui.log_listing.LogListActivity
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SOSMessageLogActivity : CalSuperActivity(withNav = true), KodeinAware {

    private lateinit var binding: ActivitySosMessageLogBinding
    override val kodein: Kodein by kodein()

    companion object {
        var truckListing: Activity? = null
    }

    private var notificationList: ArrayList<notificationList> = ArrayList()

    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_sos_message_log)
        enableBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        setPageTitle("SOS and Msgs Log Section")
        showHelpButton()
        bindToViewModel()
        initview()
        getNotificationDetails()
    }

    private fun getNotificationDetails() {
        showProgress()
        viewModel.getNotification()
    }

    private fun getNotificationList() {
        showProgress()
        viewModel.getNotificationList("1")
    }
    private fun initview() {
        binding.buttonSosSection.setOnClickListener {
            getNotificationList()
        }
        binding.btnLog.setOnClickListener {
            startActivity(Intent(this,LogListActivity::class.java))
        }
        binding.btnMegLogSection.setOnClickListener {
            getNotificationDetails()

        }
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveNotificationResponse.observe(
            this,
            Observer<Event<NotificationResponce>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        notificationDetails(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveNotificationResponseError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )
        viewModel.mediatorLiveNotificationListRes.observe(
            this,
            Observer<Event<ArrayList<notificationList>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        notificationList.clear()
                        notificationList.addAll(it.getContent()!!)
                        if ( notificationList.size > 0) {
                            startActivity(
                                Intent(this, AlertActivity::class.java).putExtra(
                                    KeywordsAndConstants.DATA, toJson(notificationList)
                                ).putExtra(KeywordsAndConstants.IS_FORM_DASHBOARD,false)
                            )
                        } else {
                            showMessageInDialog("No Msg Log found.")
                        }
                    }

                }
            }
        )
        viewModel.mediatorLiveNotificationListResError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )

    }

    private fun notificationDetails(notificationResponce: NotificationResponce) {
        var userDetails = viewModel.giveRepository().getUserData()
        userDetails!!.stripe_account_id = notificationResponce.stripe_check.stripe_account_id
        viewModel.giveRepository().saveUserDataToPref(userDetails)
        notificationList = notificationResponce.notification_list
        if (notificationList.size > 0) {
            startActivity(
                Intent(this, AlertActivity::class.java).putExtra(
                    KeywordsAndConstants.DATA, toJson(notificationList)
                ).putExtra(KeywordsAndConstants.IS_FORM_DASHBOARD,false)
            )
        } else {
            showMessageInDialog("No Msg Log found.")
        }
    }

    override fun onResume() {
        super.onResume()

    }
}