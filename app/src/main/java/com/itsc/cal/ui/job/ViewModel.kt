package  com.itsc.cal.ui.job

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveMeteralData: MediatorLiveData<Event<ArrayList<MeterialResponse>>> =
        MediatorLiveData()
    val mediatorLiveMeteralDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveTruckData: MediatorLiveData<Event<ArrayList<TruckDataResponse>>> =
        MediatorLiveData()
    val mediatorLiveTruckDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveEquipementData: MediatorLiveData<Event<ArrayList<EquipementResponse>>> =
        MediatorLiveData()
    val mediatorLiveEquipementDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveLoadData: MediatorLiveData<Event<ArrayList<LoadResponse>>> = MediatorLiveData()
    val mediatorLiveLoadDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLivePostJob: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLivePostJobError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDefalutCard: MediatorLiveData<Event<CardResponse>> = MediatorLiveData()
    val mediatorLiveDefalutCardError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDataPaymentPercentage: MediatorLiveData<Event<PaymentPecentageResponse>> =
        MediatorLiveData()
    val mediatorLiveDataPaymentPercentageError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveCardListing: MediatorLiveData<Event<ArrayList<CardListResponse>>> =
        MediatorLiveData()
    val mediatorLiveCardListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveCardListing.addSource(
            repository.mutableLiveDataCardList
        ) { t -> mediatorLiveCardListing.postValue(t) }
        mediatorLiveCardListingError.addSource(
            repository.mutableLiveDataCardListError
        ) { t -> mediatorLiveCardListingError.postValue(t) }

        mediatorLiveMeteralData.addSource(
            repository.mutableLiveDataMeteralData
        ) { t -> mediatorLiveMeteralData.postValue(t) }
        mediatorLiveMeteralDataError.addSource(
            repository.mutableLiveDataSignUpError
        ) { t -> mediatorLiveMeteralDataError.postValue(t) }

        mediatorLiveTruckData.addSource(
            repository.mutableLiveDataTruckData
        ) { t -> mediatorLiveTruckData.postValue(t) }
        mediatorLiveTruckDataError.addSource(
            repository.mutableLiveDataTruckDataError
        ) { t -> mediatorLiveTruckDataError.postValue(t) }

        mediatorLiveEquipementData.addSource(
            repository.mutableLiveDataEquipementData
        ) { t -> mediatorLiveEquipementData.postValue(t) }
        mediatorLiveEquipementDataError.addSource(
            repository.mutableLiveDataEquipementDataError
        ) { t -> mediatorLiveEquipementDataError.postValue(t) }

        mediatorLiveLoadData.addSource(
            repository.mutableLiveDataLoadData
        ) { t -> mediatorLiveLoadData.postValue(t) }
        mediatorLiveLoadDataError.addSource(
            repository.mutableLiveDataLoadError
        ) { t -> mediatorLiveLoadDataError.postValue(t) }

     /*   mediatorLivePostJob.addSource(
            repository.mutableLiveDataPostJob
        ) { t -> mediatorLivePostJob.postValue(t) }
        mediatorLivePostJobError.addSource(
            repository.mutableLiveDataPostJobError
        ) { t -> mediatorLivePostJobError.postValue(t) }
*/

        mediatorLiveDefalutCard.addSource(
            repository.mutableLiveDataDefaultCard
        ) { t -> mediatorLiveDefalutCard.postValue(t) }
        mediatorLiveDefalutCardError.addSource(
            repository.mutableLiveDataDefaultCardError
        ) { t -> mediatorLiveDefalutCardError.postValue(t) }

        mediatorLiveDataPaymentPercentage.addSource(
            repository.mutableLiveDataPaymentPercentageJobPost
        ) { t -> mediatorLiveDataPaymentPercentage.postValue(t) }
        mediatorLiveDataPaymentPercentageError.addSource(
            repository.mutableLiveDataPaymentPercentageJobPostError
        ) { t -> mediatorLiveDataPaymentPercentageError.postValue(t) }
    }

    fun getMeterialData() = repository.getMasterMeterialData()
    fun getLoadData() = repository.getLoadData()
    fun getTruckData() = repository.getMasterTruckData()
    fun getEquiptmentData() = repository.getMasterEquipementlData()
    fun getDefaultCard() = repository.getDefaultCard()
    fun getPaymentPercentage() = repository.getPaymentPercentagePostJob()
    fun getCardList() = repository.getCardList()
}