package com.itsc.cal.ui.my_booking

import android.os.Bundle
import android.os.Handler
import androidx.viewpager.widget.ViewPager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityMyBookingBinding
import com.itsc.cal.ui.my_booking.adapter.ViewPagerAdapterBooking
import com.itsc.cal.ui.my_booking.fragment.CurrentBookingFragement

class MyBookingActivity : CalSuperActivity(withNav = true) {

    private lateinit var binding: ActivityMyBookingBinding
    private val pagerFragmentList = listOf(
        CurrentBookingFragement.newInstance("4"),
        CurrentBookingFragement.newInstance("5")

    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_my_booking)
        enableBackButton()
        setPageTitle("My Booking")
        showHelpButton()
        setPagerAdapter()

    }

    private fun setPagerAdapter(){
        val mBookingPagerAdapter = ViewPagerAdapterBooking(supportFragmentManager,pagerFragmentList)
        binding.viewPager.adapter = mBookingPagerAdapter
        binding.viewPager.offscreenPageLimit = 1
        binding.tabs.setViewPager(binding.viewPager)
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                val myReservationFragment =
                    mBookingPagerAdapter.fragmentList[position] as? CurrentBookingFragement
                myReservationFragment?.getJobListing((position + 4).toString())
            }

        })

        Handler().postDelayed(
            {
                val myReservationFragment =
                    mBookingPagerAdapter.fragmentList[0] as? CurrentBookingFragement
                myReservationFragment?.getJobListing((0 + 4).toString())
            },
            800
        )
    }
}