package com.itsc.cal.ui.payment_method

import android.os.Bundle
import com.itsc.cal.util.generateStripeToken
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityPaymetMethodBinding
import com.itsc.cal.model.CardListResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.whiteelephant.monthpicker.MonthPickerDialog
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class PaymentMethodActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPaymetMethodBinding
    private val today = Calendar.getInstance()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var cardListResponse: CardListResponse
    private var isEditable = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_paymet_method)
        enableBackButton()
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        setPageTitle("Add New Card")
        initView()
        bindingViewModel()
    }

    private fun bindingViewModel() {
        viewModel.mediatorLiveSaveCardData.observe(
            this,
            androidx.lifecycle.Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveSaveCardDataError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveEditCardData.observe(
            this,
            androidx.lifecycle.Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveEditCardDataError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Card added successfully",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    finish()

                }
            }
        )

    }

    private fun initView() {
        intent?.let {
            isEditable = intent.getBooleanExtra(KeywordsAndConstants.isEditCard, false)
            if (isEditable) {
                cardListResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
                binding.edNameCardHolder.setText(cardListResponse.card_name)
                binding.edExpMonth.setText(cardListResponse.expiry_month)
                binding.edExpYear.setText(cardListResponse.expiry_year)
                binding.edCardNo.setText(cardListResponse.number)
                binding.edCvv.setText("")
                setPageTitle("Edit Card")
            }
        }
        binding.edNameCardHolder.apply {
            inputMode(EditTextInputMode.INPUT_TEXT)
            hint("Name of card holder")
        }

        binding.edCardNo.apply {
            inputMode(EditTextInputMode.NUMBER)
            hint("Card no")
            setMaxLength(16)
        }

        binding.edCvv.apply {
            inputMode(EditTextInputMode.NUMERIC_PASSWORD)
            hint("CVV")
            setMaxLength(3)
        }

        binding.edExpMonth.setOnClickListener {

            val builder: MonthPickerDialog.Builder = MonthPickerDialog.Builder(
                this,
                object : MonthPickerDialog.OnDateSetListener {
                    override fun onDateSet(selectedMonth: Int, selectedYear: Int) {
                        val selectedMonth =
                            (selectedMonth + 1).toString() + "/" + selectedYear
                        val fromDate = AndroidUtility.formatDateFromString(
                            "MM/yyyy",
                            "MM",
                            selectedMonth
                        )
                        binding.edExpMonth.setText(fromDate)
                    }
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH)
            )
            builder.setActivatedMonth(Calendar.JULY)
                .setMinYear(Calendar.YEAR)
                .setActivatedYear(Calendar.YEAR)
                .setMaxYear(2030)
                .setMinMonth(Calendar.JANUARY)
                .setTitle("Exp Month").showMonthOnly()
                .setOnMonthChangedListener { month ->
                    {

                    }
                }.build().show()


        }
        binding.edExpYear.setOnClickListener {

            val builder: MonthPickerDialog.Builder = MonthPickerDialog.Builder(
                this,
                object : MonthPickerDialog.OnDateSetListener {
                    override fun onDateSet(selectedMonth: Int, selectedYear: Int) {

                        binding.edExpYear.setText((selectedYear.toString()))
                    }
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH)
            )
            builder.setActivatedYear(2020)
                .setMinYear(2020)
                .setActivatedYear(2020)
                .setMaxYear(2030)
                .setTitle("Exp Year").showYearOnly()
                .setOnYearChangedListener { year ->
                    {

                    }
                }.build().show()

        }

        binding.buttonSubmit.setOnClickListener {
            ApiCalled()
        }
    }

    private fun ApiCalled() {

        if (
            binding.edNameCardHolder.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide name of card holder.")
            return
        }

        if (
            binding.edCardNo.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide card no.")
            return
        }

        if (
            binding.edCardNo.getText().toString().length != 16
        ) {
            showMessageInDialog("Please provide valid card no.")
            return
        }

        if (
            binding.edExpMonth.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide exp month.")
            return
        }

        if (
            binding.edExpYear.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide exp year.")
            return
        }

        if (
            binding.edCvv.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide cvv no.")
            return
        }


        if (
            binding.edCvv.getText().toString().length != 3
        ) {
            showMessageInDialog("Please provide valid cvv no.")
            return
        }
        generateStripeToken(
            cardNumber =  binding.edCardNo.getText().toString(),
            cardExpMonth = binding.edExpMonth.getText().toString().toInt(),
            cardExpYear = binding.edExpYear.getText().toString().toInt(),
            cardCVV = binding.edCvv.getText().toString()
        ) { token ->
            showProgress()
            if (!isEditable) {
                viewModel.addCard(
                    card_name = binding.edNameCardHolder.getText().toString(),
                    cvv = binding.edCvv.getText().toString(),
                    expiry_month = binding.edExpMonth.text.toString(),
                    expiry_year = binding.edExpYear.text.toString(),
                    number = binding.edCardNo.getText().toString()
                )
            } else {
                viewModel.editCard(
                    card_name = binding.edNameCardHolder.getText().toString(),
                    cvv = binding.edCvv.getText().toString(),
                    expiry_month = binding.edExpMonth.text.toString(),
                    expiry_year = binding.edExpYear.text.toString(),
                    number = binding.edCardNo.getText().toString(),
                    cardId = cardListResponse.card_id
                )

            }

        }
    }

}