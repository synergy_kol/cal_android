package com.itsc.cal.ui.paymentsettings

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityPaySettingsBinding
import com.itsc.cal.model.PayoutSettingResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.model.UserDetails
import com.itsc.cal.ui.add_driver.AddDriverActivity
import com.itsc.cal.ui.add_driver.AddDriverRegistrationActivity
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.login.LoginActivity
import com.itsc.cal.ui.post_a_truck.PostATruckActivity
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
class PayoutSettingsRegistrationActivity: CalSuperActivity(), KodeinAware {
    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPaySettingsBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var mPayoutSettingResponse:PayoutSettingResponse?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_pay_settings)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        setPageTitle("Payment Settings")
        initView()
        bindingViewModel()
    }

    override fun onResume() {
        super.onResume()
        showProgress()
        viewModel.getPayoutSettings()
    }

    private fun initView() {
        binding.apply {
            llBankAccount.setOnClickListener {
                val intent = Intent(this@PayoutSettingsRegistrationActivity, PayoutSetBankAccountActivity::class.java)
                intent.putExtra("payout_setting",mPayoutSettingResponse)
                startActivity(intent)
            }
            llBankCard.setOnClickListener {
                val intent = Intent(this@PayoutSettingsRegistrationActivity, PayoutSetCardActivity::class.java)
                intent.putExtra("payout_setting",mPayoutSettingResponse)
                startActivity(intent)
            }

            btnContinue.setOnClickListener {
                val userdetails = viewModel.giveRepository().getUserData()
                if(userdetails?.completed_steps=="4") {
                    when{
                        (userdetails.user_role=="4" &&
                                userdetails.user_type.equals("I",true))->{ startActivity(
                                UiUtil.clearStackAndStartNewActivity(Intent(this@PayoutSettingsRegistrationActivity, PostATruckActivity::class.java)))
                            finish()
                                }
                        (userdetails.user_role=="4" &&
                                userdetails.user_type.equals("F",true))->{ startActivity(
                            UiUtil.clearStackAndStartNewActivity(Intent(this@PayoutSettingsRegistrationActivity, AddDriverRegistrationActivity::class.java)))
                            finish()
                        }
                        (userdetails.user_role=="2" || userdetails.user_role=="3")->{
                            startActivity(
                                UiUtil.clearStackAndStartNewActivity(Intent(this@PayoutSettingsRegistrationActivity, Dashboard::class.java)))
                        }
                    }


                }
            }
            btnLogout.setOnClickListener {
                viewModel.giveRepository().clearAllData()
                startActivity(
                    UiUtil.clearStackAndStartNewActivity(
                        Intent(
                            this@PayoutSettingsRegistrationActivity,
                            LoginActivity::class.java
                        )
                    )
                )
                finish()
            }
        }
    }
    private fun bindingViewModel() {
        viewModel.mediatorLivePayoutSettings.observe(
            this,
            androidx.lifecycle.Observer<Event<PayoutSettingResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()){
                        hideProgress()
                        showData(it.getContent())
                    }
                }
            }
        )
        viewModel.mediatorLivePayoutSettingsError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun showData(payoutSettingResponse: PayoutSettingResponse?) {
        mPayoutSettingResponse = payoutSettingResponse
        binding.apply {
            if(mPayoutSettingResponse!=null) {
                llButtons.visibility = View.VISIBLE
                val userDetails = viewModel.giveRepository().getUserData()
                userDetails?.completed_steps = mPayoutSettingResponse?.completed_steps
                viewModel.giveRepository().saveUserDataToPref(userDetails!!)
            }
            if(payoutSettingResponse?.account!=null) {
                tvBankAccountNo.text =
                    "Bank Account - xxxxxxx${payoutSettingResponse?.account?.substring((payoutSettingResponse.account?.length ?: 0) - 4)}"
                tvBankAccountNo2.text =
                    "Bank Account - xxxxxxx${payoutSettingResponse?.account?.substring((payoutSettingResponse.account?.length ?: 0) - 4)}"
            }else{
                tvBankAccountNo.text =
                    "Bank Account - xxxxxxx"
                tvBankAccountNo2.text =
                    "Bank Account - xxxxxxx"
            }
            if(payoutSettingResponse?.card_no!=null) {
                tvBankCard.text =
                    "Card No -  xxxx xxxx xxxx ${payoutSettingResponse?.card_no?.substring((payoutSettingResponse.card_no?.length ?: 0) - 4)}"
            }else{
                tvBankCard.text =
                    "Card No -  xxxx xxxx xxxx"
            }
        }
    }
}