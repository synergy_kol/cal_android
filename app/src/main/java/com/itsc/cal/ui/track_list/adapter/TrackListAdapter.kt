package com.itsc.cal.ui.track_list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ChildTrackListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.NearByConductorListResponse
import com.itsc.cal.model.TruckListResponse

class TrackListAdapter(
    val mContext: Context,
    val onItemClickListener: OnItemClickListener,
    var arrayList: ArrayList<TruckListResponse>
) :
    RecyclerView.Adapter<TrackListAdapter.ViewHolder>(), Filterable {

    var suggestions: ArrayList<TruckListResponse> = ArrayList()
    var tempList: ArrayList<TruckListResponse> =
        arrayList.clone() as ArrayList<TruckListResponse>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ChildTrackListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))
    }

    inner class ViewHolder(val binding: ChildTrackListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(truckListResponse: TruckListResponse) {
            binding.textModelNo.text = truckListResponse.truck_model
            binding.textTruckNumber.text = truckListResponse.truck_vin_number
            binding.textTruckType.text = truckListResponse.truck_type_name
            var modelName = truckListResponse.truck_model!!.substring(0,1)
            binding.imgProfileImage.setImage(
                imageUrl = "${KeywordsAndConstants.BASE_URL}${truckListResponse.truckimage}",
                        needPlaceHolderImageForName = modelName
            )

            binding.btnButtonDetails.setOnClickListener {
                onItemClickListener.onItemClick(it, position = adapterPosition)
            }
        }

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                p0?.let {
                    suggestions.clear()
                    for (filterSuggestion in tempList) {

                        if (filterSuggestion.truck_reg_number!!.contains(it, ignoreCase = true))
                            suggestions.add(filterSuggestion)


                    }

                    val result = FilterResults()
                    result.values = suggestions
                    result.count = suggestions.size
                    return result
                } ?: kotlin.run {
                    return FilterResults()
                }
            }

            override fun publishResults(p0: CharSequence?, filterResults: FilterResults?) {
                arrayList = filterResults?.values as ArrayList<TruckListResponse>
                notifyDataSetChanged()

            }

            override fun convertResultToString(resultValue: Any?): CharSequence {
                resultValue?.let {
                    val filterSuggestion = (it as NearByConductorListResponse)
                    return filterSuggestion.company_name?.trim().toString()
                    // return if (filterSuggestion.type == FilterSuggestionType.HOTEL) filterSuggestion.hotel!!.name else filterSuggestion.location!!.name
                } ?: run {
                    return ""
                }
            }
        }
    }

}