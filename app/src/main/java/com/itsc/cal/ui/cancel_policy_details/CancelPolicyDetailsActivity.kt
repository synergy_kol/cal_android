package com.itsc.cal.ui.cancel_policy_details

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityCancelPolicyDetailsBinding
import com.itsc.cal.databinding.ActivitySubscriptionListBinding
import com.itsc.cal.databinding.ActivityTrackListBinding
import com.itsc.cal.databinding.VerifyDialogBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.*
import com.itsc.cal.ui.card_list.CardListActivity
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.post_a_truck.PostATruckActivity
import com.itsc.cal.ui.submit_review.SubmitReviewActivity
import com.itsc.cal.ui.subscription_details.SubsciptionDetailsActivity
import com.itsc.cal.ui.suscription.adapter.SubscriptionListAdapter
import com.itsc.cal.ui.track_list.adapter.TrackListAdapter
import com.itsc.cal.ui.truck_details.TruckDetailsActivity
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.generateStripeToken
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class CancelPolicyDetailsActivity : CalSuperActivity(withNav = true), KodeinAware {

    private lateinit var binding: ActivityCancelPolicyDetailsBinding
    override val kodein: Kodein by kodein()
    private lateinit var viewModel: ViewModel
    private lateinit var truckTypeResponse: TruckTypeResponse
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var jobListResponse: JobListResponse

    companion object {
        var truckListing: Activity? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_cancel_policy_details)
        enableBackButton()
        setPageTitle("Cancellation policy details")
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        truckListing = this

        initView()

        bindViewHolder()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initView() {

        intent.let {
            jobListResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            if (viewModel.giveRepository().getUserData()!!.user_role.equals("4")) {
                getCancelJobDetails(jobId = jobListResponse.job_id, userType = "2")
            } else {
                getCancelJobDetails(jobId = jobListResponse.job_id, userType = "1")
            }
        }

        binding.btnCancelNow.setOnClickListener {
            if (viewModel.giveRepository().getUserData()!!.user_role.equals("4")) {
                truckFleetCancel()
            } else {
                showProgress()
                viewModel.getDefaultCard()
            }

        }

    }

    private fun truckFleetCancel() {
        val dialogBinding = VerifyDialogBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        val customDialog = Dialog(this)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(dialogBinding.root)
        dialogBinding.editText.apply {
            setText("")
            gravityCenter()
            editable(true)
            typeFace(
                fontResEditText = R.font.poppins_regular
            )
            inputMode(
                EditTextInputMode.INPUT_TEXT
            )

        }
        dialogBinding.buttonActionOne.text = "CLOSE"

        dialogBinding.buttonActionTwo.text = "SUBMIT"

        customDialog.window
            ?.setLayout(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
            )

        customDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.setCancelable(true)
        customDialog.show()

        dialogBinding.buttonActionOne.setOnClickListener {
            customDialog.dismiss()
        }
        dialogBinding.buttonActionTwo.setOnClickListener {
            if (!dialogBinding.editText.getText().isEmpty()) {
                customDialog.dismiss()
                showProgress()
                viewModel.cancelJobs(
                    jobId = jobListResponse.job_id,
                    cancelReson = dialogBinding.editText.getText()
                        .toString(),
                    amount = binding.textPrice.text.toString().substring(1)
                )
            } else {
                showMessageInDialog("Please provide cancel reason.")
            }

        }
    }

    private fun supplierBuilderCancel(stripeToken: String) {
        val dialogBinding = VerifyDialogBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        val customDialog = Dialog(this)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(dialogBinding.root)
        dialogBinding.editText.apply {
            setText("")
            gravityCenter()
            editable(true)
            typeFace(
                fontResEditText = R.font.poppins_regular
            )
            inputMode(
                EditTextInputMode.INPUT_TEXT
            )

        }
        dialogBinding.buttonActionOne.text = "CLOSE"

        dialogBinding.buttonActionTwo.text = "SUBMIT"

        customDialog.window
            ?.setLayout(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
            )

        customDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.setCancelable(true)
        customDialog.show()

        dialogBinding.buttonActionOne.setOnClickListener {
            customDialog.dismiss()
        }
        dialogBinding.buttonActionTwo.setOnClickListener {
            if (!dialogBinding.editText.getText().isEmpty()) {
                customDialog.dismiss()
                showProgress()
                viewModel.cancelJobsBuilderSupplier(
                    jobId = jobListResponse.job_id,
                    cancelReson = dialogBinding.editText.getText()
                        .toString(),
                    amount = binding.textPrice.text.toString().substring(1),
                    stripeToken = stripeToken
                )
            } else {
                showMessageInDialog("Please provide cancel reason.")
            }

        }
    }

    private fun getCancelJobDetails(jobId: String, userType: String) {
        showProgress()
        viewModel.getCancelJobDetails(job_id = jobId, user_type = userType)
    }

    private fun moveToDashBoard() {
        startActivity(
            UiUtil.clearStackAndStartNewActivity(
                Intent(
                    this,
                    Dashboard::class.java
                )
            )
        )
        finish()
    }


    private fun bindViewHolder() {

        viewModel.mediatorLivejobCanceltationChargesResponse.observe(
            this,
            Observer<Event<JobCancelChargeResponce>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        jobCancelationDetails(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveJobCancelationChargesError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )

        viewModel.mediatorLiveCancelJobs.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseConfigur(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveCancelError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )


        viewModel.mediatorLiveCancelJobsBuilderSupplier.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseConfigur(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveCancelJobBuilderSupplierError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveDefalutCard.observe(
            this,
            Observer<Event<CardResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        getDefalutCardData(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDefalutCardError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                        defalutCardError("")
                    }

                }
            }
        )
    }

    private fun jobCancelationDetails(jobCancelChargeResponce: JobCancelChargeResponce) {
        binding.textPrice.text = "$${jobCancelChargeResponce.pay_amount}"
    }

    private fun getDefalutCardData(cardResponse: CardResponse) {
        val data = Base64.decode(cardResponse.cvv_number.toString(), Base64.DEFAULT)
        val text = String(data, charset("UTF-8"))
        generateStripeToken(
            cardNumber = cardResponse.number,
            cardExpMonth = cardResponse.expiry_month.toInt(),
            cardExpYear = cardResponse.expiry_year.toInt(),
            cardCVV = text
        ) { token ->
            Log.i("Token:", token)
            supplierBuilderCancel(token)
        }

    }

    private fun defalutCardError(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Please provide a default card",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(
                        Intent(
                            this@CancelPolicyDetailsActivity,
                            CardListActivity::class.java
                        )
                    )
                }
            }
        )

    }

    private fun processResponseConfigur(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    moveToDashBoard()

                }
            }
        )

    }


}