package  com.itsc.cal.ui.log_listing

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.LogListResponce
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveLogListingRead: MediatorLiveData<Event<ArrayList<LogListResponce>>> = MediatorLiveData()
    val mediatorLiveLogListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveLogListingRead.addSource(
            repository.mutableLiveDataLogListingList
        ) { t -> mediatorLiveLogListingRead.postValue(t) }
        mediatorLiveLogListingError.addSource(
            repository.mutableLiveDataLogListingtError
        ) { t -> mediatorLiveLogListingError.postValue(t) }

    }

    fun getLogDetails(){
        repository.getLogDetails()
    }
}
