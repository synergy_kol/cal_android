package com.itsc.cal.ui.sos_document.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildSosDocumentListBinding
import com.itsc.cal.interfaces.OnItemClickListener

class SOSDocumentAdapter (val mContext: Context,val arrayList:ArrayList<String>,val onItemClickListener: OnItemClickListener):
    RecyclerView.Adapter<SOSDocumentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ChildSosDocumentListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))
    }

    inner class ViewHolder(val binding: ChildSosDocumentListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(name:String) {
            binding.textDocumentName.setText(name)
            binding.btnButton.setOnClickListener {
                onItemClickListener.onItemClick(it,adapterPosition)
            }
        }

    }

}