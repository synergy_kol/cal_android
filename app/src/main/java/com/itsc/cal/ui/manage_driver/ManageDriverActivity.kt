package com.itsc.cal.ui.manage_driver

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityManageDriverBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.DriverListResponse
import com.itsc.cal.ui.add_driver.AddDriverActivity
import com.itsc.cal.ui.driver_details.DriverDetailsActivity
import com.itsc.cal.ui.manage_driver.adapter.ManageDriverAdapter
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ManageDriverActivity : CalSuperActivity(withNav = true), KodeinAware {

    private lateinit var binding: ActivityManageDriverBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    override val kodein: Kodein by kodein()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_manage_driver)
        enableBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        setPageTitle("Manage Driver")
        showHelpButton()
        initView()

        bindViewHolder()
    }

    override fun onResume() {
        super.onResume()
        getDriverList()
    }

    private fun initView() {
        binding.buttonAddDriver.setOnClickListener {
            startActivity(Intent(this, AddDriverActivity::class.java))
        }
    }

    private fun getDriverList() {
        showProgress()
        viewModel.getDriverList()
    }

    private fun bindViewHolder() {


        viewModel.mediatorLiveDriverList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<DriverListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent() ?: ArrayList())
                    }
                }
            }
        )

        viewModel.mediatorLiveDriverError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

    }

    private fun setAdapter(arrayList: ArrayList<DriverListResponse>) {
        binding.recycleViewAddDriver.apply {
            layoutManager = LinearLayoutManager(this@ManageDriverActivity)
            adapter = ManageDriverAdapter(this@ManageDriverActivity, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    startActivity(
                        Intent(
                            this@ManageDriverActivity,
                            DriverDetailsActivity::class.java
                        ).putExtra(KeywordsAndConstants.DATA, toJson(arrayList.get(position)))
                    )
                }

            }, arrayList)
        }
    }
}