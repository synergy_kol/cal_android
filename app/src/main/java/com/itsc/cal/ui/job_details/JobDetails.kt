package com.itsc.cal.ui.job_details

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityJobDetailsBinding
import com.itsc.cal.databinding.VerifyDialogBinding
import com.itsc.cal.model.JobListResponse
import com.itsc.cal.model.PayoutResponse
import com.itsc.cal.model.ProfileDetails
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.arrange_trip.ArrangeTripActivity
import com.itsc.cal.ui.builder_truck_details.BuilderTruckDetailsActivity
import com.itsc.cal.ui.cancel_policy_details.CancelPolicyDetailsActivity
import com.itsc.cal.ui.job_details.adapter.EquipementDetailsAdapter
import com.itsc.cal.ui.track_status.TrackStatusActivity
import com.itsc.cal.ui.webview.WebView
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class JobDetails : CalSuperActivity(withNav = false), KodeinAware {
    override val kodein: Kodein by kodein()
    private val repository: Repository by instance()
    private lateinit var binding: ActivityJobDetailsBinding
    private lateinit var jobResponse: JobListResponse
    private var isFromBooking = false
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var invoiceDownloadLink: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_job_details)
        setPageTitle("Job Details")
        showHelpButton()
        enableBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        initView()
        bindToViewModel()
    }

    override fun onResume() {
        super.onResume()
        showProgress()
        viewModel.getProfileDetails(viewModel.giveRepository().getUserData()!!.user_master_id)
    }

    private fun bindToViewModel() {
        viewModel.mediatorLivePayoutData.observe(
            this,
            Observer<Event<PayoutResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processPayoutData(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLivePayoutDataError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveApplyJobs.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseNew(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveApplyJobsError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.getContent()?.getMessageToShow()?.let { it1 ->
                            showMessageInDialog(
                                it1
                            )
                        }

                    }

                }
            }
        )



        viewModel.mediatorLiveHideJobs.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseNew(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveHideJobsError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveShowJobs.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseHide(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveShowJobsError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveDataProfileDetails.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        getProfileData(it.getContent()!!)
                    }
                }
            }
        )
        viewModel.mediatorLiveDataProfileDetailsError.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )

    }


    private fun getProfileData(profileDetails: ProfileDetails) {
        var userDetails = viewModel.giveRepository().getUserData()
        userDetails!!.stripe_account_id = profileDetails.stripe_account_id
        viewModel.giveRepository().saveUserDataToPref(userDetails)
        Log.i("Update-----------", "userdata")
    }


    private fun processResponseHide(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    finish()

                }
            }
        )

    }

    private fun processResponseNew(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    finish()
                }
            }
        )

    }

    private fun processPayoutData(payoutResponse: PayoutResponse) {
        binding.tvPayableAmount.text = payoutResponse.payment_amount
        binding.tvCalCommision.text = payoutResponse.cal_commission
        binding.tvEstimatedPrice.text = payoutResponse.JobEstimatePrice
        invoiceDownloadLink = "${KeywordsAndConstants.BASE_URL}${payoutResponse.job_invoice}"
        binding.btnDownloadInvoice.setOnClickListener {
            downloadFile(invoiceDownloadLink)
        }
    }

    private fun initView() {
        intent.let {
            jobResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            isFromBooking = intent.getBooleanExtra(KeywordsAndConstants.isFromBooking, false)
            if (isFromBooking) {
                binding.llBookingView.visibility = View.VISIBLE
                getPayoutDetailsAPIcalled(jobResponse.job_id)
            } else {
                binding.llBookingView.visibility = View.GONE
            }

            binding.tvPickUp.text = jobResponse.Source
            binding.tvDestination.text = jobResponse.Destination
            binding.tvJobId.text = "#CAL${jobResponse.job_id}"
            binding.tvMaterialType.text =
                "${jobResponse.material_name} ${jobResponse.material_option_name ?: ""}"
            binding.tvWeight.text = jobResponse.Weight
            binding.tvTruckType.text = jobResponse.truck_name
            binding.tvFrieghtCharges.text = "$ ${jobResponse.FreightCharges}"
            if (jobResponse.Equipment_value.equals("")) {
                binding.tvEquipment.text = "NO"
            } else {
                binding.tvEquipment.text = "Yes (${jobResponse.Equipment_value})"
            }
            binding.tvPickupDate.text = jobResponse.pickup_date
            binding.tvPickupTime.text = jobResponse.pickup_time
            binding.tvDeliveryDate.text = jobResponse.delivery_date
            binding.tvDeliveryTime.text = jobResponse.delivery_time
            binding.tvNoOfTrucks.text = jobResponse.NoOfTrucks
            binding.tvJobEstimatePrice.text = "$ ${jobResponse.JobEstimatePrice}"
            if (repository.getUserData()?.user_role.equals("2")) {
                if (jobResponse.truck_details?.size ?: 0 > 0) {
                    binding.btnTruckDetails.visibility = View.VISIBLE
                } else {
                    binding.btnTruckDetails.visibility = View.GONE
                }
                if (jobResponse.builder_otp ?: "" != "") {
                    binding.edJobCode.visibility = View.VISIBLE
                    binding.tvJobCode.visibility = View.VISIBLE
                    binding.edJobCode.text = jobResponse.builder_otp
                } else {
                    binding.edJobCode.visibility = View.GONE
                    binding.tvJobCode.visibility = View.GONE
                }

            } else if (repository.getUserData()?.user_role.equals("3")) {
                if (jobResponse.supplier_otp ?: "" != "") {
                    binding.edJobCode.visibility = View.VISIBLE
                    binding.tvJobCode.visibility = View.VISIBLE
                    binding.edJobCode.text = jobResponse.supplier_otp
                } else {
                    binding.edJobCode.visibility = View.GONE
                    binding.tvJobCode.visibility = View.GONE
                }

            } else {
                binding.edJobCode.visibility = View.GONE
                binding.tvJobCode.visibility = View.GONE
                binding.btnTruckDetails.visibility = View.GONE
            }

            if (jobResponse.job_type.equals("H")) {
                binding.llHaulOffJob.visibility = View.VISIBLE
                binding.edSpecSheet.text = jobResponse.spec_sheet
                binding.edLaiabilityInformation.text = jobResponse.lib_information
                binding.edMaterialsContent.text = jobResponse.material_cont
                binding.edMaterialsAvg.text = jobResponse.materials_available
                binding.tvEquipment.visibility = View.GONE
                binding.tvEquipmentText.visibility = View.GONE
                if (jobResponse.equipment_list?.size ?: 0 > 0) {
                    binding.recycleviewEquipementDetails.apply {
                        layoutManager = LinearLayoutManager(this@JobDetails)
                        adapter = EquipementDetailsAdapter(
                            this@JobDetails,
                            jobResponse.equipment_list ?: ArrayList()
                        )
                    }
                    binding.tvEqText.visibility = View.VISIBLE
                } else {
                    binding.tvEqText.visibility = View.GONE
                }
                binding.tvWeightText.visibility = View.GONE
                binding.tvWeight.visibility = View.GONE
                binding.tvFrieghtChargesText.visibility = View.GONE
                binding.tvFrieghtCharges.visibility = View.GONE
                //binding.btnTruckDetails.visibility = View.GONE
                /*  if (jobResponse.truck_details?.size?:0 > 0) {
                      binding.btnTruckDetails.visibility = View.VISIBLE
                  } else {
                      binding.btnTruckDetails.visibility = View.GONE
                  }*/

                if (jobResponse.job_status.equals("5")) {
                    binding.btnTracking.visibility = View.VISIBLE

                } else {
                    binding.btnTracking.visibility = View.GONE
                }
            } else {
                binding.llHaulOffJob.visibility = View.GONE
                binding.tvWeightText.visibility = View.VISIBLE
                binding.tvWeight.visibility = View.VISIBLE
                binding.tvFrieghtChargesText.visibility = View.VISIBLE
                binding.tvFrieghtCharges.visibility = View.VISIBLE
                binding.tvEquipment.visibility = View.VISIBLE
                binding.tvEquipmentText.visibility = View.VISIBLE

            }
        }
        binding.btnDownloadInvoice.setOnClickListener {
            downloadFile(invoiceDownloadLink)
        }
        binding.btnTruckDetails.setOnClickListener {
            val intent = Intent(this, BuilderTruckDetailsActivity::class.java)
            intent.putExtra(
                KeywordsAndConstants.DATA,
                toJson(jobResponse)
            )
            startActivity(intent)
        }

        binding.btnTracking.setOnClickListener {
            val intent = Intent(this, TrackStatusActivity::class.java)
            intent.putExtra(KeywordsAndConstants.isJobListing, true)
            intent.putExtra(
                KeywordsAndConstants.DATA,
                toJson(jobResponse)
            )
            startActivity(intent)
        }
        // add joblisting additional button
        if (repository.getUserData()!!.user_role.equals("4")) {
            var filterType = "1"
            intent.let {
                filterType =
                    intent.extras?.getString(KeywordsAndConstants.FILTER_TYPE, "1").toString()
            }
            if (filterType.equals("1")) {
                binding.llApplyCancel.visibility = View.VISIBLE
                binding.btnButtonUnhide.visibility = View.GONE
                binding.llConfigure.visibility = View.GONE
            } else if (filterType.equals("2")) {
                binding.llApplyCancel.visibility = View.GONE
                binding.btnButtonUnhide.visibility = View.GONE
                if (jobResponse.status.equals("2")) {
                    binding.llConfigure.visibility = View.VISIBLE
                } else {
                    binding.llConfigure.visibility = View.GONE
                }

            } else if (filterType.equals("3")) {
                binding.llApplyCancel.visibility = View.GONE
                binding.btnButtonUnhide.visibility = View.VISIBLE
                binding.llConfigure.visibility = View.GONE
            }
            binding.btnButtonApply.setOnClickListener {

                if (viewModel.giveRepository()
                        .getUserData()?.stripe_account_id.equals("")
                ) {
                    startActivity(
                        Intent(
                            this,
                            WebView::class.java
                        ).putExtra(
                            WebView.URL,
                            "${KeywordsAndConstants.BASE_URL}fleet/mobile_connect/${viewModel.giveRepository()
                                .getUserData()!!.user_master_id}"
                        ).putExtra(
                            WebView.TITLE,
                            "Stripe Connect"
                        )
                    )

                } else {
                    showProgress()
                    viewModel.applyJobs(jobResponse.job_id)
                }
            }
            binding.btnButtonCancel.setOnClickListener {

                val intent = Intent(this, CancelPolicyDetailsActivity::class.java)
                intent.putExtra(
                    KeywordsAndConstants.DATA,
                    toJson(jobResponse)
                )
                startActivity(intent)
            }
            binding.btnButtonConfigure.setOnClickListener {
                startActivity(
                    Intent(
                        this,
                        ArrangeTripActivity::class.java
                    ).putExtra(
                        KeywordsAndConstants.DATA,
                        jobResponse.job_id
                    )
                )
            }
            binding.btnButtonHide.setOnClickListener {
                showProgress()
                viewModel.hideJobs(jobResponse.job_id)
            }
            binding.btnButtonUnhide.setOnClickListener {
                showProgress()
                viewModel.showJobs(jobResponse.job_id)

            }
        } else if (repository.getUserData()!!.user_role.equals("2") || repository.getUserData()!!.user_role.equals(
                "3"
            )
        ) {
            if (jobResponse.job_status.equals("3")) {
                binding.btnTracking.visibility = View.GONE
                binding.btnReview.visibility = View.GONE
            } else {
                if (jobResponse.job_status.equals("5")) {
                    binding.btnTracking.visibility = View.VISIBLE

                } else {
                    binding.btnTracking.visibility = View.GONE
                }
                binding.btnReview.visibility = View.GONE

            }

            binding.btnApply.visibility = View.GONE
            if (jobResponse.self_job.equals("1")) {
                if (jobResponse.job_status.equals("1") || jobResponse.job_status.equals("5")) {
                    binding.btnCancel.visibility = View.VISIBLE
                } else {
                    binding.btnCancel.visibility = View.GONE
                }
            } else {
                binding.btnCancel.visibility = View.GONE
            }

            binding.btnCancel.setOnClickListener {
                val intent = Intent(this, CancelPolicyDetailsActivity::class.java)
                intent.putExtra(
                    KeywordsAndConstants.DATA,
                    toJson(jobResponse)
                )
                startActivity(intent)
            }

            binding.btnTracking.setOnClickListener {
                val intent = Intent(this, TrackStatusActivity::class.java)
                intent.putExtra(KeywordsAndConstants.isJobListing, true)
                intent.putExtra(
                    KeywordsAndConstants.DATA,
                    toJson(jobResponse)
                )
                startActivity(intent)
            }

        }


    }

    private fun getPayoutDetailsAPIcalled(jobId: String) {
        showProgress()
        viewModel.getPayoutDetails(jobId)
    }

    private fun downloadFile(url: String) {
        if (!url.isEmpty()) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        }

    }
}