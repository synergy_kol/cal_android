package com.itsc.cal.ui.post_a_haul_off.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildEquipmentListBinding
import com.itsc.cal.interfaces.OnEquipmentItemClickListener
import com.itsc.cal.model.*
import com.itsc.cal.widget.spinner.Spinner
import com.itsc.cal.widget.spinner.SpinnerData

class EquipmentAdapter(
    val mContext: Context,
    val onEquipmentItemClickListener: OnEquipmentItemClickListener,
    val arrayList: ArrayList<EquipmentListResponse>
) :
    RecyclerView.Adapter<EquipmentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ChildEquipmentListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))

    }

    inner class ViewHolder(val binding: ChildEquipmentListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(equipementResponse: EquipmentListResponse) {
            var equipmentOptionResponse: EquipmentOptionResponse = EquipmentOptionResponse("","","","")

            binding.ckEquipement.setText(equipementResponse.name)
            val items: ArrayList<SpinnerData<EquipmentOptionResponse>> = ArrayList()
            (equipementResponse?.equipment_option.forEach { source ->
                items.add(
                    SpinnerData(
                        source.option_title.toString(),
                        source
                    )
                )
            })
            if (items.size > 0)
                equipmentOptionResponse = items[0].data
            binding.spOption.setBg()
            binding.spOption.addItems(
                items,
                withDot = false,
                listener = object : Spinner.OnItemSelectedListener {
                    override fun <T> selected(item: SpinnerData<T>) {
                        equipmentOptionResponse = item.data as EquipmentOptionResponse
                        onEquipmentItemClickListener.onAddItemClick(

                            equipmentOptionResponse.equipment_option_id, equipementResponse.equipment_id,adapterPosition
                        )
                    }
                }
            )

            binding.ckEquipement.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    binding.spOption.visibility = View.VISIBLE
                    onEquipmentItemClickListener.onAddItemClick(

                        equipmentOptionResponse.equipment_option_id, equipementResponse.equipment_id,adapterPosition
                    )
                } else {
                    binding.spOption.visibility = View.GONE
                    onEquipmentItemClickListener.onRemovedItemClick(

                        equipmentOptionResponse.equipment_option_id,  equipementResponse.equipment_id,adapterPosition
                    )
                }
            }

        }

    }

}