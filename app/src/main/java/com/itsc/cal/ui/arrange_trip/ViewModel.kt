package  com.itsc.cal.ui.arrange_trip

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveTruckTypeList: MediatorLiveData<Event<ArrayList<TruckListResponse>>> =
        MediatorLiveData()
    val mediatorLiveTruckTypeListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveDriverList: MediatorLiveData<Event<ArrayList<DriverListResponse>>> =
        MediatorLiveData()
    val mediatorLiveDriverError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveJobConfigation: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveJobConfigationError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveTruckTypeList.addSource(
            repository.mutableLiveDataTruckList
        ) { t -> mediatorLiveTruckTypeList.postValue(t) }
        mediatorLiveTruckTypeListError.addSource(
            repository.mutableLiveDataTruckListError
        ) { t -> mediatorLiveTruckTypeListError.postValue(t) }

        mediatorLiveDriverList.addSource(
            repository.mutableLiveDataDriverList
        ) { t -> mediatorLiveDriverList.postValue(t) }
        mediatorLiveDriverError.addSource(
            repository.mutableLiveDataDriverListError
        ) { t -> mediatorLiveDriverError.postValue(t) }

        mediatorLiveJobConfigation.addSource(
            repository.mutableLiveDataJobConfigration
        ) { t -> mediatorLiveJobConfigation.postValue(t) }
        mediatorLiveJobConfigationError.addSource(
            repository.mutableLiveDataJobConfigrationError
        ) { t -> mediatorLiveJobConfigationError.postValue(t) }
    }

    fun getTruckType() = repository.getTruckList()
    fun getDriverList() = repository.getDriverTypeList()
    fun jobConfigaration(jobId: String, trackList: ArrayList<String>, driverId: ArrayList<String>) =
        repository.jobConfiguration(job_id = jobId, driverId = driverId, truckId = trackList)

}