@file:Suppress("MemberVisibilityCanBePrivate")

package com.itsc.cal.ui.on_boarding.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.itsc.cal.R
import com.itsc.cal.ui.on_boarding.frag.OnBoardingFragment

class OnBoardingFragsAdapter(
    fragmentManager: FragmentManager
) :
    FragmentPagerAdapter(
        fragmentManager,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {

    val one = OnBoardingFragment()
    val two = OnBoardingFragment()
    val three = OnBoardingFragment()

    init {
        one.imageId = R.drawable.on_boarding_one
        one.message = "Search for a Job"
        one.subMessage = "Connect a load connecting you"
        two.imageId = R.drawable.on_boarding_two
        two.message = "Short / Haul Off"
        two.subMessage =
            "Maximizing the days and payoff"
        three.imageId = R.drawable.on_boarding_three
        three.message = "Get paid right away"
        three.subMessage =
            "in 24Hrs or every week"
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> one
            1 -> two
            2 -> three
            else -> one
        }
    }

    override fun getCount(): Int {
        return 3
    }
}