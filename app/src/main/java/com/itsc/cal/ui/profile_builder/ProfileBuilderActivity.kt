package com.itsc.cal.ui.profile_builder

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.fxn.pix.Pix
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.core.KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
import com.itsc.cal.databinding.ActivityProfileBuilderBinding
import com.itsc.cal.interfaces.CallSuperActivityContract
import com.itsc.cal.model.BuilderListResponse
import com.itsc.cal.model.ProfileDetails
import com.itsc.cal.model.ProfileResponse
import com.itsc.cal.ui.edit_profile.EditProfile
import com.itsc.cal.ui.profile.ViewModel
import com.itsc.cal.ui.profile.ViewModelProvider
import com.itsc.cal.util.ImagePickerUtil
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.util.*

class ProfileBuilderActivity : CalSuperActivity(withNav = false), CallSuperActivityContract,
    KodeinAware {

    override val kodein: Kodein by kodein()

    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var binding: ActivityProfileBuilderBinding
    private var isOwnProfie = false
    private var userImage: String? = null
    private lateinit var builderListResponse: BuilderListResponse
    private lateinit var profileDetails: ProfileResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_profile_builder)
        binding.context = this
        enableBackButton()
        isOwnProfie = intent.extras?.getBoolean(KeywordsAndConstants.IS_OWN_PROFILE, false) ?: false
        if (isOwnProfie) {
            showAProfileEditActionbar()
        } else {
            showAProfileActionbar()
        }

        viewModel =
            androidx.lifecycle.ViewModelProvider(this, viewModelProvider).get(ViewModel::class.java)

        bindToViewModel()

        initView()

        isEditable(false)
    }

    override fun onResume() {
        super.onResume()

        if (isOwnProfie) {
            showProgress()
            viewModel.getProfileDetails(viewModel.giveRepository().getUserData()!!.user_master_id)
        }
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataProfileDetails.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        profileDetails = it.getContent()!!
                        initView(profileDetails.profile)
                    }
                }
            }
        )
        viewModel.mediatorLiveDataProfileDetailsError.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )

        viewModel.mediatorLiveDataSaveProfileDetails.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()

                        showMessageInDialog("Profile updated")
                        showAProfileEditActionbar()
                        isEditable(false)
                    }
                }
            }
        )

        viewModel.mediatorLiveDataSaveProfileDetailsError.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )
    }

    override fun saveProfile() {
        if (!isAllValid()) {
            return
        }

        showProgress()

        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        viewModel.updateProfile(
            binding.edName.text.toString().split(" ")[0],
            if (binding.edName.text.toString().split(" ").size > 1) binding.edName.text.toString()
                .split(" ")[1] else "",
            binding.editMobile.text.toString(),
            binding.editAddress.text.toString(),
            "",
            "",
            "",
            "",
            "",
            "",
            viewModel.giveRepository().getUserData()!!.user_master_id,
            binding.editCompanyName.text.toString(),
            binding.editCompanyReg.text.toString(),
            if (userImage != "") File(userImage) else null
        )
    }

    private fun isAllValid(): Boolean {
        if (
            binding.edName.text.toString().isEmpty()
        ) {
            showMessageInDialog("Please provide name")
            return false
        }
        if (
            binding.editAddress.text.toString().isEmpty()
        ) {
            showMessageInDialog("Please provide address")
            return false
        }
        if (
            binding.editCompanyName.text.toString().isEmpty()
        ) {
            showMessageInDialog("Please provide company name")
            return false
        }
        if (
            binding.editCompanyReg.text.toString().isEmpty()
        ) {
            showMessageInDialog("Please provide company registration")
            return false
        }
        if (
            binding.editMobile.text.toString().isEmpty()
        ) {
            showMessageInDialog("Please provide mobile number")
            return false
        }
        if (
            binding.editEmailAddress.text.toString().isEmpty()
        ) {
            showMessageInDialog("Please provide email")
            return false
        }
        return true
    }

    override fun editProfile() {
        /*showAProfileSaveActionbar()
        isEditable(true)*/

        startActivity(
            Intent(
                this,
                EditProfile::class.java
            )
                .putExtra(
                    KeywordsAndConstants.DATA,
                    toJson(profileDetails)
                )
        )
    }

    private fun isEditable(isEdit: Boolean) {
        if (isEdit) {
            binding.edName.isEnabled = true
            binding.editAddress.isEnabled = true
            binding.editCompanyName.isEnabled = true
            binding.editCompanyReg.isEnabled = true
            binding.editEmailAddress.isEnabled = true
            binding.editMobile.isEnabled = true
            binding.ivCamera.visibility = View.VISIBLE
        } else {
            binding.edName.isEnabled = false
            binding.editAddress.isEnabled = false
            binding.editCompanyName.isEnabled = false
            binding.editCompanyReg.isEnabled = false
            binding.editEmailAddress.isEnabled = false
            binding.editMobile.isEnabled = false
            binding.ivCamera.visibility = View.GONE
        }
    }

    private fun initView(profileDetails: ProfileDetails? = null) {

        if (!isOwnProfie) {
            intent?.let {
                builderListResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))

                var nameFirstWord = builderListResponse.fname.substring(0, 1)
                binding.edName.setText(builderListResponse.fname + " " + builderListResponse.lanme)
                binding.editMobile.setText(builderListResponse.mobile)
                binding.editEmailAddress.setText(builderListResponse.email)
                binding.editCompanyName.setText(builderListResponse.company_name)
                binding.editCompanyReg.setText(builderListResponse.company_registration)
                binding.editAddress.setText(builderListResponse.address)
                binding.imgProfile.setImage(
                    imageUrl = "${KeywordsAndConstants.BASE_URL}${builderListResponse.builder_image}",
                    isCircularImage = true,
                    needPlaceHolderImageForName = nameFirstWord
                )
                binding.editEmailAddress.setOnClickListener {
                    sendEmail(builderListResponse.email, "CAl", "")
                }
                binding.editMobile.setOnClickListener {
                    dialANumber(builderListResponse.mobile)
                }
                binding.editAddress.setOnClickListener {
                    moveToGoogleMap(builderListResponse.lat, builderListResponse.lon, "Map")
                }

            }!!

        }

        profileDetails?.let { profileData ->

            binding.imgProfile.setImage(
                imageUrl = profileData.profile_image,
                isCircularImage = true,
                needPlaceHolderImageForName = profileData.full_name.split("")[0].toUpperCase(
                    Locale.ROOT
                )
            )
            if (profileData.profile_image!=null){
                var userDetails = viewModel.giveRepository().getUserData()
                userDetails!!.profile_image = profileData.profile_image
                viewModel.giveRepository().saveUserDataToPref(userDetails)
            }
            binding.edName.setText(profileData.full_name)
            binding.editAddress.setText(profileData.address)
            binding.editCompanyName.setText(profileData.company_name)
            binding.editCompanyReg.setText(profileData.company_registration)
            binding.editMobile.setText(profileData.mobile)
            binding.editEmailAddress.setText(profileData.email)
        }

    }

    private fun dialANumber(phone: String?) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phone")
        startActivity(intent)

    }

    private fun moveToGoogleMap(lat: String, lon: String, mTitle: String) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("https://www.google.com/maps/search/?api=1&query=${lat},${lon}")
        )
        startActivity(intent)
    }

    private fun sendEmail(recipient: String, subject: String, message: String) {
        /*ACTION_SEND action to launch an email client installed on your Android device.*/
        val mIntent = Intent(Intent.ACTION_SEND)
        /*To send an email you need to specify mailto: as URI using setData() method
        and data type will be to text/plain using setType() method*/
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        // put recipient email in intent
        /* recipient is put as array because you may wanna send email to multiple emails
           so enter comma(,) separated emails, it will be stored in array*/
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        //put the Subject in the intent
        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //put the message in the intent
        mIntent.putExtra(Intent.EXTRA_TEXT, message)


        try {
            //start email intent
            startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        } catch (e: Exception) {
            //if any thing goes wrong for example no email client application or any exception
            //get and show exception message
        }

    }

    @SuppressLint("CheckResult")
    fun takePicture() {
        RxPermissions(this)
            .request(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .subscribe {
                if (!it) {
                    showMessageInDialog("Please accept permissions to take picture.")
                }

                ImagePickerUtil.pickImage(
                    context = this,
                    reqCode = REQUEST_CODE_PICK_IMAGE
                )
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)
        if (requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            intentData?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    userImage = images[0]

                    showUserImage()
                }
            }
        }
    }

    private fun showUserImage() {
        if (userImage == null)
            return

        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        binding.imgProfile.setImage(File(userImage), true)
    }
}