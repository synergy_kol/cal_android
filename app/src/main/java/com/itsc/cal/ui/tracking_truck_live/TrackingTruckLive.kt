package com.itsc.cal.ui.tracking_truck_live

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.*
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.core.content.edit
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.Projection
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityTrackingTruckLiveBinding
import com.itsc.cal.databinding.CustomMarkerDropBinding
import com.itsc.cal.databinding.CustomMarkerPickupBinding
import com.itsc.cal.interfaces.AsyncInterface
import com.itsc.cal.model.StatusReply
import com.itsc.cal.model.TruckLocation
import com.itsc.cal.model.TruckStatusResponse
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.async.CreateRoute
import com.itsc.cal.ui.truck_booking.TruckBooking
import com.itsc.cal.ui.truck_details.TruckDetailsActivity
import com.itsc.cal.util.WorkaroundMapFragment
import com.itsc.cal.util.repository.Event
import com.sagar.android.logutilmaster.LogUtil
import com.itsc.cal.util.model.Result
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.ArrayList


class TrackingTruckLive : CalSuperActivity(), KodeinAware, AsyncInterface {

    override val kodein: Kodein by kodein()

    companion object {
        const val Job_ID = "jobId"
    }

    enum class SearchPlaceFor {
        FROM,
        TO
    }

    enum class MarkerFor {
        PICK_UP,
        DROP
    }
    private lateinit var viewModel: ViewModel
    private val logUtil: LogUtil by instance()
    private val repository: Repository by instance()
    private lateinit var searchPlaceFor: TruckBooking.SearchPlaceFor
    private lateinit var binding: ActivityTrackingTruckLiveBinding
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var map: GoogleMap
    private lateinit var truckStatusResponse: TruckStatusResponse
    private var sourceLatLong: LatLng = LatLng(0.0, 0.0)
    private var destinationLatLong: LatLng = LatLng(0.0, 0.0)
    private var arrayList: ArrayList<Polyline> = ArrayList()
    private lateinit var polyline: Polyline
    private var isPolyLineVisible = false
    private var jobId = ""
    private lateinit var currentLocation: TruckBooking.LocationForFirebase
    private lateinit var sourceLatLng: LatLng
    private lateinit var destinationLatLng: LatLng
    private var truckMarker: Marker? = null
    private val viewModelProvider: ViewModelProvider by instance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_tracking_truck_live)
        binding.context = this
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        showActionbarWithBackButtonAndCall()

        enableBackButton()

        setPageTitle("Tracking Trucks Live")
        bindViewModel()
        getDataFromIntent()
        initView()
    }
    private fun initView(){
        binding.appcompatImageViewTruckNumberImage.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    TruckDetailsActivity::class.java
                ).putExtra(KeywordsAndConstants.DATA, truckStatusResponse.truck_id)
            )
        }
        binding.textViewTruckNumber.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    TruckDetailsActivity::class.java
                ).putExtra(KeywordsAndConstants.DATA, truckStatusResponse.truck_id)
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(runnable)
    }

    private fun bindViewModel() {

        viewModel.mediatorLiveDriverLocation.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<TruckLocation>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val mArrayList = it.getContent()
                        if(mArrayList?.size?:0>0) {
                            showTruckLocation(mArrayList?.get(0))
                        }else{
                            map.animateCamera(
                                CameraUpdateFactory.newLatLngBounds(
                                    LatLngBounds.builder()
                                        .include(sourceLatLng)
                                        .include(destinationLatLng)
                                        .build(),
                                    120
                                )
                            )
                        }

                    }
                }
            }
        )

        viewModel.mediatorLiveDriverLocationError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        //  handleGenericResult(it.getContent()!!)
                        it.readContent()
                        //binding.textNoDataFound.visibility = View.VISIBLE
                    }

                }
            }
        )
    }

    private fun showTruckLocation(mTruckLocation: TruckLocation?) {
            addCurrentMarker(mTruckLocation)
    }

    @SuppressLint("CheckResult", "MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        RxPermissions(this)
            .request(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .subscribe {
                if (!it) {
                    finish()
                }

                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener { locationResult ->
                        locationResult?.let { _ ->

                        }
                    }
            }
    }

    @SuppressLint("MissingPermission")
    private fun startMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync { googleMap ->
            googleMap?.let {
                map = it

                map.setOnMapLoadedCallback {
                    map.setPadding(0, 500, 0, 0)

                    /*sourceLatLng = LatLng(
                        truckStatusResponse.source_lat!!.toDouble(),
                        truckStatusResponse.source_long!!.toDouble()
                    )
                    destinationLatLng = LatLng(
                        truckStatusResponse.destination_lat!!.toDouble(),
                        truckStatusResponse.destination_long!!.toDouble()
                    )*/
                    drawMarker(sourceLatLng,0)
                    drawMarker(destinationLatLng,1)
                   /* map.addMarker(
                        MarkerOptions()
                            .title("source")
                            .position(
                                sourceLatLng
                            )
                            .icon(
                                BitmapDescriptorFactory.fromBitmap(
                                    getMarkerBitmapFromView(MarkerFor.PICK_UP)
                                )
                            )
                    )

                    map.addMarker(
                        MarkerOptions()
                            .title("destination")
                            .position(
                                destinationLatLng
                            )
                            .icon(
                                BitmapDescriptorFactory.fromBitmap(
                                    getMarkerBitmapFromView(MarkerFor.DROP)
                                )
                            )
                    )
*/
                    /*map.animateCamera(
                        CameraUpdateFactory.newLatLngBounds(
                            LatLngBounds.builder()
                                .include(sourceLatLng)
                                .include(destinationLatLng)
                                .build(),
                            120
                        )
                    )*/
                    val mSupportMapFragment: WorkaroundMapFragment =
                        mapFragment as WorkaroundMapFragment
                    mSupportMapFragment.setListener(object :
                        WorkaroundMapFragment.OnTouchListener {
                        override fun onTouch() {
                            getContainer().requestDisallowInterceptTouchEvent(true);
                        }
                    })

                    CreateRoute(
                        this,
                        sourceLatLng,
                        destinationLatLng,
                        Color.BLUE
                    ).execute()

                    //getTruckLiveLocationFromFirebase()
                }
            }
        }
    }

    fun startPlacesSearch(searchPlaceFor: TruckBooking.SearchPlaceFor) {
        this.searchPlaceFor = searchPlaceFor

        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        val fields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)

        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .build(this)
        startActivityForResult(intent, KeywordsAndConstants.AUTOCOMPLETE_PLACE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == KeywordsAndConstants.AUTOCOMPLETE_PLACE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)

                        logUtil.logV(
                            """
                                place selected:
                                ${place.name}
                                ${place.address}
                            """.trimIndent()
                        )

                        when (searchPlaceFor) {
                            TruckBooking.SearchPlaceFor.FROM -> {
                                binding.textViewFromText.text = place.name
                            }
                            TruckBooking.SearchPlaceFor.TO -> {
                                binding.textViewToText.text = place.name
                            }
                        }
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun viewNearestRoute(polylineOptionsList: java.util.ArrayList<PolylineOptions>) {
        try {
            if (isPolyLineVisible) {
                map.clear()
            }

            runOnUiThread {
                for (i in 0 until polylineOptionsList.size) {
                    map.addPolyline(polylineOptionsList[i])
                    polyline = map.addPolyline(polylineOptionsList[i])
                    isPolyLineVisible = true
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun viewNearestRouteErrorCallBack() {
        showMessageInDialog("Try again later")
    }

    private fun drawMarker(
        point: LatLng,
        position: Int
    ) {
        val markerOptions = MarkerOptions()
        markerOptions.position(point)
        if (position == 0) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.location))
            markerOptions.title("Source")
        } else if (position == 1) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.location))
            markerOptions.title("Destination")
            //  markerOptions.snippet(address)
        }

        //markerOptions.snippet(myTeamModel.getMobile() + "~" + MARKER_TEAM_TAG);
        map.addMarker(markerOptions)

    }

    private fun getDataFromIntent() {
        try {
            intent?.let {
                truckStatusResponse = fromJson(it.getStringExtra(KeywordsAndConstants.DATA)!!)
                jobId = it.getStringExtra(TruckBooking.Job_ID)!!
                //getLastLocation()

                binding.appcompatImageViewUserImage.setImage(
                    imageUrl = "${KeywordsAndConstants.BASE_URL}${truckStatusResponse.truck_image}",
                    isCircularImage = true,
                    needPlaceHolderImageForName = truckStatusResponse.name!!.split("")[0].toUpperCase(
                        Locale.ROOT
                    )
                )

                binding.textViewTruckNumber.text = truckStatusResponse.truck_reg_number
                binding.textViewAddress.text = truckStatusResponse.driver_licence
                binding.textViewName.text = truckStatusResponse.name

                binding.textViewFromText.text = truckStatusResponse.Source
                binding.textViewToText.text = truckStatusResponse.Destination
                sourceLatLng = LatLng(
                    truckStatusResponse.source_lat!!.toDouble(),
                    truckStatusResponse.source_long!!.toDouble()
                )
                destinationLatLng = LatLng(
                    truckStatusResponse.destination_lat!!.toDouble(),
                    truckStatusResponse.destination_long!!.toDouble()
                )
                startMap()
                handler.post(runnable)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
    val handler = Handler()
    val runnable = object:Runnable{
        override fun run() {
            showProgress()
            viewModel.getDriverLocation(truckStatusResponse.job_id?:"")
            handler.postDelayed(this, 2*60*1000)
        }
    }



    private fun getMarkerBitmapFromView(markerFor: MarkerFor): Bitmap? {
        when (markerFor) {
            MarkerFor.PICK_UP -> {
                val customMarkerPickupBinding =
                    CustomMarkerPickupBinding.inflate(
                        LayoutInflater.from(
                            this
                        )
                    )
                customMarkerPickupBinding.root.measure(
                    View.MeasureSpec.UNSPECIFIED,
                    View.MeasureSpec.UNSPECIFIED
                )
                customMarkerPickupBinding.root.layout(
                    0,
                    0,
                    customMarkerPickupBinding.root.measuredWidth,
                    customMarkerPickupBinding.root.measuredHeight
                )
                customMarkerPickupBinding.root.buildDrawingCache()
                val returnedBitmap = Bitmap.createBitmap(
                    customMarkerPickupBinding.root.measuredWidth,
                    customMarkerPickupBinding.root.measuredHeight,
                    Bitmap.Config.ARGB_8888
                )
                val canvas = Canvas(returnedBitmap)
                canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
                customMarkerPickupBinding.root.background?.draw(canvas)
                customMarkerPickupBinding.root.draw(canvas)
                return returnedBitmap
            }
            MarkerFor.DROP -> {
                val customMarkerDropBinding =
                    CustomMarkerDropBinding.inflate(
                        LayoutInflater.from(
                            this
                        )
                    )
                customMarkerDropBinding.root.measure(
                    View.MeasureSpec.UNSPECIFIED,
                    View.MeasureSpec.UNSPECIFIED
                )
                customMarkerDropBinding.root.layout(
                    0,
                    0,
                    customMarkerDropBinding.root.measuredWidth,
                    customMarkerDropBinding.root.measuredHeight
                )
                customMarkerDropBinding.root.buildDrawingCache()
                val returnedBitmap = Bitmap.createBitmap(
                    customMarkerDropBinding.root.measuredWidth,
                    customMarkerDropBinding.root.measuredHeight,
                    Bitmap.Config.ARGB_8888
                )
                val canvas = Canvas(returnedBitmap)
                canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
                customMarkerDropBinding.root.background?.draw(canvas)
                customMarkerDropBinding.root.draw(canvas)
                return returnedBitmap
            }
        }
    }

    private fun getTruckLiveLocationFromFirebase() {
        val truckId = truckStatusResponse.truck_id

        if (truckId == "0" || jobId == "0")
            return

        val key = "${truckId}_$jobId"

        logUtil.logV("key is $key")

        val ref = Firebase.database.reference.child("Location")

        var innerKey = ""

        val cal = Calendar.getInstance()

        ref.addValueEventListener(
            object : com.google.firebase.database.ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(snapshot: com.google.firebase.database.DataSnapshot) {
                    if (snapshot.hasChild(key)) {
                        logUtil.logV("has the child")
                        snapshot.child(key).children.forEach { children ->
                            innerKey = children.key!!
                            logUtil.logV(
                                "inner key is $innerKey"
                            )
                            currentLocation =
                                children.getValue(TruckBooking.LocationForFirebase::class.java)!!
                            logUtil.logV(
                                """
                                    location
                                    $currentLocation
                                """.trimIndent()
                            )

                            //addCurrentMarker()
                        }
                    }
                }
            }
        )
    }

    private fun addCurrentMarker(truckLocation: TruckLocation?) {
        if (truckMarker != null) {
            val lastLocation = truckMarker?.position
                //repository.getSharedPref().getString(KeywordsAndConstants.LAST_TRUCK_LOCATION, "")
            if (truckLocation!=null &&
                (lastLocation?.latitude?:0.0)==0.0 &&
                (truckLocation.lat?.trim()!="" || truckLocation.lng?.trim()!=""))
                return

            val source = LatLng(
                lastLocation?.latitude?:0.0,
                lastLocation?.longitude?:0.0
            )

            val dest = LatLng(
                truckLocation?.lat?.toDouble()?:0.0,
                truckLocation?.lng?.toDouble()?:0.0
            )

            val dLon: Double = dest.longitude - source.longitude
            val y = Math.sin(dLon) * Math.cos(dest.latitude)
            val x =
                Math.cos(source.latitude) * Math.sin(dest.latitude) - Math.sin(source.latitude) * Math.cos(
                    dest.latitude
                ) * Math.cos(dLon)
            var brng = Math.toDegrees(Math.atan2(y, x))
            brng = 360 - (brng + 360) % 360

            if (brng == 356.0) brng = 0.0
            val toRotationFinal: Float = brng.toFloat()
            val handler = Handler()
            val start: Long = SystemClock.uptimeMillis()
            val proj: Projection = map.getProjection()
            val startPoint: Point = proj.toScreenLocation(source)
            val startLatLng: LatLng = proj.fromScreenLocation(startPoint)
            val duration: Long = 500

            val startRotation: Float = truckMarker?.rotation!!

            val interpolator = LinearInterpolator()

            handler.post(object : Runnable {
                override fun run() {
                    val elapsed: Long = SystemClock.uptimeMillis() - start
                    val t: Float = interpolator.getInterpolation(
                        elapsed.toFloat()
                                / duration
                    )
                    val lng: Double = t * dest.longitude + (1 - t) * startLatLng.longitude
                    val lat: Double = t * dest.latitude + (1 - t) * startLatLng.latitude
                    val rot = t * toRotationFinal + (1 - t) * startRotation
                    val bearing = if (-rot > 180) rot / 2 else rot
                    truckMarker?.position = LatLng(lat, lng)
                    truckMarker?.rotation = bearing
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16)
                    }
                }
            })

            return
        }
        val dest = LatLng(
            truckLocation?.lat?.toDouble()?:0.0,
            truckLocation?.lng?.toDouble()?:0.0
        )
        truckMarker = map.addMarker(
            MarkerOptions()
                .title("current position")
                .position(
                    dest
                )
                .icon(
                    BitmapDescriptorFactory.fromResource(
                        R.drawable.up_arrow_truck_marker
                    )
                )
        )

        map.animateCamera(CameraUpdateFactory.newLatLngBounds(
            LatLngBounds.builder()
                .include(dest)
                .include(destinationLatLng)
                .build(),
            120
        ))
        /*repository.getSharedPref()
            .edit {
                putString(
                    KeywordsAndConstants.LAST_TRUCK_LOCATION,
                    "${currentLocation.lat},${currentLocation.long}"
                )
            }*/
    }

    fun recenterToCurrentLocation() {
        if (
            !this::currentLocation.isInitialized
        )
            return

        map.animateCamera(
            CameraUpdateFactory.newLatLng(
                LatLng(
                    currentLocation.lat.toDouble(),
                    currentLocation.long.toDouble()
                )
            )
        )
    }
}