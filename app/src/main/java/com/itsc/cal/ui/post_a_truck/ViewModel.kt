package  com.itsc.cal.ui.post_a_truck

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveTruckTypeList: MediatorLiveData<Event<ArrayList<TruckTypeResponse>>> =
        MediatorLiveData()
    val mediatorLiveTruckTypeListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveTruckTypeList.addSource(
            repository.mutableLiveDataTruckTypeList
        ) { t -> mediatorLiveTruckTypeList.postValue(t) }
        mediatorLiveTruckTypeListError.addSource(
            repository.mutableLiveDataTruckTypeListError
        ) { t -> mediatorLiveTruckTypeListError.postValue(t) }


    }

    fun getTruckType() = repository.getTruckTypeList()


}