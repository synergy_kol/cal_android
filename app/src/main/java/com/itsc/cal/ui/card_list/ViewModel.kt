package  com.itsc.cal.ui.card_list

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveCardListing: MediatorLiveData<Event<ArrayList<CardListResponse>>> =
        MediatorLiveData()
    val mediatorLiveCardListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveMarkAsDefalut: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveMarkAsDefalutError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDeleteCard: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveDeleteCardError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveCardListing.addSource(
            repository.mutableLiveDataCardList
        ) { t -> mediatorLiveCardListing.postValue(t) }
        mediatorLiveCardListingError.addSource(
            repository.mutableLiveDataCardListError
        ) { t -> mediatorLiveCardListingError.postValue(t) }


        mediatorLiveMarkAsDefalut.addSource(
            repository.mutableLiveDataMarkAsDefault
        ) { t -> mediatorLiveMarkAsDefalut.postValue(t) }
        mediatorLiveMarkAsDefalutError.addSource(
            repository.mutableLiveDataMarkAsDefaultError
        ) { t -> mediatorLiveMarkAsDefalutError.postValue(t) }


        mediatorLiveDeleteCard.addSource(
            repository.mutableLiveDataDeleteCard
        ) { t -> mediatorLiveDeleteCard.postValue(t) }
        mediatorLiveDeleteCardError.addSource(
            repository.mutableLiveDataDeleteCardError
        ) { t -> mediatorLiveDeleteCardError.postValue(t) }

    }

    fun getCardList() = repository.getCardList()
    fun marksAsDefalut(cardId:String) = repository.setAsDefault(cardId)
    fun deleteCard(cardId:String) = repository.deleteCard(cardId)



}