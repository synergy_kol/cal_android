package com.itsc.cal.ui.sos_document_submited.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.databinding.ChildSosSubmitedListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.SOSDocumentResponse

class SOSDocumentSubmitedAdapter (val mContext: Context, val arrayList:ArrayList<SOSDocumentResponse>,val onItemClickListener: OnItemClickListener):
    RecyclerView.Adapter<SOSDocumentSubmitedAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ChildSosSubmitedListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))
    }

    inner class ViewHolder(val binding: ChildSosSubmitedListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(sOSDocumentResponse:SOSDocumentResponse) {
            binding.textDocumentName.setText(sOSDocumentResponse.document_title)
            binding.llBg.setOnClickListener {
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.imgDownload.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            if (sOSDocumentResponse.document_status.equals("U")){
                binding.imgStatus.setBackgroundResource(R.drawable.ic_upload)
                binding.textStatus.text = "   Upload   "
                binding.textStatusDetails.setText("")
            }else  if (sOSDocumentResponse.document_status.equals("P")){
                binding.imgStatus.setBackgroundResource(R.drawable.ic_pending)
                binding.textStatus.text = "  Pending  "
                binding.textStatus.setTextColor(Color.parseColor("#F7F14A"))
                binding.textStatusDetails.setText("")
            }else  if (sOSDocumentResponse.document_status.equals("A")){
                binding.textStatus.text = " Approved"
                binding.imgStatus.setBackgroundResource(R.drawable.ic_accepted)
                binding.textStatus.setTextColor(Color.parseColor("#3DDC84"))
                binding.textStatusDetails.setText("")
            }else  if (sOSDocumentResponse.document_status.equals("R")){
                binding.textStatus.text = "Rejected     "
                binding.imgStatus.setBackgroundResource(R.drawable.ic_decline)
                binding.textStatus.setTextColor(Color.parseColor("#FF0000"))
                binding.textStatusDetails.setText("Tap to retake the document")
            }
            if(sOSDocumentResponse.document_image!=null &&
                !sOSDocumentResponse.document_image.equals("")){
                binding.imgDownload.visibility = View.VISIBLE
            }else{
                binding.imgDownload.visibility = View.INVISIBLE
            }
        }

    }


}