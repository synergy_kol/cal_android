package  com.itsc.cal.ui.manage_builders

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveAddBuilder: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveAddBuilderError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()



    init {

        mediatorLiveAddBuilder.addSource(
            repository.mutableLiveDatAddBuilder
        ) { t -> mediatorLiveAddBuilder.postValue(t) }
        mediatorLiveAddBuilderError.addSource(
            repository.mutableLiveDatAddBuilderError
        ) { t -> mediatorLiveAddBuilderError.postValue(t) }




    }

    fun addBuilder(
        first_name: String,
        last_name: String,
        company_name: String,
        email: String,
        phone: String,
        address: String,
        latitute: String,
        longitute: String,
        imageFile:File?

    ) =
        repository.addBuilder(
            first_name = first_name,
            last_name = last_name,
            company_name = company_name,
            email = email,
            phone = phone,
            address = address,
            latitute = latitute,
            longitute = longitute,
            imageFile = imageFile
        )



}