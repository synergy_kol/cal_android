package com.itsc.cal.ui.suscription

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySubscriptionListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.SubscriptionListResponce
import com.itsc.cal.model.TruckTypeResponse
import com.itsc.cal.ui.subscription_details.SubsciptionDetailsActivity
import com.itsc.cal.ui.suscription.adapter.SubscriptionListAdapter
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SubsciptionListActivity : CalSuperActivity(withNav = true), KodeinAware {

    private lateinit var binding: ActivitySubscriptionListBinding
    override val kodein: Kodein by kodein()
    private lateinit var viewModel: ViewModel
    private lateinit var truckTypeResponse: TruckTypeResponse
    private val viewModelProvider: ViewModelProvider by instance()

    companion object {
        var truckListing: Activity? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_subscription_list)
        enableBackButton()
        setPageTitle("Subscription List")
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        truckListing = this

        initView()

        bindViewHolder()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initView() {

        if (viewModel.giveRepository().getUserData()!!.user_role.equals("4")) {
            getCertificationDetails()
            setPageTitle("Certification")
        } else if (viewModel.giveRepository().getUserData()!!.user_role.equals("2")) {
            getSubscriptionListDetails()
            setPageTitle("Subscription List")
        } else if (viewModel.giveRepository().getUserData()!!.user_role.equals("3")) {
            getSubscriptionListDetails()
            setPageTitle("Subscription List")
        }
    }

    private fun getSubscriptionListDetails() {
        showProgress()
        viewModel.getSubscriptionListDetails(viewModel.giveRepository().getUserData()!!.city ?: "")
    }

    private fun getCertificationDetails() {
        showProgress()
        viewModel.getCertificationDetails(viewModel.giveRepository().getUserData()!!.city ?: "")
    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveSubscriptionList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<SubscriptionListResponce>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent() ?: ArrayList())

                    }
                }
            }
        )

        viewModel.mediatorLiveSubscriptionListError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )


        viewModel.mediatorLiveCirtificationList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<SubscriptionListResponce>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent() ?: ArrayList())

                    }
                }
            }
        )

        viewModel.mediatorLiveCertificationListError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }



    private fun setAdapter(arrayList: ArrayList<SubscriptionListResponce>) {
        binding.recycleViewTrackList.apply {
            layoutManager = LinearLayoutManager(this@SubsciptionListActivity)
            adapter = SubscriptionListAdapter(
                this@SubsciptionListActivity,
                arrayList,
                object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val intent = Intent(
                            this@SubsciptionListActivity,
                            SubsciptionDetailsActivity::class.java
                        )
                        intent.putExtra(
                            KeywordsAndConstants.DATA,
                            toJson(arrayList.get(position))
                        )
                        startActivity(intent)
                    }

                })
        }
    }
}