package com.itsc.cal.ui.job.my_joblist

import android.os.Bundle
import android.os.Handler
import androidx.viewpager.widget.ViewPager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityPostedJobTabBinding
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.job.adapter.ViewPagerAdapter
import com.itsc.cal.ui.job.fragment.PostedJobFragment
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MyPostedJob: CalSuperActivity(withNav = true), KodeinAware {
    override val kodein: Kodein by kodein()

    private val repository: Repository by instance()

    private lateinit var binding: ActivityPostedJobTabBinding
    private val pagerFragmentList = listOf(
        PostedJobFragment.newInstance("1"),
        PostedJobFragment.newInstance("5"),
        PostedJobFragment.newInstance("3"),
        PostedJobFragment.newInstance("4")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_posted_job_tab)

        showDashboardActionbarWithBackButton()
        setPageTitle("My Posted Job")
        setPagerAdapter()
    }

    private fun setPagerAdapter(){
        val mBookingPagerAdapter = ViewPagerAdapter(supportFragmentManager,pagerFragmentList)
        binding.viewPager.adapter = mBookingPagerAdapter
        binding.viewPager.offscreenPageLimit = 1
        binding.tabs.setViewPager(binding.viewPager)
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                val myReservationFragment =
                    mBookingPagerAdapter.fragmentList[position] as? PostedJobFragment
                if (position==1){
                    myReservationFragment?.getJobListing((5).toString())
                }else{
                    myReservationFragment?.getJobListing((position + 1).toString())
                }

            }

        })

        Handler().postDelayed(
            {
                val myReservationFragment =
                    mBookingPagerAdapter.fragmentList[0] as? PostedJobFragment
                myReservationFragment?.getJobListing((0 + 1).toString())
            },
            800
        )
    }
}