package com.itsc.cal.ui.post_a_truck

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityPostATruckBinding
import com.itsc.cal.model.PostATruckRequest
import com.itsc.cal.model.TruckTypeResponse
import com.itsc.cal.ui.post_a_truck_image.PostATruckImageActivity
import com.itsc.cal.ui.post_a_truck_image.PostATruckImageRegistrationActivity
import com.itsc.cal.ui.webview.WebView
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.itsc.cal.widget.spinner.Spinner
import com.itsc.cal.widget.spinner.SpinnerData
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class PostATruckActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPostATruckBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var truckTypeResponse: TruckTypeResponse
    companion object{
         var postATruckPage: Activity? = null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_post_a_truck)
        enableBackButton()
        setPageTitle("Post A Truck")
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        postATruckPage = this
        intiView()
        bindViewHolder()
        getTruckType()
    }

    private fun getTruckType() {
        showProgress()
        viewModel.getTruckType()
    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveTruckTypeList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<TruckTypeResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val items: ArrayList<SpinnerData<TruckTypeResponse>> = ArrayList()
                        it.getContent()!!.forEach { source ->
                            items.add(
                                SpinnerData(
                                    source.name.toString(),
                                    source
                                )
                            )
                        }
                        if (items.size > 0)
                            truckTypeResponse = items[0].data
                        binding.spinnerTruckType.setBg()
                        binding.spinnerTruckType.addItems(
                            items,
                            withDot = false,
                            listener = object : Spinner.OnItemSelectedListener {
                                override fun <T> selected(item: SpinnerData<T>) {
                                    truckTypeResponse = item.data as TruckTypeResponse

                                }
                            }
                        )

                    }
                }
            }
        )

        viewModel.mediatorLiveTruckTypeListError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun intiView() {

        binding.edVnNumber.apply {
            hint("Enter VIN number")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.edVehicleRegister.apply {
            hint("Enter Vehicle Registration number")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.edMake.apply {
            hint("Enter make")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.EdBodyStyle.apply {
            hint("Enter body style")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.EdModel.apply {
            hint("Enter Model")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.edColor.apply {
            hint("Enter Color")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.edCabinType.apply {
            hint("Enter Cabin type")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.edEmptyWeight.apply {
            hint("Enter Empty Weight")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.edEmptyWeight.apply {
            hint("Enter Empty Weight")
            inputMode(EditTextInputMode.DECIMAL)
        }

        binding.edCarryingCapaciy.apply {
            hint("Enter Carrying capacity of the vehicle")
            inputMode(EditTextInputMode.DECIMAL)
        }

        binding.EdWeightMetricTons.apply {
            hint("Enter Weight(Metric tons)")
            inputMode(EditTextInputMode.DECIMAL)
        }

        binding.edUsdot.apply {
            hint("Enter USDOT")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }


        binding.btnPostATruck.setOnClickListener {
            moveToNext()
        }
        binding.textAck.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    WebView::class.java
                ).putExtra(
                    WebView.URL,
                    "${KeywordsAndConstants.BASE_URL}home/cms/ACK"
                ).putExtra(
                    WebView.TITLE,
                    "Acknowledgement"
                )
            )
        }
    }

    private fun moveToNext() {

        if (
            binding.edVnNumber.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide VIN Number.")
            return
        }
        if (
            binding.edVehicleRegister.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Vehicle Registration Number.")
            return
        }

        if (
            binding.edMake.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Make.")
            return
        }

        if (
            binding.EdBodyStyle.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Body Style.")
            return
        }


        if (
            binding.EdModel.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Model.")
            return
        }

        if (
            binding.edColor.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Color.")
            return
        }

        if (
            binding.edCabinType.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Cabin Type.")
            return
        }

        if (
            binding.edEmptyWeight.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Empty Weight(MT).")
            return
        }
        if (
            binding.edCarryingCapaciy.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Carrying Capacity(MT).")
            return
        }

        if (
            binding.EdWeightMetricTons.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Weight(MT).")
            return
        }

        if (!binding.cbFleetOwner.isChecked){
            showMessageInDialog("Please accept the Acknowledgment.")
            return
        }

        val postATruckRequest = PostATruckRequest(
            user_master_id = viewModel.giveRepository().getUserData()!!.user_master_id,
            truck_weight_metric = binding.EdWeightMetricTons.getText().toString().trim(),
            truck_vin_number = binding.edVnNumber.getText().toString().trim(),
            truck_type = truckTypeResponse.truck_type_id,
            truck_reg_number = binding.edVehicleRegister.getText().toString().trim(),
            truck_model = binding.EdModel.getText().toString().trim(),
            truck_make = binding.edMake.getText().toString().trim(),
            truck_empty_weight = binding.edEmptyWeight.getText().toString().trim(),
            truck_color = binding.edColor.getText().toString().trim(),
            truck_carring_capacity = binding.edCarryingCapaciy.getText().toString().trim(),
            truck_cabin_type = binding.edCabinType.getText().toString().trim(),
            truck_body_style = binding.EdBodyStyle.getText().toString().trim()
        )
        val userDetails = viewModel.giveRepository().getUserData()
        if(userDetails?.completed_steps=="7") {
            startActivity(
                Intent(this, PostATruckImageActivity::class.java).putExtra(
                    KeywordsAndConstants.DATA,
                    toJson(postATruckRequest)
                )
            )
        }else{
            startActivity(
                Intent(this, PostATruckImageRegistrationActivity::class.java).putExtra(
                    KeywordsAndConstants.DATA,
                    toJson(postATruckRequest)
                )
            )
        }
    }

}