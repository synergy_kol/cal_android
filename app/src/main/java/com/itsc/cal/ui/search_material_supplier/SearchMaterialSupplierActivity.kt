package com.itsc.cal.ui.search_material_supplier

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySearchContractorBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.BuilderListResponse
import com.itsc.cal.ui.builders_details.adapter.BuilderDetailsAdapter
import com.itsc.cal.ui.profile_builder.ProfileBuilderActivity
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SearchMaterialSupplierActivity : CalSuperActivity(withNav = true), KodeinAware {

    override val kodein: Kodein by kodein()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var binding: ActivitySearchContractorBinding
    private lateinit var adapter:BuilderDetailsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_search_contractor)
        setPageTitle("Search Material/Supplier")
        showHelpButton()
        enableBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        getSupplierList()
        initView()
        bindViewModel()
    }
    private fun initView(){

        binding.textSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    adapter.getFilter().filter(s.toString())

                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })
    }
    private fun getSupplierList(){
        showProgress()
        viewModel.getSupplierList()
    }

    private fun bindViewModel() {
        viewModel.mediatorLiveSupplierListing.observe(
            this,
            Observer<Event<ArrayList<BuilderListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent() ?: ArrayList())
                    }
                }
            }
        )
        viewModel.mediatorLiveSupplierListingError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                        binding.textNoDataFound.visibility = View.VISIBLE
                        binding.recycleviewBuilder.visibility = View.GONE
                    }

                }
            }
        )
    }

    private fun setAdapter(arrayList: ArrayList<BuilderListResponse>) {
        binding.recycleviewBuilder.layoutManager = LinearLayoutManager(this)
        adapter =
            BuilderDetailsAdapter(this@SearchMaterialSupplierActivity, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                   /* startActivity(
                        Intent(
                            this@SearchMaterialSupplierActivity,
                            ProfileBuilderActivity::class.java
                        ).putExtra(KeywordsAndConstants.IS_OWN_PROFILE, false).putExtra(
                            KeywordsAndConstants.DATA,
                            toJson(arrayList.get(position))
                        )
                    )*/
                }

            }, arrayList)
        binding.recycleviewBuilder.adapter = adapter
        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                super.onChanged()
                checkEmpty()
            }

            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                checkEmpty()
            }

            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                super.onItemRangeRemoved(positionStart, itemCount)
                checkEmpty()
            }

            fun checkEmpty() {
                binding.textNoDataFound.visibility = (if (adapter.itemCount == 0) View.VISIBLE else View.GONE)
            }
        })




    }
}