package  com.itsc.cal.ui.subscription_details

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveSubscriptionList: MediatorLiveData<Event<ArrayList<SubscriptionListResponce>>> =
        MediatorLiveData()
    val mediatorLiveSubscriptionListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveDefalutCard: MediatorLiveData<Event<CardResponse>> = MediatorLiveData()
    val mediatorLiveDefalutCardError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveUserSubscription: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveUserSubscriptionError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    val mediatorLiveFleetCirtification: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveFleetCirtificationError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    val mediatorLiveCancelSubscription: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveCancelSubscriptionError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {

        mediatorLiveDefalutCard.addSource(
            repository.mutableLiveDataDefaultCardSubscription
        ) { t -> mediatorLiveDefalutCard.postValue(t) }
        mediatorLiveDefalutCardError.addSource(
            repository.mutableLiveDataDefaultCardSubscriptionError
        ) { t -> mediatorLiveDefalutCardError.postValue(t) }

        mediatorLiveUserSubscription.addSource(
            repository.mutableLiveDataUserSubscription
        ) { t -> mediatorLiveUserSubscription.postValue(t) }
        mediatorLiveUserSubscriptionError.addSource(
            repository.mutableLiveDataUserSubscriptionError
        ) { t -> mediatorLiveUserSubscriptionError.postValue(t) }

        mediatorLiveFleetCirtification.addSource(
            repository.mutableLiveDataFleetCirtification
        ) { t -> mediatorLiveFleetCirtification.postValue(t) }
        mediatorLiveFleetCirtificationError.addSource(
            repository.mutableLiveDataFleetCirtificationError
        ) { t -> mediatorLiveFleetCirtificationError.postValue(t) }

        mediatorLiveCancelSubscription.addSource(
            repository.mutableLiveDataCancelHultOffSubscriptionData
        ) { t -> mediatorLiveCancelSubscription.postValue(t) }
        mediatorLiveCancelSubscriptionError.addSource(
            repository.mutableLiveDataCancelHultOffSubscriptionError
        ) { t -> mediatorLiveCancelSubscriptionError.postValue(t) }

    }

    fun getSubscriptionListDetails(city_id: String) =
        repository.getSubscriptionListDetails(city_id = city_id)

    fun getDefaultCard() = repository.getDefaultCardSubscription()
    fun cancelSubscription() = repository.cancelHaulofSubscription()
    fun userSubscription(stripeToken: String, subscribe_id: String) =
        repository.userSubscription(stripeToken = stripeToken, subscribe_id = subscribe_id)

    fun fleetCertification(stripeToken: String, certification_id: String) =
        repository.fleetCertification(stripeToken = stripeToken, certification_id = certification_id)

}