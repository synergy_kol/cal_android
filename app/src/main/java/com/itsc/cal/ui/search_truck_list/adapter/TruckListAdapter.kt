package com.itsc.cal.ui.search_truck_list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.AdapterTruckListBinding
import com.itsc.cal.interfaces.OnItemClickListener

class TruckListAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener):
    RecyclerView.Adapter<TruckListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(AdapterTruckListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return 12
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData()


    }

    inner class ViewHolder(val binding: AdapterTruckListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData() {
            binding.tvAddReview.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.tvViewDetails.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            setRatingValue(3)
        }

        private fun setRatingValue(value:Int) {
            when (value) {
                0 -> {
                    binding.ivRatingOne.isSelected = false
                    binding.ivRatingTwo.isSelected = false
                    binding.ivRatingThree.isSelected = false
                    binding.ivRatingFour.isSelected = false
                    binding.ivRatingFive.isSelected = false

                }
                1 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = false
                    binding.ivRatingThree.isSelected = false
                    binding.ivRatingFour.isSelected = false
                    binding.ivRatingFive.isSelected = false

                }
                2 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = true
                    binding.ivRatingThree.isSelected = false
                    binding.ivRatingFour.isSelected = false
                    binding.ivRatingFive.isSelected = false
                }
                3 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = true
                    binding.ivRatingThree.isSelected = true
                    binding.ivRatingFour.isSelected = false
                    binding.ivRatingFive.isSelected = false
                }
                4 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = true
                    binding.ivRatingThree.isSelected = true
                    binding.ivRatingFour.isSelected = true
                    binding.ivRatingFive.isSelected = false
                }
                5 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = true
                    binding.ivRatingThree.isSelected = true
                    binding.ivRatingFour.isSelected = true
                    binding.ivRatingFive.isSelected = true
                }
            }
        }

    }

}