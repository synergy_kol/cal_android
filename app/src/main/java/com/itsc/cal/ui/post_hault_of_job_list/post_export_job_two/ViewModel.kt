package  com.itsc.cal.ui.post_hault_of_job_list.post_export_job_two

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {


    val mediatorLiveMaterialEquipementResult: MediatorLiveData<Event<MaterialEquipmentResponse>> =
        MediatorLiveData()
    val mediatorLiveMaterialEquipementError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    val mediatorLiveHaulJobExportStep2: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataJobExportStep2Error: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {

        mediatorLiveMaterialEquipementResult.addSource(
            repository.mutableLiveDataMaterialEquipment
        ) { t -> mediatorLiveMaterialEquipementResult.postValue(t) }
        mediatorLiveMaterialEquipementError.addSource(
            repository.mutableLiveDataMaterialEquiementError
        ) { t -> mediatorLiveMaterialEquipementError.postValue(t) }



        mediatorLiveHaulJobExportStep2.addSource(
            repository.mutableLiveDataJobExportStep2
        ) { t -> mediatorLiveHaulJobExportStep2.postValue(t) }
        mediatorLiveDataJobExportStep2Error.addSource(
            repository.mutableLiveDataJobExportStep2Error
        ) { t -> mediatorLiveDataJobExportStep2Error.postValue(t) }

    }


    fun getMaterialEquipmentList() = repository.getMaterialEquipmentList()
    fun postExportJobStep2(
        haul_request_id: String,
        MaterialType: String,
        material_other: String,
        MaterialoptionType: String,
        cubic_yards: String,
        tons: String,
        specsDocumentFile: File?
    ) =
        repository.postExportStep2(
            haul_request_id = haul_request_id,
            MaterialoptionType = MaterialoptionType,
            MaterialType = MaterialType,
            material_other = material_other,
            cubic_yards = cubic_yards,
            tons = tons,
            specsDocumentFile = specsDocumentFile
        )

}