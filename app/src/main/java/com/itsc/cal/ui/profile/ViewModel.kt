package  com.itsc.cal.ui.profile

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.ProfileDetails
import com.itsc.cal.model.ProfileResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataProfileDetails: MediatorLiveData<Event<ProfileResponse>> =
        MediatorLiveData()
    val mediatorLiveDataProfileDetailsError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveDataSaveProfileDetails: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataSaveProfileDetailsError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDataProfileDetails.addSource(
            repository.mutableLiveDataAllProfileDetail
        ) { t -> mediatorLiveDataProfileDetails.postValue(t) }
        mediatorLiveDataProfileDetailsError.addSource(
            repository.mutableLiveDataAllProfileDetailError
        ) { t -> mediatorLiveDataProfileDetailsError.postValue(t) }
        mediatorLiveDataSaveProfileDetails.addSource(
            repository.mutableLiveDataUpdateProfile
        ) { t -> mediatorLiveDataSaveProfileDetails.postValue(t) }
        mediatorLiveDataSaveProfileDetailsError.addSource(
            repository.mutableLiveDataUpdateProfileError
        ) { t -> mediatorLiveDataSaveProfileDetailsError.postValue(t) }
    }

    fun getProfileDetails(userId:String) = repository.getProfileDataNew(userId = userId)

    fun updateProfile(
        firstName: String,
        lastName: String,
        mobile: String,
        address: String,
        country: String,
        state: String,
        pinCode: String,
        city: String,
        dob: String,
        calRegistration: String,
        userId: String,
        companyName: String,
        companyRegistration: String,
        imageFile: File?
    ) {
        repository.updateProfile(
            firstName,
            lastName,
            mobile,
            address,
            country,
            state,
            pinCode,
            city,
            dob,
            calRegistration,
            userId,
            companyName,
            companyRegistration,
            imageFile
        )
    }
}