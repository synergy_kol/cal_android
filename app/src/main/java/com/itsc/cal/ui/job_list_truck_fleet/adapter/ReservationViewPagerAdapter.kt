package com.itsc.cal.ui.job_list_truck_fleet.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

import androidx.fragment.app.FragmentStatePagerAdapter

class ReservationViewPagerAdapter(fm : FragmentManager, val fragmentList : List<Fragment>) : FragmentStatePagerAdapter(fm) {


    private val titleList = arrayListOf("New job","Accepted Job","Hide job")

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }


}