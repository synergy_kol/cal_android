package com.itsc.cal.ui.paymentsettings.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildPayoutDetailsListBinding
import com.itsc.cal.model.AllPayoutData

class PayoutDetailsAdapter(val mContext: Context, val payoutList:ArrayList<AllPayoutData>):
    RecyclerView.Adapter<PayoutDetailsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ChildPayoutDetailsListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return payoutList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bindData()
    }

    inner class ViewHolder(val binding: ChildPayoutDetailsListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData() {
            val mAllPayoutData = payoutList[adapterPosition]
            binding.apply {
            tvDate.text = mAllPayoutData.created_ts
            tvTransferAmt.text = "$${mAllPayoutData.transfer_amount}"
            tvJobId.text = mAllPayoutData.job_id
            tvCalCommision.text = "$${mAllPayoutData.cal_commission}"
            }
        }

    }
}