package com.itsc.cal.ui.post_hault_of_job_list.import_export_job_details.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ChildPictureShowingBinding
import com.itsc.cal.model.ImageArray

class ImageShowingAdapter(
    val mContext: Context,
    val arrayList: ArrayList<ImageArray>
) :
    RecyclerView.Adapter<ImageShowingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ChildPictureShowingBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))


    }

    inner class ViewHolder(val binding: ChildPictureShowingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(vehicleImage: ImageArray) {
            binding.imgVehicle.setImage("${KeywordsAndConstants.BASE_URL}${vehicleImage.haul_request_image_full}")
        }


    }

}