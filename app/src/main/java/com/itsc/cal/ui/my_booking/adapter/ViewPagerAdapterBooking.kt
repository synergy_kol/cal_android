package com.itsc.cal.ui.my_booking.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPagerAdapterBooking(fm : FragmentManager, val fragmentList : List<Fragment>) : FragmentStatePagerAdapter(fm) {


    private val titleList = arrayListOf("Current Booking","Past Booking")

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }


}