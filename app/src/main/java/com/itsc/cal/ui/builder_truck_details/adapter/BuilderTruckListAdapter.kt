package com.itsc.cal.ui.builder_truck_details.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildBuilderTruckDeatilsBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.BuilderTruckDetailsResponse

class BuilderTruckListAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener, val arrayList:ArrayList<BuilderTruckDetailsResponse>):
    RecyclerView.Adapter<BuilderTruckListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(ChildBuilderTruckDeatilsBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))

    }

    inner class ViewHolder(val binding: ChildBuilderTruckDeatilsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(builderTruckDetailsResponse: BuilderTruckDetailsResponse) {
            binding.btnEmptySlip.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.btnLoadedTruckSlip.setOnClickListener {
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.tvEmptyTruckWeight.text = builderTruckDetailsResponse.empty_truck_weight
            binding.tvLoadedTruckWeight.text = builderTruckDetailsResponse.loaded_truck_weight
            binding.textTruckNo.text = builderTruckDetailsResponse.truck_reg_number

        }

    }

}