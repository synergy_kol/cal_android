package com.itsc.cal.ui.driver_job_details

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityDriverJobDetailsBinding
import com.itsc.cal.databinding.EmptyTruckDialogBinding
import com.itsc.cal.model.DriverJob
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.driver_truck_live.DriverTruckLiveActivity
import com.itsc.cal.ui.end_trip.EndTripActivity
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.generic.instance
import permissions.dispatcher.*
import java.io.File

@RuntimePermissions
class DriverJobDetailsActivity : CalSuperActivity() {

    private lateinit var binding: ActivityDriverJobDetailsBinding
    private lateinit var jobResponse: DriverJob
    private lateinit var dialogBinding: EmptyTruckDialogBinding
    private var imageUploadChalan: File? = null
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var emptyWeightStr = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_driver_job_details)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        setPageTitle("Job Details")
        enableBackButton()
        showHelpButton()
        initView()
        bindingViewModel()
    }

    private fun bindingViewModel() {
        viewModel.mediatorLiveDataEmptyTruck.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        it.readContent()
                        hideProgress()
                        startActivity(
                            Intent(
                                this@DriverJobDetailsActivity,
                                DriverTruckLiveActivity::class.java
                            ).putExtra(
                                KeywordsAndConstants.DATA,
                                toJson(jobResponse)
                            ).putExtra(KeywordsAndConstants.EMPTY_WEIGHT,emptyWeightStr)
                        )
                        finish()
                        // processResponse(it.getContent()!!.message)
                    }

                }
            }
        )
        viewModel.mediatorLiveDataEmptyTruckError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )


    }

    private fun initView() {
        intent.let {
            jobResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            binding.tvPickUp.text = jobResponse.Source
            binding.tvDestination.text = jobResponse.Destination
            binding.tvJobId.text = "#CAL${jobResponse.job_id}"
            binding.tvMaterialType.text = jobResponse.material_name
            binding.tvWeight.text = jobResponse.Weight
            binding.tvTruckType.text = jobResponse.name
            binding.tvFrieghtCharges.text = "$ ${jobResponse.FreightCharges}"
            if (jobResponse.Equipment_value.equals("")) {
                binding.tvEquipment.text = "NO"
            } else {
                binding.tvEquipment.text = "Yes (${jobResponse.Equipment_value})"
            }
            binding.tvPickupDate.text = jobResponse.pickup_date
            binding.tvPickupTime.text = jobResponse.pickup_time
            binding.tvDeliveryDate.text = jobResponse.delivery_date
            binding.tvDeliveryTime.text = jobResponse.delivery_time
            binding.tvNoOfTrucks.text = jobResponse.NoOfTrucks
            binding.tvJobEstimatePrice.text = "$ ${jobResponse.JobEstimatePrice}"
            binding.tvDriverToken.text = jobResponse.token_id
            binding.tvJobStartingOTP.text = jobResponse.supplier_otp
            binding.tvJobFinishingOTP.text = jobResponse.builder_otp
            binding.buttonTruckOnMap.setOnClickListener {
                if (jobResponse.driver_job_status == "2" || jobResponse.driver_job_status == "3") {
                    startActivity(
                        Intent(
                            this,
                            EndTripActivity::class.java
                        ).putExtra(
                            KeywordsAndConstants.DATA,
                            toJson(jobResponse)

                        )
                    )
                } else {
                    emptyWeightDialog()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data?.let {
            it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                //Log.i("got image $images")
                imageUploadChalan = File(images[0])
                dialogBinding.imgUploadChalanPhoto.setImage(
                    File(images[0]),
                    isCircularImage = false,
                    needBorderWithCircularImage = true
                )


            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(
                        Intent(
                            this@DriverJobDetailsActivity,
                            DriverTruckLiveActivity::class.java
                        ).putExtra(
                            KeywordsAndConstants.DATA,
                            toJson(jobResponse)
                        )
                    )


                }
            }
        )

    }

    private fun emptyWeightDialog() {
        dialogBinding = EmptyTruckDialogBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        val customDialog = Dialog(this)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(dialogBinding.root)
        dialogBinding.editText.apply {
            setText("")
            gravityCenter()
            editable(true)
            typeFace(
                fontResEditText = R.font.poppins_regular
            )
            inputMode(
                EditTextInputMode.NUMBER
            )
            hint("Empty weight")

        }
        dialogBinding.buttonActionOne.text = "Submit"
        customDialog.window
            ?.setLayout(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
            )

        customDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.setCancelable(true)
        customDialog.show()
        if (imageUploadChalan != null) {
            dialogBinding.imgUploadChalanPhoto.setImage(
                imageUploadChalan!!,
                isCircularImage = false,
                needBorderWithCircularImage = true
            )
        }
        dialogBinding.buttonActionOne.setOnClickListener {
            if (dialogBinding.editText.getText()
                    .toString() == ""
            ) {
                showMessageInDialog("Please provide empty truck weight(MT)")
            } else if (imageUploadChalan == null) {
                showMessageInDialog("Please upload slip photo .")

            } else {
                customDialog.dismiss()
                apiCalled(dialogBinding.editText.getText().toString())
            }

        }
        dialogBinding.imgUploadChalanPhoto.setImage(imageDrawable = resources.getDrawable(R.drawable.ic_dummy_upload_image))
        dialogBinding.imgUploadChalanPhoto.registerForOnClick {
            initiateCaptureForProfilePicture()
        }
    }

    private fun apiCalled(emptyWeight: String) {

        if (emptyWeight.isEmpty()) {
            showMessageInDialog("Please provide empty truck weight (MT) .")
            return
        }
        if (imageUploadChalan == null) {
            showMessageInDialog("Please upload slip photo.")
            return
        }
        showProgress()
        emptyWeightStr = emptyWeight
        viewModel.emptyTruckImage(
            token = jobResponse.token_id,
            challan = imageUploadChalan,
            truck_weight = emptyWeight
        )
    }

    fun initiateCaptureForProfilePicture() {
        takeProfilePictureWithPermissionCheck()
    }

    @NeedsPermission(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun takeProfilePicture() {
        ImagePickerUtil.pickImage(
            context = this,
            reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE,
            takePictureFrom = Options.TakePictureFrom.ONLY_CAMERA
        )
    }

    @OnShowRationale(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun showRationale(request: PermissionRequest) {
        showMessageWithTwoButton(
            message = "We need some permission to take pictures.",
            buttonOneText = "Okay",
            buttonTwoText = "Cancel",
            callback = object : DialogUtil.MultiButtonCallBack {
                override fun buttonOneClicked() {
                    super.buttonOneClicked()
                    request.proceed()
                }

                override fun buttonTwoClicked() {
                    super.buttonTwoClicked()
                    request.cancel()
                    finish()
                }
            }
        )
    }

    @OnPermissionDenied(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun permissionDenied() {
        showMessageWithOneButton(
            "You will not be able to take picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    @OnNeverAskAgain(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun neverAskAgain() {
        showMessageWithOneButton(
            "You will not be able to take picture picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {
                }

                override fun buttonClicked() {
                    finish()
                }
            }
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }
}