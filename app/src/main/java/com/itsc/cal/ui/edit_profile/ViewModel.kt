package  com.itsc.cal.ui.edit_profile

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {
    val mediatorLiveDataSaveProfileDetails: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataSaveProfileDetailsError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDataSaveProfileDetails.addSource(
            repository.mutableLiveDataUpdateProfile
        ) { t -> mediatorLiveDataSaveProfileDetails.postValue(t) }
        mediatorLiveDataSaveProfileDetailsError.addSource(
            repository.mutableLiveDataUpdateProfileError
        ) { t -> mediatorLiveDataSaveProfileDetailsError.postValue(t) }
    }

    fun updateProfile(
        firstName: String? = null,
        lastName: String? = null,
        mobile: String? = null,
        address: String? = null,
        country: String? = null,
        state: String? = null,
        pinCode: String? = null,
        city: String? = null,
        dob: String? = null,
        calRegistration: String? = null,
        userId: String,
        companyName: String? = null,
        companyRegistration: String? = null,
        imageFile: File? = null
    ) {
        repository.updateProfile(
            firstName,
            lastName,
            mobile,
            address,
            country,
            state,
            pinCode,
            city,
            dob,
            calRegistration,
            userId,
            companyName,
            companyRegistration,
            imageFile
        )
    }
}