package com.itsc.cal.ui.my_booking.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.itsc.cal.ui.my_booking.fragment.CurrentBookingFragement

class MyBookingPagerAdapter(val fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> {
                fragment =  CurrentBookingFragement.newInstance("4")
            }
            1 -> {
                fragment = CurrentBookingFragement.newInstance("5")
            }

        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title = ""
        when (position) {
            0 -> {
                title = "Current Booking"
            }
            1 -> {
                title = "Past Booking"
            }

        }
        return title
    }
}