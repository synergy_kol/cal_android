package com.itsc.cal.ui.two_step_verification

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityTwoStepVerificationBinding
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.add_driver.AddDriverRegistrationActivity
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.paymentsettings.PayoutSettingsRegistrationActivity
import com.itsc.cal.ui.post_a_truck.PostATruckActivity
import com.itsc.cal.ui.signup.CompleteProfileActivity
import com.itsc.cal.ui.sos_document_submited.SOSDocumentSubmitedActivity
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class TwoStepVerificationActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityTwoStepVerificationBinding
    private lateinit var timer: CountDownTimer
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var isTrust = "0"
    var prevEventTime:Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_two_step_verification)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        enableBackButton()
        setPageTitle("Verification")
        initView()
        bindToViewModel()
        setUpTimeoutForResendOtp()
        startTimeoutForResendOtp()
    }

    private fun sendOTP() {
        showProgress()

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
                override fun onComplete(task: Task<InstanceIdResult>) {
                    if (!task.isSuccessful) {
                        return
                    }
                    val token = task.result?.token ?: ""
                    Log.v("TOKEN:- ", token)
                    viewModel.resendOTP(token)
                }
            })

    }

    private fun initView() {
        sendOTP()
        val phoneNumber: Spannable =
            SpannableString(viewModel.giveRepository().getUserData()!!.mobile ?: "")
        phoneNumber.setSpan(
            ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.textBlack, null)),
            0,
            phoneNumber.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        val subText: Spannable = SpannableString(" and email ")
        subText.setSpan(
            ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.gray_dark, null)),
            0,
            subText.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        val emailAddress: Spannable =
            SpannableString(viewModel.giveRepository().getUserData()!!.email ?: "")
        emailAddress.setSpan(
            ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.textBlack, null)),
            0,
            emailAddress.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        binding.editVerificationCode.apply {
            inputMode(EditTextInputMode.NUMERIC_PASSWORD)
            setMaxLength(4)
        }
        binding.textVerificationSubText.apply {
            text = "Please enter the 4-digit verification code that sent to your number "
            append(phoneNumber)
            append(subText)
            append(emailAddress)
        }


        binding.checkBoxIAgree.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                isTrust = "1"
            } else {
                isTrust = "0"
            }
        }

        binding.textResendCode.setOnClickListener {
            setUpTimeoutForResendOtp()
            startTimeoutForResendOtp()
            sendOTP()
        }
        binding.buttonContinue.setOnClickListener {
            verificationCode()


        }
    }

    private fun startTimeoutForResendOtp() {
        if (this::timer.isInitialized) {
            binding.textTimer.setTextColor(resources.getColor(R.color.gray_dark))
            binding.textResendCode.setTextColor(resources.getColor(R.color.gray_dark))
            binding.textResendCode.isClickable = false
            binding.textResendCode.isEnabled = false
            timer.start()
        }
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataResendOTP.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        prevEventTime = System.currentTimeMillis()
                        binding.editVerificationCode.setText("")
                        showMessageInDialog(
                            /*it.getContent()?.message ?:*/ "Please check your email & phone for OTP. OTP is valid for 10 minutes"
                        )
                    }
                }
            }
        )
        viewModel.mediatorLiveDataResendOTPError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataVerifyOTP.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                        var userDetails = viewModel.giveRepository().getUserData()
                        userDetails?.is_trusted = isTrust
                        viewModel.giveRepository().saveUserDataToPref(userDetails!!)
                        moveToScreen()
                    }
                }
            }
        )
        viewModel.mediatorLiveDataVerifyOTPError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                        binding.editVerificationCode.setText("")
                    }

                }
            }

        )


    }

    fun verificationCode() {
        if (
            binding.editVerificationCode.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide verification code.")
            return
        }
        if (Math.abs(prevEventTime - System.currentTimeMillis()) > 600000) {
            showMessageInDialog("OTP Expired.")
            return
        }/*else{

        }*/
        showProgress()
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
                override fun onComplete(task: Task<InstanceIdResult>) {
                    if (!task.isSuccessful) {
                        return
                    }
                    val token = task.result?.token ?: ""
                    Log.v("TOKEN:- ", token)
                    viewModel.verifyOTP(
                        otp = binding.editVerificationCode.getText().toString().trim(),
                        device_token = token,
                        is_trusted = isTrust
                    )
                }
            })

    }

    private fun processSignUpResponse(message: String) {
        hideProgress()
        showMessageInDialog(message)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        timer.cancel()
    }

    private fun setUpTimeoutForResendOtp() {
        timer = object : CountDownTimer(
            120000,
            1000
        ) {
            @SuppressLint("SetTextI18n")
            override fun onFinish() {
                binding.textTimer.text = "(0 sec)"
                binding.textResendCode.setTextColor(resources.getColor(R.color.color_blue))
                binding.textResendCode.isClickable = true
                binding.textResendCode.isEnabled = true
            }

            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                binding.textTimer.text = "(${millisUntilFinished / 1000} sec)"


            }
        }
    }

    private fun moveToScreen() {
        val userDetails = viewModel.giveRepository().getUserData()
        when {
            (userDetails?.completed_steps=="1")->{
                startActivity(Intent(this, CompleteProfileActivity::class.java))
                finish()
            }

            (userDetails?.completed_steps=="2")&& (userDetails.user_role=="4")->{
                startActivity(Intent(this, SOSDocumentSubmitedActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="2" && (userDetails.user_role=="2" ||userDetails.user_role=="3") )->{
                startActivity(Intent(this, PayoutSettingsRegistrationActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="3" && userDetails.user_role=="4")->{
                startActivity(Intent(this, PayoutSettingsRegistrationActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="4") &&
                    (userDetails.user_role=="4") &&
                    (userDetails.user_type.equals("I"))->{

                startActivity(Intent(this, PostATruckActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="4") &&
                    (userDetails.user_role=="4") &&
                    (userDetails.user_type.equals("F"))->{

                startActivity(Intent(this, AddDriverRegistrationActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="4") &&
                    (userDetails.user_role=="2" ||userDetails.user_role=="3")->{
                startActivity(Intent(this, Dashboard::class.java))
                finish()
            }
            (userDetails?.completed_steps=="5") &&
                    (userDetails.user_type.equals("F"))->{
                startActivity(Intent(this, PostATruckActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="6" || userDetails?.completed_steps=="7")->{
                startActivity(Intent(this, Dashboard::class.java))
                finish()
            }
        }
    }
}