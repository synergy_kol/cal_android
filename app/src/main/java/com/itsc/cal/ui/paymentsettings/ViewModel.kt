package com.itsc.cal.ui.paymentsettings

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.AllPayoutData
import com.itsc.cal.model.PayoutSettingResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event
import com.itsc.cal.util.model.Result

class ViewModel(private val repository: Repository) : SuperViewModel(repository) {
    val mediatorLivePayoutAddCard: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLivePayoutAddCardError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLivePayoutAddBank: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLivePayoutAddBankError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLivePayoutSettings: MediatorLiveData<Event<PayoutSettingResponse>> = MediatorLiveData()
    val mediatorLivePayoutSettingsError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveAllPayoutDetails: MediatorLiveData<Event<ArrayList<AllPayoutData>>> = MediatorLiveData()
    val mediatorLiveAllPayoutDetailsError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLivePayoutAddCard.addSource(
            repository.mutableLiveDataPayoutAddCard
        ) { t -> mediatorLivePayoutAddCard.postValue(t) }
        mediatorLivePayoutAddCardError.addSource(
            repository.mutableLiveDataPayoutAddCardError
        ) { t -> mediatorLivePayoutAddCardError.postValue(t) }

        mediatorLivePayoutAddBank.addSource(
            repository.mutableLiveDataPayoutAddBank
        ) { t -> mediatorLivePayoutAddBank.postValue(t) }
        mediatorLivePayoutAddBankError.addSource(
            repository.mutableLiveDataPayoutAddBankError
        ) { t -> mediatorLivePayoutAddBankError.postValue(t) }

        mediatorLivePayoutSettings.addSource(
            repository.mutableLiveDataPayoutSettings
        ) { t -> mediatorLivePayoutSettings.postValue(t) }
        mediatorLivePayoutSettingsError.addSource(
            repository.mutableLiveDataPayoutSettingsError
        ) { t ->  mediatorLivePayoutSettingsError.postValue(t) }

        mediatorLiveAllPayoutDetails.addSource(
            repository.mutableLiveDataAllPayoutDetails
        ) { t -> mediatorLiveAllPayoutDetails.postValue(t) }
        mediatorLiveAllPayoutDetailsError.addSource(
            repository.mutableLiveDataAllPayoutDetailsError
        ) { t ->  mediatorLiveAllPayoutDetailsError.postValue(t) }
    }

    fun payoutAddCard( number: String, expiry_year: String, expiry_month: String,cvv:String) =
        repository.payoutAddCard(
            number = number,
            expiry_year = expiry_year,
            expiry_month = expiry_month,
            cvv = cvv
        )

    fun payoutAddBank( number: String,  routing_number:String,
                       account_holder_name:String) =
        repository.payoutAddBank(
            number = number,
            routing_number = routing_number,
            account_holder_name = account_holder_name
        )

    fun getPayoutSettings() =
        repository.getPayoutSettings()

    fun getAllPayoutDetails() =
        repository.getAllPayoutDetails()
}

