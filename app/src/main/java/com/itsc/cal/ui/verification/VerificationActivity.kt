package com.itsc.cal.ui.verification

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityVerifcationBinding
import com.itsc.cal.model.StatusReply
import com.itsc.cal.model.UserDetails
import com.itsc.cal.ui.add_driver.AddDriverRegistrationActivity
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.forgot_change_password.ChangePasswordActivity
import com.itsc.cal.ui.get_started.GetStartedActivity
import com.itsc.cal.ui.paymentsettings.PayoutSettingsRegistrationActivity
import com.itsc.cal.ui.post_a_truck.PostATruckActivity
import com.itsc.cal.ui.signup.CompleteProfileActivity
import com.itsc.cal.ui.sos_document_submited.SOSDocumentSubmitedActivity
import com.itsc.cal.ui.upload_sos_id_proof.UploadSOSActivity
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class VerificationActivity : CalSuperActivity(), KodeinAware {

    private lateinit var binding: ActivityVerifcationBinding
    private lateinit var timer: CountDownTimer
    override val kodein: Kodein by kodein()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var isFromForgotPassword: Boolean = false
    var prevEventTime:Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_verifcation)
        showAppLogoWithBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        setUpTimeoutForResendOtp()
        startTimeoutForResendOtp()

        initView()

        bindToViewModel()
    }

    private fun getSendOTPAPICalled() {
        showProgress()
        viewModel.resendOTP()
    }

    private fun getSendOTPPasswordTokenAPICalled() {
        showProgress()
        viewModel.forgotPassword(viewModel.giveRepository().getUserData()!!.email?:"")
    }

    private fun initView() {
        prevEventTime = System.currentTimeMillis()
        if (!isFromForgotPassword){
            getSendOTPAPICalled()
        }
        isFromForgotPassword =
            intent.extras!!.getBoolean(KeywordsAndConstants.isFromForgotPassword, false)

        val phoneNumber: Spannable =
            SpannableString(viewModel.giveRepository().getUserData()!!.mobile ?: "")
        phoneNumber.setSpan(
            ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.textBlack, null)),
            0,
            phoneNumber.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        val subText: Spannable = SpannableString(" and email ")
        subText.setSpan(
            ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.gray_dark, null)),
            0,
            subText.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        val emailAddress: Spannable =
            SpannableString(viewModel.giveRepository().getUserData()!!.email ?: "")
        emailAddress.setSpan(
            ForegroundColorSpan(ResourcesCompat.getColor(resources, R.color.textBlack, null)),
            0,
            emailAddress.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        binding.editVerificationCode.apply {
            inputMode(EditTextInputMode.NUMERIC_PASSWORD)
            setMaxLength(4)
        }

        binding.textVerificationSubText.apply {
            text = "Enter a 4-Digit code sent to your mobile "
            append(phoneNumber)
            append(subText)
            append(emailAddress)
        }
        binding.textResendCode.setOnClickListener {
            setUpTimeoutForResendOtp()
            startTimeoutForResendOtp()
            if (isFromForgotPassword){
                getSendOTPPasswordTokenAPICalled()
            }else{
                getSendOTPAPICalled()
            }

        }
        binding.buttonContinue.setOnClickListener {
            if (isFromForgotPassword) {
                verificationPasswordToekn()
            } else {
                verificationCode()
            }

        }
    }

    private fun startTimeoutForResendOtp() {
        if (this::timer.isInitialized) {
            binding.textTimer.setTextColor(resources.getColor(R.color.gray_dark))
            binding.textResendCode.setTextColor(resources.getColor(R.color.gray_dark))
            binding.textResendCode.isClickable = false
            binding.textResendCode.isEnabled = false
            timer.start()
        }
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataResendOTP.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()){
                        prevEventTime = System.currentTimeMillis()
                        binding.editVerificationCode.setText("")
                        processSignUpResponse(/*it.getContent()!!.message*/
                            "Please check your email & phone for OTP. OTP is valid for 10 minutes")
                    }

                }
            }
        )
        viewModel.mediatorLiveDataResendOTPError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataVerifyOTP.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                       moveToScreen()
                    }
                }
            }
        )
        viewModel.mediatorLiveDataVerifyOTPError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                        binding.editVerificationCode.setText("")
                    }

                }
            }

        )

        viewModel.mediatorLiveDataVerifyOTPPasswordToken.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                        startActivity(
                            Intent(this, ChangePasswordActivity::class.java).putExtra(
                                KeywordsAndConstants.isOTP, binding.editVerificationCode.getText().toString()
                            )
                        )
                        finish()
                    }
                }
            }
        )
        viewModel.mediatorLiveDataVerifyOTPPasswordTokenError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                        binding.editVerificationCode.setText("")
                    }

                }
            }
        )

        viewModel.mediatorLiveDataForgotPasswordResend.observe(
            this,
            Observer<Event<UserDetails>> { t ->
                t?.let {
                    if (it.shouldReadContent()){
                        it.readContent()
                        hideProgress()
                        prevEventTime = System.currentTimeMillis()
                        binding.editVerificationCode.setText("")
                        showMessageInDialog("Password Token sent to your mail and phone.")
                    }

                }
            }
        )
        viewModel.mediatorLiveDataForgotPasswordResendError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

    }

    private fun moveToScreen() {
        val userDetails = viewModel.giveRepository().getUserData()
        when {
            (userDetails?.completed_steps=="1")->{
                startActivity(Intent(this, CompleteProfileActivity::class.java))
                finish()
            }

            (userDetails?.completed_steps=="2")&& (userDetails.user_role=="4")->{
                startActivity(Intent(this, SOSDocumentSubmitedActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="2" && (userDetails.user_role=="2" ||userDetails.user_role=="3") )->{
                startActivity(Intent(this, PayoutSettingsRegistrationActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="3" && userDetails.user_role=="4")->{
                startActivity(Intent(this, PayoutSettingsRegistrationActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="4") &&
                    (userDetails.user_role=="4") &&
                    (userDetails.user_type.equals("I"))->{

                startActivity(Intent(this, PostATruckActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="4") &&
                    (userDetails.user_role=="4") &&
                    (userDetails.user_type.equals("F"))->{

                startActivity(Intent(this, AddDriverRegistrationActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="4") &&
                    (userDetails.user_role=="2" ||userDetails.user_role=="3")->{
                startActivity(Intent(this, Dashboard::class.java))
                finish()
            }
            (userDetails?.completed_steps=="5") &&
                    (userDetails.user_type.equals("F"))->{
                startActivity(Intent(this, PostATruckActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="6" || userDetails?.completed_steps=="7")->{
                startActivity(Intent(this, Dashboard::class.java))
                finish()
            }
        }
    }

    fun verificationCode() {
        if (
            binding.editVerificationCode.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide verification code.")
            return
        }
        if (Math.abs(prevEventTime - System.currentTimeMillis()) > 600000) {
            showMessageInDialog("OTP Expired.")
            return
        }
        showProgress()
        viewModel.verifyOTP(binding.editVerificationCode.getText().toString().trim())

    }

    fun verificationPasswordToekn() {
        if (
            binding.editVerificationCode.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide verification code.")
            return
        }
        showProgress()
        viewModel.verifyOTPPasswordToken(binding.editVerificationCode.getText().toString().trim())

    }

    private fun processSignUpResponse(message: String) {
        hideProgress()
        showMessageInDialog(message)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        timer.cancel()
    }

    private fun setUpTimeoutForResendOtp() {
        timer = object : CountDownTimer(
            120000,
            1000
        ) {
            @SuppressLint("SetTextI18n")
            override fun onFinish() {
                binding.textTimer.text = "(0 sec)"
                binding.textResendCode.setTextColor(resources.getColor(R.color.color_blue))
                binding.textResendCode.isClickable = true
                binding.textResendCode.isEnabled = true
            }

            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                binding.textTimer.text = "(${millisUntilFinished / 1000} sec)"


            }
        }
    }
}