package com.itsc.cal.ui.near_by_contractor_list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.NearByContractorListItemBinding
import com.itsc.cal.model.NearByConductorListResponse

class NearByContractorListAdapter(
    private val context: Context,
    private var arrayList: ArrayList<NearByConductorListResponse>
) :
    RecyclerView.Adapter<NearByContractorListAdapter.Holder>(), Filterable {

    var suggestions: ArrayList<NearByConductorListResponse> = ArrayList()
    var tempList: ArrayList<NearByConductorListResponse> =
        arrayList.clone() as ArrayList<NearByConductorListResponse>

    inner class Holder(private val binding: NearByContractorListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(nearByConductorListResponse: NearByConductorListResponse) {
            binding.recyclerView.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = NearByContractorTrucksTypeAdapter(context,nearByConductorListResponse.trucks)
            }
            binding.textViewHeading.text = nearByConductorListResponse.company_name
            binding.textViewPhoneNumber.text = nearByConductorListResponse.mobile
            binding.textViewAddress.text = nearByConductorListResponse.address
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            NearByContractorListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(arrayList.get(position))
    }
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                p0?.let {
                    suggestions.clear()
                    for (filterSuggestion in tempList) {

                        if (filterSuggestion.company_name!!.contains(it, ignoreCase = true))
                            suggestions.add(filterSuggestion)


                    }

                    val result = FilterResults()
                    result.values = suggestions
                    result.count = suggestions.size
                    return result
                } ?: kotlin.run {
                    return FilterResults()
                }
            }

            override fun publishResults(p0: CharSequence?, filterResults: FilterResults?) {
                arrayList = filterResults?.values as ArrayList<NearByConductorListResponse>
                notifyDataSetChanged()

            }

            override fun convertResultToString(resultValue: Any?): CharSequence {
                resultValue?.let {
                    val filterSuggestion = (it as NearByConductorListResponse)
                    return filterSuggestion.company_name?.trim().toString()
                    // return if (filterSuggestion.type == FilterSuggestionType.HOTEL) filterSuggestion.hotel!!.name else filterSuggestion.location!!.name
                } ?: run {
                    return ""
                }
            }
        }
    }
}