package com.itsc.cal.ui.trucks.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.AdapterTruckListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.SearchTruckListResponse

class TruckListAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener,val arrayList:ArrayList<SearchTruckListResponse>):
    RecyclerView.Adapter<TruckListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(AdapterTruckListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))


    }

    inner class ViewHolder(val binding: AdapterTruckListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(searchTruckListResponse: SearchTruckListResponse) {
            binding.tvDriverName.text = searchTruckListResponse.fname+" "+searchTruckListResponse.lanme
            binding.tvPhone.text = searchTruckListResponse.mobile
            binding.tvAddress.text = searchTruckListResponse.address
            binding.tvAddReview.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.tvViewDetails.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }

            binding.ivTruckImage.setImage(imageUrl = "${KeywordsAndConstants.BASE_URL}${searchTruckListResponse.truckimage}")
        }

        private fun setRatingValue(value:Int) {
            when (value) {
                0 -> {
                    binding.ivRatingOne.isSelected = false
                    binding.ivRatingTwo.isSelected = false
                    binding.ivRatingThree.isSelected = false
                    binding.ivRatingFour.isSelected = false
                    binding.ivRatingFive.isSelected = false

                }
                1 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = false
                    binding.ivRatingThree.isSelected = false
                    binding.ivRatingFour.isSelected = false
                    binding.ivRatingFive.isSelected = false

                }
                2 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = true
                    binding.ivRatingThree.isSelected = false
                    binding.ivRatingFour.isSelected = false
                    binding.ivRatingFive.isSelected = false
                }
                3 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = true
                    binding.ivRatingThree.isSelected = true
                    binding.ivRatingFour.isSelected = false
                    binding.ivRatingFive.isSelected = false
                }
                4 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = true
                    binding.ivRatingThree.isSelected = true
                    binding.ivRatingFour.isSelected = true
                    binding.ivRatingFive.isSelected = false
                }
                5 -> {
                    binding.ivRatingOne.isSelected = true
                    binding.ivRatingTwo.isSelected = true
                    binding.ivRatingThree.isSelected = true
                    binding.ivRatingFour.isSelected = true
                    binding.ivRatingFive.isSelected = true
                }
            }
        }

    }

}