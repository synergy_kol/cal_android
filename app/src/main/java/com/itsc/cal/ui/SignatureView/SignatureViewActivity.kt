package com.itsc.cal.ui.SignatureView

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.itsc.cal.R
import com.itsc.cal.databinding.ActivitySignatureViewBinding
import com.itsc.cal.repository.Repository
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SignatureViewActivity : AppCompatActivity(), KodeinAware {
    override val kodein: Kodein by kodein()
    private lateinit var binding:ActivitySignatureViewBinding
    private val repository: Repository by instance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_signature_view)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_signature_view
        )

        initView()
    }
    private fun initView(){
        binding.textViewTitle.setText("Signature View")

        binding.btnSubmit.setOnClickListener {
            setImage()
        }
        binding.imageBack.setOnClickListener {
            finish()
        }
        binding.btnClear.setOnClickListener {
            binding.signatureView.clearCanvas()
        }
    }
    private fun setImage(){
        if (binding.signatureView.isBitmapEmpty) {
          //  showMessageInDialog("Please provide receiver signature.")

            val builder = AlertDialog.Builder(this)
            builder.setTitle("CAL")
            builder.setMessage("Please provide a signature.")
            builder.setPositiveButton("OK") { dialogInterface, which ->

            }
            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
            return
        }
        repository.signatureBitMap = binding.signatureView.signatureBitmap
        finish()
    }
}