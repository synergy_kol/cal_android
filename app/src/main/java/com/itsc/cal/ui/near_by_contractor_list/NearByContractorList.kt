package com.itsc.cal.ui.near_by_contractor_list

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityNearByContractorListBinding
import com.itsc.cal.model.NearByConductorListResponse
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.near_by_contractor_list.adapter.NearByContractorListAdapter
import com.itsc.cal.ui.near_by_contractor_map.NearByContractorMap
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.sagar.android.logutilmaster.LogUtil
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class NearByContractorList : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()

    private val repository: Repository by instance()
    private lateinit var binding: ActivityNearByContractorListBinding
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private val logUtil: LogUtil by instance()
    private lateinit var viewModel: ViewModel
    private val viewModelProvider: ViewModelProvider by instance()
    private  var adapter:NearByContractorListAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_near_by_contractor_list)
        binding.context = this

        showDashboardActionbarWithBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        setPageTitle("Near By Independent Truck/Fleet")
        bindViewHolder()
        getLastLocation()

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    adapter?.getFilter()?.filter(s.toString())

                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })
    }

    @SuppressLint("CheckResult", "MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        RxPermissions(this)
            .request(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .subscribe {
                if (!it) {
                    finish()
                }

                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener { locationResult ->
                        locationResult?.let { location ->
                            logUtil.logV(
                                """
                                    location :
                                    ${location.latitude}
                                    ${location.longitude}
                                """.trimIndent()
                            )
                            this.lastLocation = location
                            getNearByTruck(
                                lastLocation.latitude.toString(),
                                lastLocation.longitude.toString()
                            )
                            Log.i("LatLong", "" + lastLocation)
                        }
                    }
            }
    }

    private fun setUpList(arrayList: ArrayList<NearByConductorListResponse>) {
        binding.textNoDataFound.visibility = View.GONE
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = NearByContractorListAdapter(this@NearByContractorList,arrayList)
        binding.recyclerView.addOnScrollListener(
            object : RecyclerView.OnScrollListener() {

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    if (dy > 0) {
                        // Scrolling up
                        binding.cardViewMapView.visibility = View.GONE
                    } else {
                        // Scrolling down
                        binding.cardViewMapView.visibility = View.GONE
                    }
                }
            }
        )
        binding.recyclerView.adapter = adapter

        adapter?.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                super.onChanged()
                checkEmpty()
            }

            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                checkEmpty()
            }

            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                super.onItemRangeRemoved(positionStart, itemCount)
                checkEmpty()
            }

            fun checkEmpty() {
                binding.textNoDataFound.visibility = (if (adapter?.itemCount == 0) View.VISIBLE else View.GONE)
            }
        })
     /*   binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@NearByContractorList)
            adapter = NearByContractorListAdapter(this@NearByContractorList,arrayList)
            addOnScrollListener(
                object : RecyclerView.OnScrollListener() {

                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)

                        if (dy > 0) {
                            // Scrolling up
                            binding.cardViewMapView.visibility = View.GONE
                        } else {
                            // Scrolling down
                            binding.cardViewMapView.visibility = View.VISIBLE
                        }
                    }
                }
            )
        }*/
    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveNearByTruckList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<NearByConductorListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setUpList(it.getContent() ?: ArrayList())

                    }
                }
            }
        )

        viewModel.mediatorLiveNearByTruckListError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                        binding.textNoDataFound.visibility = View.VISIBLE
                    }

                }
            }
        )
    }

    fun gotoMapView() {
        startActivity(
            Intent(
                this,
                NearByContractorMap::class.java
            )
        )
    }

    private fun getNearByTruck(lat: String, lon: String) {
        showProgress()
        viewModel.getNearByConductor(lat = lat, long = lon, distance = "10")
    }
}