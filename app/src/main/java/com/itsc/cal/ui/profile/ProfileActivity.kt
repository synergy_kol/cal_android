package com.itsc.cal.ui.profile

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityProfileBinding
import com.itsc.cal.model.ProfileDetails
import com.itsc.cal.model.ProfileResponse
import com.itsc.cal.ui.edit_profile.EditProfile
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class ProfileActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()

    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var binding: ActivityProfileBinding
    private lateinit var profileResonse: ProfileResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_profile)
        enableBackButton()
        showAProfileEditActionbar()

        viewModel =
            androidx.lifecycle.ViewModelProvider(this, viewModelProvider).get(ViewModel::class.java)

        bindToViewModel()
    }

    override fun onResume() {
        super.onResume()

        showProgress()
        viewModel.getProfileDetails(viewModel.giveRepository().getUserData()!!.user_master_id)
    }

    override fun editProfile() {
        super.editProfile()

        startActivity(
            Intent(
                this,
                EditProfile::class.java
            )
                .putExtra(
                    KeywordsAndConstants.DATA,
                    toJson(profileResonse)
                )
        )
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataProfileDetails.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        profileResonse = it.getContent()!!
                        initView(profileResonse.profile!!)
                    }
                }
            }
        )
        viewModel.mediatorLiveDataProfileDetailsError.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )
    }

    private fun initView(profileDetails: ProfileDetails) {


        binding.imgProfile.setImage(
            imageUrl = profileDetails.profile_image,
            isCircularImage = true,
            needPlaceHolderImageForName = profileDetails.full_name.split("")[0].toUpperCase(Locale.ROOT)
        )
        if (profileDetails.profile_image!=null){
            var userDetails = viewModel.giveRepository().getUserData()
            userDetails!!.profile_image = profileDetails.profile_image
            viewModel.giveRepository().saveUserDataToPref(userDetails)
        }
        binding.textViewName.text = profileDetails.full_name
        binding.textViewTruck.text = profileDetails.address
        binding.textViewJobs.text = profileDetails.total_job
        binding.textViewRating.text = profileDetails.avarage_rating
        binding.editMobile.setText(profileDetails.mobile)
        binding.editEmailAddress.setText(profileDetails.email.toString())

        val createdYear = profileDetails.created_ts.split(" ")[0].split("-")[0].toInt()
        val createdMonth = profileDetails.created_ts.split(" ")[0].split("-")[1].toInt() - 1
        val createdDate = profileDetails.created_ts.split(" ")[0].split("-")[2].toInt()

        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, createdYear)
        cal.set(Calendar.MONTH, createdMonth)
        cal.set(Calendar.DATE, createdDate)

        val createdDateVar = Date(cal.timeInMillis)

        val diff: Long = Date().time - createdDateVar.time
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        val days = hours / 24
        val weeks = hours / 7
        val years = days / 365

        if (years.toInt() != 0) {
            binding.textViewMembers.text = "$years years"
        } else if (days.toInt() != 0) {
            binding.textViewMembers.text = "$days days"
        } else {
            binding.textViewMembers.text = "$hours hours"
        }

        binding.companyName.text = profileDetails.company_name
        binding.companyRegistration.text = profileDetails.company_registration
        binding.calRegistration.text = profileDetails.cal_registration
    }
}