package  com.itsc.cal.ui.add_driver

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveAddDriver: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveAddDriverError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveAddDriver.addSource(
            repository.mutableLiveDataAddDriver
        ) { t -> mediatorLiveAddDriver.postValue(t) }
        mediatorLiveAddDriverError.addSource(
            repository.mutableLiveDataAddDriverError
        ) { t -> mediatorLiveAddDriverError.postValue(t) }


    }

    fun addDriver(driverName: String, diverLicense: String, driverNumber: String,driverLicenseFile:File?,diverPhoto:File?,email:String) =
        repository.addDriver(
            driverName = driverName,
            diverLicense = diverLicense,
            driverNumber = driverNumber,
            driverLicenseFile = driverLicenseFile,
            diverPhoto = diverPhoto,
            email = email
        )


}