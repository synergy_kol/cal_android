package com.itsc.cal.ui.track_list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityTrackListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.TruckListResponse
import com.itsc.cal.model.TruckTypeResponse
import com.itsc.cal.ui.post_a_truck.PostATruckActivity
import com.itsc.cal.ui.track_list.adapter.TrackListAdapter
import com.itsc.cal.ui.truck_details.TruckDetailsActivity
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class TruckListActivity : CalSuperActivity(withNav = true), KodeinAware {

    private lateinit var binding: ActivityTrackListBinding
    override val kodein: Kodein by kodein()
    private lateinit var viewModel: ViewModel
    private lateinit var truckTypeResponse: TruckTypeResponse
    private val viewModelProvider: ViewModelProvider by instance()
    companion object{
        var truckListing: Activity? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_track_list)
        enableBackButton()
        setPageTitle("Manage Trucks")
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        truckListing = this

        initView()

        bindViewHolder()
    }

    override fun onResume() {
        super.onResume()
        getTruckList()
    }

    private fun getTruckList() {
        showProgress()
        viewModel.getTruckList()
    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveTruckList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<TruckListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent() ?: ArrayList())

                    }
                }
            }
        )

        viewModel.mediatorLiveTruckTypeError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun initView() {
        binding.buttonAddNewTruck.setOnClickListener {
            val intent = Intent(this, PostATruckActivity::class.java)
          //  intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            startActivity(intent)

        }
    }

    private fun setAdapter(arrayList: ArrayList<TruckListResponse>) {
        binding.recycleViewTrackList.apply {
            layoutManager = LinearLayoutManager(this@TruckListActivity)
            adapter = TrackListAdapter(this@TruckListActivity, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    startActivity(
                        Intent(
                            this@TruckListActivity,
                            TruckDetailsActivity::class.java
                        ).putExtra(KeywordsAndConstants.DATA, arrayList.get(position).truck_id)
                    )
                }

            }, arrayList)
        }
    }
}