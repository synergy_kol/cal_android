package  com.itsc.cal.ui.job_list_truck_fleet.fragment

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveJobListing: MediatorLiveData<Event<ArrayList<JobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveJobListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveJobListingAccept: MediatorLiveData<Event<ArrayList<JobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveJobListingAcceptError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveJobListingHiden: MediatorLiveData<Event<ArrayList<JobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveJobListingHidenError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveApplyJobs: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveApplyJobsError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveHideJobs: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveHideJobsError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveShowJobs: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveShowJobsError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveCancelJobs: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveCancelError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDataProfileDetails: MediatorLiveData<Event<ProfileDetails>> =
        MediatorLiveData()
    val mediatorLiveDataProfileDetailsError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()



    init {
        mediatorLiveJobListing.addSource(
            repository.mutableLiveDataJobListingTruckFleet
        ) { t -> mediatorLiveJobListing.postValue(t) }
        mediatorLiveJobListingError.addSource(
            repository.mutableLiveDataJobListingTruckFleetError
        ) { t -> mediatorLiveJobListingError.postValue(t) }

        mediatorLiveJobListingAccept.addSource(
            repository.mutableLiveDataJobListingTruckFleetAccept
        ) { t -> mediatorLiveJobListingAccept.postValue(t) }
        mediatorLiveJobListingAcceptError.addSource(
            repository.mutableLiveDataJobListingTruckFleetAcceptError
        ) { t -> mediatorLiveJobListingAcceptError.postValue(t) }

        mediatorLiveJobListingHiden.addSource(
            repository.mutableLiveDataJobListingTruckFleetHide
        ) { t -> mediatorLiveJobListingHiden.postValue(t) }
        mediatorLiveJobListingHidenError.addSource(
            repository.mutableLiveDataJobListingTruckFleetHideError
        ) { t -> mediatorLiveJobListingHidenError.postValue(t) }

        mediatorLiveApplyJobs.addSource(
            repository.mutableLiveDataApplyJobs
        ) { t -> mediatorLiveApplyJobs.postValue(t) }
        mediatorLiveApplyJobsError.addSource(
            repository.mutableLiveDataApplyJobsError
        ) { t -> mediatorLiveApplyJobsError.postValue(t) }

        mediatorLiveHideJobs.addSource(
            repository.mutableLiveDataHideJobs
        ) { t -> mediatorLiveHideJobs.postValue(t) }
        mediatorLiveHideJobsError.addSource(
            repository.mutableLiveDataHideJobsError
        ) { t -> mediatorLiveHideJobsError.postValue(t) }

        mediatorLiveShowJobs.addSource(
            repository.mutableLiveDataShowJobs
        ) { t -> mediatorLiveShowJobs.postValue(t) }
        mediatorLiveShowJobsError.addSource(
            repository.mutableLiveDataShowJobsError
        ) { t -> mediatorLiveShowJobsError.postValue(t) }

        mediatorLiveCancelJobs.addSource(
            repository.mutableLiveDataCancelJobs
        ) { t -> mediatorLiveCancelJobs.postValue(t) }
        mediatorLiveCancelError.addSource(
            repository.mutableLiveDataCancelJobsError
        ) { t -> mediatorLiveCancelError.postValue(t) }

        mediatorLiveDataProfileDetails.addSource(
            repository.mutableLiveDataProfileDetail
        ) { t -> mediatorLiveDataProfileDetails.postValue(t) }
        mediatorLiveDataProfileDetailsError.addSource(
            repository.mutableLiveDataProfileDetailError
        ) { t -> mediatorLiveDataProfileDetailsError.postValue(t) }
    }

    fun getJobList(filter:String) = repository.getJobListingTruckFleet(filter = filter)
    fun getJobListAccpted(filter:String) = repository.getJobListingTruckFleetAccept(filter = filter)
    fun getJobListHiden(filter:String) = repository.getJobListingTruckFleetHide(filter = filter)
    fun applyJobs(jobId:String) = repository.applyJobs(jobId)
    fun hideJobs(jobId:String) = repository.hideJobs(jobId)
    fun showJobs(jobid:String) = repository.showJobs(jobid)
    fun cancelJobs(jobId: String,cancelReson:String,amount:String) = repository.cancelJobs(jobId,cancelReson,amount)
    fun getProfileDetails(userId:String)=repository.getProfileData(userId = userId)

}