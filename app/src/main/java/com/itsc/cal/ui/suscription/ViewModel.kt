package  com.itsc.cal.ui.suscription

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveSubscriptionList: MediatorLiveData<Event<ArrayList<SubscriptionListResponce>>> =
        MediatorLiveData()
    val mediatorLiveSubscriptionListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveCirtificationList: MediatorLiveData<Event<ArrayList<SubscriptionListResponce>>> =
        MediatorLiveData()
    val mediatorLiveCertificationListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveSubscriptionList.addSource(
            repository.mutableLiveDataSubscriptionListResult
        ) { t -> mediatorLiveSubscriptionList.postValue(t) }
        mediatorLiveSubscriptionListError.addSource(
            repository.mutableLiveDataSubscriptionListError
        ) { t -> mediatorLiveSubscriptionListError.postValue(t) }

        mediatorLiveCirtificationList.addSource(
            repository.mutableLiveDataCirtificationListResult
        ) { t -> mediatorLiveCirtificationList.postValue(t) }
        mediatorLiveCertificationListError.addSource(
            repository.mutableLiveDataCirtificationListError
        ) { t -> mediatorLiveCertificationListError.postValue(t) }


    }

    fun getSubscriptionListDetails(city_id: String) =
        repository.getSubscriptionListDetails(city_id = city_id)

    fun getCertificationDetails(city_id: String) =
        repository.getCertificationDetails(city_id = city_id)


}