package com.itsc.cal.ui.post_hault_of_job_list.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.application.ApplicationClass
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.FragmentPostedJobBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.HauljobListResponse
import com.itsc.cal.ui.post_hault_of_job_list.PostHaultOfJobList
import com.itsc.cal.ui.post_hault_of_job_list.adapter.ExportImportJobAdapter
import com.itsc.cal.ui.post_hault_of_job_list.import_export_job_details.ImportExportJobDetails
import com.itsc.cal.ui.post_hault_of_job_list.matches_jobs.MatchesJobList
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class HaultOfJobListFragment : Fragment(), KodeinAware {

    private lateinit var binding: FragmentPostedJobBinding
    private lateinit var mPostedJobAdapter: ExportImportJobAdapter
    private lateinit var baseActivity: PostHaultOfJobList
    override lateinit var kodein: Kodein
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var filter = "1"
    private var string_tag = "0"

    companion object {
        fun newInstance(filter: String): HaultOfJobListFragment {
            val mPostedJobFragment = HaultOfJobListFragment()
            val bundle = Bundle()
            bundle.putString("filter", filter)
            mPostedJobFragment.arguments = bundle
            return mPostedJobFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_posted_job, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        baseActivity = activity as PostHaultOfJobList
        kodein = (baseActivity.applicationContext as ApplicationClass).kodein
        viewModel = androidx.lifecycle.ViewModelProvider(
            this,
            viewModelProvider
        )
            .get(
                ViewModel::class.java
            )


        bindToViewModel()

    }


    private fun getFilterId(): String {
        return arguments?.getString("filter") ?: ""
    }

    public fun getJobListing(filter: String) {
        Log.i("Filter:", filter)
        binding.imgTraView.visibility = View.VISIBLE
        (baseActivity as PostHaultOfJobList).showProgress()
        this.filter = filter
        this.string_tag = filter
        if (filter.equals("0")) {
            viewModel.exportJobListing("1", string_tag)
        } else if (filter.equals("1")) {
            viewModel.exportJobListing("2", string_tag)
        } else if (filter.equals("2")) {
            viewModel.importJobListing("1", string_tag)
        } else if (filter.equals("3")) {
            viewModel.importJobListing("2", string_tag)
        }

    }

    fun showTransparentView() {
        binding.imgTraView.visibility = View.VISIBLE
        binding.textviewNoDataFound.visibility = View.GONE
    }


    private fun bindToViewModel() {
        viewModel.mediatorLiveExportJobListing.observe(
            baseActivity,
            Observer<Event<ArrayList<HauljobListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        val listToPush: ArrayList<HauljobListResponse> = ArrayList()
                        it.getContent()!!.forEach { job ->
                            if (job.filter == string_tag) {
                                listToPush.add(job)
                            }
                        }
                        (baseActivity as PostHaultOfJobList).hideProgress()
                        setPostedJobAdapterExport(listToPush)
                    }
                }
            }
        )
        viewModel.mediatorLiveExportListingError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity as PostHaultOfJobList).hideProgress()
                        if (it.getContent()!!.getErrorCode() == filter.toInt()) {
                            binding.rvPostedJob.visibility = View.GONE
                            binding.textviewNoDataFound.visibility = View.VISIBLE
                        }

                    }

                }
            }
        )

        viewModel.mediatorLiveImportJobListing.observe(
            baseActivity,
            Observer<Event<ArrayList<HauljobListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        val listToPush: ArrayList<HauljobListResponse> = ArrayList()
                        it.getContent()!!.forEach { job ->
                            if (job.filter == string_tag) {
                                listToPush.add(job)
                            }
                        }
                        (baseActivity as PostHaultOfJobList).hideProgress()
                        setPostedJobAdapterImport(listToPush)
                    }

                }
            }
        )
        viewModel.mediatorLiveImportListingError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity as PostHaultOfJobList).hideProgress()
                        if (it.getContent()!!.getErrorCode() == filter.toInt()) {
                            binding.rvPostedJob.visibility = View.GONE
                            binding.textviewNoDataFound.visibility = View.VISIBLE
                        }

                    }

                }
            }
        )
    }

    override fun onResume() {
        super.onResume()
        binding.imgTraView.visibility = View.VISIBLE
        binding.rvPostedJob.visibility = View.GONE
        binding.textviewNoDataFound.visibility = View.GONE
    }

    private fun setPostedJobAdapterExport(exportHaulOffResponse: ArrayList<HauljobListResponse>) {
        if (exportHaulOffResponse.size ?: 0 > 0) {
            binding.textviewNoDataFound.visibility = View.GONE
            var arrayList: ArrayList<HauljobListResponse> = ArrayList()

            for (i in 0 until (exportHaulOffResponse.size ?: 0)) {
                arrayList.add(exportHaulOffResponse.get(i))
            }


            binding.rvPostedJob.visibility = View.VISIBLE
            binding.imgTraView.visibility = View.GONE
            var userId = viewModel.giveRepository().getUserData()?.user_master_id
            mPostedJobAdapter = ExportImportJobAdapter(baseActivity, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    when (view.id) {
                        R.id.cvPostedJob -> {
                            val intent = Intent(baseActivity, ImportExportJobDetails::class.java)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                (baseActivity).toJson(arrayList.get(position))
                            )
                            startActivity(intent)
                        }

                        R.id.text_import_request -> {
                            val intent = Intent(baseActivity, MatchesJobList::class.java)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                (baseActivity).toJson(arrayList.get(position))
                            )
                            intent.putExtra(KeywordsAndConstants.ACCEPT_JOB, "1")
                            startActivity(intent)
                        }
                    }
                }

            }, arrayList, userId)
            val layoutManager =
                LinearLayoutManager(baseActivity, LinearLayoutManager.VERTICAL, false)
            binding.rvPostedJob.layoutManager = layoutManager
            binding.rvPostedJob.adapter = mPostedJobAdapter
        } else {
            binding.textviewNoDataFound.visibility = View.VISIBLE
            binding.rvPostedJob.visibility = View.GONE
            binding.imgTraView.visibility = View.VISIBLE
        }

    }

    private fun setPostedJobAdapterImport(importHaulOffResponse: ArrayList<HauljobListResponse>) {
        if (importHaulOffResponse.size ?: 0 > 0) {
            binding.textviewNoDataFound.visibility = View.GONE
            var arrayList: ArrayList<HauljobListResponse> = ArrayList()

            for (i in 0 until (importHaulOffResponse.size ?: 0)) {
                arrayList.add(importHaulOffResponse.get(i))
            }


            binding.rvPostedJob.visibility = View.VISIBLE
            binding.imgTraView.visibility = View.GONE
            var userId = viewModel.giveRepository().getUserData()?.user_master_id
            mPostedJobAdapter = ExportImportJobAdapter(baseActivity, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    when (view.id) {
                        R.id.cvPostedJob -> {
                            val intent = Intent(baseActivity, ImportExportJobDetails::class.java)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                (baseActivity).toJson(arrayList.get(position))
                            )
                            startActivity(intent)
                        }

                        R.id.text_import_request -> {
                            val intent = Intent(baseActivity, MatchesJobList::class.java)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                (baseActivity).toJson(arrayList.get(position))
                            )
                            intent.putExtra(KeywordsAndConstants.ACCEPT_JOB, "2")
                            startActivity(intent)
                        }
                    }
                }

            }, arrayList, userId)
            val layoutManager =
                LinearLayoutManager(baseActivity, LinearLayoutManager.VERTICAL, false)
            binding.rvPostedJob.layoutManager = layoutManager
            binding.rvPostedJob.adapter = mPostedJobAdapter
        } else {
            binding.textviewNoDataFound.visibility = View.VISIBLE
            binding.rvPostedJob.visibility = View.GONE
            binding.imgTraView.visibility = View.VISIBLE
        }

    }

    private fun moveToJobDetails() {

    }

}