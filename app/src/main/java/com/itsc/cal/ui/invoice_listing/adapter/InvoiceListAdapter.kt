package com.itsc.cal.ui.invoice_listing.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildInvoiceListingBinding
import com.itsc.cal.interfaces.OnItemClickListener

class InvoiceListAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener):
    RecyclerView.Adapter<InvoiceListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(ChildInvoiceListingBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return 12
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData()


    }

    inner class ViewHolder(val binding: ChildInvoiceListingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData() {

        }

    }

}