package com.itsc.cal.ui.builders_details

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityBuilderDetailsBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.BuilderListResponse
import com.itsc.cal.model.PostJobRequest
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.builders_details.adapter.BuilderDetailsAdapter
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.manage_builders.ManageBuildersActivity
import com.itsc.cal.ui.profile_builder.ProfileBuilderActivity
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class BuilderDetailsActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var binding: ActivityBuilderDetailsBinding
    private lateinit var postJobRequest: PostJobRequest
    private var isPostJobs = false
    var type:String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_builder_details)
        enableBackButton()
        showBuilderDetailsActionbar()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        setPageTitle("Manage Builders")
        initView()
        bindViewModel()
    }

    private fun initView() {
        intent?.let {
            isPostJobs = intent.getBooleanExtra(KeywordsAndConstants.isPostJob, false)
            if (isPostJobs) {
                intent?.let {
                    postJobRequest = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))

                }
            }
            if (isPostJobs){
                if (viewModel.giveRepository().getUserData()!!.user_role.equals("3")){
                    getBuilderList()
                    setPageTitle("Tag Builder")
                    type = "builder"
                    binding.buttonAddNewBuilder.visibility = View.VISIBLE
                }else{
                    getSupplierList()
                    setPageTitle("Tag Supplier")
                    type = "supplier"
                    binding.buttonAddNewBuilder.visibility = View.GONE
                }
            }else{
                getBuilderList()
            }

        }


        binding.buttonAddNewBuilder.setOnClickListener {
            startActivity(Intent(this, ManageBuildersActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()



    }

    private fun getBuilderList() {
        showProgress()
        viewModel.getBuilderList()
    }

    private fun getSupplierList() {
        showProgress()
        viewModel.getSupplierList()
    }

    private fun bindViewModel() {
        viewModel.mediatorLiveBuilderListing.observe(
            this,
            Observer<Event<ArrayList<BuilderListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent() ?: ArrayList())
                    }
                }
            }
        )
        viewModel.mediatorLiveBuilderListingError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                        binding.textNoDataFound.visibility = View.VISIBLE
                        binding.recycleViewBuilderDetails.visibility = View.GONE
                    }

                }
            }
        )

        viewModel.mediatorLiveSupplierListing.observe(
            this,
            Observer<Event<ArrayList<BuilderListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent() ?: ArrayList())
                    }
                }
            }
        )
        viewModel.mediatorLiveSupplierListingError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                        binding.textNoDataFound.visibility = View.VISIBLE
                        binding.recycleViewBuilderDetails.visibility = View.GONE
                    }

                }
            }
        )

        viewModel.mediatorLivePostJob.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLivePostJobError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@BuilderDetailsActivity,
                                Dashboard::class.java
                            )
                        )
                    )
                    finish()

                }
            }
        )

    }

    private fun setAdapter(arrayList: ArrayList<BuilderListResponse>) {
        binding.recycleViewBuilderDetails.apply {
            layoutManager = LinearLayoutManager(this@BuilderDetailsActivity)
            adapter =
                BuilderDetailsAdapter(this@BuilderDetailsActivity, object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        if (!isPostJobs) {
                            startActivity(
                                Intent(
                                    this@BuilderDetailsActivity,
                                    ProfileBuilderActivity::class.java
                                ).putExtra(KeywordsAndConstants.IS_OWN_PROFILE, false).putExtra(
                                    KeywordsAndConstants.DATA,
                                    toJson(arrayList.get(position))
                                )
                            )
                        } else {

                            showMessageWithTwoButton(
                                message = "Are you want to tag this ${type} in this job?",
                                cancellable = true,
                                buttonOneText = "Yes",
                                buttonTwoText = "No",
                                callback = object : DialogUtil.MultiButtonCallBack {
                                    override fun buttonOneClicked() {
                                        super.buttonOneClicked()
                                        apiCalled(arrayList.get(position).user_master_id)
                                    }

                                    override fun buttonTwoClicked() {
                                        super.buttonTwoClicked()
                                        dialogCancelled()
                                    }
                                }
                            )
                        }

                    }

                }, arrayList)
        }

    }

    private fun apiCalled(builderId: String) {
        showProgress()
        viewModel.postJob(
            source_lat = postJobRequest?.source_lat.toString(),
            source_long = postJobRequest?.source_long.toString(),
            destination_lat = postJobRequest?.destination_lat.toString(),
            destination_long = postJobRequest?.destination_long.toString(),
            delivery_date = postJobRequest.delivery_date,
            delivery_time = postJobRequest.delivery_time,
            Destination = postJobRequest.Destination,
            Equipment = postJobRequest.Equipment,
            FreightCharges = postJobRequest.FreightCharges,
            JobEstimatePrice = postJobRequest.JobEstimatePrice,
            LodeType = postJobRequest.LodeType,
            MaterialType = postJobRequest.MaterialType,
            NoOfTrucks = postJobRequest.NoOfTrucks,
            pickup_date = postJobRequest.pickup_date,
            pickup_time = postJobRequest.pickup_time,
            priority = postJobRequest.priority,
            Source = postJobRequest.Source,
            TruckType = postJobRequest.TruckType,
            Weight = postJobRequest.Weight,
            Equipment_value = postJobRequest.Equipment_value,
            builderId = builderId,
            card_id = postJobRequest.card_id,
            payment_type = postJobRequest.payment_type,
            stripeToken = postJobRequest.stripeToken,
            jobPayableAmount = postJobRequest.jobPayableAmount
        )
    }
}