package  com.itsc.cal.ui.driver_job_details

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataEmptyTruck: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataEmptyTruckError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDataEmptyTruck.addSource(
            repository.mutableLiveEmptyTruck
        ) { t -> mediatorLiveDataEmptyTruck.postValue(t) }
        mediatorLiveDataEmptyTruckError.addSource(
            repository.mutableLiveEmptyTrucError
        ) { t -> mediatorLiveDataEmptyTruckError.postValue(t) }


    }

    fun emptyTruckImage(challan: File?, token: String,truck_weight:String) =
        repository.emptyTruckImage(challan = challan, token = token,truck_weight =truck_weight )

}
