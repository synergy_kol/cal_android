package  com.itsc.cal.ui.track_status

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveTruckStatusList: MediatorLiveData<Event<ArrayList<TruckStatusResponse>>> =
        MediatorLiveData()
    val mediatorLiveTruckStatusListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveTruckStatusList.addSource(
            repository.mutableLiveDataTruckStatusList
        ) { t -> mediatorLiveTruckStatusList.postValue(t) }
        mediatorLiveTruckStatusListError.addSource(
            repository.mutableLiveDataTruckStatusListError
        ) { t -> mediatorLiveTruckStatusListError.postValue(t) }


    }

    fun getTruckStatus(job_id: String, builder_id: String) =
        repository.getTruckStatus(job_id = job_id, builder_id = builder_id)


}