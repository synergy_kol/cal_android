package  com.itsc.cal.ui.post_hault_of_job_list.post_export_job_three

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {


    val mediatorLiveMaterialEquipementResult: MediatorLiveData<Event<ArrayList<EquipementResponse>>> =
        MediatorLiveData()
    val mediatorLiveMaterialEquipementError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveSpreadingEquipementResult: MediatorLiveData<Event<ArrayList<EquipementResponse>>> =
        MediatorLiveData()
    val mediatorLiveSpreadingEquipementError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveHaulJobExportStep3: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataJobExportStep3Error: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveHaulJobImportStep2: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataJobImportStep2Error: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveImportExportData: MediatorLiveData<Event<HauljobListResponse>> = MediatorLiveData()
    val mediatorLiveImportExportError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {

        mediatorLiveMaterialEquipementResult.addSource(
            repository.mutableLiveDataLoadingEquipment
        ) { t -> mediatorLiveMaterialEquipementResult.postValue(t) }
        mediatorLiveMaterialEquipementError.addSource(
            repository.mutableLiveDataLoadingEquiementError
        ) { t -> mediatorLiveMaterialEquipementError.postValue(t) }

        mediatorLiveSpreadingEquipementResult.addSource(
            repository.mutableLiveDataSpreadingEquipment
        ) { t -> mediatorLiveSpreadingEquipementResult.postValue(t) }
        mediatorLiveSpreadingEquipementError.addSource(
            repository.mutableLiveDataSpreadingEquiementError
        ) { t -> mediatorLiveSpreadingEquipementError.postValue(t) }


        mediatorLiveHaulJobExportStep3.addSource(
            repository.mutableLiveDataJobExportStep3
        ) { t -> mediatorLiveHaulJobExportStep3.postValue(t) }
        mediatorLiveDataJobExportStep3Error.addSource(
            repository.mutableLiveDataJobExportStep3Error
        ) { t -> mediatorLiveDataJobExportStep3Error.postValue(t) }

        mediatorLiveHaulJobImportStep2.addSource(
            repository.mutableLiveDataJobImportStep2
        ) { t -> mediatorLiveHaulJobImportStep2.postValue(t) }
        mediatorLiveDataJobImportStep2Error.addSource(
            repository.mutableLiveDataJobImportStep2Error
        ) { t -> mediatorLiveDataJobImportStep2Error.postValue(t) }

        mediatorLiveImportExportData.addSource(
            repository.mutableLiveDataSearchImportExportDetails
        ) { t -> mediatorLiveImportExportData.postValue(t) }
        mediatorLiveImportExportError.addSource(
            repository.mutableLiveDataSearchImportExportError
        ) { t -> mediatorLiveImportExportError.postValue(t) }

    }


    fun getMaterialEquipmentList() = repository.getLoadingEquipmentOptionList()

    fun getSpreadingEquipmentList() = repository.getSpreadingEquipmentOptionList()

    fun getJobDetails(jobId:String){
        repository.matchesJobDetails(jobId)
    }

    fun postExportJobStep3(
        haul_request_id: String,
        equipment_type: String,
        equipment_other: String,
        equipment_any: String,
        additional_notes: String
    ) =
        repository.postExportStep3(
            haul_request_id = haul_request_id,
            equipment_type = equipment_type,
            equipment_other = equipment_other,
            equipment_any = equipment_any,
            additional_notes = additional_notes

        )


    fun postImportJobStep2(
        haul_request_id: String,
        equipment_type: String,
        equipment_other: String,
        equipment_any: String,
        additional_notes: String,
        material_other:String,
        MaterialType:String,
        spreading_equipment_other:String,
        tons:String,
        cubic_yards:String,
        MaterialoptionType:String,
        spreading_equipment_type:String
    ) =
        repository.postImporStep2(
            haul_request_id = haul_request_id,
            equipment_type = equipment_type,
            equipment_other = equipment_other,
            equipment_any = equipment_any,
            additional_notes = additional_notes,
            material_other = material_other,
            MaterialType = MaterialType,
            spreading_equipment_type = spreading_equipment_type,
            spreading_equipment_other = spreading_equipment_other,
            tons = tons,
            cubic_yards = cubic_yards,
            MaterialoptionType = MaterialoptionType
        )

}