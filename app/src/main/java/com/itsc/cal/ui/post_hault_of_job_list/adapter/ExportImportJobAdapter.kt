package com.itsc.cal.ui.post_hault_of_job_list.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.databinding.AdapterImportExportBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.HauljobListResponse

class ExportImportJobAdapter(
    val mContext: Context,
    val onItemClickListener: OnItemClickListener,
    val arrayList: ArrayList<HauljobListResponse>,
    val userId: String? = null
) :
    RecyclerView.Adapter<ExportImportJobAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            AdapterImportExportBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))

    }

    inner class ViewHolder(val binding: AdapterImportExportBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(jobListResponse: HauljobListResponse) {
            binding.tvSource.text = jobListResponse.address
            binding.tvJobId.text = "#CAL${jobListResponse.haul_request_id}"
            //  binding.tvDestination.text=jobListResponse.Destination
            binding.tvWeight.text = "${jobListResponse.cubic_yards} Cubic"
            binding.tvWeightTons.text = "${jobListResponse.tons} TONS"

            binding.tvEquipment.text =
                if (jobListResponse.loading_equipment_name == null || jobListResponse.loading_equipment_name == "")
                    "No" else
                    "${jobListResponse.loading_equipment_name}"

            if (jobListResponse.material_option_name == null || jobListResponse.material_option_name == "") {
                binding.tvMaterialType.text =
                    "${jobListResponse.material_name}"
            } else {
                binding.tvMaterialType.text =
                    "${jobListResponse.material_name}(${jobListResponse.material_option_name})"
            }

            binding.tvTruckPayInText.text = "Point of Contact: ${jobListResponse.point_of_contact}"
            binding.tvTruckPayIn.text = "Main Number: ${jobListResponse.main_number}"
            binding.tvEmail.text = "Email: ${jobListResponse.email}"
            binding.cvPostedJob.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
            binding.textImportRequest.setOnClickListener {
                Log.i("test,","212121")
                onItemClickListener.onItemClick(it, adapterPosition)
            }
            if (userId.equals(jobListResponse.user_id)) {
                binding.cvPostedJob.setBackgroundColor(mContext.resources.getColor(R.color.text_sky_light_blue))

                if (jobListResponse.import_requests?.size ?: 0 > 0) {
                    binding.textImportRequest.visibility = View.VISIBLE
                    binding.textImportRequest.setText(
                        (jobListResponse.import_requests?.size ?: 0).toString()
                    )
                } else {
                    if (jobListResponse.export_requests?.size ?: 0 > 0) {
                        binding.textImportRequest.visibility = View.VISIBLE
                        binding.textImportRequest.setText(
                            (jobListResponse.export_requests?.size ?: 0).toString()
                        )
                    } else {
                        binding.textImportRequest.visibility = View.GONE
                    }
                }
            } else {
                binding.cvPostedJob.setBackgroundColor(mContext.resources.getColor(R.color.white))
                binding.textImportRequest.visibility = View.GONE
            }


            /*  if (jobListResponse.job_status.equals("3")){
                  binding.btnReview.visibility = View.VISIBLE
              }else{
                  binding.btnReview.visibility = View.GONE
              }
              binding.btnReview.setOnClickListener {
                  onItemClickListener.onItemClick(it,adapterPosition)
              }*/

        }

    }

}