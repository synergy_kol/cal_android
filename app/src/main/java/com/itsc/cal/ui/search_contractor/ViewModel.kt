package  com.itsc.cal.ui.search_contractor

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveBuilderListing: MediatorLiveData<Event<ArrayList<BuilderListResponse>>> =
        MediatorLiveData()
    val mediatorLiveBuilderListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveBuilderListing.addSource(
            repository.mutableLiveDataBuilderList
        ) { t -> mediatorLiveBuilderListing.postValue(t) }
        mediatorLiveBuilderListingError.addSource(
            repository.mutableLiveDataBuilderListError
        ) { t -> mediatorLiveBuilderListingError.postValue(t) }

    }

    fun getBuilderList() = repository.getBuilderList()


}