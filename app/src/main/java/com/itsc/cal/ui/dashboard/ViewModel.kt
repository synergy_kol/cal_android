package  com.itsc.cal.ui.dashboard

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.BannerResponce
import com.itsc.cal.model.DashboardStatus
import com.itsc.cal.model.HaulofSubscriptionStatusResponce
import com.itsc.cal.model.NotificationResponce
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveNotificationResponse: MediatorLiveData<Event<NotificationResponce>> = MediatorLiveData()
    val mediatorLiveNotificationResponseError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveFleetUserDashBoardResponse: MediatorLiveData<Event<DashboardStatus>> = MediatorLiveData()
    val mediatorLiveFleetUserDashBoardResponseError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveHaultOfSubscriptionResponse: MediatorLiveData<Event<HaulofSubscriptionStatusResponce>> = MediatorLiveData()
    val mediatorLiveHaultOfSubscriptionResponseError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveBannerResponse: MediatorLiveData<Event<ArrayList<BannerResponce>>> = MediatorLiveData()
    val mediatorLiveBannerResponseError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveNotificationResponse.addSource(
            repository.mutableLiveDataNotificationDetails
        ) { t -> mediatorLiveNotificationResponse.postValue(t) }
        mediatorLiveNotificationResponseError.addSource(
            repository.mutableLiveDataNotificationDetailsError
        ) { t -> mediatorLiveNotificationResponseError.postValue(t) }

        mediatorLiveFleetUserDashBoardResponse.addSource(
            repository.mutableLiveDataFleetUserDashboardData
        ) { t -> mediatorLiveFleetUserDashBoardResponse.postValue(t) }
        mediatorLiveFleetUserDashBoardResponseError.addSource(
            repository.mutableLiveDataFleetUserDashboardDataError
        ) { t -> mediatorLiveFleetUserDashBoardResponseError.postValue(t) }

        mediatorLiveHaultOfSubscriptionResponse.addSource(
            repository.mutableLiveDataHaultOfJobDashboardData
        ) { t -> mediatorLiveHaultOfSubscriptionResponse.postValue(t) }
        mediatorLiveHaultOfSubscriptionResponseError.addSource(
            repository.mutableLiveDataHaultOfJobDashboardError
        ) { t -> mediatorLiveHaultOfSubscriptionResponseError.postValue(t) }

        mediatorLiveBannerResponse.addSource(
            repository.mutableLiveDataGetBannerListResult
        ) { t -> mediatorLiveBannerResponse.postValue(t) }
        mediatorLiveBannerResponseError.addSource(
            repository.mutableLiveDataGetBannerListError
        ) { t -> mediatorLiveBannerResponseError.postValue(t) }

    }

    fun getNotification(){
        repository.getNotification()
    }

    fun getBannerList(){
        repository.getBannerList()
    }
    fun getFleetUserDashboardStatus(){
        repository.fleetcheckDashboardStatus()
    }

    fun getHaulofSubscriptionStatus(){
        repository.getHaulofSubscriptionStatus()
    }
}
