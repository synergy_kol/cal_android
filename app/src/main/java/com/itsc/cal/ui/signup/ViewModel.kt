package  com.itsc.cal.ui.signup

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataSignup: MediatorLiveData<Event<UserDetails>> = MediatorLiveData()
    val mediatorLiveDataSignupError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveCityList: MediatorLiveData<Event<ArrayList<CityResponce>>> =
        MediatorLiveData()
    val mediatorLiveCityListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveStateList: MediatorLiveData<Event<ArrayList<StateResponce>>> =
        MediatorLiveData()
    val mediatorLiveStateListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveDataCompleteInfo: MediatorLiveData<Event<UserDetails>> = MediatorLiveData()
    val mediatorLiveDataCompleteInfoError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDataSignup.addSource(
            repository.mutableLiveDataSignup
        ) { t -> mediatorLiveDataSignup.postValue(t) }
        mediatorLiveDataSignupError.addSource(
            repository.mutableLiveDataSignUpError
        ) { t -> mediatorLiveDataSignupError.postValue(t) }

        mediatorLiveCityList.addSource(
            repository.mutableLiveDataCityList
        ) { t -> mediatorLiveCityList.postValue(t) }
        mediatorLiveCityListError.addSource(
            repository.mutableLiveDataCityListError
        ) { t -> mediatorLiveCityListError.postValue(t) }

        mediatorLiveStateList.addSource(
            repository.mutableLiveDataStateList
        ) { t -> mediatorLiveStateList.postValue(t) }
        mediatorLiveStateListError.addSource(
            repository.mutableLiveDataStateListError
        ) { t -> mediatorLiveStateListError.postValue(t) }

        mediatorLiveDataCompleteInfo.addSource(
            repository.mutableLiveDataCompleteInfo
        ) { t -> mediatorLiveDataCompleteInfo.postValue(t) }
        mediatorLiveDataCompleteInfoError.addSource(
            repository.mutableLiveDataCompleteInfoError
        ) { t -> mediatorLiveDataCompleteInfoError.postValue(t) }

    }

    fun signUp(
        email: String,
        password: String,
        deviceToken: String,
        fname: String,
        lname: String,
        user_role: String,
        license:String,
        company_registration:String,
        company_name:String,
        mobile:String,
        numberof_truck:String,
        user_type:String,
        latitude:String,
        longitude:String,
        city:String,
        state:String
    ) =
        repository.signUp(
            email = email,
            passowrd = password,
            deviceToken = deviceToken,
            fname = fname,
            lname = lname,
            user_role = user_role,
            license = license,
            company_registration = company_registration,
            company_name = company_name,
            mobile = mobile,
            numberof_truck = numberof_truck,
            user_type = user_type,
            latitude = latitude,
            longitude = longitude,
            city = city,
            state = state
        )

    fun getCity(stateId:String) = repository.getCityList(stateId)
    fun getState() = repository.getStateList()

    fun completeInfo(
        dob: String,
        companyLegalName: String,
        companyDba: String,
        phone2: String,
        email2: String,
        companyEin: String,
        driverLicense: String,
        driverLicenseExp: String,
        street_address: String,
        zip: String,
        is_billing_address: String,
        bStreetAddress: String,
        city: String,
        state: String,
        b_zip: String
    ) = repository.completeInfo(
        dob = dob,
        companyLegalName = companyLegalName,
        companyDba=companyDba,
        phone2=phone2,
        email2=email2,
        companyEin=companyEin,
        driverLicense = driverLicense,
        driverLicenseExp=driverLicenseExp,
        street_address = street_address,
        zip = zip,
        is_billing_address = is_billing_address,
        bStreetAddress = bStreetAddress,
        city = city,
        state = state,
        b_zip = b_zip
    )
}