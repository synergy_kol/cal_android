package com.itsc.cal.ui.builder_my_jobs_list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityBuilderMyJobsListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse
import com.itsc.cal.ui.builder_my_jobs_list.adapter.BuilderMyJobAdapter
import com.itsc.cal.ui.job_details.JobDetails
import com.itsc.cal.ui.submit_review.SubmitReviewActivity
import com.itsc.cal.ui.track_status.TrackStatusActivity
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class BuilderMyJobsList : CalSuperActivity(), KodeinAware {
    private lateinit var binding: ActivityBuilderMyJobsListBinding
    private var isBuilderJobs = false
    override val kodein: Kodein by kodein()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_builder_my_jobs_list)
        enableBackButton()
        setPageTitle("Job List")
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        showHelpButton()
        showDashboardActionbarWithBackButton()
        initView()
        bindToViewModel()
    }

    private fun initView() {
        isBuilderJobs = intent.getBooleanExtra(KeywordsAndConstants.IS_BUILDER_JOBS, false)
        callAPI()
    }

    private fun callAPI() {
        showProgress()
        viewModel.getJobList()
    }

    fun bindToViewModel() {
        viewModel.mediatorLiveJobBuilderListing.observe(
            this,
            Observer<Event<ArrayList<JobListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setPostedJobAdapter(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveJobListingBuilderError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                        binding.textNoDataFound.visibility = View.VISIBLE
                        binding.recycleviewMyJobList.visibility = View.GONE
                    }

                }
            }
        )

    }

    private fun setPostedJobAdapter(arrayList: ArrayList<JobListResponse>) {
        binding.recycleviewMyJobList.apply {
            layoutManager = LinearLayoutManager(this@BuilderMyJobsList)
            adapter = BuilderMyJobAdapter(this@BuilderMyJobsList, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {

                    when(view.id){
                        R.id.cvPostedJob->{
                            val intent = Intent(this@BuilderMyJobsList, JobDetails::class.java)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                toJson(arrayList.get(position))
                            )
                            intent.putExtra(KeywordsAndConstants.isFromBooking,false)
                            startActivity(intent)
                        }
                        R.id.btn_tracking->{
                            val intent = Intent(this@BuilderMyJobsList, TrackStatusActivity::class.java)
                            intent.putExtra(KeywordsAndConstants.isJobListing,true)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                toJson(arrayList.get(position))
                            )
                            startActivity(intent)
                        }
                        R.id.btn_review->{

                            val intent = Intent(this@BuilderMyJobsList, SubmitReviewActivity::class.java)
                            intent.putExtra(
                                KeywordsAndConstants.DATA,
                                toJson(arrayList.get(position))
                            )
                            startActivity(intent)
                        }
                    }

                }

            }, isBuilderJobs, arrayList)
        }
    }
}
