package com.itsc.cal.ui.dashboard_change_password

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityDashboardChangePasswordBinding
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class DashboardChangePasswordActivity : CalSuperActivity(withNav = true), KodeinAware {

    private lateinit var binding:ActivityDashboardChangePasswordBinding
    override val kodein: Kodein by kodein()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_dashboard_change_password)
        setPageTitle("Change password")
        showHelpButton()
        enableBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        initView()
        bindToViewModel()
    }
    private fun initView(){

        binding.editOldPassword.apply {
            hint("Enter Old Password")
            inputMode(EditTextInputMode.PASSWORD)
        }

        binding.editPassword.apply {
            hint("Enter New Password")
            inputMode(EditTextInputMode.PASSWORD)
        }


        binding.editConPassword.apply {
            hint("Enter Confirm Password")
            inputMode(EditTextInputMode.PASSWORD)
        }
        binding.buttonSubmit.setOnClickListener {
            apiCalled()
        }
    }

    private fun apiCalled() {

        if (
            binding.editOldPassword.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Old Password.")
            return
        }
        if (
            binding.editPassword.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide New Password.")
            return
        }
        if (!AndroidUtility.validatePassword( binding.editPassword.getText().toString())){
            showMessageInDialog(resources.getString(R.string.is_valid_new_password))
            return
        }
        if (binding.editConPassword.getText().isEmpty()) {
            showMessageInDialog("Please provide valid Confirm Password.")
            return
        }
        if (!binding.editConPassword.getText().trim()
                .equals(binding.editPassword.getText().trim())
        ) {
            showMessageInDialog("New Password and Confirm password are not same.")
            return
        }

        showProgress()
        viewModel.changePassword(
            password = binding.editOldPassword.getText().toString().trim(),
            newPassword =  binding.editPassword.getText().toString().trim()
        )
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataChangePassword.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processForgotPasswordResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveDataChangePasswordError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

    }

    private fun processForgotPasswordResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@DashboardChangePasswordActivity,
                                Dashboard::class.java
                            )
                        )
                    )
                    finish()

                }
            }
        )

    }
}