package  com.itsc.cal.ui.driver_access_point

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.itsc.cal.model.DriverJob
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataDriverJobByToken: MediatorLiveData<Event<DriverJob>> = MediatorLiveData()
    val mediatorLiveDataDriverJobByTokenError: MediatorLiveData<Event<Result>> = MediatorLiveData()

    init {
        mediatorLiveDataDriverJobByToken.addSource(
            repository.mutableLiveDataDriverJobByToken,
            Observer {
                mediatorLiveDataDriverJobByToken.postValue(it)
            }
        )
        mediatorLiveDataDriverJobByTokenError.addSource(
            repository.mutableLiveDataDriverJobByTokenError,
            Observer {
                mediatorLiveDataDriverJobByTokenError.postValue(it)
            }
        )
    }

    fun getDriverJobByToken(token: String) {
        repository.getDriverJobByToken(token)
    }
}