package  com.itsc.cal.ui.two_step_verification

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataResendOTP: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveDataResendOTPError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDataVerifyOTP: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveDataVerifyOTPError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDataResendOTP.addSource(
            repository.mutableLiveDataTwoStepVerification
        ) { t -> mediatorLiveDataResendOTP.postValue(t) }
        mediatorLiveDataResendOTPError.addSource(
            repository.mutableLiveDataTwoStepVerificationError
        ) { t -> mediatorLiveDataResendOTPError.postValue(t) }

        mediatorLiveDataVerifyOTP.addSource(
            repository.mutableLiveDataOTPVerifyTwoStepVerification
        ) { t -> mediatorLiveDataVerifyOTP.postValue(t) }
        mediatorLiveDataVerifyOTPError.addSource(
            repository.mutableLiveDataOTPVerifyTwoStepVerificationError
        ) { t -> mediatorLiveDataVerifyOTPError.postValue(t) }

    }

    fun resendOTP(deviceToken:String)=repository.twoStepOtpVerification(deviceToken)
    fun verifyOTP(otp:String,is_trusted:String,device_token:String)=repository.verifyOTPTwoStepVerification(otp = otp,is_trusted =is_trusted,device_token = device_token )

}