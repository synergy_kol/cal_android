package com.itsc.cal.ui.on_boarding

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityOnBoardingBinding
import com.itsc.cal.ui.login.LoginActivity
import com.itsc.cal.ui.on_boarding.adapter.OnBoardingFragsAdapter
import java.util.*

class OnBoarding : CalSuperActivity() {

    private lateinit var binding: ActivityOnBoardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_on_boarding)
        binding.context = this

        binding.viewPager.apply {
            adapter = OnBoardingFragsAdapter(supportFragmentManager)
            offscreenPageLimit = 3
        }
        binding.indicator.setViewPager(
            binding.viewPager
        )

        val handler = Handler()
        var currentPage = 0
        val Update = Runnable {
            if (currentPage == 3) {
                currentPage = 0
            }
            binding.viewPager.setCurrentItem(currentPage++, true)

        }

        Timer().schedule(
            object : TimerTask() {
                // task to be scheduled
                override fun run() {
                    handler.post(Update)
                }
            },
            KeywordsAndConstants.DELAY_FOR_ADVERTISEMENT,
            KeywordsAndConstants.DELAY_FOR_ADVERTISEMENT
        )

    }

    fun login() {
        startActivity(
            Intent(
                this,
                LoginActivity::class.java
            )
        )
        finish()
    }
}
