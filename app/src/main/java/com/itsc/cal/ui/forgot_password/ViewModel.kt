package  com.itsc.cal.ui.forgot_password

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.UserDetails
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataForgotPassword: MediatorLiveData<Event<UserDetails>> = MediatorLiveData()
    val mediatorLiveDataForgotPasswordError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveDataForgotPassword.addSource(
            repository.mutableLiveDataForgotPassword
        ) { t -> mediatorLiveDataForgotPassword.postValue(t) }
        mediatorLiveDataForgotPasswordError.addSource(
            repository.mutableLiveDataForgotPasswordError
        ) { t -> mediatorLiveDataForgotPasswordError.postValue(t) }

    }

    fun forgotPassword(emailMobile:String)=repository.forgotPassword(emailMobile)

}