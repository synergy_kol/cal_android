package com.itsc.cal.ui.post_hault_of_job_list.matches_jobs

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.*
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.HauljobListResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.post_a_haul_off.PostAHaulOffActivity
import com.itsc.cal.ui.post_hault_of_job_list.matches_jobs.adapter.MatchesJobListAdapter
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MatchesJobList : CalSuperActivity(withNav = true), KodeinAware {
    override val kodein: Kodein by kodein()

    private val repository: Repository by instance()

    private lateinit var binding: ActivityPostedMatchesJobListingBinding
    private lateinit var matchesJobListAdapter: MatchesJobListAdapter
    private var pos = 0
    private lateinit var jobResponse: HauljobListResponse
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_posted_matches_job_listing)

        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        showDashboardActionbarWithBackButton()

        initView()
        bindToViewModel()

    }

    override fun onResume() {
        super.onResume()
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveExportRequestData.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseRequest(it.getContent()!!.message)
                    }
                }
            }
        )
        viewModel.mediatorLiveExportRequestDataError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )


        viewModel.mediatorLiveImportRequestData.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseRequest(it.getContent()!!.message)
                    }
                }
            }
        )
        viewModel.mediatorLiveImportRequestDataError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )


        viewModel.mediatorLiveRequestAcceptedByExproterData.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponse(it.getContent()!!.message)
                    }
                }
            }
        )
        viewModel.mediatorLiveRequestAcceptedByExproterError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveRequestRejectData.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponse(it.getContent()!!.message)
                    }
                }
            }
        )
        viewModel.mediatorLiveRequestRejectError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@MatchesJobList,
                                Dashboard::class.java
                            )
                        )
                    )
                    finish()
                }
            }
        )
    }

    private fun processResponseRequest(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Request sent successfully",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@MatchesJobList,
                                Dashboard::class.java
                            )
                        )
                    )
                    finish()
                }
            }
        )

    }

    private fun initView() {
        intent.let {
            jobResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            var accessJob = intent.getStringExtra(KeywordsAndConstants.ACCEPT_JOB)
            if (accessJob.equals("1")) {  // Expoter Apply
                binding.textNoJobFound.visibility = View.GONE
                matchesJobListAdapter = MatchesJobListAdapter(this, object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        when (view.id) {
                            R.id.cvPostedJob -> {

                            }
                            R.id.btn_apply -> {

                                if (jobResponse.haul_type.equals(
                                        "E"
                                    )
                                ) {
                                    requestAccesptedByExpoter(
                                        jobResponse.import_requests?.get(
                                            position
                                        )?.haul_request_user_map_id ?: ""
                                    )
                                } else {

                                }

                            }
                            R.id.btn_cancel -> {

                                requestReject(
                                    jobResponse.import_requests?.get(position)?.haul_request_user_map_id
                                        ?: ""
                                )

                            }
                        }
                    }

                }, jobResponse.import_requests ?: ArrayList(), accessJob)
                val layoutManager =
                    LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                binding.recycleviewMatchesJobList.layoutManager = layoutManager
                binding.recycleviewMatchesJobList.adapter = matchesJobListAdapter

                setPageTitle("Requested Job")
            } else if (accessJob.equals("2")) { // impoter apply
                binding.textNoJobFound.visibility = View.GONE
                matchesJobListAdapter = MatchesJobListAdapter(this, object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        when (view.id) {
                            R.id.cvPostedJob -> {

                            }
                            R.id.btn_apply -> {

                                if (jobResponse.haul_type.equals(
                                        "I"
                                    )
                                ) {
                                    Log.i("Import Apply", "Apply")

                                    startActivity(
                                        Intent(
                                            this@MatchesJobList,
                                            PostAHaulOffActivity::class.java
                                        ).putExtra(
                                            KeywordsAndConstants.DATA,
                                            toJson(jobResponse)
                                        ).putExtra(
                                            KeywordsAndConstants.DESTINATION_DATA,
                                            toJson(jobResponse.export_requests!!.get(position))
                                        )
                                    )
                                }
                            }
                            R.id.btn_cancel -> {

                                requestReject(
                                    jobResponse.export_requests?.get(position)?.haul_request_user_map_id
                                        ?: ""
                                )

                            }
                        }
                    }

                }, jobResponse.export_requests ?: ArrayList(), accessJob)
                val layoutManager =
                    LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                binding.recycleviewMatchesJobList.layoutManager = layoutManager
                binding.recycleviewMatchesJobList.adapter = matchesJobListAdapter
                setPageTitle("Requested Job")
            } else {  //  match job list
                if (jobResponse.suggested_requests?.size ?: 0 > 0) {
                    binding.textNoJobFound.visibility = View.GONE
                    matchesJobListAdapter = MatchesJobListAdapter(this, object : OnItemClickListener {
                        override fun onItemClick(view: View, position: Int) {
                            when (view.id) {
                                R.id.cvPostedJob -> {

                                }
                                R.id.btn_review -> {


                                }
                                R.id.btn_request -> {

                                    if (jobResponse.suggested_requests?.get(position)?.haul_type.equals(
                                            "E"
                                        )
                                    ) {
                                        Log.i("import->exportAPI:", "exportAPI")
                                        Log.i(
                                            "Export:", jobResponse.suggested_requests?.get(
                                                position
                                            )?.haul_request_id!!
                                        )

                                        Log.i("import:", (jobResponse.haul_request_id))
                                        sendExportRequest(
                                            exporter_request_id = jobResponse.suggested_requests?.get(
                                                position
                                            )?.haul_request_id!!,
                                            importer_request_id = (jobResponse.haul_request_id)
                                        )

                                        /*  sendExportRequest(
                                              exporter_request_id = jobResponse.haul_request_id,
                                              importer_request_id = jobResponse.suggested_requests?.get(
                                                  position
                                              )?.haul_request_id!!
                                          )*/
                                    } else {
                                        Log.i("Export->importAPI:", "importAPI")

                                        Log.i(
                                            "Export:", jobResponse.suggested_requests?.get(
                                                position
                                            )?.haul_request_id!!
                                        )

                                        Log.i("import:", (jobResponse.haul_request_id))
                                        sendImportRequest(
                                            importer_request_id = jobResponse.suggested_requests?.get(
                                                position
                                            )?.haul_request_id!!,
                                            exporter_request_id = (jobResponse.haul_request_id)
                                        )
                                    }

                                }
                            }
                        }

                    }, jobResponse.suggested_requests ?: ArrayList(), accessJob)
                    val layoutManager =
                        LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                    binding.recycleviewMatchesJobList.layoutManager = layoutManager
                    binding.recycleviewMatchesJobList.adapter = matchesJobListAdapter
                }else{
                    binding.textNoJobFound.visibility = View.VISIBLE
                }
                setPageTitle("Matched Job")
            }

        }

    }

    private fun sendExportRequest(importer_request_id: String, exporter_request_id: String) {
        showProgress()
        viewModel.sendExportRequest(
            importer_request_id = importer_request_id,
            exporter_request_id = exporter_request_id
        )
    }

    private fun sendImportRequest(importer_request_id: String, exporter_request_id: String) {
        showProgress()
        viewModel.sendImportRequest(
            importer_request_id = importer_request_id,
            exporter_request_id = exporter_request_id
        )
    }

    private fun requestAccesptedByExpoter(mapId: String) {
        showProgress()
        viewModel.requestAcceptedByExpoter(
            mapId
        )
    }

    private fun requestReject(mapId: String) {
        showProgress()
        viewModel.requestReject(
            mapId
        )
    }


}