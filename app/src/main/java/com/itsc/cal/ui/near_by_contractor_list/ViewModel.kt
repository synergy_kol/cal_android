package  com.itsc.cal.ui.near_by_contractor_list

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveNearByTruckList: MediatorLiveData<Event<ArrayList<NearByConductorListResponse>>> =
        MediatorLiveData()
    val mediatorLiveNearByTruckListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveNearByTruckList.addSource(
            repository.mutableLiveDataNearByConductorList
        ) { t -> mediatorLiveNearByTruckList.postValue(t) }
        mediatorLiveNearByTruckListError.addSource(
            repository.mutableLiveDataNearByConductorListError
        ) { t -> mediatorLiveNearByTruckListError.postValue(t) }


    }

    fun getNearByConductor(lat: String, long: String, distance: String = "10") =
        repository.getNearByConductorList(lat = lat, long = long, distance = distance)


}