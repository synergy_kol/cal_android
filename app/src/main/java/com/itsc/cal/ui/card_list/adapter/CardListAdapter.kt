package com.itsc.cal.ui.card_list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.databinding.ChildCardListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.CardListResponse

class CardListAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener,val arrayList:ArrayList<CardListResponse>):
    RecyclerView.Adapter<CardListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(ChildCardListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))

    }

    inner class ViewHolder(val binding: ChildCardListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(cardListResponse: CardListResponse) {
            binding.imgMenu.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.imgSetDefault.setOnClickListener {
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.textCardNo.text = cardListResponse.number
            binding.textName.text = cardListResponse.card_name
            binding.textExpDate.text = "${cardListResponse.expiry_month}/${cardListResponse.expiry_year}"
            if (cardListResponse.is_default.equals("0")){
                binding.imgSetDefault.setBackgroundResource(R.drawable.ic_circle)
            }else if (cardListResponse.is_default.equals("1")){
                binding.imgSetDefault.setBackgroundResource(R.drawable.ic_check_card)
            }
        }

    }

}