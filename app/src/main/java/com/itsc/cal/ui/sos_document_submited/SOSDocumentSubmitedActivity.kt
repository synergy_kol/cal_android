package com.itsc.cal.ui.sos_document_submited

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySOSDocumentSubmitedBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.SOSDocumentResponse
import com.itsc.cal.model.SOSResponce
import com.itsc.cal.receiver.DownloadReceiver
import com.itsc.cal.ui.login.LoginActivity
import com.itsc.cal.ui.paymentsettings.PayoutSettingsRegistrationActivity
import com.itsc.cal.ui.sos_document_submited.adapter.SOSDocumentSubmitedAdapter
import com.itsc.cal.ui.upload_sos_id_proof.UploadSOSActivity
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File

class SOSDocumentSubmitedActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivitySOSDocumentSubmitedBinding
    private lateinit var arrayList: ArrayList<SOSDocumentResponse>
    private lateinit var sosDocumentResponse: SOSDocumentResponse
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var sosAdapter: SOSDocumentSubmitedAdapter
    private val downloadReceiver = DownloadReceiver()
    private lateinit var sosResponce: SOSResponce
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_s_o_s_document_submited)

        showASOSDocumentSubmitedActionbar()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        if (viewModel.giveRepository()
                .getUserData()!!.user_role.equals("4") && viewModel.giveRepository()
                .getUserData()!!.sos_verification.equals(
                    "0"
                )
        ) {
            enableBackButton()
        } else {
            enableBackButton()

        }
        initView()
        bindToViewModel()
    }

    override fun goBack() {
        onBackPressed()
    }

    /* override fun onBackPressed() {

     }*/


    override fun onBackPressed() {
        super.onBackPressed()
        /*if (viewModel.giveRepository()
                .getUserData()!!.user_role.equals("4") && viewModel.giveRepository()
                .getUserData()!!.sos_verification.equals(
                    "0"
                )
        ) {

        } else {
            super.onBackPressed()
        }*/
    }

    override fun onResume() {
        super.onResume()
        getSoSListAPICalled()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(downloadReceiver)
    }

    private fun getSoSListAPICalled() {
        showProgress()
        viewModel.getSOSDocumentList()
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveSOSDocumentListing.observe(
            this,
            Observer<Event<SOSResponce>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        sosResponce = it.getContent()!!
                        sosDocumentListResponse()
                    }

                }
            }
        )
        viewModel.mediatorLiveSOSDocumentListingError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

    }

    private fun sosDocumentListResponse() {
        if(!this::sosResponce.isInitialized){
            return
        }

        val sosStatus = sosResponce.sos_verification
        val userDetails = viewModel.giveRepository().getUserData()
        userDetails!!.sos_verification = sosStatus
        userDetails.completed_steps = sosResponce.completed_steps
        viewModel.giveRepository().saveUserDataToPref(userDetails)

        Log.i("SOSStatus", sosStatus)
        for (i in 0 until sosResponce.document_list.size) {
            for (j in 0 until arrayList.size) {
                if (arrayList.get(j).document_type.equals(sosResponce.document_list[i].document_type)) {
                    sosDocumentResponse = SOSDocumentResponse(
                        document_id = sosResponce.document_list[i].document_id,
                        document_image = sosResponce.document_list[i].document_image,
                        document_status = sosResponce.document_list[i].document_status,
                        document_title = arrayList.get(j).document_title,
                        document_type = arrayList.get(j).document_type,
                        isMandatory = arrayList.get(j).isMandatory
                    )
                    arrayList.removeAt(j)
                    arrayList.add(j, sosDocumentResponse)
                }
            }
        }
        sosAdapter.notifyDataSetChanged()


    }

    private fun initView() {
        intent.also {
            if (it?.getStringExtra("from").equals("menu")) {
                binding.llButtons.visibility = View.GONE
            }else{
                binding.llButtons.visibility = View.VISIBLE
            }
        }
        binding.btnLogout.setOnClickListener {
            viewModel.giveRepository().clearAllData()
            startActivity(
                UiUtil.clearStackAndStartNewActivity(
                    Intent(
                        this,
                        LoginActivity::class.java
                    )
                )
            )
            finish()
        }
        binding.btnSOSNext.setOnClickListener {
            val userDetails = viewModel.giveRepository().getUserData()
            if(userDetails?.completed_steps=="2"){
                DialogUtil(this@SOSDocumentSubmitedActivity).
                showMessage("Please upload or retake all your documents before move to next step")
                return@setOnClickListener
            }
            if(userDetails?.completed_steps=="3") {
                startActivity(UiUtil.clearStackAndStartNewActivity(
                    Intent(this, PayoutSettingsRegistrationActivity::class.java)
                ))
            }
        }
        binding.textMessage.text = "Thank you, ${viewModel.giveRepository()
            .getUserData()!!.fname} ${viewModel.giveRepository()
            .getUserData()!!.lanme}\nYou have created your CAL Account"
        arrayList = ArrayList<SOSDocumentResponse>()
        sosDocumentResponse = SOSDocumentResponse(
            document_id = "0",
            document_image = "",
            document_status = "U",
            document_title = "Valid Driver License / Front/Back*",
            document_type = "SH",
            isMandatory = "1"
        )
        arrayList.add(sosDocumentResponse)

        sosDocumentResponse = SOSDocumentResponse(
            document_id = "0",
            document_image = "",
            document_status = "U",
            document_title = "Valid Medical Certificate*",
            document_type = "PP",
            isMandatory = "1"
        )
        arrayList.add(sosDocumentResponse)

        sosDocumentResponse = SOSDocumentResponse(
            document_id = "0",
            document_image = "",
            document_status = "U",
            document_title = "USDOT Number*",
            document_type = "PI",
            isMandatory = "1"
        )
        arrayList.add(sosDocumentResponse)

        sosDocumentResponse = SOSDocumentResponse(
            document_id = "0",
            document_image = "",
            document_status = "U",
            document_title = "Truck/Trailer Registration*",
            document_type = "VC",
            isMandatory = "1"
        )
        arrayList.add(sosDocumentResponse)

        sosDocumentResponse = SOSDocumentResponse(
            document_id = "0",
            document_image = "",
            document_status = "U",
            document_title = "Insurance Document / Certificate of Insurance*",
            document_type = "IN",
            isMandatory = "1"
        )
        arrayList.add(sosDocumentResponse)

        sosDocumentResponse = SOSDocumentResponse(
            document_id = "0",
            document_image = "",
            document_status = "U",
            document_title = "EIN*",
            document_type = "NP",
            isMandatory = "1"
        )
        arrayList.add(sosDocumentResponse)

        sosDocumentResponse = SOSDocumentResponse(
            document_id = "0",
            document_image = "",
            document_status = "U",
            document_title = "1099*",
            document_type = "TP",
            isMandatory = "1"
        )
        arrayList.add(sosDocumentResponse)

        val filter = IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        registerReceiver(downloadReceiver, filter)

      /*  sosDocumentResponse = SOSDocumentResponse(
            document_id = "0",
            document_image = "",
            document_status = "U",
            document_title = "DBE",
            document_type = "DB",
            isMandatory = "0"
        )
        arrayList.add(sosDocumentResponse)

        sosDocumentResponse = SOSDocumentResponse(
            document_id = "0",
            document_image = "",
            document_status = "U",
            document_title = "HUB/SBE/WBE",
            document_type = "HU",
            isMandatory = "0"
        )*/


        binding.recycleViewSOSSubmited.layoutManager = LinearLayoutManager(this)
        sosAdapter = SOSDocumentSubmitedAdapter(
            this@SOSDocumentSubmitedActivity,
            arrayList,
            object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    when(view.id){
                        R.id.imgDownload->{
                            downloadFile("${KeywordsAndConstants.BASE_URL}${arrayList.get(position).document_image}")
                        }
                        R.id.ll_bg->{
                            startActivity(
                                Intent(
                                    this@SOSDocumentSubmitedActivity,
                                    UploadSOSActivity::class.java
                                ).putExtra(KeywordsAndConstants.DATA, toJson(arrayList.get(position)))
                            )
                        }
                    }

                }

            })

        binding.recycleViewSOSSubmited.adapter = sosAdapter

    }

    fun downloadFile(mPayoutLink: String) {
        val f_url = mPayoutLink
        if (TextUtils.isEmpty(mPayoutLink)) {
            return
        }

        var mediaStorageDir:File?=null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            mediaStorageDir = File(
                Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS
                ).toString() + "/" + getString(R.string.app_name)
                        + "/Files"
            )
        } else {
            mediaStorageDir = File(
                (Environment.getExternalStorageDirectory()
                    .toString() + "/" + getString(R.string.app_name)
                        + "/Files")
            )
        }
        try {
            if (!mediaStorageDir.exists()) {
                Log.d("Error", "mkdirs: " + mediaStorageDir.mkdirs())
                if (!mediaStorageDir.mkdirs()) {
                    return
                }
            }
            val mFileName = f_url.substring(f_url.lastIndexOf('/').plus(1), f_url.length)
            val file = File(mediaStorageDir.absolutePath + "/" + mFileName)
            //file.createNewFile()

            Log.d("FILE_DOWNLOAD", file.absolutePath + " = " + file.exists())

            val downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val request = DownloadManager.Request(Uri.parse(mPayoutLink))
            request.setTitle(getString(R.string.app_name))
            request.setDestinationUri(Uri.fromFile(file))
            //request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,mFileName)
            request.allowScanningByMediaScanner()
            sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)))
            val id = downloadManager.enqueue(request)
            viewModel.giveRepository().getSharedPref()
                .edit()
                .putString("referenceId",id.toString())
        } catch (e: Exception) {
            Log.d("FILE_DOWNLOAD", Log.getStackTraceString(e))
        }
    }
}