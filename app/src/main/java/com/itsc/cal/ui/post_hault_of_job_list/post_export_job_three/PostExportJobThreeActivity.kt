package com.itsc.cal.ui.post_hault_of_job_list.post_export_job_three

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityPostExportStepThreeBinding
import com.itsc.cal.model.*
import com.itsc.cal.ui.post_hault_of_job_list.matches_jobs.MatchesJobList
import com.itsc.cal.ui.post_hault_of_job_list.post_export_job.PostExportJobActivity
import com.itsc.cal.ui.post_hault_of_job_list.post_export_job_two.PostExportJobTwoActivity
import com.itsc.cal.util.*
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.itsc.cal.widget.spinner.Spinner
import com.itsc.cal.widget.spinner.SpinnerData
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import kotlin.collections.ArrayList


class PostExportJobThreeActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPostExportStepThreeBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    companion object {
        var postATruckPage: Activity? = null
    }

    private lateinit var materialResponse: EquipementResponse
    private lateinit var materialResponseSpreading: EquipementResponse
    private var material_option: MaterialOptionResponse = MaterialOptionResponse("", "", "", "")
    private var haul_request_id = ""
    private var importExport = ""
    private lateinit var importJobPostStep2: ImportJobPostStep2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_post_export_step_three)
        enableBackButton()
        setPageTitle("Step 3")
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        postATruckPage = this
        intiView()
        bindViewHolder()
        getMaerialEquipmentList()
    }

    private fun getMaerialEquipmentList() {
        showProgress()
        viewModel.getMaterialEquipmentList()
    }

    private fun getSpreadingEquipmentList() {
        showProgress()
        viewModel.getSpreadingEquipmentList()
    }


    private fun bindViewHolder() {

        viewModel.mediatorLiveHaulJobExportStep3.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseExport(it.getContent()!!.message)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataJobExportStep3Error.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveHaulJobImportStep2.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseImport(it.getContent()!!.message)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataJobImportStep2Error.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveMaterialEquipementResult.observe(
            this,
            Observer<Event<ArrayList<EquipementResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setEquipmentList(it.getContent())
                    }

                }
            }
        )

        viewModel.mediatorLiveMaterialEquipementError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveSpreadingEquipementResult.observe(
            this,
            Observer<Event<ArrayList<EquipementResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setSpreadingEquipmentList(it.getContent())
                    }

                }
            }
        )

        viewModel.mediatorLiveSpreadingEquipementError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveImportExportData.observe(
            this,
            Observer<Event<HauljobListResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processPayoutData(it.getContent()!!)
                    }
                }
            }
        )
        viewModel.mediatorLiveImportExportError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )
    }

    private fun processPayoutData(jobResponse: HauljobListResponse) {
        val intent = Intent(this, MatchesJobList::class.java)
        intent.putExtra(
            KeywordsAndConstants.DATA,
            toJson(jobResponse)
        )
        intent.putExtra(KeywordsAndConstants.ACCEPT_JOB,"0")
        startActivity(intent)
        PostExportJobActivity.postATruckPage?.finish()
        PostExportJobTwoActivity.postATruckPage?.finish()
        finish()
    }

    private fun setEquipmentList(materialEquipmentResponse: ArrayList<EquipementResponse>?) {

        if (materialEquipmentResponse != null) {
            val items: ArrayList<SpinnerData<EquipementResponse>> = ArrayList()
            (materialEquipmentResponse.forEach { source ->
                items.add(
                    SpinnerData(
                        source.name.toString(),
                        source
                    )
                )
            })
            arrayListOf(
                SpinnerData(
                    "ANY",
                    "ANY"
                )
            )
            items.add(SpinnerData("ANY", EquipementResponse("ANY", "ANY")))
            arrayListOf(
                SpinnerData(
                    "Other",
                    "Other"
                )
            )
            items.add(SpinnerData("Other", EquipementResponse("Other", "Other")))


            if (items.size > 0)
                materialResponse = items[0].data
            binding.SpinnerLoadingEquipmemtType.setBg()
            binding.SpinnerLoadingEquipmemtType.addItems(
                items,
                withDot = false,
                listener = object : Spinner.OnItemSelectedListener {
                    override fun <T> selected(item: SpinnerData<T>) {
                        materialResponse = item.data as EquipementResponse
                        if (materialResponse.name.equals(
                                "Other",
                                true
                            ) || materialResponse.name.equals("ANY", true)
                        ) {
                            binding.editEquipmentOther.visibility = View.VISIBLE

                        } else {
                            binding.editEquipmentOther.visibility = View.GONE
                        }

                    }
                }
            )


        }


    }

    private fun setSpreadingEquipmentList(materialEquipmentResponse: ArrayList<EquipementResponse>?) {

        if (materialEquipmentResponse != null) {

            val items: ArrayList<SpinnerData<EquipementResponse>> = ArrayList()
            (materialEquipmentResponse.forEach { source ->
                items.add(
                    SpinnerData(
                        source.name.toString(),
                        source
                    )
                )
            })

            arrayListOf(
                SpinnerData(
                    "Other",
                    "Other"
                )
            )
            items.add(SpinnerData("Other", EquipementResponse("Other", "Other")))


            if (items.size > 0)
                materialResponseSpreading = items[0].data
            binding.SpinnerSpreadingEquipmemtType.setBg()
            binding.SpinnerSpreadingEquipmemtType.addItems(
                items,
                withDot = false,
                listener = object : Spinner.OnItemSelectedListener {
                    override fun <T> selected(item: SpinnerData<T>) {
                        materialResponseSpreading = item.data as EquipementResponse
                        if (materialResponseSpreading.name.equals(
                                "Other",
                                true
                            )
                        ) {
                            binding.editSpreadingEquipmentOther.visibility = View.VISIBLE

                        } else {
                            binding.editSpreadingEquipmentOther.visibility = View.GONE
                        }

                    }
                }
            )


        }

    }

    private fun intiView() {
        intent.let {
            haul_request_id = intent.extras?.getString(KeywordsAndConstants.DATA, "").toString()
            importExport =
                intent.extras?.getString(KeywordsAndConstants.IMPORT_EXPORT, "").toString()
            Log.i("haul_request_id:", haul_request_id)
            if (importExport.equals("E")) {
                binding.llSpreadingEquipment.visibility = View.GONE
                setPageTitle("Add Haul-OFF Export Job- Step 3")
            } else {
                getSpreadingEquipmentList()
                setPageTitle("Add Haul-OFF Import Job- Step 3")
                binding.llSpreadingEquipment.visibility = View.VISIBLE
                intent?.let {
                    importJobPostStep2 =
                        fromJson(intent.getStringExtra(KeywordsAndConstants.IMPORT_DATA_MODEL))

                }
            }
        }

        binding.btnPostATruck.setOnClickListener {
            moveToNext()
        }

        binding.ednotes.apply {
            hint("Enter Notes")
            inputMode(EditTextInputMode.INPUT_TEXT_MULTI_LINE)
        }

    }

    private fun moveToNext() {

        if (materialResponse.equipment_id.equals(
                "Other",
                true
            ) && binding.editEquipmentOther.getText().toString().trim() == ""
        ) {
            showMessageInDialog("Please provide Other Equipment.")
            return
        }

        if (materialResponse.equipment_id.equals(
                "ANY",
                true
            ) && binding.editEquipmentOther.getText().toString().trim() == ""
        ) {
            showMessageInDialog("Please provide ANY Equipment.")
            return
        }

     /*   if (binding.ednotes.getText().toString().trim() == "") {
            showMessageInDialog("Please provide Additional Notes.")
            return
        }*/

        showProgress()
        var ANY = ""
        var Other = ""
        if (materialResponse.equipment_id.equals(
                "ANY",
                true
            )
        ) {
            ANY = binding.editEquipmentOther.getText().toString().trim()
        } else if (materialResponse.equipment_id.equals(
                "Other",
                true
            )
        ) {
            Other = binding.editEquipmentOther.getText().toString().trim()
        } else {
             ANY = ""
             Other = ""
        }
        if (importExport.equals("E")) {
            viewModel.postExportJobStep3(
                haul_request_id = haul_request_id,
                equipment_type = materialResponse.equipment_id,
                equipment_other = Other,
                equipment_any = ANY,
                additional_notes = binding.ednotes.getText().toString().trim()
            )
        } else {

            viewModel.postImportJobStep2(
                haul_request_id = haul_request_id,
                equipment_type = materialResponse.equipment_id,
                equipment_other = Other,
                equipment_any = ANY,
                additional_notes = binding.ednotes.getText().toString().trim(),
                MaterialoptionType = importJobPostStep2.MaterialoptionType,
                cubic_yards = importJobPostStep2.cubic_yards,
                tons = importJobPostStep2.tons,
                spreading_equipment_other = binding.editSpreadingEquipmentOther.getText().toString().trim(),
                spreading_equipment_type =  materialResponseSpreading.equipment_id,
                MaterialType = importJobPostStep2.MaterialType,
                material_other = importJobPostStep2.material_other
            )

        }

    }


    private fun processResponseExport(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Haul-OFF Export Job successfully posted.",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    showProgress()
                    viewModel.getJobDetails(haul_request_id)

                }
            }
        )

    }

    private fun processResponseImport(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Haul-OFF Import Job successfully posted.",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    showProgress()
                    viewModel.getJobDetails(haul_request_id)

                }
            }
        )

    }

    @SuppressLint("CheckResult")
    fun takePicture() {
        RxPermissions(this)
            .request(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .subscribe {
                if (!it) {
                    showMessageInDialog("Please accept permissions to take picture.")
                }

                ImagePickerUtil.pickImage(
                    context = this,
                    reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
                )
            }
    }

}