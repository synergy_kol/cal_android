package  com.itsc.cal.ui.dashboard_change_password

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataChangePassword: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveDataChangePasswordError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveDataChangePassword.addSource(
            repository.mutableLiveDataUpdatePassword
        ) { t -> mediatorLiveDataChangePassword.postValue(t) }
        mediatorLiveDataChangePasswordError.addSource(
            repository.mutableLiveDataUpdatePasswordError
        ) { t -> mediatorLiveDataChangePasswordError.postValue(t) }

    }

    fun changePassword(password: String, newPassword: String) =
        repository.updatePassword(password = password, new_password = newPassword)

}