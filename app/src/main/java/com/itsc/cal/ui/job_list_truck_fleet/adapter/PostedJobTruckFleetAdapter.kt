package com.itsc.cal.ui.job_list_truck_fleet.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.databinding.AdapterPostedJobTruckFleetBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse

class PostedJobTruckFleetAdapter(
    val mContext: Context,
    val onItemClickListener: OnItemClickListener,
    val arrayList: ArrayList<JobListResponse>,
    val filterType: String
) :
    RecyclerView.Adapter<PostedJobTruckFleetAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            AdapterPostedJobTruckFleetBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData()

    }

    inner class ViewHolder(val binding: AdapterPostedJobTruckFleetBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bindData() {
            val jobListResponse = arrayList.get(adapterPosition)
            binding.tvSource.text = jobListResponse.Source
            binding.tvJobId.text = "#CAL${jobListResponse.job_id}"
            binding.tvDestination.text = jobListResponse.Destination
            binding.tvWeight.text = "${jobListResponse.Weight} QT"
            binding.tvDate.text = jobListResponse.pickup_date
            binding.tvEquipmentText.text =
                if (jobListResponse.Equipment_value == "null" || jobListResponse.Equipment_value=="")
                    "Equipment: No" else
                    "Equipment: ${jobListResponse.Equipment_value}"
            binding.tvEquipment.text = ""
            binding.tvMaterialTypeText.text = "Material: ${jobListResponse.material_name}"
            binding.textTruckName.text = jobListResponse.truck_name
            binding.tvTruckPayInText.text = "Other Charges: $ ${jobListResponse.FreightCharges}"
            binding.tvTruckPayIn.text = "Estimated Price: $ ${jobListResponse.JobEstimatePrice}"

            if (jobListResponse.job_type.equals("H")){
                binding.cvPostedJob.setBackgroundColor(mContext.resources.getColor(R.color.text_sky_light_blue))
                binding.textIconHl.visibility = View.VISIBLE
                binding.tvWeight.visibility = View.GONE
                binding.tvTruckPayInText.visibility = View.GONE
                binding.tvEquipmentText.visibility = View.GONE
            }else{
                binding.cvPostedJob.setBackgroundColor(mContext.resources.getColor(R.color.white))
                binding.textIconHl.visibility = View.GONE
                binding.tvWeight.visibility = View.VISIBLE
                binding.tvTruckPayInText.visibility = View.VISIBLE
                binding.tvEquipmentText.visibility = View.VISIBLE
            }

            binding.cvPostedJob.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
            if (filterType.equals("1")) {
                binding.llApplyCancel.visibility = View.VISIBLE
                binding.btnButtonUnhide.visibility = View.GONE
                binding.llConfigure.visibility = View.GONE
            } else if (filterType.equals("2")) {
                binding.llApplyCancel.visibility = View.GONE
                binding.btnButtonUnhide.visibility = View.GONE
                if (jobListResponse.status.equals("2")) {
                    binding.llConfigure.visibility = View.VISIBLE
                } else {
                    binding.llConfigure.visibility = View.GONE
                }

            } else if (filterType.equals("3")) {
                binding.llApplyCancel.visibility = View.GONE
                binding.btnButtonUnhide.visibility = View.VISIBLE
                binding.llConfigure.visibility = View.GONE
            }
            binding.btnButtonApply.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
            binding.btnButtonCancel.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
            binding.btnButtonConfigure.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
            binding.btnButtonHide.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
            binding.btnButtonUnhide.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
        }

    }

}

