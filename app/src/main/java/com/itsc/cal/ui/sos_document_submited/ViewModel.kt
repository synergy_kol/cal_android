package  com.itsc.cal.ui.sos_document_submited

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveSOSDocumentListing: MediatorLiveData<Event<SOSResponce>> =
        MediatorLiveData()
    val mediatorLiveSOSDocumentListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveSOSDocumentListing.addSource(
            repository.mutableLiveDataSOSDocumentListing
        ) { t -> mediatorLiveSOSDocumentListing.postValue(t) }
        mediatorLiveSOSDocumentListingError.addSource(
            repository.mutableLiveDataSOSDocumentListingError
        ) { t -> mediatorLiveSOSDocumentListingError.postValue(t) }


    }

    fun getSOSDocumentList() = repository.getSOSDocumentList()



}