package  com.itsc.cal.ui.payment_method

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveSaveCardData: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveSaveCardDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveEditCardData: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveEditCardDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveSaveCardData.addSource(
            repository.mutableLiveDataSaveCard
        ) { t -> mediatorLiveSaveCardData.postValue(t) }
        mediatorLiveSaveCardDataError.addSource(
            repository.mutableLiveDataSaveCardError
        ) { t -> mediatorLiveSaveCardDataError.postValue(t) }


        mediatorLiveEditCardData.addSource(
            repository.mutableLiveDataEditCard
        ) { t -> mediatorLiveEditCardData.postValue(t) }
        mediatorLiveEditCardDataError.addSource(
            repository.mutableLiveDataEditCardError
        ) { t -> mediatorLiveEditCardDataError.postValue(t) }
    }


    fun addCard(card_name: String, number: String, expiry_year: String, expiry_month: String,cvv:String) =
        repository.addCard(
            card_name = card_name,
            number = number,
            expiry_year = expiry_year,
            expiry_month = expiry_month,
            cvv = cvv
        )

    fun editCard(card_name: String, number: String, expiry_year: String, expiry_month: String,cvv:String,cardId:String) =
        repository.editCard(
            card_name = card_name,
            number = number,
            expiry_year = expiry_year,
            expiry_month = expiry_month,
            cvv = cvv,
            card_id = cardId
        )

}