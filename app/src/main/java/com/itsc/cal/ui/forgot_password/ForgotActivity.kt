package com.itsc.cal.ui.forgot_password

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityForgotBinding
import com.itsc.cal.model.UserDetails
import com.itsc.cal.ui.verification.VerificationActivity
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ForgotActivity : CalSuperActivity(), KodeinAware {

    private lateinit var binding: ActivityForgotBinding
    override val kodein: Kodein by kodein()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_forgot)
        showAppLogoWithBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        initView()
        bindToViewModel()
    }

    private fun initView() {
        binding.editEmailMobile.apply {
            hint("Email")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }
        binding.buttonForgotPassword.setOnClickListener {
            forgotPasswordAPICalled()
        }
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataForgotPassword.observe(
            this,
            Observer<Event<UserDetails>> { t ->
                t?.let {
                    if (it.shouldReadContent()){
                        it.readContent()
                        processForgotPasswordResponse()
                    }

                }
            }
        )
        viewModel.mediatorLiveDataForgotPasswordError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )


    }

    fun forgotPasswordAPICalled() {
        if (
            binding.editEmailMobile.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide valid email.")
            return
        }
        showProgress()
        viewModel.forgotPassword(binding.editEmailMobile.getText().toString().trim())

    }

    private fun processForgotPasswordResponse() {
        hideProgress()

        startActivity(
            Intent(
                this,
                VerificationActivity::class.java
            ).putExtra(KeywordsAndConstants.isFromForgotPassword, true)
        )
    }
}