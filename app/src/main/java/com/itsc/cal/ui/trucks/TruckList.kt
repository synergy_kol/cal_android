package com.itsc.cal.ui.trucks

import android.graphics.Typeface
import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityTruckListBinding
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.trucks.adapter.TruckListPagerAdapter
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class TruckList: CalSuperActivity(withNav = true), KodeinAware {
    override val kodein: Kodein by kodein()

    private val repository: Repository by instance()
    private lateinit var binding: ActivityTruckListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_truck_list)

        showDashboardActionbar()
        // setUpDrawer(repository.getNavMenuItems())
        setPageTitle("Trucks List")
        setPagerAdapter()
    }

    private fun setPagerAdapter(){
        val mTruckListPagerAdapter = TruckListPagerAdapter(supportFragmentManager)
        binding.viewPager.adapter = mTruckListPagerAdapter
        binding.viewPager.offscreenPageLimit = 2
        binding.tabs.setTypeface(Typeface.createFromAsset(assets, "poppins_bold.ttf"), Typeface.NORMAL)
        binding.tabs.setViewPager(binding.viewPager)
    }
}