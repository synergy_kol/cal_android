package com.itsc.cal.ui.paymentsettings

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityPayoutDetailsBinding
import com.itsc.cal.model.AllPayoutData
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.paymentsettings.adapter.PayoutDetailsAdapter
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import com.itsc.cal.util.model.Result
class PayoutDetailsActivity: CalSuperActivity(), KodeinAware {
    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPayoutDetailsBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_payout_details)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        enableBackButton()
        showHelpButton()
        bindingViewModel()
        setPageTitle("Payout Details")
        //initView()
    }

    override fun onResume() {
        super.onResume()
        showProgress()
        viewModel.getAllPayoutDetails()
    }


    private fun bindingViewModel() {
        viewModel.mediatorLiveAllPayoutDetails.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<AllPayoutData>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent()!!)
                    }
                        //processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveAllPayoutDetailsError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun setAdapter(payoutList: ArrayList<AllPayoutData>) {
        binding.rvPayoutDetails.apply {
            layoutManager = LinearLayoutManager(this@PayoutDetailsActivity)
            adapter = PayoutDetailsAdapter(this@PayoutDetailsActivity, payoutList)
        }
    }
}