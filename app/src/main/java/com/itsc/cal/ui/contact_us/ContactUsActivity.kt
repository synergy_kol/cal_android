package com.itsc.cal.ui.contact_us

import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityContactUsBinding

class ContactUsActivity : CalSuperActivity() {

    private lateinit var binding:ActivityContactUsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_contact_us)
        setPageTitle("Contact Us")
        enableBackButton()
    }
}