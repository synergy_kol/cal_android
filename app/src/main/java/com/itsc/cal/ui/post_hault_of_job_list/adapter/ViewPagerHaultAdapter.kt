package com.itsc.cal.ui.post_hault_of_job_list.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPagerHaultAdapter(fm : FragmentManager, val fragmentList : List<Fragment>) : FragmentStatePagerAdapter(fm) {


    private val titleList = arrayListOf("All Export","Self Export","All Import","Self Import")

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }


}