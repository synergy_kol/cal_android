package com.itsc.cal.ui.search_job.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildVehicleTypeListBinding
import com.itsc.cal.interfaces.OnItemClickListener

class SearchJobAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener):
    RecyclerView.Adapter<SearchJobAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(ChildVehicleTypeListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return 12
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData()


    }

    inner class ViewHolder(val binding: ChildVehicleTypeListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData() {

        }

    }

}