package com.itsc.cal.ui.builders_details.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ChildBuilderDetailsBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.BuilderListResponse
import com.itsc.cal.model.NearByConductorListResponse

class BuilderDetailsAdapter(
    val mContext: Context,
    val onItemClickListener: OnItemClickListener,
    var arrayList: ArrayList<BuilderListResponse>
) :
    RecyclerView.Adapter<BuilderDetailsAdapter.ViewHolder>(), Filterable {

    var suggestions: ArrayList<BuilderListResponse> = ArrayList()
    var tempList: ArrayList<BuilderListResponse> =
        arrayList.clone() as ArrayList<BuilderListResponse>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ChildBuilderDetailsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))
    }

    inner class ViewHolder(val binding: ChildBuilderDetailsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(builderListResponse: BuilderListResponse) {
            var nameFirstWord = ""
            if (builderListResponse.fname.length > 1){
                nameFirstWord = builderListResponse.fname.substring(0, 1)
            }else{
                nameFirstWord= builderListResponse.fname
            }
            if (!TextUtils.isEmpty(builderListResponse.builder_image)){
                binding.imgProfileImage.setImage(
                    imageUrl = "${KeywordsAndConstants.BASE_URL}${builderListResponse.builder_image}",
                    isCircularImage = true,
                    needPlaceHolderImageForName = nameFirstWord
                )
            }else{
                binding.imgProfileImage.setImage(
                    imageUrl = "${KeywordsAndConstants.BASE_URL}${builderListResponse.image}",
                    isCircularImage = true,
                    needPlaceHolderImageForName = nameFirstWord
                )
            }

            binding.textName.text = builderListResponse.fname + " " + builderListResponse.lanme
            binding.textMobile.text = builderListResponse.mobile
            binding.textEmail.text = builderListResponse.email

            binding.clView.setOnClickListener {
                onItemClickListener.onItemClick(it, position = adapterPosition)
            }
        }

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                p0?.let {
                    suggestions.clear()
                    for (filterSuggestion in tempList) {

                        if (filterSuggestion.fname!!.contains(
                                it,
                                ignoreCase = true
                            ) || filterSuggestion.lanme!!.contains(
                                it,
                                ignoreCase = true
                            ) || filterSuggestion.email!!.contains(
                                it,
                                ignoreCase = true
                            ) || filterSuggestion.mobile!!.contains(it, ignoreCase = true)
                        )
                            suggestions.add(filterSuggestion)


                    }

                    val result = FilterResults()
                    result.values = suggestions
                    result.count = suggestions.size
                    return result
                } ?: kotlin.run {
                    return FilterResults()
                }
            }

            override fun publishResults(p0: CharSequence?, filterResults: FilterResults?) {
                arrayList = filterResults?.values as ArrayList<BuilderListResponse>
                notifyDataSetChanged()

            }

            override fun convertResultToString(resultValue: Any?): CharSequence {
                resultValue?.let {
                    val filterSuggestion = (it as NearByConductorListResponse)
                    return filterSuggestion.fname?.trim().toString()
                    // return if (filterSuggestion.type == FilterSuggestionType.HOTEL) filterSuggestion.hotel!!.name else filterSuggestion.location!!.name
                } ?: run {
                    return ""
                }
            }
        }
    }

}