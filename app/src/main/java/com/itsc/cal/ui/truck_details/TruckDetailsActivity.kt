package com.itsc.cal.ui.truck_details

import android.os.Bundle
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityTruckDetailsBinding
import com.itsc.cal.databinding.CarouselItemBinding
import com.itsc.cal.model.TruckDetailsResponse
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class TruckDetailsActivity : CalSuperActivity(withNav = true), KodeinAware {

    private lateinit var binding:ActivityTruckDetailsBinding
    override val kodein: Kodein by kodein()
    private lateinit var viewModel: ViewModel
    private val viewModelProvider: ViewModelProvider by instance()
    private var truckId:String=""
    private var imageList:ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_truck_details)
        enableBackButton()

        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        initView()
        showHelpButton()

        bindViewHolder()
    }
    private fun initView(){
        intent?.let {
            truckId = intent.getStringExtra(KeywordsAndConstants.DATA)?:""
            getTruckDetails(truckId)
        }
    }

    private fun getTruckDetails(truckId:String){
        showProgress()
        viewModel.getTruckList(truckId)
    }

    private fun bindViewHolder() {
        viewModel.mediatorLiveTruckDetails.observe(
            this,
            androidx.lifecycle.Observer<Event<TruckDetailsResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setData(it.getContent()!!)

                    }
                }
            }
        )

        viewModel.mediatorLiveTruckDetailsError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun setData(truckDetailsResponse: TruckDetailsResponse){
        binding.textColor.text = truckDetailsResponse.truck_color
        binding.textModel.text = truckDetailsResponse.truck_model
        binding.textWeight.text = truckDetailsResponse.truck_weight_metric
        binding.textNumber.text = truckDetailsResponse.truck_vin_number
        binding.textType.text = truckDetailsResponse.truck_type_name
        setPageTitle(truckDetailsResponse.truck_vin_number?:"Truck Detail")
        imageList.add("${KeywordsAndConstants.BASE_URL}${truckDetailsResponse.truckimage}")
        for (i in 0 until truckDetailsResponse.images.size){
            imageList.add(truckDetailsResponse.images.get(i).full_image?:"")
        }
        setUpCarousel(imageList)
    }
    private fun setUpCarousel(imageList:ArrayList<String>) {
        binding.carousel.setViewListener { position ->
            val carouselItemBinding =
                CarouselItemBinding.inflate(
                    layoutInflater,
                    null,
                    false
                )
            carouselItemBinding.imageView.setImage(
                imageUrl = imageList.get(position)
            )
            carouselItemBinding.root
        }
        binding.carousel.pageCount = imageList.size
    }
}