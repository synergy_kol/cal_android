package  com.itsc.cal.ui.my_booking.fragment

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveJobListingCurrent: MediatorLiveData<Event<ArrayList<JobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveJobListingCurrentError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveJobListingPast: MediatorLiveData<Event<ArrayList<JobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveJobListingPastError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveJobListingCurrent.addSource(
            repository.mutableLiveDataJobListingCurrent
        ) { t -> mediatorLiveJobListingCurrent.postValue(t) }
        mediatorLiveJobListingCurrentError.addSource(
            repository.mutableLiveDataJobListingCurrentError
        ) { t -> mediatorLiveJobListingCurrentError.postValue(t) }

        mediatorLiveJobListingPast.addSource(
            repository.mutableLiveDataJobListingPast
        ) { t -> mediatorLiveJobListingPast.postValue(t) }
        mediatorLiveJobListingPastError.addSource(
            repository.mutableLiveDataJobListingPastError
        ) { t -> mediatorLiveJobListingPastError.postValue(t) }


    }

    fun getJobListCurrent(filter:String) = repository.getMyBookingCurrent(filter = filter)

    fun getJobListPast(filter:String) = repository.getMyBookingPast(filter = filter)



}