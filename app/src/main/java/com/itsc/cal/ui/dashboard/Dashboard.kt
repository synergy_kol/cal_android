package com.itsc.cal.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.core.KeywordsAndConstants.DELAY_FOR_ADVERTISEMENT
import com.itsc.cal.databinding.ActivityDashboardBinding
import com.itsc.cal.enums.Menus
import com.itsc.cal.enums.UserType
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.alert.AlertActivity
import com.itsc.cal.ui.dashboard.adapter.HomeSlidingAdapter
import com.itsc.cal.ui.dashboard.adapter.MenusAdapter
import com.itsc.cal.ui.post_hault_of_job_list.post_export_job_two.PostExportJobTwoActivity
import com.itsc.cal.ui.sos_document_submited.SOSDocumentSubmitedActivity
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class Dashboard : CalSuperActivity(withNav = true), KodeinAware {

    override val kodein: Kodein by kodein()

    private val repository: Repository by instance()
    private lateinit var binding: ActivityDashboardBinding
    private lateinit var userType: UserType
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var menuItems: ArrayList<DashboardGridItem>
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var notificationCount = ""
    private lateinit var menusAdapter: MenusAdapter
    private var notificationList: ArrayList<notificationList> = ArrayList()
    var driver_no = 0
    var subscription_status = 0
    var truck_no = 0
    var certificate_status = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_dashboard)

        userType = repository.getUserType()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        showDashboardActionbar()

        setUpDrawer(repository.getNavMenuItems())
        if (repository.getUserData()!!.user_role.equals("4")) {
            setPageTitle("Independent Truck/Fleet")
        } else if (repository.getUserData()!!.user_role.equals("2")) {
            setPageTitle("Contractor/Builder")
        } else if (repository.getUserData()!!.user_role.equals("3")) {
            setPageTitle("Material Manufacturer/Supplier")
        }


        bindToViewModel()
        setUpMenuItems()
    }

    private fun getNotificationDetails() {
        showProgress()
        viewModel.getNotification()
    }

    private fun bannerList() {
        showProgress()
        viewModel.getBannerList()
    }

    private fun fleetUserDashboadResponse() {
        showProgress()
        viewModel.getFleetUserDashboardStatus()
    }

    private fun getHaulofSubscriptionStatus() {
        showProgress()
        viewModel.getHaulofSubscriptionStatus()
    }

    override fun onResume() {
        super.onResume()
        bannerList()
        getNotificationDetails()

        if (repository.getUserData()!!.user_role.equals("4") && repository.getUserData()!!.sos_verification.equals(
                "0"
            )
        ) {
            startActivity(Intent(this, SOSDocumentSubmitedActivity::class.java).putExtra("from", "menu"))
        }
        if (repository.getUserData()!!.user_role.equals("4")) {
            fleetUserDashboadResponse()
        } else if (repository.getUserData()!!.user_role.equals("2")) {
            getHaulofSubscriptionStatus()
        } else if (repository.getUserData()!!.user_role.equals("3")) {
            getHaulofSubscriptionStatus()
        }

        setUpDrawer(repository.getNavMenuItems())
    }

    private fun imageSlidingAdapter(mDashboardImageList: ArrayList<BannerResponce>) {
        var mHomeSlidingAdapter = HomeSlidingAdapter(this, mDashboardImageList)
        binding.imgCalLogo.adapter = mHomeSlidingAdapter
        binding.indicator.setViewPager(binding.imgCalLogo);
        var density = resources.displayMetrics.density
        //binding.indicator.setRadius(5 * density);
        val handler = Handler()
        var currentPage = 0
        val Update = Runnable {
            if (currentPage == mDashboardImageList.size) {
                currentPage = 0
            }
            binding.imgCalLogo.setCurrentItem(currentPage++, true)

        }

        Timer().schedule(object : TimerTask() {
            // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_FOR_ADVERTISEMENT, DELAY_FOR_ADVERTISEMENT)

    }


    private fun bindToViewModel() {
        viewModel.mediatorLiveNotificationResponse.observe(
            this,
            Observer<Event<NotificationResponce>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        notificationDetails(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveNotificationResponseError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )


        viewModel.mediatorLiveFleetUserDashBoardResponse.observe(
            this,
            Observer<Event<DashboardStatus>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        fleetUserDashboardResponse(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveFleetUserDashBoardResponseError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )

        viewModel.mediatorLiveHaultOfSubscriptionResponse.observe(
            this,
            Observer<Event<HaulofSubscriptionStatusResponce>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        HaultOfSubsriptionResponse(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveHaultOfSubscriptionResponseError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )

        viewModel.mediatorLiveBannerResponse.observe(
            this,
            Observer<Event<ArrayList<BannerResponce>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        imageSlidingAdapter(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveBannerResponseError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )

    }

    private fun notificationDetails(notificationResponce: NotificationResponce) {
        var userDetails = viewModel.giveRepository().getUserData()
        userDetails!!.stripe_account_id = notificationResponce.stripe_check.stripe_account_id
        viewModel.giveRepository().saveUserDataToPref(userDetails)
        notificationCount = notificationResponce.notification_count.count
        Log.i("NotiCount:", notificationCount)
        notificationList = notificationResponce.notification_list

        if (notificationCount != "") {
            menusAdapter.setAlertCount(notificationCount.toInt())
        }
    }

    private fun fleetUserDashboardResponse(dashboardStatus: DashboardStatus) {
        driver_no = dashboardStatus.driver_no ?: 0
        truck_no = dashboardStatus.truck_no ?: 0
        certificate_status = dashboardStatus.certificate_status ?: 0
    }

    private fun HaultOfSubsriptionResponse(haulofSubscriptionStatusResponce: HaulofSubscriptionStatusResponce) {
        subscription_status = haulofSubscriptionStatusResponce.subscription_status ?: 0

    }

    private fun setUpCarousel() {
        /*  binding.carousel.setViewListener { position ->
              val carouselItemBinding =
                  CarouselItemBinding.inflate(
                      layoutInflater,
                      null,
                      false
                  )
              carouselItemBinding.imageView.setImage(
                  "https://picsum.photos/200?${position}"
              )
              carouselItemBinding.root
          }
          binding.carousel.pageCount = 10*/


    }

    private fun setUpMenuItems() {
        binding.recyclerView.apply {
            this@Dashboard.gridLayoutManager = GridLayoutManager(
                this@Dashboard,
                2
            )
            this@Dashboard.gridLayoutManager.spanSizeLookup = object :
                GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    if (
                        this@Dashboard.menuItems.size - 1 == position &&
                        this@Dashboard.menuItems.size % 2 == 1
                    ) {
                        return 2
                    }
                    return 1
                }
            }
            layoutManager = this@Dashboard.gridLayoutManager
            this@Dashboard.menuItems = repository.getDashboardMenuItems()
            menusAdapter = MenusAdapter(
                menuItems,
                this@Dashboard
            ) { item ->
                @Suppress("NON_EXHAUSTIVE_WHEN")
                when (item.menu) {
                    Menus.NEAR_BY_CONTRACTOR -> {
                        navNearByContractor()
                    }
                    Menus.MANAGE_BUILDERS -> {
                        navManageBuilders()
                    }
                    Menus.SOS_ADDITIONAL_REQUEST -> {
                        navSOSAdditionalRequest()
                    }
                    Menus.SOS_MESSAGE_LOG -> {
                        navSOSMessageLogRequest()
                    }
                    Menus.ALERTS -> {
                        if (notificationList != null && notificationList.size > 0) {
                            startActivity(
                                Intent(this@Dashboard, AlertActivity::class.java).putExtra(
                                    KeywordsAndConstants.DATA, toJson(notificationList)
                                ).putExtra(KeywordsAndConstants.IS_FORM_DASHBOARD,true)
                            )
                        } else {
                            showMessageInDialog("No notification found.")
                        }

                    }
                    Menus.LOGOUT -> {
                        navLogout()
                    }
                    Menus.TRACK_STATUS -> {
                        if (repository.getUserData()!!.user_role.equals("4")) {
                            if (certificate_status == 1) {
                                if (driver_no >= 1 && truck_no >= 1) {
                                    navTrackStatus()
                                } else if (driver_no == 0 && truck_no == 0) {
                                    showMessageInDialog("Please provide at least one drive and at least one truck.")
                                } else if (driver_no == 0) {
                                    showMessageInDialog("Please provide at least one drive.")
                                } else if (truck_no == 0) {
                                    showMessageInDialog("Please provide at least one truck.")
                                }
                            } else {
                                showMessageWithOneButton(
                                    message = "Pay certification free",
                                    cancellable = true,
                                    buttonText = "OK",
                                    callback = object : DialogUtil.CallBack {
                                        override fun buttonClicked() {
                                            super.buttonClicked()
                                            navSubscriptionList()
                                        }
                                    }
                                )
                            }

                        } else {
                            navTrackStatus()
                        }

                    }
                    Menus.SEARCH_TRUCK -> {
                        navSearchTruck()
                    }
                    Menus.MY_POSTED_JOBS -> {
                        navMyPostedJob()
                    }
                    Menus.POST_A_JOB -> {
                        navPostAJob()
                    }
                    Menus.SEARCH_CONTRACTOR -> {
                        navSearchContractor()
                    }
                    Menus.SEARCH_MATERIAL_SUPPLIER->{
                        navSearchMaterialSupplier()
                    }
                    Menus.MY_JOBS -> {
                        navMyJobs()
                    }
                    Menus.MANAGE_DRIVER -> {
                        if (certificate_status == 1) {
                            navManagerDriver()
                        } else {
                            showMessageWithOneButton(
                                message = "Pay certification free",
                                cancellable = true,
                                buttonText = "OK",
                                callback = object : DialogUtil.CallBack {
                                    override fun buttonClicked() {
                                        super.buttonClicked()
                                        navSubscriptionList()
                                    }
                                }
                            )
                        }

                    }
                    Menus.POST_TRUCK -> {
                        navPostATruck()
                    }
                    Menus.MANAGE_TRUCK -> {
                        if (certificate_status == 1) {
                            navManageTruck()
                        } else {
                            showMessageWithOneButton(
                                message = "Pay certification free",
                                cancellable = true,
                                buttonText = "OK",
                                callback = object : DialogUtil.CallBack {
                                    override fun buttonClicked() {
                                        super.buttonClicked()
                                        navSubscriptionList()
                                    }
                                }
                            )
                        }

                    }
                    Menus.MY_BOOKINGS -> {
                        if (certificate_status == 1) {
                            if (driver_no >= 1 && truck_no >= 1) {
                                navMyBookings()
                            } else if (driver_no == 0 && truck_no == 0) {
                                showMessageInDialog("Please provide at least one drive and at least one truck.")
                            } else if (driver_no == 0) {
                                showMessageInDialog("Please provide at least one drive.")
                            } else if (truck_no == 0) {
                                showMessageInDialog("Please provide at least one truck.")
                            }
                        } else {
                            showMessageWithOneButton(
                                message = "Pay certification free",
                                cancellable = true,
                                buttonText = "OK",
                                callback = object : DialogUtil.CallBack {
                                    override fun buttonClicked() {
                                        super.buttonClicked()
                                        navSubscriptionList()
                                    }
                                }
                            )
                        }
                    }

                    Menus.SEARCH_FOR_JOBS -> {
                        navSearchJob()
                    }

                    Menus.JOB_LIST_TRUCK_FLEET -> {
                        if (certificate_status == 1) {
                            if (driver_no >= 1 && truck_no >= 1) {
                                navJobListTruckFleet()
                            } else if (driver_no == 0 && truck_no == 0) {
                                showMessageInDialog("Please provide at least one drive and at least one truck.")
                            } else if (driver_no == 0) {
                                showMessageInDialog("Please provide at least one drive.")
                            } else if (truck_no == 0) {
                                showMessageInDialog("Please provide at least one truck.")
                            }
                        } else {
                            showMessageWithOneButton(
                                message = "Pay certification free",
                                cancellable = true,
                                buttonText = "OK",
                                callback = object : DialogUtil.CallBack {
                                    override fun buttonClicked() {
                                        super.buttonClicked()
                                        navSubscriptionList()
                                    }
                                }
                            )
                        }

                    }
                    Menus.TRACKING_TRUCKS_LIVE -> {
                        navTrackingTrucksLive()
                    }
                    Menus.SUBSCRIPTION_LIST -> {
                        navSubscriptionList()
                    }
                    Menus.HAUL_OFF -> {
                        if (subscription_status == 0) {
                            showMessageWithOneButton(
                                message = "Pay Haul-OFF Subscription free",
                                cancellable = true,
                                buttonText = "OK",
                                callback = object : DialogUtil.CallBack {
                                    override fun buttonClicked() {
                                        super.buttonClicked()
                                        navSubscriptionList()
                                    }
                                }
                            )
                        } else if (subscription_status == 1) {
                            navHaulOff()
                        } else if (subscription_status == 2) {
                            showMessageWithOneButton(
                                message = "Your Haul-OFF subscription plan expired",
                                cancellable = true,
                                buttonText = "OK",
                                callback = object : DialogUtil.CallBack {
                                    override fun buttonClicked() {
                                        super.buttonClicked()
                                        navSubscriptionList()
                                    }
                                }
                            )
                        }

                    }
                }
            }
            adapter = menusAdapter
        }
    }
}