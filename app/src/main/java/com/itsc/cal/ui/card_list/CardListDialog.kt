package com.itsc.cal.ui.card_list

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.View
import android.view.Window
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.interfaces.OnCardSelectedListener
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.CardListResponse
import com.itsc.cal.ui.card_list.adapter.SelectCardListAdapter

class CardListDialog(val mContext: Context, val arrayList: ArrayList<CardListResponse>?, val mOnCardSelectedListener: OnCardSelectedListener):
    Dialog(mContext),
    View.OnClickListener {
private lateinit var buttonAddNewCard:AppCompatButton
private lateinit var rvCardList:RecyclerView
private  var selectedCard:CardListResponse?=null
    init{
        val displayMetrics = DisplayMetrics()
        window?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_select_card)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window?.setLayout((width/1.2).toInt(), (height/2))
        initView()
    }

    private fun initView() {
        buttonAddNewCard = findViewById(R.id.buttonAddNewCard)
        rvCardList = findViewById(R.id.rvCardList)
        buttonAddNewCard.setOnClickListener(this)
        if(arrayList==null && arrayList?.size==0){
            buttonAddNewCard.text = "Add New Card"
        }else{
            arrayList?.get(0)?.isSelected = true
            selectedCard = arrayList?.get(0)
            buttonAddNewCard.text = "Submit"
        }
        setAdapter()
    }


    override fun onClick(view: View?) {
       when(view?.id){
           R.id.buttonAddNewCard->{
               dismiss()
               mOnCardSelectedListener.onSelectCard(view as AppCompatButton, selectedCard)
           }
       }
    }

    private fun setAdapter(){
        rvCardList.apply {
            layoutManager = LinearLayoutManager(mContext)
            adapter = SelectCardListAdapter(mContext,arrayList, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    when (view.id) {
                        R.id.llSelectCard -> {
                            selectCard(position)
                        }
                        R.id.rbCardList -> {
                            selectCard(position)
                        }
                    }
                }

            })
        }
    }
    private fun selectCard(position:Int){
        arrayList?.get(position)?.isSelected = true
        selectedCard = arrayList?.get(position)
        for(i in 0 until (arrayList?.size?:0)){
            if(i!=position) {
                arrayList?.get(i)?.isSelected = false
            }
        }
        rvCardList.adapter?.notifyDataSetChanged()
    }
}