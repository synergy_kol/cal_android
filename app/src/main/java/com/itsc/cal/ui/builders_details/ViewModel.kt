package  com.itsc.cal.ui.builders_details

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveBuilderListing: MediatorLiveData<Event<ArrayList<BuilderListResponse>>> =
        MediatorLiveData()
    val mediatorLiveBuilderListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveSupplierListing: MediatorLiveData<Event<ArrayList<BuilderListResponse>>> =
        MediatorLiveData()
    val mediatorLiveSupplierListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLivePostJob: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLivePostJobError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveBuilderListing.addSource(
            repository.mutableLiveDataBuilderList
        ) { t -> mediatorLiveBuilderListing.postValue(t) }
        mediatorLiveBuilderListingError.addSource(
            repository.mutableLiveDataBuilderListError
        ) { t -> mediatorLiveBuilderListingError.postValue(t) }


        mediatorLiveSupplierListing.addSource(
            repository.mutableLiveDataSupplierList
        ) { t -> mediatorLiveSupplierListing.postValue(t) }
        mediatorLiveSupplierListingError.addSource(
            repository.mutableLiveDataSupplierListError
        ) { t -> mediatorLiveSupplierListingError.postValue(t) }


        mediatorLivePostJob.addSource(
            repository.mutableLiveDataPostJob
        ) { t -> mediatorLivePostJob.postValue(t) }
        mediatorLivePostJobError.addSource(
            repository.mutableLiveDataPostJobError
        ) { t -> mediatorLivePostJobError.postValue(t) }
    }

    fun getBuilderList() = repository.getBuilderList()

    fun getSupplierList() = repository.getSupplierList()

    fun postJob(
        source_lat: String,
        Equipment_value: String,
        Weight: String,
        TruckType: String,
        Source: String,
        priority: String,
        pickup_time: String,
        pickup_date: String,
        NoOfTrucks:String,
        MaterialType:String,
        LodeType:String,
        JobEstimatePrice:String,
        FreightCharges:String,
        Equipment:String,
        Destination:String,
        delivery_time:String,
        delivery_date:String,
        destination_long:String,
        destination_lat:String,
        source_long:String,
        builderId:String,
        payment_type:String,
        stripeToken:String,
        card_id:String,
        jobPayableAmount:String
    ) =
        repository.postJob(
            source_lat = source_lat,
            Equipment_value = Equipment_value,
            Weight = Weight,
            TruckType = TruckType,
            Source = Source,
            priority = priority,
            pickup_time = pickup_time,
            pickup_date = pickup_date,
            NoOfTrucks = NoOfTrucks,
            MaterialType = MaterialType,
            LodeType = LodeType,
            JobEstimatePrice = JobEstimatePrice,
            FreightCharges = FreightCharges,
            Equipment = Equipment,
            Destination = Destination,
            delivery_time = delivery_time,
            delivery_date = delivery_date,
            destination_long = destination_long,
            destination_lat = destination_lat,
            source_long = source_long,
            builderId = builderId,
            payment_type = payment_type,
            stripeToken = stripeToken,
            card_id = card_id,
            jobPayableAmount= jobPayableAmount
        )

}