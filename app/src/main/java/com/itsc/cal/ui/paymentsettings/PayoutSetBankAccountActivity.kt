package com.itsc.cal.ui.paymentsettings

import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityPaySetBankAccountBinding
import com.itsc.cal.model.PayoutSettingResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.repository.Event
import com.itsc.cal.util.model.Result
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class PayoutSetBankAccountActivity:CalSuperActivity(), KodeinAware {
    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPaySetBankAccountBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_pay_set_bank_account)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        enableBackButton()
        showHelpButton()
        bindingViewModel()
        setPageTitle("ADD/EDIT BANK ACCOUNT")
        initView()
    }
    private fun getPaymentSettingResponse():PayoutSettingResponse?{
       return intent.getParcelableExtra("payout_setting") as PayoutSettingResponse?
    }

    private fun initView() {
        val paySetting = getPaymentSettingResponse()

        binding.apply {
            if(paySetting!=null && paySetting.connect_type=="2"){
                editTextAccNum.setText(paySetting.account?:"")
                editTextRoutingNumber.setText(paySetting.routing?:"")
            }

            editTextAccountHolderName.apply {
                inputMode(EditTextInputMode.INPUT_TEXT)
            }
           editTextRoutingNumber.apply {
               inputMode(EditTextInputMode.NUMBER)
           }
            editTextAccNum.apply {
                inputMode(EditTextInputMode.NUMBER)
            }
            edtReAccNum.apply {
                inputMode(EditTextInputMode.NUMBER)
            }
            buttonSave.setOnClickListener {
                ApiCalled()
            }
        }
    }

    private fun bindingViewModel() {
        viewModel.mediatorLivePayoutAddBank.observe(
            this,
            androidx.lifecycle.Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLivePayoutAddBankError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }
    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Bank added successfully",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    finish()

                }
            }
        )

    }

    private fun ApiCalled() {

        if (binding.editTextAccountHolderName.getText().isEmpty()) {
            showMessageInDialog("Please provide account holder name.")
            return
        }
        if (binding.editTextRoutingNumber.getText().isEmpty()) {
            showMessageInDialog("Please provide routing number")
            return
        }

        if (binding.editTextAccNum.getText().isEmpty()) {
            showMessageInDialog("Please provide account number.")
            return
        }

        if (binding.edtReAccNum.getText().isEmpty()) {
            showMessageInDialog("Please provide re-enter account number")
            return
        }
        if (binding.edtReAccNum.getText()!=binding.editTextAccNum.getText()) {
            showMessageInDialog("Account Number does not match")
            return
        }


        showProgress()
        viewModel.payoutAddBank(
            routing_number = binding.editTextRoutingNumber.getText(),
            number = binding.editTextAccNum.getText(),
                    account_holder_name =  binding.editTextAccountHolderName.getText()
        )
    }
}