package  com.itsc.cal.ui.post_a_haul_off

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveTruckTypeList: MediatorLiveData<Event<ArrayList<TruckTypeResponse>>> =
        MediatorLiveData()
    val mediatorLiveTruckTypeListError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    val mediatorLiveDefalutCard: MediatorLiveData<Event<CardResponse>> = MediatorLiveData()
    val mediatorLiveDefalutCardError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveMaterialEquipementResult: MediatorLiveData<Event<MaterialEquipmentResponse>> =
        MediatorLiveData()
    val mediatorLiveMaterialEquipementError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveTruckData: MediatorLiveData<Event<ArrayList<TruckDataResponse>>> =
        MediatorLiveData()
    val mediatorLiveTruckDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveHaulJobConfigrationData: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveHaulJobConfigrationError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDataPaymentPercentage: MediatorLiveData<Event<PaymentPecentageResponse>> =
        MediatorLiveData()
    val mediatorLiveDataPaymentPercentageError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveTruckTypeList.addSource(
            repository.mutableLiveDataTruckTypeList
        ) { t -> mediatorLiveTruckTypeList.postValue(t) }
        mediatorLiveTruckTypeListError.addSource(
            repository.mutableLiveDataTruckTypeListError
        ) { t -> mediatorLiveTruckTypeListError.postValue(t) }


        mediatorLiveDefalutCard.addSource(
            repository.mutableLiveDataDefaultCardHault
        ) { t -> mediatorLiveDefalutCard.postValue(t) }
        mediatorLiveDefalutCardError.addSource(
            repository.mutableLiveDataDefaultCardErrorHault
        ) { t -> mediatorLiveDefalutCardError.postValue(t) }

        mediatorLiveMaterialEquipementResult.addSource(
            repository.mutableLiveDataMaterialEquipment
        ) { t -> mediatorLiveMaterialEquipementResult.postValue(t) }
        mediatorLiveMaterialEquipementError.addSource(
            repository.mutableLiveDataMaterialEquiementError
        ) { t -> mediatorLiveMaterialEquipementError.postValue(t) }


        mediatorLiveTruckData.addSource(
            repository.mutableLiveDataTruckDataHault
        ) { t -> mediatorLiveTruckData.postValue(t) }
        mediatorLiveTruckDataError.addSource(
            repository.mutableLiveDataTruckDataErrorHault
        ) { t -> mediatorLiveTruckDataError.postValue(t) }

        mediatorLiveHaulJobConfigrationData.addSource(
            repository.mutableLiveDataJobHaulConfigration
        ) { t -> mediatorLiveHaulJobConfigrationData.postValue(t) }
        mediatorLiveHaulJobConfigrationError.addSource(
            repository.mutableLiveDataJobHaulConfigrationError
        ) { t -> mediatorLiveHaulJobConfigrationError.postValue(t) }

        mediatorLiveDataPaymentPercentage.addSource(
            repository.mutableLiveDataPaymentPercentage
        ) { t -> mediatorLiveDataPaymentPercentage.postValue(t) }
        mediatorLiveDataPaymentPercentageError.addSource(
            repository.mutableLiveDataPaymentPercentageError
        ) { t -> mediatorLiveDataPaymentPercentageError.postValue(t) }

    }


    fun getDefaultCard() = repository.getDefaultCardHaul()

    fun getPaymentPercentage() = repository.getPaymentPercentage()
    fun getMaterialEquipmentList() = repository.getMaterialEquipmentList()
    fun getTruckData() = repository.getMasterTruckDataHaulOff()
    fun postJobHaulOff(
        materialType: String,
        MaterialoptionType: String,
        spec_sheet: String,
        materials_available: String,
        lib_information: String,
        material_cont: String,
        Source: String,
        Destination: String,
        haul_paymemt_amt: String,
        payment_type: String,
        source_lat: String,
        stripeToken: String,
        source_long: String,
        destination_lat: String,
        destination_long: String,
        TruckType: String,
        NoOfTrucks: String,
        pickup_date:String,
        pickup_time:String,
        delivery_time:String,
        delivery_date:String,
        equipementId: String,
        equipmentOptionId: String,
        export_request_id:String,
        exporter_id:String,
        haul_request_mapid:String,
        import_request_id:String,
        importer_id:String,
        specs_sheet:String
    ) =
        repository.jobPostHaulOff(
            materialType = materialType,
            MaterialoptionType = MaterialoptionType,
            spec_sheet = spec_sheet,
            lib_information = lib_information,
            material_cont = material_cont,
            materials_available = materials_available,
            Source = Source,
            Destination = Destination,
            haul_paymemt_amt = haul_paymemt_amt,
            payment_type = payment_type,
            stripeToken = stripeToken,
            source_lat = source_lat,
            source_long = source_long,
            destination_lat = destination_lat,
            destination_long = destination_long,
            TruckType = TruckType,
            NoOfTrucks = NoOfTrucks,
            equipmentId = equipementId,
            equipmentOptionId = equipmentOptionId,
            pickup_time = pickup_time,
            pickup_date = pickup_date,
            delivery_date = delivery_date,
            delivery_time = delivery_time,
            export_request_id=export_request_id,
            exporter_id= exporter_id,
            haul_request_mapid=haul_request_mapid,
            import_request_id=import_request_id,
            importer_id=importer_id,
            specs_sheet = specs_sheet
        )

}