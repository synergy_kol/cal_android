package com.itsc.cal.ui.arrange_trip

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityArrangeTripBinding
import com.itsc.cal.model.DriverListResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.model.TruckListResponse
import com.itsc.cal.ui.arrange_trip.adapter.DriverListAdapter
import com.itsc.cal.ui.arrange_trip.adapter.TruckListAdapter
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ArrangeTripActivity : CalSuperActivity(), KodeinAware {
    private lateinit var binding: ActivityArrangeTripBinding
    override val kodein: Kodein by kodein()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var jobId: String = ""
    private var driverId: ArrayList<String> = ArrayList()
    private var truckId: ArrayList<String> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_arrange_trip)
        enableBackButton()
        setPageTitle("Arrange Trip")
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        initView()
        getTruckList()
        getDriverList()
        bindViewHolder()

    }

    private fun initView() {
        intent?.let {
            jobId = intent.getStringExtra(KeywordsAndConstants.DATA)?:""
        }
        binding.buttonContinue.setOnClickListener {
            apicalled()
        }
    }

    private fun apicalled() {
        if (
            truckId.size == 0
        ) {
            showMessageInDialog("Please select truck.")
            return
        }

        if (
            driverId.size == 0
        ) {
            showMessageInDialog("Please select driver.")
            return
        }
        showProgress()
        viewModel.jobConfigaration(jobId,truckId,driverId)
    }

    private fun getTruckList() {
        showProgress()
        viewModel.getTruckType()
    }

    private fun getDriverList() {
        showProgress()
        viewModel.getDriverList()
    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveTruckTypeList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<TruckListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setTruckAdapter(it.getContent() ?: ArrayList())
                    }
                }
            }
        )

        viewModel.mediatorLiveTruckTypeListError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDriverList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<DriverListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setDriverAdapter(it.getContent() ?: ArrayList())
                    }
                }
            }
        )

        viewModel.mediatorLiveDriverError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveJobConfigation.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveJobConfigationError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }
    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",

            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@ArrangeTripActivity,
                                Dashboard::class.java
                            )
                        )
                    )
                    finish()

                }
            }
        )

    }

    private fun setTruckAdapter(arrayList: ArrayList<TruckListResponse>) {
        binding.recycleviewTruckList.apply {
            layoutManager =
                LinearLayoutManager(this@ArrangeTripActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = TruckListAdapter(this@ArrangeTripActivity, object : TruckListAdapter.truckOnItemClickListener {
                override fun onItemClick(position: Int) {
                    if (arrayList.get(position).selected.equals("1")) {
                        arrayList.get(position).selected = "0"
                        truckId.remove(arrayList.get(position).truck_id)
                    } else {
                        arrayList.get(position).selected = "1"
                        truckId.add(arrayList.get(position).truck_id?:"")
                    }

                    binding.recycleviewTruckList.adapter?.notifyDataSetChanged()
                }

            }, arrayList)
        }
    }

    private fun setDriverAdapter(driverList: ArrayList<DriverListResponse>) {
        binding.recycleviewDriverList.apply {
            layoutManager =
                LinearLayoutManager(this@ArrangeTripActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = DriverListAdapter(
                this@ArrangeTripActivity,
                object : DriverListAdapter.driverOnItemClickListener {
                    override fun onItemClick(position: Int) {
                        if (driverList.get(position).selected.equals("1")) {
                            driverList.get(position).selected = "0"
                            driverId.remove(driverList.get(position).driver_id)
                        } else {
                            driverList.get(position).selected = "1"
                            driverId.add(driverList.get(position).driver_id)
                        }
                        binding.recycleviewDriverList.adapter?.notifyDataSetChanged()
                    }

                },
                driverList
            )
        }
    }
}