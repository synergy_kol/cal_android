package com.itsc.cal.ui.splash_screen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants.WAIT_INTERVAL_SPLASH
import com.itsc.cal.databinding.ActivityMainBinding
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.on_boarding.OnBoarding
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SplashScreen : CalSuperActivity(trulyInfinite = true), KodeinAware {

    override val kodein: Kodein by kodein()
    private val repository: Repository by instance()
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_main)

        Handler().postDelayed(
            {
                decideWhereToRedirect()
            },
            WAIT_INTERVAL_SPLASH
        )
    }

    private fun decideWhereToRedirect() {

        repository.getUserData()?.let { userDetails ->
            if (!userDetails.user_master_id.equals("") && userDetails.verification_status.equals("1") && userDetails.is_trusted.equals("1")
                && userDetails.completed_steps.equals("6")
            ) {
                moveToDashboard()
            } else {
                proceed()
            }

        } ?: run {
            proceed()
        }

    }

    private fun moveToDashboard() {
        startActivity(Intent(this, Dashboard::class.java))
        finish()
    }

    private fun proceed() {
        startActivity(Intent(this, OnBoarding::class.java))
        finish()
    }
}
