package com.itsc.cal.ui.post_a_haul_off

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.TimePicker
import androidx.lifecycle.Observer
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityPostAHaulOffBinding
import com.itsc.cal.model.*
import com.itsc.cal.ui.card_list.CardListActivity
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.generateStripeToken
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.itsc.cal.widget.spinner.Spinner
import com.itsc.cal.widget.spinner.SpinnerData
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class PostAHaulOffActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPostAHaulOffBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var truckTypeResponse: TruckTypeResponse

    companion object {
        var postATruckPage: Activity? = null
    }

    private val AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS: Int = 1
    private val AUTOCOMPLETE_REQUEST_CODE_DESTINATION_ADDRESS: Int = 2
    private var userLatLng: LatLng? = null
    private var userLatLngDestination: LatLng? = null
    private var mLocationPermissionGranted = false
    private var latitude: String = ""
    private var longitude: String = ""
    private lateinit var materialResponse: MaterialResponse
    private var material_option: MaterialOptionResponse = MaterialOptionResponse("", "", "", "")
    private lateinit var equipementList: ArrayList<String>
    private lateinit var equipmentOptionList: ArrayList<String>
    private val hasmap: HashMap<Int, String> = HashMap<Int, String>()
    private lateinit var truckDataResponse: TruckDataResponse
    private var paymentType = ""
    private var paymentPercentage = 0.0
    private var pickup: Calendar = Calendar.getInstance()
    private var delivery: Calendar = Calendar.getInstance()
    private lateinit var jobResponseSource: HauljobListResponse
    private lateinit var jobResponceDestination: HauljobListResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_post_a_haul_off)
        enableBackButton()
        setPageTitle("Post a Job")
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        postATruckPage = this
        intiView()
        bindViewHolder()
        getTruckType()
        getPaymentPercentage()

    }



    private fun getPaymentPercentage() {
        showProgress()
        viewModel.getPaymentPercentage()
    }

    private fun getTruckType() {
        showProgress()
        viewModel.getTruckData()
    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveDefalutCard.observe(
            this,
            Observer<Event<CardResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        getDefalutCardData(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDefalutCardError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                        defalutCardError("")
                    }

                }
            }
        )

        viewModel.mediatorLiveDataPaymentPercentage.observe(
            this,
            Observer<Event<PaymentPecentageResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        paymentPercentage(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataPaymentPercentageError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveMaterialEquipementResult.observe(
            this,
            Observer<Event<MaterialEquipmentResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setEquipmentList(it.getContent())
                    }

                }
            }
        )

        viewModel.mediatorLiveMaterialEquipementError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveTruckData.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<TruckDataResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val items: ArrayList<SpinnerData<TruckDataResponse>> = ArrayList()
                        it.getContent()!!.forEach { source ->
                            items.add(
                                SpinnerData(
                                    source.name,
                                    source
                                )
                            )
                        }
                        if (items.size > 0)
                            truckDataResponse = items[0].data
                        binding.spinnerTruckType.setBg()
                        binding.spinnerTruckType.addItems(
                            items,
                            withDot = false,
                            listener = object : Spinner.OnItemSelectedListener {
                                override fun <T> selected(item: SpinnerData<T>) {
                                    truckDataResponse = item.data as TruckDataResponse


                                }
                            }
                        )

                    }
                }
            }
        )

        viewModel.mediatorLiveHaulJobConfigrationData.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponse(it.getContent()!!.message)
                    }

                }
            }
        )

        viewModel.mediatorLiveHaulJobConfigrationError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDefalutCardError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                        defalutCardError("")
                    }

                }
            }
        )

    }

    private fun intiView() {

        intent.let {
            jobResponseSource =
                fromJson(intent.getStringExtra(KeywordsAndConstants.DESTINATION_DATA))
            jobResponceDestination = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            binding.editSourceAddress.text = jobResponseSource.address
            binding.textMaterialType.text = jobResponseSource.material_name
            if (jobResponseSource.material_option_name!=null){
                binding.textMaterialOption.visibility = View.VISIBLE
                binding.textMaterialTypeOption.visibility = View.VISIBLE
                binding.textMaterialTypeOption.text = jobResponseSource.material_option_name
            }else{
                binding.textMaterialOption.visibility = View.GONE
                binding.textMaterialTypeOption.visibility = View.GONE
            }

            binding.textEquipementTypeOption.text = jobResponseSource.loading_equipment_name
            binding.editDestinationAddress.text = jobResponceDestination.address

        }
/*
        binding.edSpecSheet.apply {
            hint("Enter Spec Sheets")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }*/

        binding.edLiabilityInformation.apply {
            hint("Enter Liability Information")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.edMaterialContent.apply {
            hint("Enter Material Content")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.editNoOfTruck.apply {
            inputMode(EditTextInputMode.NUMBER)
            setMaxLength(4)
        }

        binding.edMaterialAvailable.apply {
            hint("Enter Material Available")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }
        /*binding.editJobEstimatedPrice.apply {
            inputMode(EditTextInputMode.DECIMAL)
            getDrawable(
                R.drawable.dollar
            )?.let { drawable ->
                drawableStart(drawable)
            }
            setMaxLength(10)

        }*/
        binding.editJobEstimatedPrice.setCompoundDrawablesWithIntrinsicBounds(
            getDrawable(
                R.drawable.dollar
            ), null, null, null
        )
        binding.editJobEstimatedPrice.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString() != null && s.toString() != "") {
                    var price =
                        (s.toString().toDouble() * paymentPercentage) / 100
                    binding.editJobParcialAmount.setText(
                        price.toString()
                    )
                } else {
                    binding.editJobParcialAmount.setText(
                        ""
                    )
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        binding.editJobParcialAmount.apply {
            inputMode(EditTextInputMode.DECIMAL)
            getDrawable(
                R.drawable.dollar
            )?.let { drawable ->
                drawableStart(drawable)
            }

            setMaxLength(10)
        }

        binding.btnPostATruck.setOnClickListener {
            moveToNext()
        }


        binding.radioButtonFull.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                paymentType = "F"
                binding.editJobParcialAmount.isEnabled = false
                binding.editJobParcialAmount.visibility = View.GONE
                binding.tvJobPaAmount.visibility = View.GONE
            } else {
                paymentType = "P"
                binding.editJobParcialAmount.isEnabled = false
                if (!binding.editJobEstimatedPrice.getText().toString().isEmpty()) {
                    var price =
                        (binding.editJobEstimatedPrice.getText().toString()
                            .toDouble() * paymentPercentage) / 100
                    binding.editJobParcialAmount.setText(
                        price.toString()
                    )
                } else {
                    binding.editJobParcialAmount.setText(
                        "0"
                    )
                }

                binding.editJobParcialAmount.visibility = View.VISIBLE
                binding.tvJobPaAmount.visibility = View.VISIBLE

            }
        }

        binding.radioButtonPartial.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                paymentType = "P"
                binding.editJobParcialAmount.isEnabled = false
                if (!binding.editJobEstimatedPrice.getText().toString().isEmpty()) {
                    var price =
                        (binding.editJobEstimatedPrice.getText().toString()
                            .toDouble() * paymentPercentage) / 100
                    binding.editJobParcialAmount.setText(
                        price.toString()
                    )
                } else {
                    binding.editJobParcialAmount.setText(
                        "0"
                    )
                }
                binding.editJobParcialAmount.visibility = View.VISIBLE
                binding.tvJobPaAmount.visibility = View.VISIBLE

            } else {
                paymentType = "F"
                binding.editJobParcialAmount.isEnabled = false
                binding.editJobParcialAmount.visibility = View.GONE
                binding.tvJobPaAmount.visibility = View.GONE
            }
        }

        binding.tvPickupDate.setOnClickListener {
            AndroidUtility.showDatePicker(
                supportFragmentManager,
                com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val selectedDate =
                        dayOfMonth.toString() + "/" + (monthOfYear + 1).toString() + "/" + year
                    val fromDate = AndroidUtility.formatDateFromString(
                        "dd/MM/yyyy",
                        "yyyy-MM-dd",
                        selectedDate
                    )
                    pickup.set(Calendar.YEAR, year)
                    pickup.set(Calendar.MONTH, monthOfYear)
                    pickup.set(Calendar.DATE, dayOfMonth)
                    binding.tvPickupDate.setText(fromDate)
                })
        }
        binding.tvPickupTime.setOnClickListener {
            showTimepickerDialogeStTimePickUptime()
        }
        binding.tvDeliveryDate.setOnClickListener {
            AndroidUtility.showDatePicker(
                supportFragmentManager,
                com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val selectedDate =
                        dayOfMonth.toString() + "/" + (monthOfYear + 1).toString() + "/" + year
                    val fromDate = AndroidUtility.formatDateFromString(
                        "dd/MM/yyyy",
                        "yyyy-MM-dd",
                        selectedDate
                    )
                    delivery.set(Calendar.YEAR, year)
                    delivery.set(Calendar.MONTH, monthOfYear)
                    delivery.set(Calendar.DATE, dayOfMonth)
                    binding.tvDeliveryDate.setText(fromDate)
                })
        }

        binding.tvDeliveryTime.setOnClickListener {
            showTimepickerDialogeStTimeDeliveryTime()
        }
    }

    fun showTimepickerDialogeStTimePickUptime() {
        val calendar = Calendar.getInstance()
        val mHour = calendar.get(Calendar.HOUR_OF_DAY)
        val mMinute = calendar.get(Calendar.MINUTE)

        var timePickerDialog = TimePickerDialog(this, object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {

                val fromDate = AndroidUtility.formatDateFromString(
                    "HH:mm",
                    "HH:mm",
                    "$p1:$p2"
                )
                binding.tvPickupTime.setText(fromDate)

                pickup.set(Calendar.HOUR_OF_DAY, p1)
                pickup.set(Calendar.MINUTE, p2)
            }

        }, mHour, mMinute, false)
        timePickerDialog.show()

    }

    fun showTimepickerDialogeStTimeDeliveryTime() {
        val calendar = Calendar.getInstance()
        val mHour = calendar.get(Calendar.HOUR_OF_DAY)
        val mMinute = calendar.get(Calendar.MINUTE)

        var timePickerDialog = TimePickerDialog(this, object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {

                val fromDate = AndroidUtility.formatDateFromString(
                    "HH:mm",
                    "HH:mm",
                    "$p1:$p2"
                )
                binding.tvDeliveryTime.setText(fromDate)

                delivery.set(Calendar.HOUR_OF_DAY, p1)
                delivery.set(Calendar.MINUTE, p2)
            }

        }, mHour, mMinute, false)
        timePickerDialog.show()

    }

    private fun setEquipmentList(materialEquipmentResponse: MaterialEquipmentResponse?) {



    }

    private fun moveToNext() {
/*
        if (
            binding.edSpecSheet.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Spec Sheet.")
            return
        }*/
        if (
            binding.edLiabilityInformation.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Liability Information.")
            return
        }

        if (
            binding.edMaterialContent.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Material Content.")
            return
        }

        if (
            binding.edMaterialAvailable.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Material Available .")
            return
        }

        if (
            binding.editSourceAddress.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Pickup Location.")
            return
        }
        if (
            binding.editDestinationAddress.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Drop-off Location.")
            return
        }

        if (
            binding.editNoOfTruck.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide no. of truck.")
            return
        }

        if (
            binding.editJobEstimatedPrice.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Job Estimated Price.")
            return
        }

        if (
            binding.editJobEstimatedPrice.getText().toString().toDouble() == 0.0
        ) {
            showMessageInDialog("Please provide valid Job Estimated Price.")
            return
        }

        if (paymentType == "") {
            showMessageInDialog("Please select Payment Deduction type.")
            return
        }

        if (
            binding.tvPickupDate.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Pickup Date.")
            return
        }

        if (
            binding.tvPickupTime.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Pickup Time.")
            return
        }

        if (
            binding.tvDeliveryDate.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Delivery Date.")
            return
        }

        if (
            binding.tvDeliveryTime.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Delivery Time.")
            return
        }

        if (delivery.timeInMillis < pickup.timeInMillis) {
            showMessageInDialog("Please provide Delivery Date and Time after Pickup Date and Time.")
            return
        }


        equipementList = ArrayList()
        equipmentOptionList = ArrayList()
        for (key in hasmap.keys) {
            Log.i("Element:", "$key=${hasmap[key]}")
            equipementList.add("$key")
            equipmentOptionList.add("${hasmap[key]}")
        }
        showProgress()
        viewModel.getDefaultCard()

    }

    private fun openPlacePicker() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)

        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        ).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS)
    }

    private fun openPlacePickerDestination() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)

        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        ).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_DESTINATION_ADDRESS)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val place = Autocomplete.getPlaceFromIntent(data)
                setAddress(place)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR && data != null) {
                val status = Autocomplete.getStatusFromIntent(data)
                Log.i("Status", status.statusMessage?:"")
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        } else if (requestCode == AUTOCOMPLETE_REQUEST_CODE_DESTINATION_ADDRESS) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val place = Autocomplete.getPlaceFromIntent(data)
                setAddressDestination(place)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR && data != null) {
                val status = Autocomplete.getStatusFromIntent(data)
                Log.i("Status", status.statusMessage?:"")
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    private fun setAddress(place: Place) {
        userLatLng = place.latLng
        binding.editSourceAddress.setText(place.address ?: "")
        latitude = userLatLng?.latitude.toString()
        longitude = userLatLng?.longitude.toString()
    }

    private fun setAddressDestination(place: Place) {
        userLatLngDestination = place.latLng
        binding.editDestinationAddress.setText(place.address ?: "")

    }

    private fun paymentPercentage(paymentPecentageResponse: PaymentPecentageResponse) {
        paymentPercentage = paymentPecentageResponse?.haul_job_percentage?.toDouble() ?: 0.0

    }

    private fun getDefalutCardData(cardResponse: CardResponse) {
        val data = Base64.decode(cardResponse.cvv_number.toString(), Base64.DEFAULT)
        val text = String(data, charset("UTF-8"))
        generateStripeToken(
            cardNumber = cardResponse.number,
            cardExpMonth = cardResponse.expiry_month.toInt(),
            cardExpYear = cardResponse.expiry_year.toInt(),
            cardCVV = text
        ) { token ->

            showProgress()
            viewModel.postJobHaulOff(
                materialType = jobResponceDestination.MaterialType?:"",
                MaterialoptionType = jobResponceDestination.MaterialoptionType?:"",
                spec_sheet = binding.edSpecSheet.getText().toString(),
                lib_information = binding.edLiabilityInformation.getText().toString(),
                material_cont = binding.edMaterialContent.getText().toString(),
                materials_available = binding.edMaterialAvailable.getText().toString(),
                Source = binding.editSourceAddress.getText().toString(),
                source_lat = jobResponseSource.latitute?:"0.0",
                source_long = jobResponseSource.longitute?:"0.0",
                Destination = binding.editDestinationAddress.getText().toString(),
                destination_lat = jobResponceDestination.latitute?:"0.0",
                destination_long = jobResponceDestination.longitute?:"0.0",
                equipementId = jobResponseSource.equipment_type?:"",
                equipmentOptionId = "",
                haul_paymemt_amt = binding.editJobEstimatedPrice.getText().toString(),
                payment_type = paymentType,
                stripeToken = token,
                TruckType = truckDataResponse.truck_type_id,
                NoOfTrucks = binding.editNoOfTruck.getText().toString(),
                pickup_date = binding.tvPickupDate.text.toString(),
                delivery_date = binding.tvDeliveryDate.text.toString(),
                pickup_time = binding.tvPickupTime.text.toString(),
                delivery_time = binding.tvDeliveryTime.text.toString(),
                importer_id = jobResponseSource.importer_id?:"",
                import_request_id = jobResponseSource.import_request_id?:"",
                haul_request_mapid = jobResponseSource.haul_request_user_map_id?:"",
                exporter_id = jobResponseSource.exporter_id?:"",
                export_request_id = jobResponseSource.export_request_id?:"",
                specs_sheet = jobResponseSource.specs_sheet?:""
            )
        }

    }

    private fun defalutCardError(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Please provide a default card",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(Intent(this@PostAHaulOffActivity, CardListActivity::class.java))
                }
            }
        )

    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@PostAHaulOffActivity,
                                Dashboard::class.java
                            )
                        )
                    )
                    finish()

                }
            }
        )

    }

}