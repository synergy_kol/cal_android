package com.itsc.cal.ui.track_my_job_list

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityTrackMyJobListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.ui.track_my_job_list.adapter.TrackMyJobAdapter

class TrackMyJobListActivity : CalSuperActivity() {

    private lateinit var binding:ActivityTrackMyJobListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_track_my_job_list)
        enableBackButton()
        setPageTitle("Job List")
        showDashboardActionbarWithBackButton()
        initView()
    }

    private fun initView() {
        binding.recycleviewMyJobList.apply {
            layoutManager = LinearLayoutManager(this@TrackMyJobListActivity)
            adapter = TrackMyJobAdapter(this@TrackMyJobListActivity, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {

                }

            })
        }
    }
}