package  com.itsc.cal.ui.track_list

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveTruckList: MediatorLiveData<Event<ArrayList<TruckListResponse>>> =
        MediatorLiveData()
    val mediatorLiveTruckTypeError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveTruckList.addSource(
            repository.mutableLiveDataTruckList
        ) { t -> mediatorLiveTruckList.postValue(t) }
        mediatorLiveTruckTypeError.addSource(
            repository.mutableLiveDataTruckListError
        ) { t -> mediatorLiveTruckTypeError.postValue(t) }


    }

    fun getTruckList() = repository.getTruckList()


}