package com.itsc.cal.ui.dashboard.adapter

import android.content.Context
import android.graphics.Color

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import android.graphics.LightingColorFilter
import android.os.Build
import android.text.Html
import android.widget.TextView
import com.itsc.cal.R
import com.itsc.cal.model.BannerResponce


fun Context.progressPlaceHolder(): CircularProgressDrawable {
    val circularProgressDrawable = CircularProgressDrawable(this)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.colorFilter = (LightingColorFilter(-0x1000000, Color.YELLOW))
    circularProgressDrawable.start()
    return circularProgressDrawable
}

fun RequestBuilder<Drawable>.applyPlaceHolder(context: Context): RequestBuilder<Drawable> {
    placeholder(context.progressPlaceHolder())
        .error(R.drawable.ic_no_image)
    return this
}

class HomeSlidingAdapter(
    val mContext: Context,
    val mDashboardImageList: ArrayList<BannerResponce>?
) : PagerAdapter() {
    private var inflater: LayoutInflater = LayoutInflater.from(mContext)
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(view: ViewGroup, position: Int): View {
        val imageLayout: View = inflater.inflate(R.layout.adapter_home_pager, view, false)!!
        val imageView: ImageView = imageLayout
            .findViewById<View>(R.id.image) as ImageView
        val textView: TextView = imageLayout.findViewById<View>(R.id.textTips) as TextView
        Glide.with(mContext)
            .load(mDashboardImageList?.get(position)?.banner_image).applyPlaceHolder(mContext)
            .into(imageView)
        if ("2" == mDashboardImageList?.get(position)?.type ?: "1") {
            textView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(
                    " ${mDashboardImageList?.get(position)?.banner_title}  ${mDashboardImageList?.get(
                        position
                    )?.tips}", Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                Html.fromHtml(mDashboardImageList?.get(position)?.tips)
            }
            imageView.visibility = View.GONE
        } else {
            textView.text = ""
            imageView.visibility = View.VISIBLE
        }
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return mDashboardImageList?.size ?: 0
    }
}