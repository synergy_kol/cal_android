package  com.itsc.cal.ui.post_hault_of_job_list.fragment

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveImportJobListing: MediatorLiveData<Event<ArrayList<HauljobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveImportListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveExportJobListing: MediatorLiveData<Event<ArrayList<HauljobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveExportListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveImportJobListing.addSource(
            repository.mutableLiveDataImportJob
        ) { t -> mediatorLiveImportJobListing.postValue(t) }
        mediatorLiveImportListingError.addSource(
            repository.mutableLiveDataJobImportError
        ) { t -> mediatorLiveImportListingError.postValue(t) }


        mediatorLiveExportJobListing.addSource(
            repository.mutableLiveDataExportJob
        ) { t -> mediatorLiveExportJobListing.postValue(t) }
        mediatorLiveExportListingError.addSource(
            repository.mutableLiveDataJobExportError
        ) { t -> mediatorLiveExportListingError.postValue(t) }


    }

    fun importJobListing(filter:String,tag:String) = repository.getJobImportHault(filter = filter,tag = tag)
    fun exportJobListing(filter:String,tag:String) = repository.getJobExportHault(filter = filter,tag = tag)


}