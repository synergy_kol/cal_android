package com.itsc.cal.ui.job_details.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildEquipementDetailsBinding
import com.itsc.cal.model.EquipementDetailsResponse

class EquipementDetailsAdapter (val mContext: Context, val arrayList: ArrayList<EquipementDetailsResponse>):
    RecyclerView.Adapter<EquipementDetailsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(ChildEquipementDetailsBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }
    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))

    }

    inner class ViewHolder(val binding: ChildEquipementDetailsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(equipmentDetails: EquipementDetailsResponse) {
            if (equipmentDetails.equipment_option_name?:""!=""){
                binding.edEquipementDetails.text = "${equipmentDetails.equipment_name} - ${equipmentDetails.equipment_option_name}"
            }else{
                binding.edEquipementDetails.text = "${equipmentDetails.equipment_name}"
            }

        }

    }

}