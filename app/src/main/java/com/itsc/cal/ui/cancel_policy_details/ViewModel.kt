package  com.itsc.cal.ui.cancel_policy_details

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLivejobCanceltationChargesResponse: MediatorLiveData<Event<JobCancelChargeResponce>> =
        MediatorLiveData()
    val mediatorLiveJobCancelationChargesError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveCancelJobs: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveCancelError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveCancelJobsBuilderSupplier: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveCancelJobBuilderSupplierError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDefalutCard: MediatorLiveData<Event<CardResponse>> = MediatorLiveData()
    val mediatorLiveDefalutCardError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLivejobCanceltationChargesResponse.addSource(
            repository.mutableLiveDataJobCancelationChargeData
        ) { t -> mediatorLivejobCanceltationChargesResponse.postValue(t) }
        mediatorLiveJobCancelationChargesError.addSource(
            repository.mutableLiveDataJobCancelationChargeDataError
        ) { t -> mediatorLiveJobCancelationChargesError.postValue(t) }

        mediatorLiveCancelJobs.addSource(
            repository.mutableLiveDataCancelJobsDetailsPage
        ) { t -> mediatorLiveCancelJobs.postValue(t) }
        mediatorLiveCancelError.addSource(
            repository.mutableLiveDataCancelJobsErrorDetailsPage
        ) { t -> mediatorLiveCancelError.postValue(t) }

        mediatorLiveCancelJobsBuilderSupplier.addSource(
            repository.mutableLiveDataCancelJobsBuilderSupplier
        ) { t -> mediatorLiveCancelJobsBuilderSupplier.postValue(t) }
        mediatorLiveCancelJobBuilderSupplierError.addSource(
            repository.mutableLiveDataCancelJobsErrorBuilderSupplier
        ) { t -> mediatorLiveCancelJobBuilderSupplierError.postValue(t) }

        mediatorLiveDefalutCard.addSource(
            repository.mutableLiveDataDefaultCardCancelation
        ) { t -> mediatorLiveDefalutCard.postValue(t) }
        mediatorLiveDefalutCardError.addSource(
            repository.mutableLiveDataDefaultCardCancelationError
        ) { t -> mediatorLiveDefalutCardError.postValue(t) }

    }

    fun getCancelJobDetails(job_id: String, user_type: String) =
        repository.getCancelJobDetails(job_id = job_id, user_type = user_type)

    fun cancelJobs(jobId: String, cancelReson: String, amount: String) =
        repository.cancelJobsDeatils(jobId, cancelReson, amount)

    fun cancelJobsBuilderSupplier(jobId: String, cancelReson: String, amount: String,stripeToken:String) =
        repository.cancelJobsBuilderSupplier(
            job_id = jobId,
            cancel_reason = cancelReson,
            amount = amount,
            stripeToken = stripeToken
        )

    fun getDefaultCard() = repository.getDefaultCardCancelation()
}