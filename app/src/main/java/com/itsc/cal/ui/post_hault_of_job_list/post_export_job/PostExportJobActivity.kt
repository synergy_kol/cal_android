package com.itsc.cal.ui.post_hault_of_job_list.post_export_job

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.fxn.pix.Pix
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.itsc.cal.R
import com.itsc.cal.`interface`.ImageOnItemClickListener
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityPostExportJobBinding
import com.itsc.cal.model.*
import com.itsc.cal.ui.post_hault_of_job_list.post_export_job.adapter.ImageTakenAdapter
import com.itsc.cal.ui.post_hault_of_job_list.post_export_job_two.PostExportJobTwoActivity
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import kotlin.collections.ArrayList


class PostExportJobActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPostExportJobBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    companion object {
        var postATruckPage: Activity? = null
    }

    private var imageArrayList: ArrayList<VehicleImage> = ArrayList()
    private lateinit var vehicleImage: VehicleImage
    private lateinit var imageAdapter: ImageTakenAdapter

    private val AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS: Int = 1
    private var userLatLng: LatLng? = null
    private var userLatLngDestination: LatLng? = null
    private var mLocationPermissionGranted = false
    private var latitude: String = ""
    private var longitude: String = ""
    private var haul_request_id: String = ""
    private var importExport = ""


    private var materialImageList: ArrayList<File> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_post_export_job)
        enableBackButton()

        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        postATruckPage = this
        intiView()

        bindViewHolder()

    }

    private fun createJobToken(importExport: String) {
        showProgress()
        viewModel.createRequestHaultOfJob(importExport)
    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveHaultCreateJobRequestData.observe(
            this,
            Observer<Event<CreateHaultOfRequest>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        createJobId(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveHaultCreateJobRequestError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveHaulJobExportStep1.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseExport(it.getContent()!!.message)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataJobExportStep1Error.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )


        viewModel.mediatorLiveHaulJobImportStep1.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseImport(it.getContent()!!.message)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataJobImportStep1Error.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

    }

    private fun intiView() {
        intent.let {
            importExport = intent.extras?.getString(KeywordsAndConstants.DATA, "").toString()
            Log.i("importExport:", importExport)

            if (importExport.equals("E")) {
                binding.textPictureMaterial.visibility = View.VISIBLE
                binding.recycleViewHaulOf.visibility = View.VISIBLE
                setPageTitle("Add Haul-OFF Export Job- Step 1")
            } else {
                binding.textPictureMaterial.visibility = View.GONE
                binding.recycleViewHaulOf.visibility = View.GONE
                setPageTitle("Add Haul-OFF Import Job- Step 1")
            }
            createJobToken(importExport)
        }
        imageArrayList = ArrayList<VehicleImage>()
        vehicleImage = VehicleImage("", true)
        imageArrayList.add(vehicleImage)

        binding.recycleViewHaulOf.layoutManager = GridLayoutManager(this, 4)
        imageAdapter = ImageTakenAdapter(this, imageArrayList, object : ImageOnItemClickListener {
            override fun onItemClick(position: Int) {
                takePicture()
                imageArrayList.removeAt(position)
            }

        })
        binding.recycleViewHaulOf.adapter = imageAdapter



        binding.editMainNo.apply {
            inputMode(EditTextInputMode.NUMBER)
            setMaxLength(13)
        }

        binding.editEmailAddress.apply {
            hint("Enter Email Address")
            inputMode(EditTextInputMode.EMAIL)
        }

        binding.btnPostATruck.setOnClickListener {
            moveToNext()
        }
        binding.editSourceAddress.setOnClickListener {
            openPlacePicker()
        }


    }

    private fun moveToNext() {

        if (
            binding.editSourceAddress.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Pickup Location.")
            return
        }

        if (
            binding.editPointOfContact.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Point of Contact.")
            return
        }

        if (
            binding.editMainNo.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Main number.")
            return
        }

        if (!AndroidUtility.isValidMobile(binding.editMainNo.getText().toString())) {
            showMessageInDialog(resources.getString(R.string.main_no_validration_message))
            return
        }

        if (
            binding.editEmailAddress.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Email Address.")
            return
        }
        if (
            !Patterns.EMAIL_ADDRESS.matcher(
                binding.editEmailAddress.getText().toString()
            ).matches()
        ) {
            showMessageInDialog("Please provide valid Email Address.")
            return
        }

        if (importExport.equals("E") && materialImageList.size == 0) {
            showMessageInDialog("Please upload material picture.")
            return
        }
/*
        if (importExport.equals("E") && materialImageList.size < 2) {
            showMessageInDialog("Please upload at least 2 material picture.")
            return
        }*/

        showProgress()
        if (importExport.equals("E")) {
            viewModel.postExportStep1(
                haul_request_id = haul_request_id,
                mapLat = latitude,
                mapLng = longitude,
                address = binding.editSourceAddress.getText().toString().trim(),
                point_of_contact = binding.editPointOfContact.getText().toString(),
                email = binding.editEmailAddress.getText().toString(),
                main_number = binding.editMainNo.getText().toString(),
                materialImageList = materialImageList
            )
        } else {
            viewModel.postImportStep1(
                haul_request_id = haul_request_id,
                mapLat = latitude,
                mapLng = longitude,
                address = binding.editSourceAddress.getText().toString().trim(),
                point_of_contact = binding.editPointOfContact.getText().toString(),
                email = binding.editEmailAddress.getText().toString(),
                main_number = binding.editMainNo.getText().toString()

            )
        }


    }

    private fun openPlacePicker() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)

        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        ).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val place = Autocomplete.getPlaceFromIntent(data)
                setAddress(place)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR && data != null) {
                val status = Autocomplete.getStatusFromIntent(data)
                Log.i("Status", status.statusMessage?:"")
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        } else if (requestCode == KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            data?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    vehicleImage =
                        VehicleImage(
                            images[0],
                            false
                        )
                    imageArrayList.add(vehicleImage)
                    if (imageArrayList.size <= 3) {
                        vehicleImage =
                            VehicleImage(
                                "",
                                true
                            )
                        imageArrayList.add(vehicleImage)
                    }
                    materialImageList.add(File(images[0]))
                    imageAdapter.notifyDataSetChanged()
                }
            }
        }
    }

    private fun setAddress(place: Place) {
        userLatLng = place.latLng
        binding.editSourceAddress.setText(place.address ?: "")
        latitude = userLatLng?.latitude.toString()
        longitude = userLatLng?.longitude.toString()
    }

    private fun createJobId(createHaultOfRequest: CreateHaultOfRequest) {
        haul_request_id = createHaultOfRequest.haul_request_id ?: ""
        Log.i("RequestID:", haul_request_id)
    }


    private fun processResponseExport(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Haul-OFF Export Job- Step-1 added successfully",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(
                        Intent(
                            this@PostExportJobActivity,
                            PostExportJobTwoActivity::class.java
                        ).putExtra(KeywordsAndConstants.DATA, haul_request_id)
                            .putExtra(KeywordsAndConstants.IMPORT_EXPORT, importExport)
                    )
                }
            }
        )

    }

    private fun processResponseImport(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Haul-OFF Import Job- Step-1 added successfully",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(
                        Intent(
                            this@PostExportJobActivity,
                            PostExportJobTwoActivity::class.java
                        ).putExtra(KeywordsAndConstants.DATA, haul_request_id)
                            .putExtra(KeywordsAndConstants.IMPORT_EXPORT, importExport)
                    )
                }
            }
        )

    }

    @SuppressLint("CheckResult")
    fun takePicture() {
        RxPermissions(this)
            .request(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .subscribe {
                if (!it) {
                    showMessageInDialog("Please accept permissions to take picture.")
                }

                ImagePickerUtil.pickImage(
                    context = this,
                    reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
                )
            }
    }


}