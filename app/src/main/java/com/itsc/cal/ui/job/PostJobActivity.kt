package com.itsc.cal.ui.job

import android.Manifest
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.TimePicker
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.itsc.cal.util.generateStripeToken
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.core.KeywordsAndConstants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
import com.itsc.cal.databinding.ActivityPostAJobBinding
import com.itsc.cal.interfaces.OnCardSelectedListener
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.builders_details.BuilderDetailsActivity
import com.itsc.cal.ui.card_list.CardListActivity
import com.itsc.cal.ui.card_list.CardListDialog
import com.itsc.cal.ui.payment_method.PaymentMethodActivity
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.itsc.cal.widget.spinner.Spinner
import com.itsc.cal.widget.spinner.SpinnerData
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.ArrayList

class PostJobActivity : CalSuperActivity(withNav = true), KodeinAware {
    override val kodein: Kodein by kodein()
    private val AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS: Int = 1
    private val AUTOCOMPLETE_REQUEST_CODE_DESTINATION_ADDRESS: Int = 2
    private var userLatLng: LatLng? = null
    private var userLatLngDestination: LatLng? = null
    private var mLocationPermissionGranted = false
    private var latitude: String = ""
    private var longitude: String = ""
    private val repository: Repository by instance()
    private lateinit var binding: ActivityPostAJobBinding
    private lateinit var mPlacesClient: PlacesClient
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var equipementResponse: EquipementResponse
    private lateinit var loadDataResponse: LoadResponse
    private lateinit var meterialResponse: MeterialResponse
    private lateinit var truckDataResponse: TruckDataResponse
    private var priority = "1"
    private var equipementTyep = "1"
    private var paymentType = ""
    private var pickup: Calendar = Calendar.getInstance()
    private var currentDate: Calendar = Calendar.getInstance()
    private var delivery: Calendar = Calendar.getInstance()
    private var paymentPercentage = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_post_a_job)
        setPageTitle("Post a Job")
        showHelpButton()
        enableBackButton()
        Places.initialize(getApplicationContext(), getString(R.string.place_api_key))
        mPlacesClient = Places.createClient(this)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        getLocationPermission()
        initView()
        bindToViewModel()
        getMeterialSpinnerData()

        getTruckSpinnerData()
        getEquipementSpinnerData()
        getLoadData()
        getPaymentPercentage()

       /* val data = Base64.decode("MTIz", Base64.DEFAULT)
        val text = String(data, charset("UTF-8"))
        Log.i("CVV------", text)*/

    }

    private fun getMeterialSpinnerData() {
        showProgress()
        viewModel.getMeterialData()
    }

    private fun getPaymentPercentage() {
        showProgress()
        viewModel.getPaymentPercentage()
    }

    private fun getLoadData() {
        showProgress()
        viewModel.getLoadData()
    }

    private fun getEquipementSpinnerData() {
        showProgress()
        viewModel.getEquiptmentData()
    }

    private fun getTruckSpinnerData() {
        showProgress()
        viewModel.getTruckData()
    }

    private fun initView() {
        binding.editSourceAddress.setOnClickListener {
            openPlacePicker()
        }
        binding.editDestinationAddress.setOnClickListener {
            openPlacePickerDestinationAddress()
        }
        binding.editOtherCharges.apply {
            inputMode(EditTextInputMode.DECIMAL)
            getDrawable(
                R.drawable.dollar
            )?.let { drawable ->
                drawableStart(drawable)
            }
            setMaxLength(10)
        }
        binding.editWeight.apply {
            inputMode(EditTextInputMode.DECIMAL)
            setMaxLength(8)
        }
        binding.editNoOfTruck.apply {
            inputMode(EditTextInputMode.NUMBER)
            setMaxLength(4)
        }
        /*   binding.editJobEstimatedPrice.apply {
               inputMode(EditTextInputMode.DECIMAL)
               getDrawable(
                   R.drawable.dollar
               )?.let { drawable ->
                   drawableStart(drawable)
               }
               setMaxLength(10)
           }*/
        binding.tvPickupDate.setOnClickListener {
            AndroidUtility.showDatePicker(
                supportFragmentManager,
                com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val selectedDate =
                        dayOfMonth.toString() + "/" + (monthOfYear + 1).toString() + "/" + year
                    val fromDate = AndroidUtility.formatDateFromString(
                        "dd/MM/yyyy",
                        "yyyy-MM-dd",
                        selectedDate
                    )
                    pickup.set(Calendar.YEAR, year)
                    pickup.set(Calendar.MONTH, monthOfYear)
                    pickup.set(Calendar.DATE, dayOfMonth)
                    binding.tvPickupDate.setText(fromDate)
                    binding.tvPickupTime.setText("")
                })
        }
        binding.tvPickupTime.setOnClickListener {
            if (!binding.tvPickupDate.text.toString().isEmpty()) {
                showTimepickerDialogeStTimePickUptime()
            } else {
                showMessageInDialog("Please provide Pickup Date.")
            }

        }
        binding.tvDeliveryDate.setOnClickListener {
            AndroidUtility.showDatePicker(
                supportFragmentManager,
                com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val selectedDate =
                        dayOfMonth.toString() + "/" + (monthOfYear + 1).toString() + "/" + year
                    val fromDate = AndroidUtility.formatDateFromString(
                        "dd/MM/yyyy",
                        "yyyy-MM-dd",
                        selectedDate
                    )
                    delivery.set(Calendar.YEAR, year)
                    delivery.set(Calendar.MONTH, monthOfYear)
                    delivery.set(Calendar.DATE, dayOfMonth)
                    binding.tvDeliveryDate.setText(fromDate)
                })
        }

        binding.tvDeliveryTime.setOnClickListener {
            showTimepickerDialogeStTimeDeliveryTime()
        }


        binding.spinnerPriority.apply {
            editable(true)
            setBg()
            addItems(
                arrayListOf(
                    SpinnerData(
                        "High",
                        "1"
                    ), SpinnerData(
                        "Low",
                        "3"
                    ),
                    SpinnerData(
                        "Medium",
                        "2"
                    )
                ),
                false,
                object : Spinner.OnItemSelectedListener {
                    override fun <T> selected(item: SpinnerData<T>) {
                        priority = item.data as String
                    }
                }
            )
        }


        binding.radioButtonYes.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                binding.editEquipement.visibility = View.VISIBLE
                equipementTyep = "1"
            } else {
                binding.editEquipement.visibility = View.GONE
                equipementTyep = "0"
            }
        }
        binding.radioButtonFull.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                paymentType = "F"
                binding.editJobParcialAmount.isEnabled = false
                binding.editJobParcialAmount.visibility = View.GONE
                binding.tvJobPaAmount.visibility = View.GONE
            } else {
                paymentType = "P"
                binding.editJobParcialAmount.isEnabled = false
                if (!binding.editJobEstimatedPrice.getText().toString().isEmpty()) {
                    var price =
                        (binding.editJobEstimatedPrice.getText().toString()
                            .toDouble() * paymentPercentage) / 100
                    binding.editJobParcialAmount.setText(
                        price.toString()
                    )
                } else {
                    binding.editJobParcialAmount.setText(
                        "0"
                    )
                }

                binding.editJobParcialAmount.visibility = View.VISIBLE
                binding.tvJobPaAmount.visibility = View.VISIBLE

            }
        }

        binding.radioButtonPartial.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                paymentType = "P"
                binding.editJobParcialAmount.isEnabled = false
                if (!binding.editJobEstimatedPrice.getText().toString().isEmpty()) {
                    var price =
                        (binding.editJobEstimatedPrice.getText().toString()
                            .toDouble() * paymentPercentage) / 100
                    binding.editJobParcialAmount.setText(
                        price.toString()
                    )
                } else {
                    binding.editJobParcialAmount.setText(
                        "0"
                    )
                }
                binding.editJobParcialAmount.visibility = View.VISIBLE
                binding.tvJobPaAmount.visibility = View.VISIBLE

            } else {
                paymentType = "F"
                binding.editJobParcialAmount.isEnabled = false
                binding.editJobParcialAmount.visibility = View.GONE
                binding.tvJobPaAmount.visibility = View.GONE
            }
        }
        binding.editJobEstimatedPrice.setCompoundDrawablesWithIntrinsicBounds(
            getDrawable(
                R.drawable.dollar
            ), null, null, null
        )
        binding.editJobEstimatedPrice.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString() != null && s.toString() != "") {
                    var price =
                        (s.toString().toDouble() * paymentPercentage) / 100
                    binding.editJobParcialAmount.setText(
                        price.toString()
                    )
                } else {
                    binding.editJobParcialAmount.setText(
                        ""
                    )
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        binding.editJobParcialAmount.apply {
            inputMode(EditTextInputMode.DECIMAL)
            getDrawable(
                R.drawable.dollar
            )?.let { drawable ->
                drawableStart(drawable)
            }

            setMaxLength(10)
        }

        binding.tvSubmit.setOnClickListener {
            postJobApiCalled()
        }

    }

    fun showTimepickerDialogeStTimePickUptime() {
        val calendar = Calendar.getInstance()
        val mHour = calendar.get(Calendar.HOUR_OF_DAY)
        val mMinute = calendar.get(Calendar.MINUTE)

        var timePickerDialog = TimePickerDialog(this, object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
                pickup.set(Calendar.HOUR_OF_DAY, p1)
                pickup.set(Calendar.MINUTE, p2)
                if (currentDate.timeInMillis < pickup.timeInMillis) {
                    val selectedTime = AndroidUtility.formatDateFromString(
                        "HH:mm",
                        "HH:mm",
                        "$p1:$p2"
                    )
                    binding.tvPickupTime.setText(selectedTime)
                } else {
                    showMessageInDialog("Please provide valid pickup date.")
                    binding.tvPickupTime.setText("")
                }
            }

        }, mHour, mMinute, false)
        timePickerDialog.show()

    }

    fun showTimepickerDialogeStTimeDeliveryTime() {
        val calendar = Calendar.getInstance()
        val mHour = calendar.get(Calendar.HOUR_OF_DAY)
        val mMinute = calendar.get(Calendar.MINUTE)

        var timePickerDialog = TimePickerDialog(this, object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
                val selectedTime = AndroidUtility.formatDateFromString(
                    "HH:mm",
                    "HH:mm",
                    "$p1:$p2"
                )
                binding.tvDeliveryTime.setText(selectedTime)

                delivery.set(Calendar.HOUR_OF_DAY, p1)
                delivery.set(Calendar.MINUTE, p2)
            }

        }, mHour, mMinute, false)
        timePickerDialog.show()

    }

    private fun getLocationPermission() {
        mLocationPermissionGranted = false
        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mLocationPermissionGranted = true

        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun openPlacePicker() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)

        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        ).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS)
    }

    private fun openPlacePickerDestinationAddress() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)

        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        ).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_DESTINATION_ADDRESS)
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveLoadData.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<LoadResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val items: ArrayList<SpinnerData<LoadResponse>> = ArrayList()
                        it.getContent()!!.forEach { source ->
                            items.add(
                                SpinnerData(
                                    source.name.toString(),
                                    source
                                )
                            )
                        }
                        if (items.size > 0)
                            loadDataResponse = items[0].data
                        binding.spinnerLoadType.setBg()
                        binding.spinnerLoadType.addItems(
                            items,
                            withDot = false,
                            listener = object : Spinner.OnItemSelectedListener {
                                override fun <T> selected(item: SpinnerData<T>) {
                                    loadDataResponse = item.data as LoadResponse

                                }
                            }
                        )

                    }
                }
            }
        )

        viewModel.mediatorLiveLoadDataError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
        /*  viewModel.mediatorLivePostJob.observe(
              this,
              Observer<Event<StatusReply>> { t ->
                  t?.let {
                      if (it.shouldReadContent())
                          processResponse(it.getContent()!!.message)
                  }
              }
          )
          viewModel.mediatorLivePostJobError.observe(
              this,
              Observer<Event<Result>> { t ->
                  t?.let {
                      if (it.shouldReadContent()) {
                          hideProgress()
                          handleGenericResult(it.getContent()!!)
                      }

                  }
              }
          )
  */
        viewModel.mediatorLiveEquipementData.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<EquipementResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val items: ArrayList<SpinnerData<EquipementResponse>> = ArrayList()
                        it.getContent()!!.forEach { source ->
                            items.add(
                                SpinnerData(
                                    source.name.toString(),
                                    source
                                )
                            )
                        }
                        if (items.size > 0)
                            equipementResponse = items[0].data
                        binding.editEquipement.setBg()
                        binding.editEquipement.addItems(
                            items,
                            withDot = false,
                            listener = object : Spinner.OnItemSelectedListener {
                                override fun <T> selected(item: SpinnerData<T>) {
                                    equipementResponse = item.data as EquipementResponse

                                }
                            }
                        )

                    }
                }
            }
        )

        viewModel.mediatorLiveEquipementDataError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveMeteralData.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<MeterialResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val items: ArrayList<SpinnerData<MeterialResponse>> = ArrayList()
                        it.getContent()!!.forEach { source ->
                            items.add(
                                SpinnerData(
                                    source.name,
                                    source
                                )
                            )
                        }

                        if (items.size > 0)
                            meterialResponse = items[0].data
                        binding.spinnerMaterialType.setBg()
                        binding.spinnerMaterialType.addItems(
                            items,
                            withDot = false,
                            listener = object : Spinner.OnItemSelectedListener {
                                override fun <T> selected(item: SpinnerData<T>) {
                                    meterialResponse = item.data as MeterialResponse


                                }
                            }
                        )

                    }
                }
            }
        )

        viewModel.mediatorLiveMeteralDataError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveTruckData.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<TruckDataResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val items: ArrayList<SpinnerData<TruckDataResponse>> = ArrayList()
                        it.getContent()!!.forEach { source ->
                            items.add(
                                SpinnerData(
                                    source.name,
                                    source
                                )
                            )
                        }
                        if (items.size > 0)
                            truckDataResponse = items[0].data
                        binding.spinnerTruckType.setBg()
                        binding.spinnerTruckType.addItems(
                            items,
                            withDot = false,
                            listener = object : Spinner.OnItemSelectedListener {
                                override fun <T> selected(item: SpinnerData<T>) {
                                    truckDataResponse = item.data as TruckDataResponse


                                }
                            }
                        )

                    }
                }
            }
        )

        viewModel.mediatorLiveMeteralDataError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveCardListing.observe(
            this,
            Observer<Event<ArrayList<CardListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        showCardListDialog(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveCardListingError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                        defalutCardError("")
                    }

                }
            }
        )

        viewModel.mediatorLiveDataPaymentPercentage.observe(
            this,
            Observer<Event<PaymentPecentageResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        paymentPercentage(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataPaymentPercentageError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

    }

    private fun paymentPercentage(paymentPecentageResponse: PaymentPecentageResponse) {
        paymentPercentage = paymentPecentageResponse?.general_job_percentage?.toDouble() ?: 0.0

    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val place = Autocomplete.getPlaceFromIntent(data)
                setAddress(place)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR && data != null) {
                // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(data)
                Log.i("Status", status.statusMessage?:"")
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        } else if (requestCode == AUTOCOMPLETE_REQUEST_CODE_DESTINATION_ADDRESS) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val place = Autocomplete.getPlaceFromIntent(data)
                setAddressDestination(place)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR && data != null) {
                // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(data)
                Log.i("Status", status.statusMessage?:"")
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    private fun setAddress(place: Place) {
        userLatLng = place.latLng
        binding.editSourceAddress.setText(place.address ?: "")
        latitude = userLatLng?.latitude.toString()
        longitude = userLatLng?.longitude.toString()
    }

    private fun setAddressDestination(place: Place) {
        userLatLngDestination = place.latLng
        binding.editDestinationAddress.setText(place.address ?: "")

    }

    private fun getDefaultCardData(cardListResponse: CardListResponse?) {
        val data = Base64.decode(cardListResponse?.cvv_number.toString(), Base64.DEFAULT)
        val text = String(data, charset("UTF-8"))
        val totalCharges  = (binding.editOtherCharges.getText().toDouble()+
                binding.editJobEstimatedPrice.text.toString().toDouble())

        if(cardListResponse==null)
            return
        generateStripeToken(
            cardNumber = cardListResponse.number,
            cardExpMonth = cardListResponse.expiry_month.toInt(),
            cardExpYear = cardListResponse.expiry_year.toInt(),
            cardCVV = text
        ) { token ->

            var postJobRequest = PostJobRequest(
                source_lat = userLatLng?.latitude.toString(),
                source_long = userLatLng?.longitude.toString(),
                destination_lat = userLatLngDestination?.latitude.toString(),
                destination_long = userLatLngDestination?.longitude.toString(),
                delivery_date = binding.tvDeliveryDate.text.toString(),
                delivery_time = binding.tvDeliveryTime.text.toString(),
                Destination = binding.editDestinationAddress.getText().toString(),
                Equipment = equipementTyep,
                FreightCharges = binding.editOtherCharges.getText().toString(),
                JobEstimatePrice = binding.editJobEstimatedPrice.getText().toString(),
                LodeType = loadDataResponse.load_id,
                MaterialType = meterialResponse.material_id,
                NoOfTrucks = binding.editNoOfTruck.getText().toString(),
                pickup_date = binding.tvPickupDate.getText().toString(),
                pickup_time = binding.tvPickupTime.getText().toString(),
                priority = priority,
                Source = binding.editSourceAddress.getText().toString(),
                TruckType = truckDataResponse.truck_type_id,
                Weight = binding.editWeight.getText().toString(),
                Equipment_value = equipementResponse.name,
                builderId = "",
                card_id = cardListResponse.card_id,
                payment_type = paymentType,
                stripeToken = token,
                jobPayableAmount = totalCharges.toString()
            )
            startActivity(
                Intent(
                    this,
                    BuilderDetailsActivity::class.java
                ).putExtra(KeywordsAndConstants.isPostJob, true)
                    .putExtra(KeywordsAndConstants.DATA, toJson(postJobRequest))
            )
        }

    }

    private fun defalutCardError(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Please provide a default card",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(Intent(this@PostJobActivity, CardListActivity::class.java))
                }
            }
        )

    }

    private fun postJobApiCalled() {

        if (
            binding.editSourceAddress.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Pickup Location.")
            return
        }
        if (
            binding.editDestinationAddress.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Drop-off Location.")
            return
        }

        if (
            binding.editWeight.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Weight.")
            return
        }

        if (
            binding.editOtherCharges.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Other Charges.")
            return
        }

        if (
            binding.tvPickupDate.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Pickup Date.")
            return
        }

        if (
            binding.tvPickupTime.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Pickup Time.")
            return
        }

        if (
            binding.tvDeliveryDate.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Delivery Date.")
            return
        }

        if (
            binding.tvDeliveryTime.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Delivery Time.")
            return
        }

        if (delivery.timeInMillis < pickup.timeInMillis) {
            showMessageInDialog("Please provide Delivery Date and Time after Pickup Date.")
            return
        }

        if (
            binding.editNoOfTruck.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide no. of truck.")
            return
        }

        if (
            binding.editJobEstimatedPrice.text.toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Job Estimated Price.")
            return
        }

        if (
            binding.editJobEstimatedPrice.text.toString().toDouble() == 0.0
        ) {
            showMessageInDialog("Please provide valid Job Estimated Price.")
            return
        }

        showProgress()
        viewModel.getCardList()

    }

    private fun showCardListDialog(arrayList: ArrayList<CardListResponse>?){
        val mCardListDialog = CardListDialog(this, arrayList, object:OnCardSelectedListener{
            override fun onSelectCard(view: AppCompatButton,
                                      mCardListResponse: CardListResponse?) {
                if(mCardListResponse==null){
                    startActivity(
                        Intent(this@PostJobActivity, PaymentMethodActivity::class.java).putExtra(
                            KeywordsAndConstants.isEditCard, false))
                }else{
                    getDefaultCardData(mCardListResponse)
                }
            }
        })
        mCardListDialog.show()
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    finish()

                }
            }
        )

    }
}