package com.itsc.cal.ui.end_trip

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.lifecycle.Observer
import com.fxn.pix.Pix
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityEndTripBinding
import com.itsc.cal.model.DriverJob
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.SignatureView.SignatureViewActivity
import com.itsc.cal.ui.login.LoginActivity
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import permissions.dispatcher.*
import java.io.*
import java.sql.Timestamp

@RuntimePermissions
class EndTripActivity : CalSuperActivity(), KodeinAware {
    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityEndTripBinding
    private var imageFilePathPOD: File? = null
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var jobResponse: DriverJob
    private val repository: Repository by instance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_end_trip)
        enableBackButton()
        setPageTitle("End Trip")
        showHelpButton()
        initView()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        bindingViewModel()
    }

    override fun onResume() {
        super.onResume()
        if (repository.signatureBitMap!=null)
        binding.signatureViewImage.setImageBitmap(repository.signatureBitMap)
    }

    private fun bindingViewModel() {
        viewModel.mediatorLiveEndTrip.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveEndTripError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@EndTripActivity,
                                LoginActivity::class.java
                            )
                        )
                    )
                    finish()

                }
            }
        )

    }

    private fun initView() {

        intent.let {
            jobResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
        }

        binding.edOTP.apply {
            hint("OTP")
            inputMode(EditTextInputMode.NUMBER)
        }
        binding.edReceiverName.apply {
            hint("Enter Name")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.edReceiverNRIC.apply {
            hint("Enter NRIC")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }
        binding.imgUploadPod.setImage(imageDrawable = resources.getDrawable(R.drawable.edittext_bg))

       /* binding.btnClear.setOnClickListener {
            binding.signatureView.clearCanvas()
        }*/
        binding.imgUploadPod.registerForOnClick {
            initiateCaptureForProfilePicture()


        }
        binding.signatureViewImage.setOnClickListener {
            startActivity(Intent(this,SignatureViewActivity::class.java))
        }
        binding.btnSubmit.setOnClickListener {
            apiCalled()
        }
    }

    private fun apiCalled() {

        if (binding.edReceiverName.getText().toString().isEmpty()) {
            showMessageInDialog("Please provide receiver name")
            return
        }

        if (binding.edReceiverNRIC.getText().toString().isEmpty()) {
            showMessageInDialog("Please provide receiver NRIC")
            return
        }

        if (binding.edOTP.getText().toString().isEmpty()) {
            showMessageInDialog("Please provide OTP")
            return
        }

        if (binding.edOTP.getText().toString()!=(jobResponse.builder_otp?:"".toString())){
            showMessageInDialog("Please provide valid otp")
            return
        }

        if (imageFilePathPOD == null) {
            showMessageInDialog("Please upload POD image.")
            return
        }
        if (repository.signatureBitMap==null) {
            showMessageInDialog("Please provide receiver signature.")
            return
        }

        showProgress()
        viewModel.endTrip(
            receiver_name = binding.edReceiverName.getText().toString(),
            receiver_nric = binding.edReceiverNRIC.getText().toString(),
            token_id = jobResponse.token_id,
            driver_job_status = "3",
            signature = getSignatureFile(),
            pod = imageFilePathPOD
        )

    }

    private fun getSignatureFile(): File {

        //create a file to write bitmap data
        val timestamp= Timestamp(System.currentTimeMillis())
        val f = File(applicationContext.codeCacheDir, "$timestamp.JPEG")
        f.createNewFile()
        val bitmap = repository.signatureBitMap
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 0, byteArrayOutputStream)
        val bitmapdata = byteArrayOutputStream.toByteArray()
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(f)
        } catch (fileNotFoundException: FileNotFoundException) {
            fileNotFoundException.printStackTrace()
        }
        try {
            fos?.write(bitmapdata)
            fos?.flush()
            fos?.close()
        } catch (iOException: IOException) {
            iOException.printStackTrace()
        }
        return f
    }


    fun initiateCaptureForProfilePicture() {
        takeProfilePictureWithPermissionCheck()
    }

    @NeedsPermission(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun takeProfilePicture() {
        ImagePickerUtil.pickImage(
            context = this,
            reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
        )
    }

    @OnShowRationale(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun showRationale(request: PermissionRequest) {
        showMessageWithTwoButton(
            message = "We need some permission to take pictures.",
            buttonOneText = "Okay",
            buttonTwoText = "Cancel",
            callback = object : DialogUtil.MultiButtonCallBack {
                override fun buttonOneClicked() {
                    super.buttonOneClicked()
                    request.proceed()
                }

                override fun buttonTwoClicked() {
                    super.buttonTwoClicked()
                    request.cancel()
                    finish()
                }
            }
        )
    }

    @OnPermissionDenied(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun permissionDenied() {
        showMessageWithOneButton(
            "You will not be able to take picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    @OnNeverAskAgain(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun neverAskAgain() {
        showMessageWithOneButton(
            "You will not be able to take picture picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)
        if (requestCode == KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            intentData?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    //Log.i("got image $images")


                    imageFilePathPOD = File(images[0])
                    binding.imgUploadPod.setImage(
                        File(images[0]),
                        isCircularImage = false,
                        needBorderWithCircularImage = true
                    )
                }


            }
        }
    }
}