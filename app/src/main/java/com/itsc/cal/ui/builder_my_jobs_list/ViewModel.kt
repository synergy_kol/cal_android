package  com.itsc.cal.ui.builder_my_jobs_list

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveJobBuilderListing: MediatorLiveData<Event<ArrayList<JobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveJobListingBuilderError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveJobBuilderListing.addSource(
            repository.mutableLiveDataBuilderJobListData
        ) { t -> mediatorLiveJobBuilderListing.postValue(t) }
        mediatorLiveJobListingBuilderError.addSource(
            repository.mutableLiveDataBuilderJobListDataError
        ) { t -> mediatorLiveJobListingBuilderError.postValue(t) }


    }

    fun getJobList() = repository.getBuilderJobList()



}