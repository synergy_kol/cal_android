package com.itsc.cal.ui.signup

import android.app.Activity
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import androidx.lifecycle.Observer
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySignupContractorBuilderBinding
import com.itsc.cal.model.CityResponce
import com.itsc.cal.model.StateResponce
import com.itsc.cal.model.UserDetails
import com.itsc.cal.ui.get_started.GetStartedActivity
import com.itsc.cal.ui.login.LoginActivity
import com.itsc.cal.ui.verification.VerificationActivity
import com.itsc.cal.ui.webview.WebView
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.repository.Event
import com.sagar.android.logutilmaster.LogUtil
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class SignUpActivity : CalSuperActivity(), KodeinAware {

    private lateinit var binding: ActivitySignupContractorBuilderBinding
    private var signupType = ""
    override val kodein: Kodein by kodein()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var roleType = ""
    private var userType = "I"
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private val logUtil: LogUtil by instance()
    private var lat = ""
    private val AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS: Int = 1
    private var lon = ""
    private var stateId: String = ""
    private var cityId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_signup_contractor_builder)
        showAppLogoWithBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        stateList()
        initView()

        bindToViewModel()
    }

    private fun stateList() {
        showProgress()
        viewModel.getState()
    }

    private fun cityList(stateId: String) {
        viewModel.getCity(stateId)
    }

    private fun initView() {

        signupType = intent.getStringExtra(KeywordsAndConstants.SIGNUP_TYPE) ?: ""
        if (signupType.equals(KeywordsAndConstants.INDEPENDENT_TRUCK)) {
            roleType = "4"
            binding.editDriverPermiteNo.visibility = View.GONE
            binding.textDriverPermitNo.visibility = View.GONE
            binding.textDriverPermitNoStar.visibility = View.GONE
            binding.llUserType.visibility = View.VISIBLE
            binding.edYourLocation.visibility = View.VISIBLE
            binding.textYourLocation.visibility = View.VISIBLE

            binding.editCompanyName.visibility = View.GONE
            binding.textCompanyName.visibility = View.GONE
            binding.textCompanyNameStar.visibility = View.GONE

            binding.editCompanyRegistration.visibility = View.GONE
            binding.textCompanyReg.visibility = View.GONE
            binding.textCompanyRegStar.visibility = View.GONE

        } else if (signupType.equals(KeywordsAndConstants.MATERIAL_MANUFACTURER)) {
            roleType = "3"
            binding.editDriverPermiteNo.visibility = View.GONE
            binding.textDriverPermitNo.visibility = View.GONE
            binding.textDriverPermitNoStar.visibility = View.GONE
            binding.llUserType.visibility = View.GONE

            binding.editCompanyName.visibility = View.VISIBLE
            binding.textCompanyName.visibility = View.VISIBLE
            binding.textCompanyNameStar.visibility = View.VISIBLE

            binding.editCompanyRegistration.visibility = View.GONE
            binding.textCompanyReg.visibility = View.GONE
            binding.textCompanyRegStar.visibility = View.GONE

            binding.edYourLocation.visibility = View.GONE
            binding.textYourLocation.visibility = View.GONE

        } else if (signupType.equals(KeywordsAndConstants.CONTRATOR_BUILDER)) {
            roleType = "2"
            binding.editDriverPermiteNo.visibility = View.GONE
            binding.textDriverPermitNo.visibility = View.GONE
            binding.textDriverPermitNoStar.visibility = View.GONE
            binding.llUserType.visibility = View.GONE

            binding.editCompanyName.visibility = View.VISIBLE
            binding.textCompanyName.visibility = View.VISIBLE
            binding.textCompanyNameStar.visibility = View.VISIBLE

            binding.editCompanyRegistration.visibility = View.VISIBLE
            binding.textCompanyReg.visibility = View.VISIBLE
            binding.textCompanyRegStar.visibility = View.VISIBLE

            binding.edYourLocation.visibility = View.GONE
            binding.textYourLocation.visibility = View.GONE
        }

        binding.editFirstName.apply {
            hint("Enter First Name")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }
        binding.editLastName.apply {
            hint("Enter Last Name")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }
        binding.editCompanyName.apply {
            hint("Company Name")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }
        binding.editCompanyRegistration.apply {
            hint("Company Tax ID")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.editMobileNo.apply {
            hint("Enter your Mobile Number")
            inputMode(EditTextInputMode.NUMBER)
        }

        binding.editEmail.apply {
            hint("Enter Email")
            inputMode(EditTextInputMode.EMAIL)
        }

        binding.editPassword.apply {
            hint("Enter Password")
            inputMode(EditTextInputMode.PASSWORD)
        }

        binding.editConPassword.apply {
            hint("Enter Confirm Password")
            inputMode(EditTextInputMode.PASSWORD)
        }

        binding.editDriverPermiteNo.apply {
            hint("Enter Driver Permit No or License")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

        binding.editNoOfFleet.apply {
            hint("Enter No of Truck")
            inputMode(EditTextInputMode.NUMBER)
        }
        binding.textTermCondition.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    WebView::class.java
                ).putExtra(
                    WebView.URL,
                    "${KeywordsAndConstants.BASE_URL}home/cms/TNC"
                ).putExtra(
                    WebView.TITLE,
                    resources.getString(R.string.term_and_condition)
                )
            )
        }

        binding.radioButtonFleet.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                binding.editNoOfFleet.visibility = View.VISIBLE
                userType = "F"
            } else {
                binding.editNoOfFleet.visibility = View.GONE
                userType = "I"
            }
        }

        binding.textSignup.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        binding.buttonSignup.setOnClickListener {
            signUp()
        }
        binding.edYourLocation.setOnClickListener {
            openPlacePicker()
        }
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataSignup.observe(
            this,
            Observer<Event<UserDetails>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processSignUpResponse(it.getContent()!!)
                }
            }
        )
        viewModel.mediatorLiveDataSignupError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveStateList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<StateResponce>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        var mStateList = it.getContent() ?: ArrayList()
                        if (mStateList?.size ?: 0 > 0) {
                            binding.spinnerState.setItems(mStateList as List<StateResponce>)
                            binding.spinnerState.setOnItemSelectedListener {
                                stateId = it.id
                                cityList(stateId)
                                binding.spinnerCity.setText("")
                            }
                        }
                    }
                }
            }
        )

        viewModel.mediatorLiveCityList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<CityResponce>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        var mCityList = it.getContent() ?: ArrayList()
                        if (mCityList?.size ?: 0 > 0) {
                            binding.spinnerCity.setItems(mCityList as List<CityResponce>)
                            binding.spinnerCity.setOnItemSelectedListener {
                                cityId = it.id
                            }
                        }

                    }
                }
            }
        )

    }

    private fun processSignUpResponse(userDetails: UserDetails) {
        hideProgress()
        startActivity(Intent(this, GetStartedActivity::class.java)
            .putExtra(KeywordsAndConstants.USER_ROLE, roleType))
        finish()
        /*startActivity(
            Intent(
                this,
                VerificationActivity::class.java
            ).putExtra(KeywordsAndConstants.isFromForgotPassword, false)
        )
        finish()*/
    }


    fun signUp() {

        if (roleType.equals("4") && userType.equals("F") && binding.editNoOfFleet.getText()
                .toString().isEmpty()
        ) {
            showMessageInDialog("Please provide no of truck.")
            return
        }
        if (
            binding.editFirstName.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide first name.")
            return
        }

        if (
            binding.editLastName.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide last name.")
            return
        }

        if (binding.editCompanyName.getText().toString()
                .isEmpty() && (roleType.equals("3") || roleType.equals("2"))
        ) {
            showMessageInDialog("Please provide company name.")
            return
        }

        if (binding.editCompanyRegistration.getText().toString()
                .isEmpty() && roleType.equals("2")
        ) {
            showMessageInDialog("Please provide Company Tax ID.")
            return
        }

        if (
            binding.editMobileNo.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide mobile number.")
            return
        }
        if (!AndroidUtility.isValidMobile(binding.editMobileNo.getText().toString())) {
            showMessageInDialog(resources.getString(R.string.mobile_no_validration_message))
            return
        }

      /*  if (binding.editDriverPermiteNo.getText().toString().isEmpty() && roleType.equals("4")) {
            showMessageInDialog("Please provide permit no or license no.")
            return
        }*/
        if (
            binding.spinnerState.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide State.")
            return
        }

        if (
            binding.spinnerCity.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide City.")
            return
        }

        if (
            binding.editEmail.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide email.")
            return
        }
        if (
            !Patterns.EMAIL_ADDRESS.matcher(
                binding.editEmail.getText().toString()
            ).matches()
        ) {
            showMessageInDialog("Please provide valid email.")
            return
        }
        if (
            binding.editPassword.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide password.")
            return
        }
        if (!AndroidUtility.validatePassword(binding.editPassword.getText().toString())) {
            showMessageInDialog(resources.getString(R.string.is_valid_passwd))
            return
        }
        if (binding.editConPassword.getText().isEmpty()) {
            showMessageInDialog("Please provide valid confirm password.")
            return
        }
        if (!binding.editConPassword.getText().trim()
                .equals(binding.editPassword.getText().trim())
        ) {
            showMessageInDialog("Password and confirm password are not same.")
            return
        }

        if (binding.edYourLocation.getText().toString()
                .isEmpty() && roleType.equals("4")
        ) {
            showMessageInDialog("Please provide your location.")
            return
        }

        if (!binding.checkBoxIAgree.isChecked) {
            showMessageInDialog(resources.getString(R.string.accept_term_condition))
            return
        }



        showProgress()
        if (roleType.equals("4")) {
            viewModel.signUp(
                email = binding.editEmail.getText().toString().trim(),
                password = binding.editPassword.getText().toString(),
                deviceToken = "",
                fname = binding.editFirstName.getText().trim(),
                lname = binding.editLastName.getText().trim(),
                mobile = binding.editMobileNo.getText().trim(),
                company_name = binding.editCompanyName.getText().trim(),
                company_registration = binding.editCompanyRegistration.getText().trim(),
                license = "", //binding.editDriverPermiteNo.getText().trim(),
                user_role = roleType,
                user_type = userType,
                numberof_truck = binding.editNoOfFleet.getText().trim(),
                latitude = lat,
                longitude = lon,
                city = cityId,
                state = stateId
            )
        } else {
            viewModel.signUp(
                email = binding.editEmail.getText().trim(),
                password = binding.editPassword.getText().trim(),
                deviceToken = "",
                fname = binding.editFirstName.getText().trim(),
                lname = binding.editLastName.getText().trim(),
                mobile = binding.editMobileNo.getText().trim(),
                company_name = binding.editCompanyName.getText().trim(),
                company_registration = binding.editCompanyRegistration.getText().trim(),
                license = "",//binding.editDriverPermiteNo.getText().trim(),
                user_role = roleType,
                user_type = "",
                numberof_truck = "",
                latitude = "",
                longitude = "",
                city = cityId,
                state = stateId
            )
        }

    }

    private fun openPlacePicker() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)

        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        ).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val place = Autocomplete.getPlaceFromIntent(data)
                setAddress(place)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR && data != null) {
                // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(data)
                Log.i("Status", status.statusMessage?:"")
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    private fun setAddress(place: Place) {
        binding.edYourLocation.setText(place.address ?: "")
        lat = place.latLng?.latitude.toString()
        lon = place.latLng?.longitude.toString()
    }


}