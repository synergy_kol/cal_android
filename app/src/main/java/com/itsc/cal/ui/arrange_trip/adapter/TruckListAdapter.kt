package com.itsc.cal.ui.arrange_trip.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ChildSelectedTruckBinding
import com.itsc.cal.model.TruckListResponse

class TruckListAdapter(
    val mContext: Context,
    val onItemClickListener: truckOnItemClickListener,
    val arrayList: ArrayList<TruckListResponse>
) :
    RecyclerView.Adapter<TruckListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ChildSelectedTruckBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))
    }

    inner class ViewHolder(val binding: ChildSelectedTruckBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(typeResponse: TruckListResponse) {
            binding.llView.setOnClickListener {
                onItemClickListener.onItemClick( adapterPosition)
            }
            binding.image.setImage(imageUrl = "${KeywordsAndConstants.BASE_URL}${typeResponse.truckimage}")
            binding.image.registerForOnClick {
                onItemClickListener.onItemClick( adapterPosition)
            }
            binding.textName.text = typeResponse.truck_vin_number
            if (typeResponse.selected.equals("1")){
                binding.llView.setBackgroundResource(R.drawable.manage_driver_bg)
            }else{
                binding.llView.setBackgroundResource(R.color.colorGrey2)
            }

        }

    }
    public interface truckOnItemClickListener{
        fun onItemClick(position:Int)
    }

}