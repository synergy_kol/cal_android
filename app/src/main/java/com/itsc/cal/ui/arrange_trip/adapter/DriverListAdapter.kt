package com.itsc.cal.ui.arrange_trip.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ChildSelectedDriverBinding
import com.itsc.cal.model.DriverListResponse

class DriverListAdapter(
    val mContext: Context,
    val onItemClickListener: driverOnItemClickListener,
    val arrayList: ArrayList<DriverListResponse>
) :
    RecyclerView.Adapter<DriverListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ChildSelectedDriverBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))
    }

    inner class ViewHolder(val binding: ChildSelectedDriverBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(driverListResponse: DriverListResponse) {
            binding.textDriverName.text = driverListResponse.driver_name
            var dName = driverListResponse.driver_name.substring(0,1)
            binding.imgProfileImage.setImage(
                imageUrl = "${KeywordsAndConstants.BASE_URL}${driverListResponse.driverimage}",
                isCircularImage = true,
                needPlaceHolderImageForName = dName
            )
            binding.llView.setOnClickListener {
                onItemClickListener.onItemClick(adapterPosition)
            }
            binding.imgProfileImage.registerForOnClick {
                onItemClickListener.onItemClick(adapterPosition)
            }
            if (driverListResponse.selected.equals("1")){
                binding.llView.setBackgroundResource(R.drawable.manage_driver_bg)
            }else{
                binding.llView.setBackgroundResource(R.color.colorGrey2)
            }
        }

    }

    public interface driverOnItemClickListener{
        fun onItemClick(position:Int)
    }

}