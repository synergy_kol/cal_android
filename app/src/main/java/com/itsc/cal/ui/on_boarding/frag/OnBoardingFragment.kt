package com.itsc.cal.ui.on_boarding.frag

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.itsc.cal.databinding.FragmentOnBoardingBinding

/**
 * A simple [Fragment] subclass.
 */
class OnBoardingFragment : Fragment() {

    private lateinit var binding: FragmentOnBoardingBinding
    private lateinit var requiredContext: Context
    var imageId: Int = 0
    var message: String = ""
    var subMessage: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOnBoardingBinding.inflate(
            inflater,
            null,
            false
        )
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        this.requiredContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.image.setImageResource(imageId)
        binding.textViewMessage.text = message
        binding.textViewSubMessage.text = subMessage
    }
}
