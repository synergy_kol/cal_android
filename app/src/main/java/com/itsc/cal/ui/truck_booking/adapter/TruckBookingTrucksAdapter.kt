package com.itsc.cal.ui.truck_booking.adapter

import android.content.Context
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.TruckBokingsListItemBinding
import kotlin.random.Random

class TruckBookingTrucksAdapter(private val context: Context) :
    RecyclerView.Adapter<TruckBookingTrucksAdapter.Holder>() {

    data class Truck(
        var id: String,
        var selected: Boolean = false
    )

    val trucks = arrayListOf(
        Truck(
            System.currentTimeMillis().toString() + Random.nextLong(99999)
        ),
        Truck(
            System.currentTimeMillis().toString() + Random.nextLong(99999)
        ),
        Truck(
            System.currentTimeMillis().toString() + Random.nextLong(99999)
        ),
        Truck(
            System.currentTimeMillis().toString() + Random.nextLong(99999)
        ),
        Truck(
            System.currentTimeMillis().toString() + Random.nextLong(99999)
        ),
        Truck(
            System.currentTimeMillis().toString() + Random.nextLong(99999)
        )
    )

    inner class Holder(private val binding: TruckBokingsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(truck: Truck) {
            binding.container.setOnClickListener {
                trucks.forEach {
                    it.selected = truck.id == it.id
                }
                notifyDataSetChanged()
            }

            TransitionManager.beginDelayedTransition(binding.container)
            val constraintSet = ConstraintSet()
            constraintSet.clone(
                binding.container
            )
            constraintSet.constrainPercentHeight(
                binding.appcompatImageViewTruck.id,
                if (truck.selected) 0.7f else 0.5f
            )
            constraintSet.applyTo(
                binding.container
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            TruckBokingsListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return trucks.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(trucks[position])
    }
}