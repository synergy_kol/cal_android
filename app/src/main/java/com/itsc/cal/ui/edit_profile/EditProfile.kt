package com.itsc.cal.ui.edit_profile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.fxn.pix.Pix
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.core.KeywordsAndConstants.EDIT_PROFILE_REQUEST_CODE
import com.itsc.cal.databinding.ActivityEditProfileBinding
import com.itsc.cal.model.ProfileDetails
import com.itsc.cal.model.ProfileResponse
import com.itsc.cal.ui.signup.CompleteProfileActivity
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.itsc.cal.widget.edittext.MultiLineConfig
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File

class EditProfile : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()

    companion object {
        private const val AUTOCOMPLETE_REQUEST_CODE_ADDRESS = 123
    }

    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var binding: ActivityEditProfileBinding
    private lateinit var profileResponse: ProfileResponse
    private var userImage: String? = null
    private lateinit var selectedPlace: Place

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_edit_profile)
        binding.context = this

        enableBackButton()
        setPageTitle("Edit Profile")

        viewModel =
            androidx.lifecycle.ViewModelProvider(this, viewModelProvider).get(ViewModel::class.java)

        intent?.let {
            profileResponse = fromJson(it.getStringExtra(KeywordsAndConstants.DATA)!!)

            setUI()
        } ?: run {
            finish()
        }

        bindToViewModel()
    }

    private fun setUI() {
        val profileDetails = profileResponse.profile ?: return
        binding.imageViewProfile.setImage(
            profileDetails.profile_image,
            isCircularImage = true
        )
        binding.editTextFirstName.setText(profileDetails.fname)
        binding.editTextLastName.setText(profileDetails.lanme)
       // binding.editTextDateOfBirth.text = profileDetails.dob
        binding.editTextAddress.text = profileDetails.address
        binding.editTextCompanyName.setText(profileDetails.company_name)
        binding.editTextCompanyRegistration.setText(profileDetails.company_registration)

        if (profileDetails.user_type == "2") {
            binding.textViewCompanyRegistration.visibility = View.VISIBLE
            binding.editTextCompanyRegistration.visibility = View.VISIBLE
        } else {
            binding.textViewCompanyRegistration.visibility = View.GONE
            binding.editTextCompanyRegistration.visibility = View.GONE
        }

        binding.apply {
            editTextFirstName.inputMode(EditTextInputMode.INPUT_TEXT, MultiLineConfig(1,1))
            editTextLastName.inputMode(EditTextInputMode.INPUT_TEXT, MultiLineConfig(1,1))
            editTextCompanyName.inputMode(EditTextInputMode.INPUT_TEXT, MultiLineConfig(1,1))
            editTextCompanyRegistration.inputMode(EditTextInputMode.INPUT_TEXT, MultiLineConfig(1,1))
        }
    }

    @SuppressLint("CheckResult")
    fun takePicture() {
        RxPermissions(this)
            .request(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .subscribe {
                if (!it) {
                    showMessageInDialog("Please accept permissions to take picture.")
                }

                ImagePickerUtil.pickImage(
                    context = this,
                    reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
                )
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)
        if (requestCode == KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            intentData?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    userImage = images[0]

                    showUserImage()
                }
            }
        }
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE_ADDRESS) {
            @Suppress("ControlFlowWithEmptyBody")
            if (resultCode == Activity.RESULT_OK && intentData != null) {
                val place = Autocomplete.getPlaceFromIntent(intentData)
                setAddress(place)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR && intentData != null) {
                // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(intentData)
            } else if (resultCode == Activity.RESULT_CANCELED) {
            }
        }
        if(requestCode == EDIT_PROFILE_REQUEST_CODE){
            if (resultCode == Activity.RESULT_OK){
                finish()
            }
        }
    }

    private fun showUserImage() {
        if (userImage == null)
            return

        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        binding.imageViewProfile.setImage(File(userImage), true)
    }

    /*fun showDateOfBirthPicker() {
        AndroidUtility.showDatePickerMax(
            supportFragmentManager,
            com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val selectedDate =
                    dayOfMonth.toString() + "/" + (monthOfYear + 1).toString() + "/" + year
                val fromDate = AndroidUtility.formatDateFromString(
                    "dd/MM/yyyy",
                    "yyyy-MM-dd",
                    selectedDate
                )
                binding.editTextDateOfBirth.text = fromDate
            })
    }*/

    fun showAddressPicker() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)

        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        ).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_ADDRESS)
    }

    private fun setAddress(place: Place) {
        this.selectedPlace = place

        binding.editTextAddress.text = place.address ?: ""
    }

    fun saveData() {
        showProgress()

        viewModel.updateProfile(
            if (binding.editTextFirstName.getText()
                    .isEmpty()
            ) null else binding.editTextFirstName.getText(),
            if (binding.editTextLastName.getText()
                    .isEmpty()
            ) null else binding.editTextLastName.getText(),
            null,
            if (binding.editTextAddress.text
                    .isEmpty()
            ) null else binding.editTextAddress.text.toString(),
            null,
            null,
            null,
            null,
           null,
            null,
            viewModel.giveRepository().getUserData()?.user_master_id?:"0",
            if (binding.editTextCompanyName.getText().toString()
                    .isEmpty()
            ) null else binding.editTextCompanyName.getText().toString(),
            if (profileResponse.profile?.user_type == "2") {
                if (binding.editTextCompanyRegistration.getText()
                        .isEmpty()
                ) null else binding.editTextCompanyRegistration.getText().toString()
            } else {
                null
            },
            if (userImage != null) File(userImage!!) else null
        )
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataSaveProfileDetails.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                        val intent  = Intent(this@EditProfile, CompleteProfileActivity::class.java)
                        intent.putExtra("from", "edit")
                            .putExtra(
                            KeywordsAndConstants.DATA,
                            toJson(profileResponse)
                        )
                        startActivityForResult(intent, EDIT_PROFILE_REQUEST_CODE)
                        /*showMessageWithOneButton("Profile updated", object : DialogUtil.CallBack {
                            override fun buttonClicked() {
                                super.buttonClicked()

                                finish()
                            }
                        })*/
                    }
                }
            }
        )

        viewModel.mediatorLiveDataSaveProfileDetailsError.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )
    }
}