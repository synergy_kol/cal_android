package  com.itsc.cal.ui.manage_driver

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDriverList: MediatorLiveData<Event<ArrayList<DriverListResponse>>> =
        MediatorLiveData()
    val mediatorLiveDriverError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {

        mediatorLiveDriverList.addSource(
            repository.mutableLiveDataDriverList
        ) { t -> mediatorLiveDriverList.postValue(t) }
        mediatorLiveDriverError.addSource(
            repository.mutableLiveDataDriverListError
        ) { t -> mediatorLiveDriverError.postValue(t) }


    }
    fun getDriverList() = repository.getDriverTypeList()
}