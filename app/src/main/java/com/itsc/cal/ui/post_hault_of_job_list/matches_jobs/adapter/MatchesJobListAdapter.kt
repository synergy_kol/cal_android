package com.itsc.cal.ui.post_hault_of_job_list.matches_jobs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.AdapterMatchesJobListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.HauljobListResponse

class MatchesJobListAdapter(
    val mContext: Context,
    val onItemClickListener: OnItemClickListener,
    val arrayList: ArrayList<HauljobListResponse>,
    val accessJob: String? = null
) :
    RecyclerView.Adapter<MatchesJobListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            AdapterMatchesJobListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))

    }

    inner class ViewHolder(val binding: AdapterMatchesJobListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(jobListResponse: HauljobListResponse) {
            binding.tvSource.text = jobListResponse.address
            binding.tvJobId.text = "#CAL${jobListResponse.haul_request_id}"
            //  binding.tvDestination.text=jobListResponse.Destination
            binding.tvWeight.text = "${jobListResponse.cubic_yards} Cubic"
            binding.tvWeightTons.text = "${jobListResponse.tons} TONS"

            binding.tvEquipment.text =
                if (jobListResponse.loading_equipment_name == null || jobListResponse.loading_equipment_name == "")
                    "No" else
                    "${jobListResponse.loading_equipment_name}"

            if (jobListResponse.material_option_name == null || jobListResponse.material_option_name == "") {
                binding.tvMaterialType.text =
                    "${jobListResponse.material_name}"
            } else {
                binding.tvMaterialType.text =
                    "${jobListResponse.material_name}(${jobListResponse.material_option_name})"
            }

            binding.tvTruckPayInText.text = "Point of Contact: ${jobListResponse.point_of_contact}"
            binding.tvTruckPayIn.text = "Main Number: ${jobListResponse.main_number}"
            binding.tvEmail.text = "Email: ${jobListResponse.email}"
            binding.cvPostedJob.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
           /* if (accessJob.equals("1") || accessJob.equals("2")) {

                binding.btnApply.visibility = View.VISIBLE
                binding.btnCancel.visibility = View.VISIBLE
                binding.btnRequest.visibility = View.GONE
            } else {
                binding.btnApply.visibility = View.GONE
                binding.btnCancel.visibility = View.GONE
                binding.btnRequest.visibility = View.VISIBLE
            }*/

            if (accessJob.equals("1")){
                if (jobListResponse.request_type_to.equals("E")){
                    if (jobListResponse.is_request.equals("1")){
                        binding.btnApply.visibility = View.VISIBLE
                        binding.btnApply.setText("Accept")
                        binding.btnCancel.visibility = View.VISIBLE
                        binding.btnCancel.setText("Reject")
                        binding.btnApply.setOnClickListener {
                            onItemClickListener.onItemClick(it,adapterPosition)
                        }

                        binding.btnCancel.setOnClickListener {
                            onItemClickListener.onItemClick(it,adapterPosition)
                        }
                        binding.btnRequest.visibility = View.GONE
                    }else if (jobListResponse.is_request.equals("4")){
                        binding.btnRequest.visibility = View.VISIBLE
                        binding.btnRequest.setText("Accepted & Notified to Importer")
                    }else if (jobListResponse.is_request.equals("3")){
                        binding.btnRequest.visibility = View.VISIBLE
                        binding.btnRequest.setText("Rejected")
                    }
                }else{
                        if (jobListResponse.is_request.equals("1")){
                            binding.btnRequest.visibility = View.VISIBLE
                            binding.btnRequest.setText("Requested to Importer")
                        }else if (jobListResponse.is_request.equals("3")){
                            binding.btnRequest.visibility = View.VISIBLE
                            binding.btnRequest.setText("Rejected By Importer")
                        }
                }
            }else if (accessJob.equals("2")){
                    if (jobListResponse.request_type_to.equals("I")){
                            if (jobListResponse.is_request.equals("1")){
                                binding.btnApply.visibility = View.VISIBLE
                                binding.btnApply.setText("Accept")
                                binding.btnCancel.visibility = View.VISIBLE
                                binding.btnCancel.setText("Reject")
                                binding.btnApply.setOnClickListener {
                                    onItemClickListener.onItemClick(it,adapterPosition)
                                }

                                binding.btnCancel.setOnClickListener {
                                    onItemClickListener.onItemClick(it,adapterPosition)
                                }
                                binding.btnRequest.visibility = View.GONE
                            }else if (jobListResponse.is_request.equals("3")){
                                binding.btnRequest.visibility = View.VISIBLE
                                binding.btnRequest.setText("Rejected")
                            }
                    }else{

                        if (jobListResponse.is_request.equals("1")){
                            binding.btnRequest.visibility = View.VISIBLE
                            binding.btnRequest.setText("Requested to Exporter")
                        }else if (jobListResponse.is_request.equals("4")){
                            binding.btnApply.visibility = View.VISIBLE
                            binding.btnApply.setText("Pay & Create Job")
                            binding.btnApply.setOnClickListener {
                                onItemClickListener.onItemClick(it,adapterPosition)
                            }
                        }
                        else if (jobListResponse.is_request.equals("3")){
                            binding.btnRequest.visibility = View.VISIBLE
                            binding.btnRequest.setText("Rejected By Exporter")
                        }
                    }
            }else{
                binding.btnRequest.visibility = View.VISIBLE
                binding.btnApply.visibility = View.GONE
                binding.btnCancel.visibility = View.GONE
                binding.btnRequest.setText("Send Request")
                binding.btnRequest.setOnClickListener {
                    onItemClickListener.onItemClick(it,adapterPosition)
                }
            }



            /*  if (jobListResponse.job_status.equals("3")){
                  binding.btnReview.visibility = View.VISIBLE
              }else{
                  binding.btnReview.visibility = View.GONE
              }
              binding.btnReview.setOnClickListener {
                  onItemClickListener.onItemClick(it,adapterPosition)
              }*/

        }

    }

}