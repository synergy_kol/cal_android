package com.itsc.cal.ui.builder_truck_details

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityBuilderTruckDetailsBinding
import com.itsc.cal.databinding.DialogViewSlipBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse
import com.itsc.cal.ui.builder_truck_details.adapter.BuilderTruckListAdapter
import com.itsc.cal.widget.image_view.ImageView

class BuilderTruckDetailsActivity : CalSuperActivity() {
    private lateinit var binding: ActivityBuilderTruckDetailsBinding
    private lateinit var jobResponse: JobListResponse
    private lateinit var dialogBinding: DialogViewSlipBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_builder_truck_details)
        enableBackButton()
        setPageTitle("Trucks Information")
        initView()

    }

    private fun initView() {
        intent.let {
            jobResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))

            binding.recycleviewBuilderTruckDetails.apply {
                layoutManager = LinearLayoutManager(this@BuilderTruckDetailsActivity)
                adapter = BuilderTruckListAdapter(this@BuilderTruckDetailsActivity, object :
                    OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        when (view.id) {
                            R.id.btn_empty_slip -> {
                                viewSlipDialog(jobResponse.truck_details?.get(position)?.empty_truck_slip?:"")
                            }
                            R.id.btn_loaded_truck_slip -> {
                                viewSlipDialog(jobResponse.truck_details?.get(position)?.loaded_truck_slip?:"")
                            }
                        }
                    }

                }, jobResponse.truck_details?:ArrayList())
            }
        }
    }

    private fun viewSlipDialog(imageUrl: String) {
        dialogBinding = DialogViewSlipBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        val customDialog = Dialog(this)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(dialogBinding.root)

        customDialog.window
            ?.setLayout(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
            )

        customDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.setCancelable(true)
        customDialog.show()
        if (imageUrl != null) {
            dialogBinding.imageView.setImage(
                imageUrl,
                isCircularImage = false,
                needBorderWithCircularImage = true
            )
            dialogBinding.imageView.configureZoom(ImageView.ZoomEnabled.YES)
        }

    }
}