package com.itsc.cal.ui.builder_my_job_list_with_haul_job.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.databinding.AdapterPostedJobBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse

class PostedJobBuilderAdapter(
    val mContext: Context,
    val onItemClickListener: OnItemClickListener,
    val arrayList: ArrayList<JobListResponse>
) :
    RecyclerView.Adapter<PostedJobBuilderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            AdapterPostedJobBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))

    }

    inner class ViewHolder(val binding: AdapterPostedJobBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(jobListResponse: JobListResponse) {
            binding.tvSource.text = jobListResponse.Source
            binding.tvJobId.text = "#CAL${jobListResponse.job_id}"
            binding.tvDestination.text = jobListResponse.Destination
            binding.tvWeight.text = "${jobListResponse.Weight} QT"
            binding.tvDate.text = jobListResponse.pickup_date

            if (jobListResponse.job_type.equals("H")) {
                binding.cvPostedJob.setBackgroundColor(mContext.resources.getColor(R.color.text_sky_light_blue))
                binding.textIconHl.visibility = View.VISIBLE
                binding.tvWeight.visibility = View.GONE
                binding.tvTruckPayInText.visibility = View.GONE
                binding.tvEquipmentText.visibility = View.GONE
            } else {
                binding.cvPostedJob.setBackgroundColor(mContext.resources.getColor(R.color.white))
                binding.textIconHl.visibility = View.GONE
                binding.tvWeight.visibility = View.VISIBLE
                binding.tvTruckPayInText.visibility = View.VISIBLE
                binding.tvEquipmentText.visibility = View.VISIBLE
            }

            binding.tvEquipmentText.text =
                if (jobListResponse.Equipment_value == "null" || jobListResponse.Equipment_value == "")
                    "Equipment: No" else
                    "Equipment: ${jobListResponse.Equipment_value}"
            binding.tvEquipment.text = ""
            binding.tvMaterialTypeText.text = "Material: ${jobListResponse.material_name}"
            binding.textTruckName.text = jobListResponse.truck_name
            binding.tvTruckPayInText.text = "Freight Charges: $ ${jobListResponse.FreightCharges}"
            binding.tvTruckPayIn.text = "Estimated Price: $ ${jobListResponse.JobEstimatePrice}"
            binding.cvPostedJob.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }

            if (jobListResponse.job_status.equals("3")) {
                binding.btnTracking.visibility = View.GONE
                binding.btnReview.visibility = View.GONE
            } else {
                if (jobListResponse.job_status.equals("5")) {
                    binding.btnTracking.visibility = View.VISIBLE

                } else {
                    binding.btnTracking.visibility = View.GONE
                }
                binding.btnReview.visibility = View.GONE

            }

            binding.btnApply.visibility = View.GONE
            if (jobListResponse.self_job.equals("1")) {
                if (jobListResponse.job_status.equals("1") || jobListResponse.job_status.equals("5")) {
                    binding.btnCancel.visibility = View.VISIBLE
                } else {
                    binding.btnCancel.visibility = View.GONE
                }
            } else {
                binding.btnCancel.visibility = View.GONE
            }

            binding.btnCancel.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }

            binding.btnTracking.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
            binding.btnReview.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
        }


    }

}