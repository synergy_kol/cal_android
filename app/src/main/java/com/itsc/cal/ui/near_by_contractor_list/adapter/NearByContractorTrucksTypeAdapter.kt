package com.itsc.cal.ui.near_by_contractor_list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.NearByContractorListItemTruckItemBinding
import com.itsc.cal.model.TruckListResponse

class NearByContractorTrucksTypeAdapter(private val context: Context,private val arrayList:ArrayList<TruckListResponse>) :
    RecyclerView.Adapter<NearByContractorTrucksTypeAdapter.Holder>() {

    inner class Holder(private val binding: NearByContractorListItemTruckItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(truckListResponse: TruckListResponse) {
           /* binding.appcompatImageViewImage.setImageDrawable(
                ResourcesCompat.getDrawable(
                    context.resources,
                    R.drawable.haul_off,
                    null
                )
            )*/
          /*  binding.appcompatImageViewImage.setColorFilter(
                ResourcesCompat.getColor(
                    context.resources,
                    R.color.color_blue,
                    null
                )
            )*/
            binding.appcompatImageViewImage.setImage("${KeywordsAndConstants.BASE_URL}${truckListResponse.truckimage}")
            binding.textViewText.text = truckListResponse.truck_type_name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            NearByContractorListItemTruckItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(arrayList.get(position))
    }
}