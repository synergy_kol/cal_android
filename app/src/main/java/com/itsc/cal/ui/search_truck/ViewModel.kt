package  com.itsc.cal.ui.search_truck

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {


    val mediatorLiveTruckData: MediatorLiveData<Event<ArrayList<TruckDataResponse>>> =
        MediatorLiveData()
    val mediatorLiveTruckDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveSearchTruckData: MediatorLiveData<Event<ArrayList<SearchTruckListResponse>>> =
        MediatorLiveData()
    val mediatorLiveSearchTruckDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {

        mediatorLiveTruckData.addSource(
            repository.mutableLiveDataTruckData
        ) { t -> mediatorLiveTruckData.postValue(t) }
        mediatorLiveTruckDataError.addSource(
            repository.mutableLiveDataTruckDataError
        ) { t -> mediatorLiveTruckDataError.postValue(t) }

        mediatorLiveSearchTruckData.addSource(
            repository.mutableLiveDataSearchTruckList
        ) { t -> mediatorLiveSearchTruckData.postValue(t) }
        mediatorLiveSearchTruckDataError.addSource(
            repository.mutableLiveDataSearchTruckListError
        ) { t -> mediatorLiveSearchTruckDataError.postValue(t) }
    }

    fun getTruckData() = repository.getMasterTruckData()
    fun searchTruckList(truck_type: String, fleet_type: String) =
        repository.searchTruck(truck_type = truck_type, fleet_type = fleet_type)
}