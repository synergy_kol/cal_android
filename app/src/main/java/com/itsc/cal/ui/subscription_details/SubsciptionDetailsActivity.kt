package com.itsc.cal.ui.subscription_details

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySubscriptionDetailsBinding
import com.itsc.cal.databinding.ActivitySubscriptionListBinding
import com.itsc.cal.databinding.ActivityTrackListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.*
import com.itsc.cal.ui.builders_details.BuilderDetailsActivity
import com.itsc.cal.ui.card_list.CardListActivity
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.login.LoginActivity
import com.itsc.cal.ui.post_a_truck.PostATruckActivity
import com.itsc.cal.ui.suscription.adapter.SubscriptionListAdapter
import com.itsc.cal.ui.track_list.adapter.TrackListAdapter
import com.itsc.cal.ui.truck_details.TruckDetailsActivity
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.generateStripeToken
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SubsciptionDetailsActivity : CalSuperActivity(withNav = true), KodeinAware {

    private lateinit var binding: ActivitySubscriptionDetailsBinding
    override val kodein: Kodein by kodein()
    private lateinit var viewModel: ViewModel
    private lateinit var truckTypeResponse: TruckTypeResponse
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var subscriptionListResponce: SubscriptionListResponce

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_subscription_details)
        enableBackButton()
        setPageTitle("Subscription Details")
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)


        initView()

        bindViewHolder()
    }

    override fun onResume() {
        super.onResume()

    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveDefalutCard.observe(
            this,
            Observer<Event<CardResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        getDefalutCardData(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDefalutCardError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                        defalutCardError("")
                    }

                }
            }
        )

        viewModel.mediatorLiveUserSubscription.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseNew(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveUserSubscriptionError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveCancelSubscription.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseNew(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveCancelSubscriptionError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveFleetCirtification.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponseNew(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveFleetCirtificationError.observe(
            this,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )
    }

    private fun processResponseNew(message: String) {

        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    startActivity(
                        UiUtil.clearStackAndStartNewActivity(
                            Intent(
                                this@SubsciptionDetailsActivity,
                                Dashboard::class.java
                            )
                        )
                    )
                    finish()
                }
            }
        )

    }

    private fun getDefalutCardData(cardResponse: CardResponse) {
        val data = Base64.decode(cardResponse.cvv_number.toString(), Base64.DEFAULT)
        val text = String(data, charset("UTF-8"))
        generateStripeToken(
            cardNumber = cardResponse.number,
            cardExpMonth = cardResponse.expiry_month.toInt(),
            cardExpYear = cardResponse.expiry_year.toInt(),
            cardCVV = text
        ) { token ->
            Log.i("Token:", token)
            if (viewModel.giveRepository().getUserData()!!.user_role.equals("4")) {

                showProgress()
                viewModel.fleetCertification(
                    stripeToken = token,
                    certification_id = subscriptionListResponce.id ?: ""
                )
            } else {
                showProgress()
                viewModel.userSubscription(
                    stripeToken = token,
                    subscribe_id = subscriptionListResponce.id ?: ""
                )
            }

        }

    }

    private fun defalutCardError(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Please provide a default card",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    startActivity(
                        Intent(
                            this@SubsciptionDetailsActivity,
                            CardListActivity::class.java
                        )
                    )
                }
            }
        )

    }

    private fun initView() {
        intent.let {
            subscriptionListResponce = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            binding.textContact.text = subscriptionListResponce.title
            binding.textDescription.text =
                "Amount:$ ${subscriptionListResponce.amount} + Sales Tax :$ ${subscriptionListResponce.sale_tax}"
            binding.textPrice.text = "$ ${subscriptionListResponce.tax_amount}"

            if (viewModel.giveRepository().getUserData()!!.user_role.equals("4")) {
                if (subscriptionListResponce.user_taken_status.equals("1")) {

                    val fromDate = AndroidUtility.formatDateFromString(
                        "yyyy-MM-dd hh:mm:ss",
                        "dd MMM yyyy",
                        subscriptionListResponce.details.subscription_date ?: ""
                    )

                    val toDate = AndroidUtility.formatDateFromString(
                        "yyyy-MM-dd hh:mm:ss",
                        "dd MMM yyyy",
                        subscriptionListResponce.details.subscription_end_date ?: ""
                    )
                    binding.textSubscrbdetails.text = "${fromDate} to ${toDate}"
                    binding.textSubscrbdetails.visibility = View.VISIBLE
                    binding.btnCancelSubscribtion.visibility = View.GONE
                    binding.btnApplyNow.visibility = View.GONE
                } else {
                    binding.btnCancelSubscribtion.visibility = View.GONE
                    binding.btnApplyNow.visibility = View.VISIBLE
                    binding.textSubscrbdetails.visibility = View.GONE
                }
            } else {
                if (subscriptionListResponce.user_taken_status.equals("1")) {

                    val fromDate = AndroidUtility.formatDateFromString(
                        "yyyy-MM-dd hh:mm:ss",
                        "dd MMM yyyy",
                        subscriptionListResponce.details.subscription_date ?: ""
                    )

                    val toDate = AndroidUtility.formatDateFromString(
                        "yyyy-MM-dd hh:mm:ss",
                        "dd MMM yyyy",
                        subscriptionListResponce.details.subscription_end_date ?: ""
                    )
                    binding.textSubscrbdetails.text = "${fromDate} to ${toDate}"
                    binding.textSubscrbdetails.visibility = View.VISIBLE
                    binding.btnCancelSubscribtion.visibility = View.VISIBLE
                    binding.btnApplyNow.visibility = View.GONE
                } else {
                    binding.btnCancelSubscribtion.visibility = View.GONE
                    binding.btnApplyNow.visibility = View.VISIBLE
                    binding.textSubscrbdetails.visibility = View.GONE
                }
            }


        }

        binding.btnApplyNow.setOnClickListener {


            showProgress()
            viewModel.getDefaultCard()
        }
        binding.btnCancelSubscribtion.setOnClickListener {
            showMessageWithTwoButton(
                message = "Do you want to Cancel subscription?",
                buttonOneText = "Yes",
                buttonTwoText = "No",
                callback = object : DialogUtil.MultiButtonCallBack {
                    override fun buttonOneClicked() {
                        super.buttonOneClicked()

                        showProgress()
                        viewModel.cancelSubscription()
                    }

                    override fun buttonTwoClicked() {
                        super.buttonTwoClicked()
                    }
                }
            )
        }
    }


}