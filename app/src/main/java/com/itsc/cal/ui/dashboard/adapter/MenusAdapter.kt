package com.itsc.cal.ui.dashboard.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.DashboardMenuItemBinding
import com.itsc.cal.enums.Menus
import com.itsc.cal.model.DashboardGridItem

class MenusAdapter(
    private val list: ArrayList<DashboardGridItem>,
    private val context: Context,
    private val clicked: (dashboardGridItem: DashboardGridItem) -> Unit
) :
    RecyclerView.Adapter<MenusAdapter.Holder>() {

    private var alertCount = 0

    fun setAlertCount(count: Int) {
        alertCount = count
        notifyDataSetChanged()
    }

    inner class Holder(private val binding: DashboardMenuItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: DashboardGridItem) {
            binding.top.visibility = View.GONE
            binding.right.visibility = View.GONE

            if (adapterPosition != 0 && adapterPosition != 1) {
                binding.top.visibility = View.VISIBLE
            }

            if (adapterPosition % 2 == 0) {
                if (adapterPosition != list.size - 1)
                    binding.right.visibility = View.VISIBLE
            }

            binding.logo.setImageDrawable(
                ResourcesCompat.getDrawable(
                    context.resources,
                    item.image,
                    null
                )
            )
            binding.textViewMenuName.text = item.name

            binding.container.setOnClickListener {
                clicked(item)
            }

            binding.textViewCount.visibility = View.GONE
            if (item.menu == Menus.ALERTS && alertCount != 0) {
                binding.textViewCount.visibility = View.VISIBLE
                binding.textViewCount.text = alertCount.toString()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            DashboardMenuItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }
}