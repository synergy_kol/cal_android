package  com.itsc.cal.ui.login

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.UserDetails
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataLogin: MediatorLiveData<Event<UserDetails>> = MediatorLiveData()
    val mediatorLiveDataLoginError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDataLogin.addSource(
            repository.mutableLiveDataLogin
        ) { t -> mediatorLiveDataLogin.postValue(t) }
        mediatorLiveDataLoginError.addSource(
            repository.mutableLiveDataLoginError
        ) { t -> mediatorLiveDataLoginError.postValue(t) }

    }

    fun login(email: String, password: String, deviceToken: String,lat:String,lng:String) =
        repository.login(email = email, passowrd = password, deviceToken = deviceToken,lat = lat,long = lng)

}
