package com.itsc.cal.ui.track_status

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityTrackStatusBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse
import com.itsc.cal.model.TruckStatusResponse
import com.itsc.cal.ui.track_status.adapter.TrackStatusAdapter
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class TrackStatusActivity : CalSuperActivity(), KodeinAware {

    private lateinit var binding: ActivityTrackStatusBinding
    override val kodein: Kodein by kodein()
    private lateinit var viewModel: ViewModel
    private lateinit var jobListResponse: JobListResponse
    private var isFromJobListing = false
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var adapter: TrackStatusAdapter
    var checkInitAdapter = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_track_status)
        enableBackButton()
        setPageTitle("Truck Status")
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        showTrackStatusActionbar()
        initView()
        bindViewHolder()
    }

    private fun initView() {
        intent.let {
            isFromJobListing = intent.getBooleanExtra(KeywordsAndConstants.isJobListing, false)
            if (isFromJobListing) {
                jobListResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
                getTrackListing(jobId = jobListResponse.job_id, userId = "")
            } else {
                getTrackListing(
                    jobId = "",
                    userId = viewModel.giveRepository().getUserData()!!.user_master_id
                )
            }
        }
        binding.textSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (checkInitAdapter) {
                        adapter.getFilter().filter(s.toString())
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })


    }

    private fun getTrackListing(jobId: String, userId: String) {
        showProgress()
        viewModel.getTruckStatus(job_id = jobId, builder_id = userId)
    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveTruckStatusList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<TruckStatusResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent() ?: ArrayList())

                    }
                }
            }
        )

        viewModel.mediatorLiveTruckStatusListError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        //  handleGenericResult(it.getContent()!!)
                        it.readContent()
                        binding.textNoDataFound.visibility = View.VISIBLE
                    }

                }
            }
        )
    }

    private fun setAdapter(arrayList: ArrayList<TruckStatusResponse>) {
        if (arrayList.size > 0) {
            binding.textNoDataFound.visibility = View.GONE

            checkInitAdapter = true
            binding.recycleviewTrackStatus.layoutManager = LinearLayoutManager(this)
            adapter = TrackStatusAdapter(
                this@TrackStatusActivity,
                arrayList,
                object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                    }

                })
            binding.recycleviewTrackStatus.adapter = adapter

            adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onChanged() {
                    super.onChanged()
                    checkEmpty()
                }

                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    super.onItemRangeInserted(positionStart, itemCount)
                    checkEmpty()
                }

                override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                    super.onItemRangeRemoved(positionStart, itemCount)
                    checkEmpty()
                }

                fun checkEmpty() {

                    binding.textNoDataFound.visibility =
                        (if (adapter.itemCount == 0) View.VISIBLE else View.GONE)
                }
            })
        } else {

            binding.textNoDataFound.visibility = View.VISIBLE

        }


    }
}