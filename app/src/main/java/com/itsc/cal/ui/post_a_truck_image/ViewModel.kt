package  com.itsc.cal.ui.post_a_truck_image

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLivePostATruck: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLivePostATruckError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLivePostATruck.addSource(
            repository.mutableLiveDataPostATruck
        ) { t -> mediatorLivePostATruck.postValue(t) }
        mediatorLivePostATruckError.addSource(
            repository.mutableLiveDataPostATruckError
        ) { t -> mediatorLivePostATruckError.postValue(t) }


    }

    fun addTruck(postATruckRequest: PostATruckRequest) = repository.addTruck(postATruckRequest)


}