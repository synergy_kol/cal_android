package com.itsc.cal.ui.signup

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.core.KeywordsAndConstants.DATE_FORMAT
import com.itsc.cal.databinding.ActivityCompleteYourProfileBinding
import com.itsc.cal.model.*
import com.itsc.cal.ui.paymentsettings.PayoutSettingsRegistrationActivity
import com.itsc.cal.ui.sos_document_submited.SOSDocumentSubmitedActivity
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class CompleteProfileActivity:CalSuperActivity() {
    private lateinit var binding: ActivityCompleteYourProfileBinding
    override val kodein: Kodein by kodein()
    private lateinit var viewModel: ViewModel
    private val viewModelProvider: ViewModelProvider by instance()
    private var stateId: String = ""
    private var cityId: String = ""
    private var userDetails: UserDetails? = null
    private var profileResponse:ProfileResponse?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_complete_your_profile)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        hideBackButton()
        showHelpButton()
        //setPageTitle("Complete Profile Information")
        setTitle()
        initView()
    }
    private fun setTitle(){
        intent.also {
            if(it?.getStringExtra("from").equals("edit")){
                setPageTitle("Edit Profile")
                profileResponse = fromJson(it.getStringExtra(KeywordsAndConstants.DATA)!!)
                enableBackButton()
                setUI()
            }else{
                setPageTitle("Complete Profile Information")
                hideBackButton()
            }
        }
    }


    private fun initView(){
        binding.apply {
            editCompanyName.apply {
                hint("Enter Company Name")
                inputMode(EditTextInputMode.INPUT_TEXT)
            }
            editCompanyDba.apply {
                hint("Enter Company DBA")
                inputMode(EditTextInputMode.INPUT_TEXT)
            }
            editCompanyEin.apply {
                hint("Enter Company EIN")
                inputMode(EditTextInputMode.INPUT_TEXT)
            }

            editSecondPhone.apply {
                hint("Enter your Mobile Number")
                inputMode(EditTextInputMode.NUMBER)
            }
            editSecondEmail.apply {
                hint("Enter your Email")
                inputMode(EditTextInputMode.EMAIL)
            }
            editZipCode.apply {
                hint("Enter your Zip Code")
                inputMode(EditTextInputMode.NUMBER)
            }
            editZipCode2.apply {
                hint("Enter your Zip Code")
                inputMode(EditTextInputMode.NUMBER)
            }
            editStreetAddress.apply {
                hint("Enter your Street Address")
                inputMode(EditTextInputMode.INPUT_TEXT)
            }
            editStreetAddress2.apply {
                hint("Enter your Street Address")
                inputMode(EditTextInputMode.INPUT_TEXT)
            }
        tvDateOfBirth.setOnClickListener {
            showDateOfBirthPicker()
          }
        tvDrivingLicExp.setOnClickListener {
            showExpiryPicker()
         }
            llIsBillAddress.setOnClickListener {
                cbBillToAddress.isChecked = !cbBillToAddress.isChecked
                onCheckBoxClicked()
            }
            cbBillToAddress.setOnClickListener {
                onCheckBoxClicked()
            }
            buttonSignup.setOnClickListener {
                makeAPICall()
            }
            if(profileResponse==null) {
                userDetails = viewModel.giveRepository().getUserData()
                spinnerState.setText(userDetails?.state_name ?: "")
                spinnerCity.setText(userDetails?.city_name ?: "")
                stateId = userDetails?.state ?: ""
                cityId = userDetails?.city ?: ""
            }

        }


        bindViewModel()
        stateList()
    }

    private fun setUI(){
        val completeProfileData = profileResponse?.complete_profile_info?:return
        val billingInfo = profileResponse?.billing_info
        binding.apply {
            editCompanyName.setText(completeProfileData.company_legal_name?:"")
            editCompanyDba.setText(completeProfileData.company_dba?:"")
            editCompanyEin.setText(completeProfileData.company_ein?:"")
            editSecondPhone.setText(completeProfileData.phone_2?:"")
            editSecondEmail.setText(completeProfileData.email_2?:"")
            editStreetAddress.setText(completeProfileData.street_address?:"")
            editZipCode.setText(completeProfileData.zip?:"")
            editDriverLicNum.setText(completeProfileData.driver_license?:"")
            editStreetAddress2.setText(billingInfo?.street_address?:"")
            editZipCode2.setText(billingInfo?.zip?:"")
            stateId = billingInfo?.state?:""
            cityId = billingInfo?.city?:""
            spinnerCity.setText(billingInfo?.city_name)
            spinnerState.setText(billingInfo?.state_name)
            tvDrivingLicExp.text = AndroidUtility.formatDateFromString("yyyy-MM-dd",DATE_FORMAT,completeProfileData.driver_license_exp?:"")
            tvDateOfBirth.text = AndroidUtility.formatDateFromString("yyyy-MM-dd",DATE_FORMAT,completeProfileData.dob?:"")
            cbBillToAddress.isChecked = completeProfileData.is_billing_address=="1"
        }
    }

    private fun onCheckBoxClicked(){
        binding.apply {

        if(cbBillToAddress.isChecked &&
            editStreetAddress.getText().trim()!="" &&
            editZipCode.getText().trim()!=""){
            editStreetAddress2.setText(editStreetAddress.getText().trim())
            editZipCode2.setText(editZipCode.getText().trim())
            setAddressEnabledDisable(false)
        }else{
            setAddressEnabledDisable(true)
        }
        }
    }

    private fun setAddressEnabledDisable(isEnabled:Boolean){
        binding.apply {
        editStreetAddress2.editable(isEnabled)
            editZipCode2.editable(isEnabled)
            if(isEnabled){
                editStreetAddress2.setText("")
                editZipCode2.setText("")
            }
        }
    }


    private fun stateList() {
        showProgress()
        viewModel.getState()
    }

    private fun cityList(stateId: String) {
        viewModel.getCity(stateId)
    }

    private fun bindViewModel(){
        viewModel.mediatorLiveDataCompleteInfo.observe(
            this,
            androidx.lifecycle.Observer<Event<UserDetails>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processSignUpResponse(it.getContent()!!)

                    }
                }
            }
        )

        viewModel.mediatorLiveDataCompleteInfoError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                    }
                }
            }
        )
        viewModel.mediatorLiveStateList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<StateResponce>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        var mStateList = it.getContent() ?: ArrayList()
                        if (mStateList.size  > 0) {
                            binding.spinnerState.setItems(mStateList as List<StateResponce>)
                            binding.spinnerState.setOnItemSelectedListener {
                                stateId = it.id
                                cityList(stateId)
                                binding.spinnerCity.setText("")
                            }

                        }
                    }
                }
            }
        )

        viewModel.mediatorLiveCityList.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<CityResponce>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val mCityList = it.getContent() ?: ArrayList()
                        if (mCityList.size ?: 0 > 0) {
                            binding.spinnerCity.setItems(mCityList as List<CityResponce>)
                            binding.spinnerCity.setOnItemSelectedListener {
                                cityId = it.id
                            }
                        }
                    }
                }
            }
        )
    }

    fun showDateOfBirthPicker() {
        AndroidUtility.showDatePickerMax(
            supportFragmentManager,
            com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val selectedDate =
                    dayOfMonth.toString() + "/" + (monthOfYear + 1).toString() + "/" + year
                val fromDate = AndroidUtility.formatDateFromString(
                    "dd/MM/yyyy",
                    "dd/MM/yyyy",
                    selectedDate
                )
                binding.tvDateOfBirth.text = fromDate
            })
    }

    fun showExpiryPicker() {
        AndroidUtility.showDatePickerConext(
            supportFragmentManager,
            com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val selectedDate =
                    dayOfMonth.toString() + "/" + (monthOfYear + 1).toString() + "/" + year
                val fromDate = AndroidUtility.formatDateFromString(
                    "dd/MM/yyyy",
                    "dd/MM/yyyy",
                    selectedDate
                )
                binding.tvDrivingLicExp.text = fromDate
            })
    }

    private fun makeAPICall(){
        val dob = binding.tvDateOfBirth.text.toString().trim()
        val companyName = binding.editCompanyName.getText().trim()
        val companyDba = binding.editCompanyDba.getText().trim()
        val companyEin = binding.editCompanyEin.getText().trim()
        val phone = binding.editSecondPhone.getText().trim()
        val email = binding.editSecondEmail.getText().trim()
        val driveLicNum = binding.editDriverLicNum.getText().trim()
        val driveLicExp = binding.tvDrivingLicExp.text.toString().trim()
        val streetAddress = binding.editStreetAddress.getText().trim()
        val zipCode = binding.editZipCode.getText().trim()
        val isbillAddressSame = if(binding.cbBillToAddress.isChecked) "1" else  "0"
        val streetAddress2 =  binding.editStreetAddress2.getText().trim()
        val zipCode2 = binding.editZipCode2.getText().trim()

        if (dob.isEmpty()) {
            showMessageInDialog("Please provide Date of Birth.")
            return
        }
        if (companyName.isEmpty()) {
            showMessageInDialog("Please provide Company Leagal Name")
            return
        }
        if (phone.isEmpty()) {
            showMessageInDialog("Please provide phone number")
            return
        }
        if (!AndroidUtility.isValidMobile(phone)) {
            showMessageInDialog(resources.getString(R.string.mobile_no_validration_message))
            return
        }
        if (email.isEmpty()) {
            showMessageInDialog("Please provide email")
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            showMessageInDialog("Please provide valid email.")
            return
        }
        if (streetAddress.isEmpty()) {
            showMessageInDialog("Please provide your street address.")
            return
        }
        if (zipCode.isEmpty()) {
            showMessageInDialog("Please provide your zip code.")
            return
        }

        if (isbillAddressSame=="0" && streetAddress2.isEmpty()) {
            showMessageInDialog("Please provide your street address.")
            return
        }
        if (isbillAddressSame=="0" && zipCode2.isEmpty()) {
            showMessageInDialog("Please provide your zip code.")
            return
        }
        showProgress()
        viewModel.completeInfo(
            dob,companyName,companyDba,phone,email, companyEin,driveLicNum,driveLicExp,
            streetAddress, zipCode, isbillAddressSame,streetAddress2, cityId, stateId, zipCode2
        )

    }
    private fun processSignUpResponse(userDetails: UserDetails) {
        hideProgress()

        intent.also {
            if (it?.getStringExtra("from").equals("edit")) {
                showMessageWithOneButton("Profile info updated successfully.", object:DialogUtil.CallBack{
                    override fun buttonClicked() {
                        super.buttonClicked()
                        val returnIntent = Intent()
                        setResult(RESULT_OK, returnIntent)
                        finish()
                    }
                }, false)

            }else{
                viewModel.giveRepository().saveUserDataToPref(userDetails)
                if(userDetails.user_role.trim().equals("4",true)) {
                    startActivity(
                        Intent(this, SOSDocumentSubmitedActivity::class.java)
                    )
                    finish()
                }else{
                    startActivity(UiUtil.clearStackAndStartNewActivity(Intent(this, PayoutSettingsRegistrationActivity::class.java)))
                    finish()
                }
            }
        }


    }

}