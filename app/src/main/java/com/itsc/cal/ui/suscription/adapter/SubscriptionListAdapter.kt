package com.itsc.cal.ui.suscription.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.databinding.ChildSubscriptionListBinding
import com.itsc.cal.databinding.ChildVehicleTypeListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.SubscriptionListResponce
import com.itsc.cal.util.AndroidUtility

class SubscriptionListAdapter(
    val mContext: Context,
    val arrayList: ArrayList<SubscriptionListResponce>,
    val onItemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<SubscriptionListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ChildSubscriptionListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))
    }

    inner class ViewHolder(val binding: ChildSubscriptionListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(subscriptionListResponce: SubscriptionListResponce) {
            binding.textContact.text = subscriptionListResponce.title
            binding.textDescription.text =
                "Amount:$ ${subscriptionListResponce.amount} + Sales Tax :$ ${subscriptionListResponce.sale_tax}"
            binding.textPrice.text = "$ ${subscriptionListResponce.tax_amount}"
            binding.llWeekly.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
            if (subscriptionListResponce.user_taken_status.equals("1")) {
                binding.btnSubscription.text = "Subscribe"
                binding.btnSubscription.setBackgroundResource(R.drawable.button_bg_green_small_corner)

                val fromDate = AndroidUtility.formatDateFromString(
                    "yyyy-MM-dd hh:mm:ss",
                    "dd MMM yyyy",
                    subscriptionListResponce.details.subscription_date?:""
                )

                val toDate = AndroidUtility.formatDateFromString(
                    "yyyy-MM-dd hh:mm:ss",
                    "dd MMM yyyy",
                    subscriptionListResponce.details.subscription_end_date?:""
                )
                binding.textSubscrbdetails.text =  "${fromDate} to ${toDate}"
                binding.textSubscrbdetails.visibility = View.VISIBLE
            } else {
                binding.btnSubscription.text = "Pay Now"
                binding.btnSubscription.setBackgroundResource(R.drawable.button_bg_blue_small_corner)
                binding.textSubscrbdetails.text = ""
                binding.textSubscrbdetails.visibility = View.GONE
            }
            binding.btnSubscription.setOnClickListener {
                onItemClickListener.onItemClick(it, adapterPosition)
            }
        }

    }

}