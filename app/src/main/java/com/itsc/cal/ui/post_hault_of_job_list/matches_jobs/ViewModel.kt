package  com.itsc.cal.ui.post_hault_of_job_list.matches_jobs

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveImportRequestData: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveImportRequestDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveExportRequestData: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveExportRequestDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveRequestAcceptedByExproterData: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveRequestAcceptedByExproterError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveRequestRejectData: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveRequestRejectError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveImportRequestData.addSource(
            repository.mutableLiveDataImportRequestData
        ) { t -> mediatorLiveImportRequestData.postValue(t) }
        mediatorLiveImportRequestDataError.addSource(
            repository.mutableLiveDataImportRequestError
        ) { t -> mediatorLiveImportRequestDataError.postValue(t) }


        mediatorLiveExportRequestData.addSource(
            repository.mutableLiveDataExportRequestData
        ) { t -> mediatorLiveExportRequestData.postValue(t) }
        mediatorLiveExportRequestDataError.addSource(
            repository.mutableLiveDataExportRequestError
        ) { t -> mediatorLiveExportRequestDataError.postValue(t) }

        mediatorLiveRequestAcceptedByExproterData.addSource(
            repository.mutableLiveDataRequestAcceptedByExpoterData
        ) { t -> mediatorLiveRequestAcceptedByExproterData.postValue(t) }
        mediatorLiveRequestAcceptedByExproterError.addSource(
            repository.mutableLiveDataRequestAcceptedByExpoterError
        ) { t -> mediatorLiveRequestAcceptedByExproterError.postValue(t) }

        mediatorLiveRequestRejectData.addSource(
            repository.mutableLiveDataRequestRejectData
        ) { t -> mediatorLiveRequestRejectData.postValue(t) }
        mediatorLiveRequestRejectError.addSource(
            repository.mutableLiveDataRequestRejectError
        ) { t -> mediatorLiveRequestRejectError.postValue(t) }


    }


    fun sendImportRequest(importer_request_id: String,exporter_request_id:String ) =
        repository.sendImportRequest(importer_request_id = importer_request_id,exporter_request_id = exporter_request_id)


    fun sendExportRequest(importer_request_id: String,exporter_request_id:String ) =
        repository.sendExportRequest(importer_request_id = importer_request_id,exporter_request_id = exporter_request_id)

    fun requestAcceptedByExpoter(mapId:String){
        repository.requestAcceptedByExporter(mapId)
    }

    fun requestReject(mapId:String){
        repository.requestCancel(mapId)
    }

}