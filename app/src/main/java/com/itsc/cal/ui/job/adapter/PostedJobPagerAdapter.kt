package com.itsc.cal.ui.job.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.itsc.cal.ui.job.fragment.PostedJobFragment

class PostedJobPagerAdapter(val fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> {
                fragment =  PostedJobFragment.newInstance("1")
            }
            1 -> {
                fragment = PostedJobFragment.newInstance("2")
            }
            2 -> {
                fragment = PostedJobFragment.newInstance("3")
            }
            3 -> {
                fragment = PostedJobFragment.newInstance("4")
            }
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title = ""
        when (position) {
            0 -> {
                title = "upcoming"
            }
            1 -> {
                title = "assigned"
            }
            2 -> {
                title = "past"
            }
            3 -> {
                title = "cancelled"
            }
        }
        return title
    }
}