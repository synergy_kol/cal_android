package com.itsc.cal.ui.manage_builders

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.fxn.pix.Pix
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityManageBuildersBinding
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import permissions.dispatcher.*
import java.io.File

@RuntimePermissions
class ManageBuildersActivity : CalSuperActivity(), KodeinAware {
    override val kodein: Kodein by kodein()
    private var userLatLng: LatLng? = null
    private var userLatLngDestination: LatLng? = null
    private var mLocationPermissionGranted = false
    private var latitude: String = ""
    private var longitude: String = ""
    private val repository: Repository by instance()
    private lateinit var mPlacesClient: PlacesClient
    private lateinit var binding: ActivityManageBuildersBinding
    private val AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS: Int = 1
    private var imageFilePath: File? = null
    private var image: String = ""
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_manage_builders)
        enableBackButton()
        setPageTitle("Add Builder")
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        getLocationPermission()
        initView()
        showHelpButton()
        bindViewHolder()
    }

    private fun bindViewHolder() {

        viewModel.mediatorLiveAddBuilder.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveAddBuilderError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )



    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    finish()

                }
            }
        )

    }

    private fun initView() {

        binding.editAddress.setOnClickListener {
            openPlacePicker()
        }

        binding.edEmailAddress.apply {
            hint("Enter Email Address")
            inputMode(EditTextInputMode.EMAIL)
        }

        binding.edFname.apply {
            hint("Enter First Name")
            inputMode(EditTextInputMode.EMAIL)
        }
        binding.edLastName.apply {
            hint("Enter Last Name")
            inputMode(EditTextInputMode.EMAIL)
        }

        binding.edPhoneNumber.apply {
            hint("Enter Phone Number")
            inputMode(EditTextInputMode.NUMBER)
        }

        binding.edCompanyName.apply {
            hint("Enter Company Name")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }
        binding.imgBuilderImage.visibility = View.GONE
        binding.textAddFile.setOnClickListener {
            initiateCaptureForProfilePicture()
        }
        binding.buttonSubmit.setOnClickListener {
            apiCalled()
        }


    }

    private fun getLocationPermission() {
        mLocationPermissionGranted = false
        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mLocationPermissionGranted = true

        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                KeywordsAndConstants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun openPlacePicker() {
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)

        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.FULLSCREEN, fields
        ).build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE_SOURCE_ADDRESS) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val place = Autocomplete.getPlaceFromIntent(data)
                setAddress(place)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR && data != null) {
                // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(data)
                Log.i("Status", status.statusMessage?:"")
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        } else if (requestCode == KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            data?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    //Log.i("got image $images")
                    binding.imgBuilderImage.visibility = View.VISIBLE
                    imageFilePath = File(images[0])
                    binding.imgBuilderImage.setImage(
                        File(images[0]),
                        isCircularImage = true,
                        needBorderWithCircularImage = true
                    )
                    binding.textAddFile.visibility = View.GONE
                }
            }
        }
    }

    private fun setAddress(place: Place) {
        userLatLng = place.latLng
        binding.editAddress.setText(place.address ?: "")
        latitude = userLatLng?.latitude.toString()
        longitude = userLatLng?.longitude.toString()
    }

    @NeedsPermission(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun takeProfilePicture() {
        ImagePickerUtil.pickImage(
            context = this,
            reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
        )
    }

    @OnShowRationale(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun showRationale(request: PermissionRequest) {
        showMessageWithTwoButton(
            message = "We need some permission to take pictures.",
            buttonOneText = "Okay",
            buttonTwoText = "Cancel",
            callback = object : DialogUtil.MultiButtonCallBack {
                override fun buttonOneClicked() {
                    super.buttonOneClicked()
                    request.proceed()
                }

                override fun buttonTwoClicked() {
                    super.buttonTwoClicked()
                    request.cancel()
                    finish()
                }
            }
        )
    }

    @OnPermissionDenied(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun permissionDenied() {
        showMessageWithOneButton(
            "You will not be able to take picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    @OnNeverAskAgain(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun neverAskAgain() {
        showMessageWithOneButton(
            "You will not be able to take picture picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    fun initiateCaptureForProfilePicture() {
        takeProfilePictureWithPermissionCheck()
    }

    private fun apiCalled() {

        if (
            binding.edFname.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide First Name.")
            return
        }

        if (
            binding.edLastName.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Last Name.")
            return
        }

        if (binding.edCompanyName.getText().toString().isEmpty()) {
            showMessageInDialog("Please provide Company Name.")
            return
        }
        if (
            binding.edEmailAddress.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Email Address.")
            return
        }
        if (
            !Patterns.EMAIL_ADDRESS.matcher(
                binding.edEmailAddress.getText().toString()
            ).matches()
        ) {
            showMessageInDialog("Please provide valid email.")
            return
        }
        if (
            binding.edPhoneNumber.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Phone Number.")
            return
        }

        if (!AndroidUtility.isValidMobile(binding.edPhoneNumber.getText().toString())) {
            showMessageInDialog(resources.getString(R.string.mobile_no_validration_message_phone_number))
            return
        }

        if (
            binding.editAddress.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Address.")
            return
        }

        if (imageFilePath == null) {
            showMessageInDialog("Please provide an Image.")
            return
        }
        showProgress()
        viewModel.addBuilder(
            first_name = binding.edFname.getText().toString().trim(),
            imageFile = imageFilePath,
            latitute = userLatLng?.latitude.toString(),
            longitute = userLatLng?.longitude.toString(),
            address = binding.editAddress.text.toString(),
            phone = binding.edPhoneNumber.getText().toString(),
            email = binding.edEmailAddress.getText().toString(),
            company_name = binding.edCompanyName.getText().toString(),
            last_name = binding.edLastName.getText().toString().trim()
        )

    }


}