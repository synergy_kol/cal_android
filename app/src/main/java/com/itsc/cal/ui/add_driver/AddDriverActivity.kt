package com.itsc.cal.ui.add_driver

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import androidx.lifecycle.Observer
import com.fxn.pix.Pix
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityAddDriverBinding
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.webview.WebView
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import permissions.dispatcher.*
import java.io.File

@RuntimePermissions
class AddDriverActivity : CalSuperActivity(withNav = true), KodeinAware {
    private lateinit var binding: ActivityAddDriverBinding
    override val kodein: Kodein by kodein()
    private var imageFilePathDriverLicense: File? = null
    private var imageFilePathDriver: File? = null
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var view: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_add_driver)
        enableBackButton()
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        setPageTitle("Add Driver")
        initView()
        bindingViewModel()
    }

    private fun bindingViewModel() {
        viewModel.mediatorLiveAddDriver.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveAddDriverError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    finish()

                }
            }
        )

    }

    private fun initView() {
        binding.edFname.apply {
            hint("Enter driver name")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }
        binding.edMobileNo.apply {
            hint("Enter mobile number")
            inputMode(EditTextInputMode.NUMBER)
        }

        binding.edEmail.apply {
            hint("Enter driver email")
            inputMode(EditTextInputMode.EMAIL)
        }

        binding.edLicenceNumber.apply {
            hint("DLUSA1234599")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }
        binding.textDriverLicensePhoto.setOnClickListener {
            view = 1
            initiateCaptureForProfilePicture()
        }
        binding.textDriverPhoto.setOnClickListener {
            view = 2
            initiateCaptureForProfilePicture()
        }

        binding.imgDriverPhoto.registerForOnClick {
            view = 2
            initiateCaptureForProfilePicture()
        }

        binding.imgDriverLicensePhoto.registerForOnClick {
            view = 1
            initiateCaptureForProfilePicture()
        }


        binding.textAck.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    WebView::class.java
                ).putExtra(
                    WebView.URL,
                    "${KeywordsAndConstants.BASE_URL}home/cms/ACK"
                ).putExtra(
                    WebView.TITLE,
                    "Acknowledgement"
                )
            )
        }
        binding.buttonSave.setOnClickListener {
            apicalled()
        }
    }

    private fun apicalled() {

        if (binding.edFname.getText().toString().isEmpty()) {
            showMessageInDialog("Please provide Driver Full Name .")
            return
        }

        if (
            binding.edEmail.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide Driver Email.")
            return
        }
        if (
            !Patterns.EMAIL_ADDRESS.matcher(
                binding.edEmail.getText().toString()
            ).matches()
        ) {
            showMessageInDialog("Please provide valid email.")
            return
        }

        if (binding.edMobileNo.getText().toString().isEmpty()) {
            showMessageInDialog("Please provide Driver Mobile Number.")
            return
        }

        if (!AndroidUtility.isValidMobile(binding.edMobileNo.getText().toString())) {
            showMessageInDialog(resources.getString(R.string.mobile_no_validration_message))
            return
        }

        if (binding.edLicenceNumber.getText().toString().isEmpty()) {
            showMessageInDialog("Please provide Driver License Number.")
            return
        }

        if (imageFilePathDriverLicense == null) {
            showMessageInDialog("Please provide Driver License Photo.")
            return
        }

        if (imageFilePathDriver == null) {
            showMessageInDialog("Please provide Driver Photo.")
            return
        }
        if (!binding.cbFleetOwner.isChecked){
            showMessageInDialog("Please accept the Acknowledgment.")
            return
        }
        showProgress()
        viewModel.addDriver(
            driverName = binding.edFname.getText().toString(),
            diverPhoto = imageFilePathDriver,
            driverLicenseFile = imageFilePathDriverLicense,
            diverLicense = binding.edLicenceNumber.getText().toString(),
            driverNumber = binding.edMobileNo.getText().toString(),
            email = binding.edEmail.getText().toString()
        )
    }

    fun initiateCaptureForProfilePicture() {
        takeProfilePictureWithPermissionCheck()
    }

    @NeedsPermission(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun takeProfilePicture() {
        ImagePickerUtil.pickImage(
            context = this,
            reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
        )
    }

    @OnShowRationale(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun showRationale(request: PermissionRequest) {
        showMessageWithTwoButton(
            message = "We need some permission to take pictures.",
            buttonOneText = "Okay",
            buttonTwoText = "Cancel",
            callback = object : DialogUtil.MultiButtonCallBack {
                override fun buttonOneClicked() {
                    super.buttonOneClicked()
                    request.proceed()
                }

                override fun buttonTwoClicked() {
                    super.buttonTwoClicked()
                    request.cancel()
                    finish()
                }
            }
        )
    }

    @OnPermissionDenied(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun permissionDenied() {
        showMessageWithOneButton(
            "You will not be able to take picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    @OnNeverAskAgain(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun neverAskAgain() {
        showMessageWithOneButton(
            "You will not be able to take picture picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)
        if (requestCode == KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            intentData?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    //Log.i("got image $images")
                    when (view) {
                        1 -> {
                            binding.imgDriverLicensePhoto.visibility = View.VISIBLE
                            binding.textDriverLicensePhoto.visibility = View.GONE
                            binding.ivDriverLicensePhoto.visibility = View.VISIBLE
                            imageFilePathDriverLicense = File(images[0])
                            binding.imgDriverLicensePhoto.setImage(
                                File(images[0]),
                                isCircularImage = false,
                                needBorderWithCircularImage = true
                            )
                        }
                        2 -> {
                            binding.imgDriverPhoto.visibility = View.VISIBLE
                            binding.textDriverPhoto.visibility = View.GONE
                            binding.ivDriverPhoto.visibility = View.VISIBLE
                            imageFilePathDriver = File(images[0])
                            binding.imgDriverPhoto.setImage(
                                File(images[0]),
                                isCircularImage = false,
                                needBorderWithCircularImage = true
                            )

                        }

                    }


                }
            }
        }
    }
}