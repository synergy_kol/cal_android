package com.itsc.cal.ui.near_by_contractor_map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.BitmapDrawable
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityNearByContractorMapBinding
import com.itsc.cal.databinding.CustomMarkerBinding
import com.itsc.cal.ui.near_by_contractor_list.NearByContractorList
import com.itsc.cal.util.WorkaroundMapFragment
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlin.random.Random


class NearByContractorMap : CalSuperActivity() {

    private lateinit var binding: ActivityNearByContractorMapBinding
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_near_by_contractor_map)
        binding.context = this

        showActionbarWithBackButtonAndCall()

        enableBackButton()

        setPageTitle("Maps View")

        binding.editTextSearch.apply {
            drawableStart(
                BitmapDrawable(
                    resources,
                    Bitmap.createScaledBitmap(
                        (ResourcesCompat.getDrawable(
                            resources,
                            R.drawable.magnify_icon,
                            null
                        ) as BitmapDrawable).bitmap,
                        24,
                        24,
                        true
                    )
                )
            )
            backgroundDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.edittext_bg_with_corner_radius,
                    null
                )!!
            )
            hint("All Contractors Near You")
        }

        getLastLocation()
    }

    @SuppressLint("CheckResult", "MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        RxPermissions(this)
            .request(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .subscribe {
                if (!it) {
                    finish()
                }

                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener { locationResult ->
                        locationResult?.let { location ->
                            startMap(location)
                        }
                    }
            }
    }

    @SuppressLint("MissingPermission")
    private fun startMap(currentLocation: Location) {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync { googleMap ->
            googleMap?.let {
                map = it

                val mSupportMapFragment: WorkaroundMapFragment =
                    mapFragment as WorkaroundMapFragment
                mSupportMapFragment.setListener(object :
                    WorkaroundMapFragment.OnTouchListener {
                    override fun onTouch() {
                        getContainer().requestDisallowInterceptTouchEvent(true);
                    }
                })

                setUpMarkersOnMap(currentLocation)
            }
        }
    }

    private fun setUpMarkersOnMap(currentLocation: Location) {
        val latLng1 = LatLng(22.572645, 88.363892)
        val latLng2 = LatLng(22.600699, 88.403125)
        val latLng3 = LatLng(22.520803, 88.444695)
        val latLng4 = LatLng(22.660274, 88.487973)
        val latLng5 = LatLng(22.511923, 88.265351)

        map.addMarker(
            MarkerOptions()
                .title("title one")
                .position(
                    latLng1
                )
                .icon(
                    BitmapDescriptorFactory.fromBitmap(
                        getMarkerBitmapFromView()
                    )
                )
        )

        map.addMarker(
            MarkerOptions()
                .title("title one")
                .position(
                    latLng2
                )
                .icon(
                    BitmapDescriptorFactory.fromBitmap(
                        getMarkerBitmapFromView()
                    )
                )
        )

        map.addMarker(
            MarkerOptions()
                .title("title one")
                .position(
                    latLng3
                )
                .icon(
                    BitmapDescriptorFactory.fromBitmap(
                        getMarkerBitmapFromView()
                    )
                )
        )

        map.addMarker(
            MarkerOptions()
                .title("title one")
                .position(
                    latLng4
                )
                .icon(
                    BitmapDescriptorFactory.fromBitmap(
                        getMarkerBitmapFromView()
                    )
                )
        )

        map.addMarker(
            MarkerOptions()
                .title("title one")
                .position(
                    latLng5
                )
                .icon(
                    BitmapDescriptorFactory.fromBitmap(
                        getMarkerBitmapFromView()
                    )
                )
        )

        map.animateCamera(
            CameraUpdateFactory.newLatLngBounds(
                LatLngBounds.builder()
                    .include(latLng1)
                    .include(latLng2)
                    .include(latLng3)
                    .include(latLng4)
                    .include(latLng5)
                    .build(),
                100
            )
        )
    }

    fun gotoListView() {
        startActivity(
            Intent(
                this,
                NearByContractorList::class.java
            )
        )
    }

    private fun getMarkerBitmapFromView(): Bitmap? {
        val customMarkerView =
            CustomMarkerBinding.inflate(
                LayoutInflater.from(
                    this
                )
            )
        if (Random.nextBoolean())
            customMarkerView.appcompatImageViewHeart.visibility = View.VISIBLE
        customMarkerView.root.measure(
            View.MeasureSpec.UNSPECIFIED,
            View.MeasureSpec.UNSPECIFIED
        )
        customMarkerView.root.layout(
            0,
            0,
            customMarkerView.root.measuredWidth,
            customMarkerView.root.measuredHeight
        )
        customMarkerView.root.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(
            customMarkerView.root.measuredWidth, customMarkerView.root.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        customMarkerView.root.background?.draw(canvas)
        customMarkerView.root.draw(canvas)
        return returnedBitmap
    }
}