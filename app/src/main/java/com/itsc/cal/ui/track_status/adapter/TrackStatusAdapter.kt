package com.itsc.cal.ui.track_status.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ChildTrackStatusBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.NearByConductorListResponse
import com.itsc.cal.model.TruckStatusResponse
import com.itsc.cal.ui.tracking_truck_live.TrackingTruckLive
import com.itsc.cal.ui.truck_booking.TruckBooking

class TrackStatusAdapter(
    val mContext: Context,
    var arrayList: ArrayList<TruckStatusResponse>,
    val onItemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<TrackStatusAdapter.ViewHolder>(), Filterable {
    var suggestions: ArrayList<TruckStatusResponse> = ArrayList()
    var tempList: ArrayList<TruckStatusResponse> =
        arrayList.clone() as ArrayList<TruckStatusResponse>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ChildTrackStatusBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))
    }

    inner class ViewHolder(val binding: ChildTrackStatusBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(truckStatusResponse: TruckStatusResponse) {
            binding.imgProfileImage.setImage(
                imageUrl = "${KeywordsAndConstants.BASE_URL}${truckStatusResponse.truck_image}",
                isCircularImage = true,
                needPlaceHolderImageForName = truckStatusResponse.truck_reg_number!!.substring(0, 1)
            )
            binding.textRegNo.text = truckStatusResponse.truck_reg_number
            binding.textTruckAddress.text = truckStatusResponse.Source
            if (truckStatusResponse.driver_job_status.equals("0")) {
                binding.textStatus.text = "Status: Not Started"
            } else if (truckStatusResponse.driver_job_status.equals("1")) {
                binding.textStatus.text = "Status: Started"
            } else if (truckStatusResponse.driver_job_status.equals("2")) {
                binding.textStatus.text = "Status: End"
            } else if (truckStatusResponse.driver_job_status.equals("3")) {
                binding.textStatus.text = "Status: Completed "
            }
            binding.container.setOnClickListener {
                mContext.startActivity(
                    Intent(
                        mContext,
                        TrackingTruckLive::class.java
                    )
                        .putExtra(
                            KeywordsAndConstants.DATA,
                            Gson().toJson(truckStatusResponse)
                        )
                        .putExtra(
                            TruckBooking.Job_ID,
                            truckStatusResponse.job_id
                        )
                )
            }
        }

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                p0?.let {
                    suggestions.clear()
                    for (filterSuggestion in tempList) {

                        if (filterSuggestion.truck_reg_number!!.contains(it, ignoreCase = true))
                            suggestions.add(filterSuggestion)

                    }

                    val result = FilterResults()
                    result.values = suggestions
                    result.count = suggestions.size
                    return result
                } ?: kotlin.run {
                    return FilterResults()
                }
            }

            override fun publishResults(p0: CharSequence?, filterResults: FilterResults?) {
                arrayList = filterResults?.values as ArrayList<TruckStatusResponse>
                notifyDataSetChanged()

            }

            override fun convertResultToString(resultValue: Any?): CharSequence {
                resultValue?.let {
                    val filterSuggestion = (it as NearByConductorListResponse)
                    return filterSuggestion.company_name?.trim().toString()
                    // return if (filterSuggestion.type == FilterSuggestionType.HOTEL) filterSuggestion.hotel!!.name else filterSuggestion.location!!.name
                } ?: run {
                    return ""
                }
            }
        }
    }

}