package  com.itsc.cal.ui.upload_sos_id_proof

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataSubmitSOS: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveDataSubmitSOSError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDataUpdateSOS: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveDataUpdateSOSError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDataSubmitSOS.addSource(
            repository.mutableLiveDataSubmitSOS
        ) { t -> mediatorLiveDataSubmitSOS.postValue(t) }
        mediatorLiveDataSubmitSOSError.addSource(
            repository.mutableLiveDataSubmitSOSError
        ) { t -> mediatorLiveDataSubmitSOSError.postValue(t) }

        mediatorLiveDataUpdateSOS.addSource(
            repository.mutableLiveDataUpdateSOS
        ) { t -> mediatorLiveDataUpdateSOS.postValue(t) }
        mediatorLiveDataUpdateSOSError.addSource(
            repository.mutableLiveDataUpdateSOSError
        ) { t -> mediatorLiveDataUpdateSOSError.postValue(t) }

    }

    fun submitSOS(imageFile: File?, sos_document_type: String) =
        repository.submitSOS(imageFile = imageFile, sos_document_type = sos_document_type)

    fun updateSOS(imageFile: File?, document_id: String) =
        repository.updateSOS(imageFile = imageFile, document_id = document_id)

}
