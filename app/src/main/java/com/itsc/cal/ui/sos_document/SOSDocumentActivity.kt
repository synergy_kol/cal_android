package com.itsc.cal.ui.sos_document

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivitySOSDocumentBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.ui.sos_document.adapter.SOSDocumentAdapter
import com.itsc.cal.ui.sos_document_submited.SOSDocumentSubmitedActivity

class SOSDocumentActivity : CalSuperActivity() {

    private lateinit var binding:ActivitySOSDocumentBinding
    private lateinit var arrayList:ArrayList<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_s_o_s_document)
        enableBackButton()
        showHelpButton()
        setPageTitle("Required Documents")
        initView()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
    private fun initView(){
        arrayList = ArrayList()
        arrayList.add("Valid Driver License / Front/Back")
        arrayList.add("Valid Medical Certificate")
        arrayList.add("USDOT Number")
        arrayList.add("Truck/Trailer Registration")
        arrayList.add("Insurance Document / Certificate of Insurance")
        arrayList.add("EIN")
        arrayList.add("1099")
       /* arrayList.add("DBE")
        arrayList.add("BUUB SBE WBE")*/
        binding.recycleViewSosDocument.apply {
            layoutManager = LinearLayoutManager(this@SOSDocumentActivity)
            adapter = SOSDocumentAdapter(this@SOSDocumentActivity,arrayList,object:OnItemClickListener{
                override fun onItemClick(view: View, position: Int) {
                    startActivity(Intent(this@SOSDocumentActivity,SOSDocumentSubmitedActivity::class.java))
                }

            })
        }
    }
}