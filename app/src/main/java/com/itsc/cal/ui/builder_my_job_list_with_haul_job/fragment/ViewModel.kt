package  com.itsc.cal.ui.builder_my_job_list_with_haul_job.fragment

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveJobListing: MediatorLiveData<Event<ArrayList<JobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveJobListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveJobListing.addSource(repository.mutableLiveDataBuilderJobListing) { t -> mediatorLiveJobListing.postValue(t) }
        mediatorLiveJobListingError.addSource(repository.mutableLiveDataBuilderJobListingError) { t -> mediatorLiveJobListingError.postValue(t) }


    }

    fun getJobList(filter:String) = repository.getJobBuilderListingNew(filter = filter)



}