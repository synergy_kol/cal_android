package  com.itsc.cal.ui.job.fragment

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveJobListing: MediatorLiveData<Event<ArrayList<JobListResponse>>> =
        MediatorLiveData()
    val mediatorLiveJobListingError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveJobListing.addSource(
            repository.mutableLiveDataJobListing
        ) { t -> mediatorLiveJobListing.postValue(t) }
        mediatorLiveJobListingError.addSource(
            repository.mutableLiveDataJobListingError
        ) { t -> mediatorLiveJobListingError.postValue(t) }


    }

    fun getJobList(filter:String) = repository.getJobListing(filter = filter)



}