package com.itsc.cal.ui.driver_details

import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityDriverDetailsBinding
import com.itsc.cal.model.DriverListResponse

class DriverDetailsActivity : CalSuperActivity(withNav = true) {
    private lateinit var binding: ActivityDriverDetailsBinding
    private lateinit var driverListResponse: DriverListResponse
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_driver_details)
        enableBackButton()
        setPageTitle("Driver Details")
        showHelpButton()
        initView()
    }

    private fun initView() {


        intent?.let {
            driverListResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))

            binding.textName.text = driverListResponse.driver_name
            binding.textEmail.text = driverListResponse.email
            binding.textLicenseNo.text=driverListResponse.driver_licence
            binding.textMobileNo.text = driverListResponse.driver_number
            binding.imgProfileImage.setImage(
                imageUrl = "${KeywordsAndConstants.BASE_URL}${driverListResponse.driverimage}",
                isCircularImage = true,
                needPlaceHolderImageForName = driverListResponse.driver_name.substring(0, 1)
            )

            binding.imgLicense.setImage(
                imageUrl = "${KeywordsAndConstants.BASE_URL}${driverListResponse.driverlicenceimage}",
                isCircularImage = false
            )

        }

        
    }
}