package com.itsc.cal.ui.async;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.itsc.cal.core.KeywordsAndConstants;
import com.itsc.cal.interfaces.AsyncInterface;
import com.itsc.cal.util.DistanceDurationParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class CreateRoute extends AsyncTask<String, String, String> {
    private static final String TAG = "CreateRoute";

    private AsyncInterface inter;
    private LatLng source, destination;
    private String response = "";
    private int responseCode = 0;


    private int routeColor = 0;

    public CreateRoute(Context locateMap, LatLng source, LatLng destination, int routeColor) {
        this.inter = (AsyncInterface) locateMap;
        this.source = source;
        this.destination = destination;
        this.routeColor = routeColor;
    }
	
	/*public CreateRoute(Fragment locateMap, LatLng source, OutLetModel outLetModel, int routeColor)
	{
		this.inter = (AsyncInterface) locateMap;
		this.source = source;
		this.outLetModel = outLetModel;
		this.routeColor = routeColor;
	}*/

    @Override
    protected String doInBackground(String... params) {
        try {
            try {
                response = downloadUrl(getUrl(source, destination));
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            Log.e(TAG, "response:" + response);

        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
            //e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            if (!response.equalsIgnoreCase("")) {
                DistanceDurationParser parser = new DistanceDurationParser(response);
                if (parser.getStatus().equalsIgnoreCase("OK")) {
                    new RouteDraw(inter, routeColor).execute(response);
                } else {
                    inter.viewNearestRouteErrorCallBack();
                }
            } else {
                inter.viewNearestRouteErrorCallBack();
            }


        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
            inter.viewNearestRouteErrorCallBack();
        }
    }


    private String getUrl(LatLng startLocation, LatLng destLocation) {
        String str_origin = "origin=" + startLocation.latitude + "," + startLocation.longitude;
        String str_dest = "destination=" + destLocation.latitude + "," + destLocation.longitude;
        String sensor = "sensor=false";
        String key = "key=AIzaSyCC-ntTbEoZM_NMpllqfj2PqtXBQT7cjcU";
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&mode=driving&transit_mode=car"+"&"+key;// +"&"+waypoints;
        String output = "json";
        String url = KeywordsAndConstants.GOOGLE_MAP_DIRECTION_API_BASE_URL + output + "?" + parameters;
        Log.d(TAG, "url " + url);
        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

}
