package com.itsc.cal.ui.job_list_truck_fleet.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.itsc.cal.ui.job_list_truck_fleet.fragment.PostedJobTruckListFragment

class PostedJobPagerTurckListAdapter(val fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> {
                fragment = PostedJobTruckListFragment.newInstance("1")
            }
            1 -> {
                fragment = PostedJobTruckListFragment.newInstance("2")
            }
            2 -> {
                fragment = PostedJobTruckListFragment.newInstance("3")
            }

        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title = ""
        when (position) {
            0 -> {
                title = "New job "
            }
            1 -> {
                title = "Accepted Job "
            }
            2 -> {
                title = "Hide job "
            }


        }
        return title
    }
}