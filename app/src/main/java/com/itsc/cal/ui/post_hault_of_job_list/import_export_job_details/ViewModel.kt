package  com.itsc.cal.ui.post_hault_of_job_list.import_export_job_details

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.HauljobListResponse
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveImportExportData: MediatorLiveData<Event<HauljobListResponse>> = MediatorLiveData()
    val mediatorLiveImportExportError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    init {
        mediatorLiveImportExportData.addSource(
            repository.mutableLiveDataImportExportDetails
        ) { t -> mediatorLiveImportExportData.postValue(t) }
        mediatorLiveImportExportError.addSource(
            repository.mutableLiveDataImportExportError
        ) { t -> mediatorLiveImportExportError.postValue(t) }

    }
    fun getJobDetails(jobId:String){
        repository.getImportExportJobDetails(jobId)
    }

}
