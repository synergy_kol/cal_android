package com.itsc.cal.ui.alert.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildAlertBinding
import com.itsc.cal.model.notificationList

class AlertAdapter (val mContext: Context,val notificationList:ArrayList<notificationList>):
    RecyclerView.Adapter<AlertAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ChildAlertBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(notificationList.get(position))
    }

    inner class ViewHolder(val binding: ChildAlertBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(notificationList:notificationList) {
            binding.textDateTime.text = notificationList.created_ts
            binding.textMessageDetails.text = notificationList.notification_description
            binding.textNotiTitle.text = notificationList.notification_title
            if (notificationList.alert_type.equals("1")){
                binding.cardBg.setBackgroundColor(Color.parseColor("#ff0000"))
            }else if (notificationList.alert_type.equals("2")){
                binding.cardBg.setBackgroundColor(Color.parseColor("#ffff00"))
            }else if (notificationList.alert_type.equals("3")){
                binding.cardBg.setBackgroundColor(Color.parseColor("#0074B7"));
            }
        }

    }

}