package com.itsc.cal.ui.paymentsettings

import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityPaySetCardBinding
import com.itsc.cal.model.PayoutSettingResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.util.AndroidUtility
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.whiteelephant.monthpicker.MonthPickerDialog
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class PayoutSetCardActivity:CalSuperActivity(), KodeinAware {
    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPaySetCardBinding
    private val today = Calendar.getInstance()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_pay_set_card)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        enableBackButton()
        showHelpButton()
        setPageTitle("ADD CARD")
        initView()
        bindingViewModel()
    }
    private fun getPaymentSettingResponse(): PayoutSettingResponse?{
        return intent.getParcelableExtra("payout_setting") as PayoutSettingResponse?
    }
    private fun initView() {
        val paySetting = getPaymentSettingResponse()
        if(paySetting!=null && paySetting.connect_type=="1"){
            binding.editTextCardNum.setText(paySetting.card_no?:"")
        }
        binding.editTextFirstName.apply {
            inputMode(EditTextInputMode.INPUT_TEXT)
            hint("First Name")
        }
        binding.editTextLastName.apply {
            inputMode(EditTextInputMode.INPUT_TEXT)
            hint("Last Name")
        }
        binding.editTextCardNum.apply {
            inputMode(EditTextInputMode.NUMBER)
            hint("Card no")
            setMaxLength(16)
        }

        binding.edCvv.apply {
            inputMode(EditTextInputMode.NUMERIC_PASSWORD)
            hint("CVV")
            setMaxLength(3)
        }
        binding.edExpMonth.setOnClickListener {

            val builder: MonthPickerDialog.Builder = MonthPickerDialog.Builder(
                this,
                { selectedMonth, selectedYear ->
                    val selectedMonth =
                        (selectedMonth + 1).toString() + "/" + selectedYear
                    val fromDate = AndroidUtility.formatDateFromString(
                        "MM/yyyy",
                        "MM",
                        selectedMonth
                    )
                    binding.edExpMonth.setText(fromDate)
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH)
            )
            builder.setActivatedMonth(today.get(Calendar.MONTH)+1)
                .setMinYear(Calendar.YEAR)
                .setActivatedYear(Calendar.YEAR)
                .setMaxYear(2030)
                .setMinMonth(Calendar.JANUARY)
                .setTitle("Exp Month").showMonthOnly()
                .setOnMonthChangedListener { month ->
                }.build().show()


        }
        binding.edExpYear.setOnClickListener {

            val builder: MonthPickerDialog.Builder = MonthPickerDialog.Builder(
                this,
                { selectedMonth, selectedYear ->
                    binding.edExpYear.setText((selectedYear.toString()))
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH)
            )
            builder.setActivatedYear(today.get(Calendar.YEAR))
                .setMinYear(today.get(Calendar.YEAR))
                .setActivatedYear(today.get(Calendar.YEAR))
                .setMaxYear(today.get(Calendar.YEAR)+10)
                .setTitle("Exp Year").showYearOnly()
                .setOnYearChangedListener { year ->
                }.build().show()

        }
        binding.buttonSave.setOnClickListener {
            ApiCalled()
        }

    }

    private fun bindingViewModel() {
        viewModel.mediatorLivePayoutAddCard.observe(
            this,
            androidx.lifecycle.Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLivePayoutAddCardError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Card added successfully",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    finish()

                }
            }
        )

    }

    private fun ApiCalled() {

        if (binding.editTextFirstName.getText().isEmpty()) {
            showMessageInDialog("Please provide first name of card holder.")
            return
        }
        if (binding.editTextLastName.getText().isEmpty()) {
            showMessageInDialog("Please provide last name of card holder.")
            return
        }

        if (binding.editTextCardNum.getText().isEmpty()) {
            showMessageInDialog("Please provide card no.")
            return
        }

        if (binding.editTextCardNum.getText().toString().length != 16) {
            showMessageInDialog("Please provide valid card no.")
            return
        }

        if (binding.edExpMonth.getText().toString().isEmpty()) {
            showMessageInDialog("Please provide exp month.")
            return
        }

        if (binding.edExpYear.getText().toString().isEmpty()) {
            showMessageInDialog("Please provide exp year.")
            return
        }

        if (binding.edCvv.getText().isEmpty()) {
            showMessageInDialog("Please provide cvv no.")
            return
        }

        if (binding.edCvv.getText().length != 3) {
            showMessageInDialog("Please provide valid cvv no.")
            return
        }
        showProgress()
        viewModel.payoutAddCard(
            cvv = binding.edCvv.getText().toString(),
            expiry_month = binding.edExpMonth.text.toString(),
            expiry_year = binding.edExpYear.text.toString(),
            number = binding.editTextCardNum.getText().toString()
        )
    }
}