package  com.itsc.cal.ui.post_hault_of_job_list.post_export_job

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveHaulJobExportStep1: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataJobExportStep1Error: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveHaulJobImportStep1: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveDataJobImportStep1Error: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveHaultCreateJobRequestData: MediatorLiveData<Event<CreateHaultOfRequest>> =
        MediatorLiveData()
    val mediatorLiveHaultCreateJobRequestError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {

        mediatorLiveHaulJobExportStep1.addSource(
            repository.mutableLiveDataJobExportStep1
        ) { t -> mediatorLiveHaulJobExportStep1.postValue(t) }
        mediatorLiveDataJobExportStep1Error.addSource(
            repository.mutableLiveDataJobExportStep1Error
        ) { t -> mediatorLiveDataJobExportStep1Error.postValue(t) }

        mediatorLiveHaulJobImportStep1.addSource(
            repository.mutableLiveDataJobImportStep1
        ) { t -> mediatorLiveHaulJobImportStep1.postValue(t) }
        mediatorLiveDataJobImportStep1Error.addSource(
            repository.mutableLiveDataJobImportStep1Error
        ) { t -> mediatorLiveDataJobImportStep1Error.postValue(t) }

        mediatorLiveHaultCreateJobRequestData.addSource(
            repository.mutableLiveDataCreateHaultRequest
        ) { t -> mediatorLiveHaultCreateJobRequestData.postValue(t) }
        mediatorLiveHaultCreateJobRequestError.addSource(
            repository.mutableLiveDataCreateHaultError
        ) { t -> mediatorLiveHaultCreateJobRequestError.postValue(t) }


    }


    fun createRequestHaultOfJob(importExport:String) = repository.createRequestHaultOfJob(importExport)

    fun postExportStep1(
        haul_request_id: String,
        mapLat: String,
        mapLng: String,
        address: String,
        point_of_contact: String,
        main_number: String,
        email: String,
        materialImageList: ArrayList<File>
    ) =
        repository.postExportStep1(
            haul_request_id = haul_request_id,
            mapLat = mapLat,
            mapLng = mapLng,
            address = address,
            point_of_contact = point_of_contact,
            main_number = main_number,
            email = email,
            materialImageList = materialImageList
        )


    fun postImportStep1(
        haul_request_id: String,
        mapLat: String,
        mapLng: String,
        address: String,
        point_of_contact: String,
        main_number: String,
        email: String

    ) =
        repository.postImportStep1(
            haul_request_id = haul_request_id,
            mapLat = mapLat,
            mapLng = mapLng,
            address = address,
            point_of_contact = point_of_contact,
            main_number = main_number,
            email = email
        )

}