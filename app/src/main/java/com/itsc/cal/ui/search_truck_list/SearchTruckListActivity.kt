package com.itsc.cal.ui.search_truck_list

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySearchTruckListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.SearchTruckListResponse
import com.itsc.cal.ui.truck_details.TruckDetailsActivity
import com.itsc.cal.ui.trucks.adapter.TruckListAdapter

class SearchTruckListActivity : CalSuperActivity() {

    private lateinit var binding: ActivitySearchTruckListBinding
    private lateinit var mTruckListAdapter: TruckListAdapter
    private lateinit var arrayList: ArrayList<SearchTruckListResponse>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_search_truck_list)
        setPageTitle("Truck List")
        enableBackButton()
        showHelpButton()

        initView()
    }

    private fun initView() {
        intent.let {
            arrayList =
                fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            Log.i("SearchList:", "" + arrayList.size)
            setTruckListAdapter(arrayList)

        }
    }

    private fun setTruckListAdapter(arrayList: ArrayList<SearchTruckListResponse>) {
        mTruckListAdapter = TruckListAdapter(this, object : OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                when (view.id) {
                    R.id.tvViewDetails -> {
                        startActivity(
                            Intent(
                                this@SearchTruckListActivity,
                                TruckDetailsActivity::class.java
                            ).putExtra(KeywordsAndConstants.DATA, arrayList.get(position).truck_id)
                        )
                    }

                }
            }

        }, arrayList)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewSearchTruckList.layoutManager = layoutManager
        binding.recyclerViewSearchTruckList.adapter = mTruckListAdapter
    }
}