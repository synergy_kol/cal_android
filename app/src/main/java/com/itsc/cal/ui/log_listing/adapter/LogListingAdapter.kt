package com.itsc.cal.ui.log_listing.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildLogListngBinding
import com.itsc.cal.model.LogListResponce
import com.itsc.cal.model.notificationList

class LogListingAdapter (val mContext: Context, val logListingList:ArrayList<LogListResponce>):
    RecyclerView.Adapter<LogListingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ChildLogListngBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return logListingList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(logListingList.get(position))
    }

    inner class ViewHolder(val binding: ChildLogListngBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(notificationList:LogListResponce) {
            binding.textDateTime.text = notificationList.created_at
            binding.textLocation.text = "${notificationList.lat} ${notificationList.lng}"
            binding.textNotiTitle.text = notificationList.action
        }

    }

}