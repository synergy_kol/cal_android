package com.itsc.cal.ui.post_hault_of_job_list.import_export_job_details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityJobDetailsImportExportBinding
import com.itsc.cal.model.HauljobListResponse
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.post_hault_of_job_list.import_export_job_details.adapter.ImageShowingAdapter
import com.itsc.cal.ui.post_hault_of_job_list.matches_jobs.MatchesJobList
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class ImportExportJobDetails : CalSuperActivity(withNav = false), KodeinAware {
    override val kodein: Kodein by kodein()
    private val repository: Repository by instance()
    private lateinit var binding: ActivityJobDetailsImportExportBinding
    private lateinit var jobResponse: HauljobListResponse
    private var isFromBooking = false
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var invoiceDownloadLink: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_job_details_import_export)

        showHelpButton()
        enableBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        initView()
        bindToViewModel()
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveImportExportData.observe(
            this,
            Observer<Event<HauljobListResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processPayoutData(it.getContent()!!)
                    }
                }
            }
        )
        viewModel.mediatorLiveImportExportError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

    }

    private fun processPayoutData(jobResponse: HauljobListResponse) {
        this.jobResponse = jobResponse
        binding.imgSpecImage.setImage("${KeywordsAndConstants.BASE_URL}${jobResponse.specs_sheet_full}")
        if (jobResponse.material_image?.size?:0>0){
            binding.recycleviewMaterialImage.apply {
                layoutManager = GridLayoutManager(this@ImportExportJobDetails, 4)
                adapter = ImageShowingAdapter(this@ImportExportJobDetails,jobResponse.material_image?:ArrayList())
            }
        }else{
            binding.recycleviewMaterialImage.visibility
        }

    }

    private fun getJobDetails(haul_request_id:String){
        showProgress()
        viewModel.getJobDetails(haul_request_id)
    }

    private fun initView() {
        intent.let {
            jobResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))

            if (jobResponse.haul_type.equals("E")){
                setPageTitle("Export Job Details")
            }else{
                setPageTitle("Import Job Details")
                binding.tvMaterialImage.visibility = View.GONE
                binding.recycleviewMaterialImage.visibility = View.GONE
            }
            var userId= viewModel.giveRepository().getUserData()!!.user_master_id
            if (userId.equals(jobResponse.user_id)){
                binding.btnMachesJob.visibility = View.VISIBLE
            }else{
                binding.btnMachesJob.visibility = View.GONE
            }

            getJobDetails(jobResponse.haul_request_id)
            binding.tvPickUp.text = jobResponse.address
            binding.tvJobId.text = "#CAL${jobResponse.haul_request_id}"
            binding.edEmail.text = jobResponse.email
            binding.edPointOfContact.text = jobResponse.point_of_contact
            binding.edMainNumber.text = jobResponse.main_number
            binding.edSizeMesurement.text = "${jobResponse.cubic_yards} Cubic / ${jobResponse.tons} Tons"
            if (jobResponse.material_option_name == null || jobResponse.material_option_name == "") {
                binding.edMaterialType.text =
                    "${jobResponse.material_name}"
            } else {
                binding.edMaterialType.text =
                    "${jobResponse.material_name}(${jobResponse.material_option_name})"
            }

            binding.edLoadingEquipement.text = jobResponse.loading_equipment_name
            binding.edNote.text = jobResponse.additional_notes

            binding.btnMachesJob.setOnClickListener {
                val intent = Intent(this, MatchesJobList::class.java)
                intent.putExtra(
                    KeywordsAndConstants.DATA,
                   toJson(jobResponse)
                )
                intent.putExtra(KeywordsAndConstants.ACCEPT_JOB,"0")
                startActivity(intent)
            }

        }

    }

    private fun downloadFile(url: String) {
        if (!url.isEmpty()) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        }

    }
}