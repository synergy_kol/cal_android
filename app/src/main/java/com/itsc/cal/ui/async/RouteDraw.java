package com.itsc.cal.ui.async;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.itsc.cal.interfaces.AsyncInterface;
import com.itsc.cal.util.PathJSONParser;


class RouteDraw extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>>
{
	private AsyncInterface inter;
	private int routeColor=0;

	public RouteDraw(AsyncInterface inter, int routeColor)
	{
		this.inter = inter;
		this.routeColor = routeColor;
	}

	@Override
	protected List<List<HashMap<String, String>>> doInBackground(String... jsonData)
	{
		JSONObject jObject;
		List<List<HashMap<String, String>>> routes = null;

		try
		{
			jObject = new JSONObject(jsonData[0]);
			PathJSONParser parser = new PathJSONParser();
			routes = parser.parse(jObject);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			inter.viewNearestRouteErrorCallBack();
		}
		return routes;
	}

	@Override
	protected void onPostExecute(List<List<HashMap<String, String>>> routes)
	{
		ArrayList<LatLng> points = null;
		ArrayList<PolylineOptions> polylineOptionsList = new ArrayList<PolylineOptions>();
		PolylineOptions polyLineOptions = null;
		try
		{
			for (int i = 0; i < routes.size(); i++)
			{
				points = new ArrayList<LatLng>();
				polyLineOptions = new PolylineOptions();
				List<HashMap<String, String>> path = routes.get(i);

				for (int j = 0; j < path.size(); j++)
				{
					HashMap<String, String> point = path.get(j);

					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);

					points.add(position);
				}

				polyLineOptions.addAll(points);
				/*if(routeColor==Color.GREEN)
				{
					polyLineOptions.width(12);
				}
				else if (routeColor==Color.BLUE)
				{
					polyLineOptions.width(8);
				}
				else if (routeColor==Color.BLACK)
				{
					polyLineOptions.width(4);
				}*/
				polyLineOptions.color(routeColor);
			}
			polylineOptionsList.add(polyLineOptions);
			inter.viewNearestRoute(polylineOptionsList);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
			inter.viewNearestRouteErrorCallBack();
		}
		
	}
}
