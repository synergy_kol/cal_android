package  com.itsc.cal.ui.truck_details

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveTruckDetails: MediatorLiveData<Event<TruckDetailsResponse>> =
        MediatorLiveData()
    val mediatorLiveTruckDetailsError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveTruckDetails.addSource(
            repository.mutableLiveDataTruckDetails
        ) { t -> mediatorLiveTruckDetails.postValue(t) }
        mediatorLiveTruckDetailsError.addSource(
            repository.mutableLiveDataTruckDetailsError
        ) { t -> mediatorLiveTruckDetailsError.postValue(t) }


    }

    fun getTruckList(truckId:String) = repository.truckDetails(truckId)


}