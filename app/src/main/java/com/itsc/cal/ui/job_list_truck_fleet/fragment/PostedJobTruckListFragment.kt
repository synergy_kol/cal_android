package com.itsc.cal.ui.job_list_truck_fleet.fragment

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.application.ApplicationClass
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.FragmentPostedJobBinding
import com.itsc.cal.databinding.VerifyDialogBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse
import com.itsc.cal.model.ProfileDetails
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.arrange_trip.ArrangeTripActivity
import com.itsc.cal.ui.cancel_policy_details.CancelPolicyDetailsActivity
import com.itsc.cal.ui.job_details.JobDetails
import com.itsc.cal.ui.job_list_truck_fleet.JobListTruckFleetActivity
import com.itsc.cal.ui.job_list_truck_fleet.adapter.PostedJobTruckFleetAdapter
import com.itsc.cal.ui.webview.WebView
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.sagar.android.logutilmaster.LogUtil
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class PostedJobTruckListFragment : Fragment(), KodeinAware {

    private lateinit var binding: FragmentPostedJobBinding
    private lateinit var baseActivity: JobListTruckFleetActivity
    override lateinit var kodein: Kodein
    private val logUtil: LogUtil by instance()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var filterType = "1"

    companion object {
        fun newInstance(filter: String): PostedJobTruckListFragment {
            val mPostedJobFragment = PostedJobTruckListFragment()
            val bundle = Bundle()
            bundle.putString("filter", filter)
            mPostedJobFragment.arguments = bundle
            return mPostedJobFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_posted_job, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseActivity = activity as JobListTruckFleetActivity
        kodein = (baseActivity.applicationContext as ApplicationClass).kodein
        viewModel = androidx.lifecycle.ViewModelProvider(
            this,
            viewModelProvider
        )
            .get(
                ViewModel::class.java
            )


        bindToViewModel()

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    private fun getFilterId(): String {
        return arguments?.getString("filter") ?: ""
    }

    fun getJobListing(filter: String) {
        binding.imgTraView.visibility = View.VISIBLE
        (baseActivity).showProgress()
        if (filter.equals("1")) {
            filterType = "1"
            viewModel.getProfileDetails(viewModel.giveRepository().getUserData()!!.user_master_id)
            viewModel.getJobList(filter)
        } else if (filter.equals("2")) {
            viewModel.getProfileDetails(viewModel.giveRepository().getUserData()!!.user_master_id)
            filterType = "2"
            viewModel.getJobListAccpted(filter)

        } else if (filter.equals("3")) {
            filterType = "3"
            viewModel.getJobListHiden(filter)

        }

    }


    fun bindToViewModel() {
        viewModel.mediatorLiveJobListing.observe(
            baseActivity,
            Observer<Event<ArrayList<JobListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        binding.rvPostedJob.visibility = View.VISIBLE
                        setPostedJobAdapter(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveJobListingError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        it.readContent()
                        binding.textviewNoDataFound.visibility = View.VISIBLE
                        binding.rvPostedJob.visibility = View.GONE
                    }

                }
            }
        )

        viewModel.mediatorLiveJobListingAccept.observe(
            baseActivity,
            Observer<Event<ArrayList<JobListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        binding.rvPostedJob.visibility = View.VISIBLE
                        setPostedJobAdapter(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveJobListingAcceptError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        it.readContent()
                        binding.textviewNoDataFound.visibility = View.VISIBLE
                        binding.rvPostedJob.visibility = View.GONE
                    }

                }
            }
        )

        viewModel.mediatorLiveJobListingHiden.observe(
            baseActivity,
            Observer<Event<ArrayList<JobListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        binding.rvPostedJob.visibility = View.VISIBLE
                        setPostedJobAdapter(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveJobListingHidenError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        it.readContent()
                        binding.textviewNoDataFound.visibility = View.VISIBLE
                        binding.rvPostedJob.visibility = View.GONE
                    }

                }
            }
        )

        viewModel.mediatorLiveApplyJobs.observe(
            baseActivity,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        binding.rvPostedJob.visibility = View.VISIBLE
                        processResponseNew(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveApplyJobsError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        it.getContent()?.getMessageToShow()?.let { it1 ->
                            (baseActivity).showMessageInDialog(
                                it1
                            )
                        }

                    }

                }
            }
        )

        viewModel.mediatorLiveHideJobs.observe(
            baseActivity,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        processResponseNew(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveHideJobsError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveShowJobs.observe(
            baseActivity,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        processResponseHide(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveShowJobsError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveCancelJobs.observe(
            baseActivity,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        processResponseConfigur(it.getContent()?.message ?: "")
                    }

                }
            }
        )
        viewModel.mediatorLiveCancelError.observe(
            baseActivity,
            Observer<Event<com.itsc.cal.util.model.Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        it.readContent()

                    }

                }
            }
        )

        viewModel.mediatorLiveDataProfileDetails.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        getProfileData(it.getContent()!!)
                    }
                }
            }
        )
        viewModel.mediatorLiveDataProfileDetailsError.observe(
            this,
            Observer {
                it?.let {
                    if (it.shouldReadContent()) {
                        (baseActivity).hideProgress()
                        (baseActivity).handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )

    }

    private fun getProfileData(profileDetails: ProfileDetails) {
        var userDetails = viewModel.giveRepository().getUserData()
        userDetails!!.stripe_account_id = profileDetails.stripe_account_id
        viewModel.giveRepository().saveUserDataToPref(userDetails)
        Log.i("Update-----------", "userdata")
    }

    override fun onResume() {
        super.onResume()
        baseActivity.showProgress()
        viewModel.getProfileDetails(viewModel.giveRepository().getUserData()!!.user_master_id)
    }

    private fun processResponseNew(message: String) {
        (baseActivity).hideProgress()
        (baseActivity).showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    (baseActivity).showProgress()
                    viewModel.getJobList("1")

                }
            }
        )

    }

    private fun dialogCancel(jobId: String, position: Int) {
    }

    private fun processResponseHide(message: String) {
        (baseActivity).hideProgress()
        (baseActivity).showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    (baseActivity).showProgress()
                    viewModel.getJobList("3")

                }
            }
        )

    }

    private fun processResponseConfigur(message: String) {
        (baseActivity).hideProgress()
        (baseActivity).showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    (baseActivity).showProgress()
                    viewModel.getJobList("2")

                }
            }
        )

    }

    fun setPostedJobAdapter(arrayList: ArrayList<JobListResponse>) {
        logUtil.logV(
            """
                 got data here
                 ${arrayList.size}
             """.trimIndent()
        )
        (baseActivity).hideProgress()
        if (arrayList.size > 0) {
            binding.textviewNoDataFound.visibility = View.GONE
            binding.rvPostedJob.visibility = View.VISIBLE

            val layoutManager =
                LinearLayoutManager(baseActivity, LinearLayoutManager.VERTICAL, false)
            binding.rvPostedJob.layoutManager = layoutManager
            binding.rvPostedJob.adapter =
                PostedJobTruckFleetAdapter(baseActivity, object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        when (view.id) {
                            R.id.cvPostedJob -> {
                                Log.i("FilterTypeButton:",filterType)
                                val intent = Intent(baseActivity, JobDetails::class.java)
                                intent.putExtra(
                                    KeywordsAndConstants.DATA,
                                    (baseActivity).toJson(arrayList.get(position))
                                )
                                intent.putExtra(KeywordsAndConstants.FILTER_TYPE,filterType)
                                intent.putExtra(KeywordsAndConstants.isFromBooking, false)
                                startActivity(intent)
                            }
                            R.id.btn_button_apply -> {
                                if (viewModel.giveRepository()
                                        .getUserData()?.stripe_account_id.equals("")
                                ) {
                                    startActivity(
                                        Intent(
                                            baseActivity,
                                            WebView::class.java
                                        ).putExtra(
                                            WebView.URL,
                                            "${KeywordsAndConstants.BASE_URL}fleet/mobile_connect/${viewModel.giveRepository()
                                                .getUserData()!!.user_master_id}"
                                        ).putExtra(
                                            WebView.TITLE,
                                            "Stripe Connect"
                                        )
                                    )

                                } else {
                                    (baseActivity).showProgress()
                                    viewModel.applyJobs(arrayList.get(position).job_id)


                                }

                            }
                            R.id.btn_button_hide -> {
                                (baseActivity).showProgress()
                                viewModel.hideJobs(arrayList.get(position).job_id)
                            }
                            R.id.btn_button_cancel -> {
                                val intent = Intent(baseActivity, CancelPolicyDetailsActivity::class.java)
                                intent.putExtra(
                                    KeywordsAndConstants.DATA,
                                    baseActivity.toJson(arrayList.get(position))
                                )
                                startActivity(intent)

                            }
                            R.id.btn_button_configure -> {
                                startActivity(
                                    Intent(
                                        baseActivity,
                                        ArrangeTripActivity::class.java
                                    ).putExtra(
                                        KeywordsAndConstants.DATA,
                                        arrayList.get(position).job_id
                                    )
                                )
                            }
                            R.id.btn_button_unhide -> {
                                (baseActivity).showProgress()
                                viewModel.showJobs(arrayList.get(position).job_id)
                                arrayList.removeAt(position)
                                binding.rvPostedJob.adapter?.notifyDataSetChanged()

                            }
                        }
                    }

                }, arrayList, filterType)
            binding.imgTraView.visibility = View.GONE

        } else {
            binding.textviewNoDataFound.visibility = View.VISIBLE
            binding.rvPostedJob.visibility = View.GONE
        }
    }

    fun moveToJobDetails() {

    }

}