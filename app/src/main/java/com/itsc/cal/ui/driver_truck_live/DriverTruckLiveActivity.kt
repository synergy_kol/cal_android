package com.itsc.cal.ui.driver_truck_live

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.TimePicker
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.maps.android.PolyUtil
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityDriverTruckLiveBinding
import com.itsc.cal.databinding.MarksAsDelayBinding
import com.itsc.cal.databinding.SosDialogBinding
import com.itsc.cal.databinding.StartJobDialogBinding
import com.itsc.cal.interfaces.AsyncInterface
import com.itsc.cal.model.DriverJob
import com.itsc.cal.model.SOSReason
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.async.CreateRoute
import com.itsc.cal.ui.end_trip.EndTripActivity
import com.itsc.cal.ui.tracking_truck_live.TrackingTruckLive
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.util.WorkaroundMapFragment
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.itsc.cal.widget.spinner.Spinner
import com.itsc.cal.widget.spinner.SpinnerData
import com.sagar.android.logutilmaster.LogUtil
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import permissions.dispatcher.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

@RuntimePermissions
class DriverTruckLiveActivity : CalSuperActivity(), KodeinAware, AsyncInterface {

    private lateinit var dialogBinding: StartJobDialogBinding
    private lateinit var sosDialog: SosDialogBinding
    override val kodein: Kodein by kodein()

    enum class SearchPlaceFor {
        FROM,
        TO
    }

    private val logUtil: LogUtil by instance()
    private lateinit var searchPlaceFor: SearchPlaceFor
    private lateinit var binding: ActivityDriverTruckLiveBinding
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var map: GoogleMap
    private var lastLocation: Location?=null
    private var sourceLatLong: LatLng = LatLng(0.0, 0.0)
    private var destinationLatLong: LatLng = LatLng(0.0, 0.0)
    private var arrayList: ArrayList<Polyline> = ArrayList()
    private lateinit var polyline: Polyline
    private var isPolyLineVisible = false
    private lateinit var jobResponse: DriverJob
    private var imageUploadChalan: File? = null
    private var imageUploadTruckPhoto: File? = null
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var jobStarted = false
    private lateinit var sOSReason: SOSReason
    private var imageId = 0
    private var emptyWeight = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_driver_truck_live)
        binding.context = this
        setPageTitle("Trucks Live")
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        enableBackButton()
        showHelpButton()
        getLastLocation()
        bindingViewModel()
        binding.buttonMarkAsDelay.setOnClickListener {
            markAsDelay()
        }
    }

    private fun bindingViewModel() {
        viewModel.mediatorLiveDataStartJob.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()){
                        hideProgress()
                        //processResponse(it.getContent()!!.message)
                        processResponse("Job has been started")
                    }

                }
            }
        )
        viewModel.mediatorLiveDataStartJobError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataMarksAsDelay.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        showMessageInDialog("Delay Reason Reported")
                    }

                }
            }
        )
        viewModel.mediatorLiveDataMarksAsDelayError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveEndTrip.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processEndTripResponse(it.getContent()!!.message)
                    }

                }
            }
        )
        viewModel.mediatorLiveEndTripError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )

        viewModel.mediatorLiveSubmitSOSReason.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        showMessageInDialog(it.getContent()!!.message)
                    }

                }
            }
        )
        viewModel.mediatorLiveSubmitSOSReasonError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )

        viewModel.mediatorLiveSOSReasonListing.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<SOSReason>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val items: ArrayList<SpinnerData<SOSReason>> = ArrayList()
                        it.getContent()!!.forEach { source ->
                            items.add(
                                SpinnerData(
                                    source.sos_reason,
                                    source
                                )
                            )
                        }
                        items.add(
                            SpinnerData(
                                "Other",
                                SOSReason("Other", "Other")
                            )
                        )
                        if (items.size > 0)
                            sOSReason = items[0].data
                        sosDialog.spinnerReason.setBg()
                        sosDialog.spinnerReason.addItems(
                            items,
                            withDot = false,
                            listener = object : Spinner.OnItemSelectedListener {
                                override fun <T> selected(item: SpinnerData<T>) {
                                    sOSReason = item.data as SOSReason
                                    if (sOSReason.sos_reason.equals("Other")) {
                                        sosDialog.editTextTypeYourReview.visibility = View.VISIBLE
                                        sosDialog.textOther.visibility = View.VISIBLE
                                    } else {
                                        sosDialog.editTextTypeYourReview.visibility = View.GONE
                                        sosDialog.textOther.visibility = View.GONE
                                    }
                                }
                            }
                        )

                    }
                }
            }
        )

        viewModel.mediatorLiveSOSReasonListingError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )
        viewModel.mediatorLiveUpdCurrLocation.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        //showMessageInDialog(it.getContent()?.message?:"")
                    }

                }
            }
        )
        viewModel.mediatorLiveUpdCurrLocationError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }
                }
            }
        )
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    jobStarted()
                }
            }
        )
    }

    private fun initView(overrideWithLatLng: LatLng? = null) {
        intent.let {
            jobResponse = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            emptyWeight = intent.getStringExtra(KeywordsAndConstants.EMPTY_WEIGHT)?:""

            if (jobStarted || jobResponse.driver_job_status == "1") {
                viewModel
                    .giveRepository().getSharedPref()
                    .edit()
                    .putString(
                        KeywordsAndConstants.TRUCK_ID_FOR_FIREBASE,
                        jobResponse.truck_id
                    )
                    .apply()
                viewModel
                    .giveRepository().getSharedPref()
                    .edit()
                    .putString(
                        KeywordsAndConstants.JOB_ID_FOR_FIREBASE,
                        jobResponse.job_id
                    )
                    .apply()

                if(overrideWithLatLng!=null && (overrideWithLatLng.latitude!=0.0 ||
                            overrideWithLatLng.longitude!=0.0 )){
                    viewModel.updateDriverCurrentLocation(jobResponse.job_id,
                        overrideWithLatLng.latitude.toString(),
                        overrideWithLatLng.longitude.toString())
                }

                viewModel.giveRepository().startMonitoringLocation()
            }

            binding.textViewFromText.text = jobResponse.Source
            binding.textViewToText.text = jobResponse.Destination
           /* sourceLatLong =
                LatLng(jobResponse.source_lat.toDouble(), jobResponse.source_long.toDouble())*/
            if (jobStarted) {
                sourceLatLong =
                    LatLng(jobResponse.source_lat.toDouble(), jobResponse.source_long.toDouble())
            } else
                sourceLatLong =
                    if (jobResponse.driver_job_status == "0") LatLng(
                        lastLocation?.latitude?:0.0,
                        lastLocation?.longitude?:0.0
                    ) else LatLng(
                        jobResponse.source_lat.toDouble(),
                        jobResponse.source_long.toDouble()
                    )
            overrideWithLatLng?.let { toOverride ->
                sourceLatLong = toOverride
            }
            drawMarker(sourceLatLong, 0)
            /*destinationLatLong = LatLng(
                jobResponse.destination_lat.toDouble(),
                jobResponse.destination_long.toDouble()
            )*/
            if (jobStarted) {
                destinationLatLong = LatLng(
                    jobResponse.destination_lat.toDouble(),
                    jobResponse.destination_long.toDouble()
                )
            } else
                destinationLatLong = if (jobResponse.driver_job_status == "0") LatLng(
                    jobResponse.source_lat.toDouble(),
                    jobResponse.source_long.toDouble()
                ) else LatLng(
                    jobResponse.destination_lat.toDouble(),
                    jobResponse.destination_long.toDouble()
                )
            drawMarker(destinationLatLong, 1)
            /*  drawMarker(sourceLatLong, 0)
              drawMarker(destinationLatLong, 1)*/
            if (sourceLatLong.latitude != 0.0 && destinationLatLong.latitude != 0.0) {
                CreateRoute(
                    this,
                    sourceLatLong,
                    destinationLatLong,
                    Color.BLUE
                ).execute()
            }

            showButtonsWithJobCondition()
        }
    }


    @SuppressLint("CheckResult", "MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        RxPermissions(this)
            .request(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .subscribe {
                if (!it) {
                    finish()
                }

                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener { locationResult ->
                        locationResult?.let { location ->
                            logUtil.logV(
                                """
                                    location :
                                    ${location.latitude}
                                    ${location.longitude}
                                """.trimIndent()
                            )
                            this.lastLocation = location
                        }
                    }
                startMap()
            }
    }

    @SuppressLint("MissingPermission")
    private fun startMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync { googleMap ->
            googleMap?.let {
                this.map = it

                map.setPadding(0, 500, 0, 0)

                map.setOnMapLoadedCallback {
                   /* map.animateCamera(
                        CameraUpdateFactory.newCameraPosition(
                            CameraPosition.builder()
                                .target(
                                    LatLng(
                                        lastLocation.latitude,
                                        lastLocation.longitude
                                    )
                                )
                                .zoom(12f)
                                .build()
                        )

                    )*/

                    val mSupportMapFragment: WorkaroundMapFragment =
                        mapFragment as WorkaroundMapFragment
                    mSupportMapFragment.setListener(object :
                        WorkaroundMapFragment.OnTouchListener {
                        override fun onTouch() {
                            getContainer().requestDisallowInterceptTouchEvent(true);
                        }
                    })
                }

                initView()
            }
        }
    }

    private fun startPlacesSearch(searchPlaceFor: SearchPlaceFor) {
        this.searchPlaceFor = searchPlaceFor

        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        val fields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)

        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .build(this)
        startActivityForResult(intent, KeywordsAndConstants.AUTOCOMPLETE_PLACE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == KeywordsAndConstants.AUTOCOMPLETE_PLACE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)

                        logUtil.logV(
                            """
                                place selected:
                                ${place.name}
                                ${place.address}
                            """.trimIndent()
                        )

                        when (searchPlaceFor) {
                            SearchPlaceFor.FROM -> {
                                binding.textViewFromText.text = place.name
                                sourceLatLong = place.latLng ?: LatLng(0.0, 0.0)

                                drawMarker(sourceLatLong, 0)

                                if (sourceLatLong.latitude != 0.0 && destinationLatLong.latitude != 0.0) {
                                    CreateRoute(
                                        this,
                                        sourceLatLong,
                                        destinationLatLong,
                                        Color.BLUE
                                    ).execute()
                                }

                            }
                            SearchPlaceFor.TO -> {
                                binding.textViewToText.text = place.name
                                destinationLatLong = place.latLng ?: LatLng(0.0, 0.0)
                                drawMarker(destinationLatLong, 1)
                                if (sourceLatLong.latitude != 0.0 && destinationLatLong.latitude != 0.0) {
                                    CreateRoute(
                                        this,
                                        sourceLatLong,
                                        destinationLatLong,
                                        Color.BLUE
                                    ).execute()
                                }

                            }
                        }
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        } else if (requestCode == KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            data?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    //Log.i("got image $images")
                    if (imageId == 5001) {
                        imageUploadChalan = File(images[0])
                        dialogBinding.imgUploadChalanPhoto.setImage(
                            File(images[0]),
                            isCircularImage = false,
                            needBorderWithCircularImage = true
                        )
                    } else if (imageId == 5002) {
                        imageUploadTruckPhoto = File(images[0])
                        dialogBinding.imgUploadTruckPhoto.setImage(
                            File(images[0]),
                            isCircularImage = false,
                            needBorderWithCircularImage = true
                        )
                    }

                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun viewNearestRoute(polylineOptionsList: ArrayList<PolylineOptions>) {
        try {

            if (isPolyLineVisible) {
                map.clear()
                polyline.remove()
                drawMarker(sourceLatLong, 0)
                drawMarker(destinationLatLong, 1)
            }
            runOnUiThread(Runnable {

                for (i in 0 until polylineOptionsList.size) {
                    map.addPolyline(polylineOptionsList[i])
                    polyline = map.addPolyline(polylineOptionsList[i])
                    isPolyLineVisible = true
                }
            })
        } catch (e: Exception) {

            e.printStackTrace()
        }

        repositionCamera()
    }

    override fun viewNearestRouteErrorCallBack() {
        showMessageInDialog("Try again later")
    }

    private fun drawMarker(
        point: LatLng,
        position: Int
    ) {
        if (position == 0) {
            val markerOptions = MarkerOptions()
            markerOptions.position(point)
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.location))
            markerOptions.title("You")
            map.addMarker(markerOptions)
        } else if (position == 1) {
            val markerOptions = MarkerOptions()
            markerOptions.position(point)
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.location))
            markerOptions.title("Destination")
            map.addMarker(markerOptions)
            //  markerOptions.snippet(address)
        }

        //markerOptions.snippet(myTeamModel.getMobile() + "~" + MARKER_TEAM_TAG);

    }

    fun clickOnSOS() {

        sosDialog = SosDialogBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        val customDialog = Dialog(this)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(sosDialog.root)
        customDialog.window
            ?.setLayout(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
            )

        customDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.setCancelable(true)
        viewModel.getSOSReasonList()
        customDialog.show()
        sosDialog.buttonRate.setOnClickListener {
            customDialog.dismiss()
            sosReasonAPiCalled()
        }
    }

    private fun sosReasonAPiCalled() {
        if (sOSReason.sos_reason.equals("Other") && sosDialog.editTextTypeYourReview.text.toString()
                .isEmpty()
        ) {
            showMessageInDialog("Please provide other reason")
            return
        }
        showProgress()
        if (sOSReason.sos_reason.equals("Other")) {
            viewModel.submitSOSReason(
                driver_id = jobResponse.driver_id,
                token_id = jobResponse.token_id,
                reason = sosDialog.editTextTypeYourReview.text.toString()
            )
        } else {
            viewModel.submitSOSReason(
                driver_id = jobResponse.driver_id,
                token_id = jobResponse.token_id,
                reason = sOSReason.sos_reason
            )
        }

    }


    fun clickedStartTrip() {
        checkCanStartJob()
    }

    private fun startJob() {
        dialogBinding = StartJobDialogBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        val customDialog = Dialog(this)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(dialogBinding.root)
        dialogBinding.editText.apply {
            setText("")
            gravityCenter()
            editable(true)
            typeFace(
                fontResEditText = R.font.poppins_regular
            )
            inputMode(
                EditTextInputMode.NUMERIC_PASSWORD
            )
            hint("OTP")

        }

        dialogBinding.editTextTruckWeight.apply {
            setText("")
            gravityCenter()
            editable(true)
            typeFace(
                fontResEditText = R.font.poppins_regular
            )
            inputMode(
                EditTextInputMode.NUMBER
            )
            hint("Loaded Truck Weight")

        }
        dialogBinding.buttonActionOne.text = "Start Job"
        customDialog.window
            ?.setLayout(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
            )

        customDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.setCancelable(true)
        customDialog.show()
        if (imageUploadChalan != null) {
            dialogBinding.imgUploadChalanPhoto.setImage(
                imageUploadChalan!!,
                isCircularImage = false,
                needBorderWithCircularImage = true
            )
        }
        if (imageUploadTruckPhoto != null) {
            dialogBinding.imgUploadTruckPhoto.setImage(
                imageUploadTruckPhoto!!,
                isCircularImage = false,
                needBorderWithCircularImage = true
            )
        }
        dialogBinding.buttonActionOne.setOnClickListener {
            if (dialogBinding.editText.getText().toString().isEmpty()) {
                showMessageInDialog("Please provide OTP to start job")

            }else if (dialogBinding.editText.getText()
                    .toString() != jobResponse.supplier_otp?:"".toString()
            ) {
                showMessageInDialog("Invalid OTP")

            } else if (imageUploadChalan == null) {
                showMessageInDialog("Please upload challan image.")


            } else if (dialogBinding.editTextTruckWeight.getText().toString() == "") {
                showMessageInDialog("Please provide loaded truck weight(MT) .")

            } else if (dialogBinding.editTextTruckWeight.getText().toString().toInt() < emptyWeight.toInt()) {
                showMessageInDialog("Invalid loaded truck weight(MT) .")

            } else if (imageUploadTruckPhoto == null) {
                showMessageInDialog("Please upload slip photo .")

            } else {
                customDialog.dismiss()
                apiCalled(
                    dialogBinding.editText.getText().toString(),
                    dialogBinding.editTextTruckWeight.getText().toString()
                )
            }

        }
        dialogBinding.imgUploadChalanPhoto.setImage(imageDrawable = resources.getDrawable(R.drawable.ic_dummy_upload_image))
        dialogBinding.imgUploadTruckPhoto.setImage(imageDrawable = resources.getDrawable(R.drawable.ic_dummy_upload_image))
        dialogBinding.imgUploadChalanPhoto.registerForOnClick {
            imageId = 5001
            initiateCaptureForProfilePicture()
        }
        dialogBinding.imgUploadTruckPhoto.registerForOnClick {
            imageId = 5002
            initiateCaptureForProfilePicture()
        }
    }

    private fun apiCalled(token: String, truckWeight: String) {

        if (token.isEmpty()) {
            showMessageInDialog("Invalid Verification Code.")
            return
        }
        if (imageUploadChalan == null) {
            showMessageInDialog("Please upload challan image .")
            return
        }
        if (truckWeight.isEmpty()) {
            showMessageInDialog("Please provide loaded truck weight(MT) .")
            return
        }
        else if (truckWeight.toInt() < emptyWeight.toInt()) {
            showMessageInDialog("Invalid loaded truck weight(MT) .")
            return
        }
        if (imageUploadTruckPhoto == null) {
            showMessageInDialog("Please upload slip photo .")
            return
        }
        showProgress()
        viewModel.startJob(
            token = jobResponse.token_id,
            challan = imageUploadChalan,
            truckImage = imageUploadTruckPhoto,
            truckWeight = truckWeight
        )
    }

    fun markAsDelay() {

        val dialogBindingMarksAsDelay = MarksAsDelayBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        val customDialog = Dialog(this)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(dialogBindingMarksAsDelay.root)
        dialogBindingMarksAsDelay.editText.apply {
            setText("")
            gravityCenter()
            editable(true)
            typeFace(
                fontResEditText = R.font.poppins_regular
            )
            inputMode(
                EditTextInputMode.INPUT_TEXT_MULTI_LINE
            )
            hint("Enter delay reason")

        }
        dialogBindingMarksAsDelay.buttonActionOne.text = "Marks as Delay"
        customDialog.window
            ?.setLayout(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
            )

        customDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.setCancelable(true)
        customDialog.show()

        dialogBindingMarksAsDelay.edDelayTime.setOnClickListener {
            val calendar = Calendar.getInstance()
            val mHour = calendar.get(Calendar.HOUR_OF_DAY)
            val mMinute = calendar.get(Calendar.MINUTE)

            var timePickerDialog =
                TimePickerDialog(this, object : TimePickerDialog.OnTimeSetListener {
                    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
                        dialogBindingMarksAsDelay.edDelayTime.setText("$p1:$p2")
                    }

                }, mHour, mMinute, false)
            timePickerDialog.show()
        }

        dialogBindingMarksAsDelay.buttonActionOne.setOnClickListener {
            if (dialogBindingMarksAsDelay.editText.getText().isEmpty()
            ) {
                showMessageInDialog("Please provide delay reason")
            } else if (dialogBindingMarksAsDelay.edDelayTime.text.toString().isEmpty()) {
                showMessageInDialog("Please provide delay time .")

            } else {
                customDialog.dismiss()
                showProgress()
                viewModel.marksAsDelay(
                    token = jobResponse.token_id,
                    delayReason = dialogBindingMarksAsDelay.editText.getText(),
                    delayTime = dialogBindingMarksAsDelay.edDelayTime.text.toString()
                )
            }

        }


    }


    private fun repositionCamera() {
        try {
            map.animateCamera(
                CameraUpdateFactory.newLatLngBounds(
                    LatLngBounds.builder()
                        .include(sourceLatLong)
                        .include(destinationLatLong)
                        .build(),
                    100
                )
            )
        } catch (ex: Exception) {
            ex.printStackTrace()

            Handler().postDelayed(
                {
                    repositionCamera()
                },
                500
            )
        }
    }

    private fun showButtonsWithJobCondition() {
        binding.buttonStartTrip.visibility = View.GONE
        binding.buttonEndTrip.visibility = View.GONE
        binding.buttonSOS.visibility = View.GONE
        binding.buttonMarkAsDelay.visibility = View.GONE

        when (jobResponse.driver_job_status) {
            "0" -> {
                binding.buttonStartTrip.visibility = View.VISIBLE
            }
            else -> {
                binding.buttonEndTrip.visibility = View.VISIBLE
                binding.buttonSOS.visibility = View.VISIBLE
                binding.buttonMarkAsDelay.visibility = View.VISIBLE
            }
        }
        if (jobStarted) {
            binding.buttonEndTrip.visibility = View.VISIBLE
            binding.buttonSOS.visibility = View.VISIBLE
            binding.buttonMarkAsDelay.visibility = View.VISIBLE
            binding.buttonStartTrip.visibility = View.GONE
        }
    }

    fun initiateCaptureForProfilePicture() {
        takeProfilePictureWithPermissionCheck()
    }

    @NeedsPermission(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun takeProfilePicture() {
        ImagePickerUtil.pickImage(
            context = this,
            reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE,
            takePictureFrom = Options.TakePictureFrom.ONLY_CAMERA
        )
    }

    @OnShowRationale(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun showRationale(request: PermissionRequest) {
        showMessageWithTwoButton(
            message = "We need some permission to take pictures.",
            buttonOneText = "Okay",
            buttonTwoText = "Cancel",
            callback = object : DialogUtil.MultiButtonCallBack {
                override fun buttonOneClicked() {
                    super.buttonOneClicked()
                    request.proceed()
                }

                override fun buttonTwoClicked() {
                    super.buttonTwoClicked()
                    request.cancel()
                    finish()
                }
            }
        )
    }

    @OnPermissionDenied(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun permissionDenied() {
        showMessageWithOneButton(
            "You will not be able to take picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    @OnNeverAskAgain(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun neverAskAgain() {
        showMessageWithOneButton(
            "You will not be able to take picture picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {
                }

                override fun buttonClicked() {
                    finish()
                }
            }
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    private fun jobStarted() {
        map.clear()
        jobStarted = true
        jobResponse.driver_job_status = "1"
        initView()
    }

    private val broadcastReDraw = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            p1?.let { intentResult ->
                try {
                    val onPath = PolyUtil.isLocationOnPath(
                        LatLng(
                            intentResult.getDoubleExtra("lat", 0.0),
                            intentResult.getDoubleExtra("long", 0.0)
                        ),
                        polyline.points,
                        polyline.isGeodesic,
                        10.0
                    )

                    if (!onPath) {
                        map.clear()
                        initView(
                            overrideWithLatLng = LatLng(
                                intentResult.getDoubleExtra("lat", 0.0),
                                intentResult.getDoubleExtra("long", 0.0)
                            )
                        )
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }
        }
    }

    override fun onResume() {
        super.onResume()
       /* Handler().postDelayed(Runnable {
            getLastLocation()
        },60000)*/

        registerReceiver(
            broadcastReDraw,
            IntentFilter(KeywordsAndConstants.MAY_RE_DRAW_POLY_LINES)
        )
    }

    override fun onPause() {
        super.onPause()

       // unregisterReceiver(broadcastReDraw)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReDraw)
    }

    fun endTrip() {
        checkCanEndJob()
    }

    private fun processEndTripResponse(message: String) {
        showMessageWithOneButton(
            message = message,
            cancellable = false,
            callback = object : DialogUtil.CallBack {

                override fun buttonClicked() {
                    super.buttonClicked()

                    viewModel.giveRepository().stopMonitoringLocation()

                    startActivity(
                        Intent(
                            this@DriverTruckLiveActivity,
                            EndTripActivity::class.java
                        )
                            .putExtra(
                                KeywordsAndConstants.DATA,
                                toJson(jobResponse)
                            )
                    )
                    finish()
                }
            }
        )
    }

    @SuppressLint("CheckResult", "MissingPermission")
    private fun checkCanStartJob() {
        showProgress()
        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener { locationResult ->
                locationResult?.let { location: Location ->
                    logUtil.logV(
                        """
                                    location :
                                    ${location.latitude}
                                    ${location.longitude}
                                """.trimIndent()
                    )

                    val startLocation = Location("")
                    val startLatLng = LatLng(
                        jobResponse.source_lat.toDouble(),
                        jobResponse.source_long.toDouble()
                    )
                    startLocation.latitude = startLatLng.latitude
                    startLocation.longitude = startLatLng.longitude
                    val currentLocation = location
                    val distance = startLocation.distanceTo(currentLocation)

                    hideProgress()
                    if (distance > KeywordsAndConstants.START_JOB_MIN_DISTANCE_MTS) {
                        showMessageInDialog("Please be inside ${KeywordsAndConstants.START_JOB_MIN_DISTANCE_MTS} meters range of start job location to start the job.")
                        return@let
                    }

                    startJob()
                }
            }
            .addOnFailureListener {
                hideProgress()
                showMessageInDialog("Unable to get your location. Please try again.")
            }
    }

    @SuppressLint("CheckResult", "MissingPermission")
    private fun checkCanEndJob() {
        showProgress()
        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener { locationResult ->
                locationResult?.let { location: Location ->
                    logUtil.logV(
                        """
                                    location :
                                    ${location.longitude}
                                    ${location.longitude}
                                """.trimIndent()
                    )

                    val endJobLocation = Location("")
                    val endLatLng = LatLng(
                        jobResponse.destination_lat.toDouble(),
                        jobResponse.destination_long.toDouble()
                    )
                    endJobLocation.latitude = endLatLng.latitude
                    endJobLocation.longitude = endLatLng.longitude
                    val currentLocation = location

                    val distance = endJobLocation.distanceTo(currentLocation)

                    hideProgress()
                    if (distance > KeywordsAndConstants.END_JOB_MIN_DISTANCE_MTS) {
                        showMessageInDialog("Please be inside ${KeywordsAndConstants.END_JOB_MIN_DISTANCE_MTS} meters range of end job location to end the job.")
                        return@let
                    }

                    endTripJob()
                }
            }
            .addOnFailureListener {
                hideProgress()
                showMessageInDialog("Unable to get your location. Please try again.")
            }
    }

    private fun endTripJob() {
        showProgress()
        viewModel.endTrip(
            token_id = jobResponse.token_id,
            driver_job_status = "2"
        )
    }


}
