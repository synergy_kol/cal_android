package  com.itsc.cal.ui.tracking_truck_live

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDriverLocation: MediatorLiveData<Event<ArrayList<TruckLocation>>> =
        MediatorLiveData()
    val mediatorLiveDriverLocationError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveDriverLocation.addSource(
            repository.mutableLiveDataDriverLocation
        ) { t -> mediatorLiveDriverLocation.postValue(t) }
        mediatorLiveDriverLocationError.addSource(
            repository.mutableLiveDataDriverLocationError
        ) { t -> mediatorLiveDriverLocationError.postValue(t) }


    }

    fun getDriverLocation(job_id: String) =
        repository.getDriverLocation(job_id = job_id)


}