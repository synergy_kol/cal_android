package com.itsc.cal.ui.search_job

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySearchJobBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.ui.builder_my_jobs_list.BuilderMyJobsList
import com.itsc.cal.ui.search_job.adapter.SearchJobAdapter

class SearchJobActivity : CalSuperActivity(withNav = true) {
    private lateinit var binding: ActivitySearchJobBinding
    private lateinit var searchJobAdapter: SearchJobAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_search_job)
        enableBackButton()
        setPageTitle("Search for jobs")
        showHelpButton()
        setAdapter()
        initView()
    }

    private fun initView() {
        binding.bunSearch.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    BuilderMyJobsList::class.java
                ).putExtra(KeywordsAndConstants.IS_BUILDER_JOBS, false)
            )
        }
    }

    private fun setAdapter() {

        searchJobAdapter = SearchJobAdapter(this, object : OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {

            }

        })
        binding.recycleViewVehicleType.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.recycleViewVehicleType.adapter = searchJobAdapter

    }
}