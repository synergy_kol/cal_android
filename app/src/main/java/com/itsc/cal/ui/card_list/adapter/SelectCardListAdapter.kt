package com.itsc.cal.ui.card_list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.R
import com.itsc.cal.databinding.ChildCardListBinding
import com.itsc.cal.databinding.ChildSelectCardListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.CardListResponse

class SelectCardListAdapter (val mContext: Context, val arrayList:ArrayList<CardListResponse>?,val onItemClickListener: OnItemClickListener):
    RecyclerView.Adapter<SelectCardListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(ChildSelectCardListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData()

    }

    inner class ViewHolder(val binding: ChildSelectCardListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData() {
            val cardListResponse = arrayList?.get(adapterPosition)
            binding.llSelectCard.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.rbCardList.setOnClickListener {
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.rbCardList.isChecked = cardListResponse?.isSelected?:false
            binding.textCardNo.text = cardListResponse?.number
            binding.textName.text = cardListResponse?.card_name
            binding.textExpDate.text = "${cardListResponse?.expiry_month}/${cardListResponse?.expiry_year}"

        }

    }

}