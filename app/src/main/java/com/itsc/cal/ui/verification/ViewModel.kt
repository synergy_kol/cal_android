package  com.itsc.cal.ui.verification

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.StatusReply
import com.itsc.cal.model.UserDetails
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveDataResendOTP: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveDataResendOTPError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDataVerifyOTP: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveDataVerifyOTPError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDataVerifyOTPPasswordToken: MediatorLiveData<Event<StatusReply>> = MediatorLiveData()
    val mediatorLiveDataVerifyOTPPasswordTokenError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveDataForgotPasswordResend: MediatorLiveData<Event<UserDetails>> = MediatorLiveData()
    val mediatorLiveDataForgotPasswordResendError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveDataResendOTP.addSource(
            repository.mutableLiveDataResendOTP
        ) { t -> mediatorLiveDataResendOTP.postValue(t) }
        mediatorLiveDataResendOTPError.addSource(
            repository.mutableLiveDataResendOTPError
        ) { t -> mediatorLiveDataResendOTPError.postValue(t) }

        mediatorLiveDataVerifyOTP.addSource(
            repository.mutableLiveDataOTPVerify
        ) { t -> mediatorLiveDataVerifyOTP.postValue(t) }
        mediatorLiveDataVerifyOTPError.addSource(
            repository.mutableLiveDataOTPVerifyError
        ) { t -> mediatorLiveDataVerifyOTPError.postValue(t) }


        mediatorLiveDataVerifyOTPPasswordToken.addSource(
            repository.mutableLiveDataOTPVerifyPasswordToken
        ) { t -> mediatorLiveDataVerifyOTPPasswordToken.postValue(t) }
        mediatorLiveDataVerifyOTPPasswordTokenError.addSource(
            repository.mutableLiveDataOTPVerifyPasswordTokenError
        ) { t -> mediatorLiveDataVerifyOTPPasswordTokenError.postValue(t) }


        mediatorLiveDataForgotPasswordResend.addSource(
            repository.mutableLiveDataForgotPasswordResend
        ) { t -> mediatorLiveDataForgotPasswordResend.postValue(t) }
        mediatorLiveDataForgotPasswordResendError.addSource(
            repository.mutableLiveDataForgotPasswordResendError
        ) { t -> mediatorLiveDataForgotPasswordResendError.postValue(t) }


    }

    fun resendOTP()=repository.resendOTP()
    fun verifyOTP(otp:String)=repository.verifyOTP(otp)
    fun verifyOTPPasswordToken(otp:String)=repository.verifyOTPPasswordToken(otp)
    fun forgotPassword(emailMobile:String)=repository.forgotPasswordResend(emailMobile)

}