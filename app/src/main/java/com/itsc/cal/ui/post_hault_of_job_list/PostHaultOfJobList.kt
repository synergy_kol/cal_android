package com.itsc.cal.ui.post_hault_of_job_list

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.Window
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.viewpager.widget.ViewPager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityPostedHaultOfJobListBinding
import com.itsc.cal.databinding.DialogImportExportBinding
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.post_hault_of_job_list.adapter.ViewPagerHaultAdapter
import com.itsc.cal.ui.post_hault_of_job_list.fragment.HaultOfJobListFragment
import com.itsc.cal.ui.post_hault_of_job_list.post_export_job.PostExportJobActivity
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class PostHaultOfJobList : CalSuperActivity(withNav = true), KodeinAware {
    override val kodein: Kodein by kodein()

    private val repository: Repository by instance()

    private lateinit var binding: ActivityPostedHaultOfJobListBinding
    private val pagerFragmentList = listOf(
        HaultOfJobListFragment.newInstance("1"),
        HaultOfJobListFragment.newInstance("2"),
        HaultOfJobListFragment.newInstance("3"),
        HaultOfJobListFragment.newInstance("4")
    )
    private var importExport = ""
    private var pos = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_posted_hault_of_job_list)

        showDashboardActionbarWithBackButton()
        setPageTitle("Haul-OFF Request List")
        initView()


    }
    override fun onResume() {
        super.onResume()
        setPagerAdapter()

    }
    private fun initView() {
        binding.btnAddMore.setOnClickListener {
            dialogImportOrExport()
        }
    }

    private fun dialogImportOrExport() {

        var dialogBinding = DialogImportExportBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        val customDialog = Dialog(this)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(dialogBinding.root)
        customDialog.window
            ?.setLayout(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
            )

        customDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.setCancelable(true)
        customDialog.show()
        dialogBinding.btnExport.setOnClickListener {
            customDialog.dismiss()
            startActivity(
                Intent(this, PostExportJobActivity::class.java).putExtra(
                    KeywordsAndConstants.DATA,
                    "E"
                )
            )
        }
        dialogBinding.btnImport.setOnClickListener {
            customDialog.dismiss()
            startActivity(
                Intent(this, PostExportJobActivity::class.java).putExtra(
                    KeywordsAndConstants.DATA,
                    "I"
                )
            )
        }

    }

    private fun setPagerAdapter() {
        val mBookingPagerAdapter = ViewPagerHaultAdapter(supportFragmentManager, pagerFragmentList)
        binding.viewPager.adapter = mBookingPagerAdapter
        binding.viewPager.offscreenPageLimit = 1
        binding.tabs.setViewPager(binding.viewPager)
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                val myReservationFragment =
                    mBookingPagerAdapter.fragmentList[position] as? HaultOfJobListFragment
                myReservationFragment?.showTransparentView()
                myReservationFragment?.getJobListing((position).toString())
                pos = position

            }

        })

        Handler().postDelayed(
            {
                val myReservationFragment =
                    mBookingPagerAdapter.fragmentList[pos] as? HaultOfJobListFragment
                myReservationFragment?.getJobListing((pos).toString())
                binding.viewPager.setCurrentItem(pos)
            },
            800
        )
    }
}