package  com.itsc.cal.ui.job_details

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.PayoutResponse
import com.itsc.cal.model.ProfileDetails
import com.itsc.cal.model.StatusReply
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLivePayoutData: MediatorLiveData<Event<PayoutResponse>> = MediatorLiveData()
    val mediatorLivePayoutDataError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveApplyJobs: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveApplyJobsError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveShowJobs: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveShowJobsError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveHideJobs: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveHideJobsError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    val mediatorLiveDataProfileDetails: MediatorLiveData<Event<ProfileDetails>> =
        MediatorLiveData()
    val mediatorLiveDataProfileDetailsError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()


    init {
        mediatorLivePayoutData.addSource(
            repository.mutableLiveDataPayoutDetails
        ) { t -> mediatorLivePayoutData.postValue(t) }
        mediatorLivePayoutDataError.addSource(
            repository.mutableLiveDataPayoutDetailsError
        ) { t -> mediatorLivePayoutDataError.postValue(t) }

        mediatorLiveApplyJobs.addSource(
            repository.mutableLiveDataApplyJobsDetailsPage
        ) { t -> mediatorLiveApplyJobs.postValue(t) }
        mediatorLiveApplyJobsError.addSource(
            repository.mutableLiveDataApplyJobsErrorDetailsPage
        ) { t -> mediatorLiveApplyJobsError.postValue(t) }



        mediatorLiveShowJobs.addSource(
            repository.mutableLiveDataShowJobsDetails
        ) { t -> mediatorLiveShowJobs.postValue(t) }
        mediatorLiveShowJobsError.addSource(
            repository.mutableLiveDataShowJobsErrorDetails
        ) { t -> mediatorLiveShowJobsError.postValue(t) }

        mediatorLiveHideJobs.addSource(
            repository.mutableLiveDataHideJobsDetails
        ) { t -> mediatorLiveHideJobs.postValue(t) }
        mediatorLiveHideJobsError.addSource(
            repository.mutableLiveDataHideJobsErrorDetails
        ) { t -> mediatorLiveHideJobsError.postValue(t) }

        mediatorLiveDataProfileDetails.addSource(
            repository.mutableLiveDataProfileDetail
        ) { t -> mediatorLiveDataProfileDetails.postValue(t) }
        mediatorLiveDataProfileDetailsError.addSource(
            repository.mutableLiveDataProfileDetailError
        ) { t -> mediatorLiveDataProfileDetailsError.postValue(t) }

    }

    fun getPayoutDetails(jobId:String){
        repository.getPayoutDetails(jobId)
    }

    fun applyJobs(jobId:String) = repository.applyJobsDetailsPage(jobId)
    fun getProfileDetails(userId:String)=repository.getProfileData(userId = userId)
    fun hideJobs(jobId:String) = repository.hideJobsDetails(jobId)
    fun showJobs(jobid:String) = repository.showJobsDetails(jobid)
}
