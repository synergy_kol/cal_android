package com.itsc.cal.ui.post_a_truck_image

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.fxn.pix.Pix
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityPostATruckImageBinding
import com.itsc.cal.model.PostATruckRequest
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.post_a_truck.PostATruckActivity
import com.itsc.cal.ui.track_list.TruckListActivity
import com.itsc.cal.ui.webview.WebView
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import permissions.dispatcher.*
import java.io.File


@RuntimePermissions
class PostATruckImageRegistrationActivity : CalSuperActivity(), KodeinAware {
    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPostATruckImageBinding
    private var imageFilePathTruckFront: File? = null
    private var imageFilePathTruckRere: File? = null
    private var imageFilePathTruckLeftSide: File? = null
    private var imageFilePathTruckRightSide: File? = null
    private var imageFilePathTruckPrimary: File? = null
    private var imageFilePathTruckDOTS: File? = null
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var view: Int = 0
    private lateinit var postTruckRequest: PostATruckRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = setLayout(R.layout.activity_post_a_truck_image)
        enableBackButton()
        showHelpButton()
        setPageTitle("Upload Truck Images")
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        initView()
        bindingViewModel()
    }

    private fun bindingViewModel() {
        viewModel.mediatorLivePostATruck.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLivePostATruckError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun processResponse(message: String) {
        hideProgress()
        val userDetails = viewModel.giveRepository().getUserData()
        userDetails?.completed_steps = "7"
        viewModel.giveRepository().saveUserDataToPref(userDetails!!)
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                     startActivity(
                         UiUtil.clearStackAndStartNewActivity(
                             Intent(
                                 this@PostATruckImageRegistrationActivity,
                                 Dashboard::class.java
                             )
                         )
                     )
                    this@PostATruckImageRegistrationActivity.finish()

                }
            }
        )

    }

    private fun initView() {

        intent?.let {
            postTruckRequest = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
        }

        binding.imgTruckFrontImage.setImage(imageDrawable = resources.getDrawable(R.drawable.truck_font))
        binding.imgTruckRare.setImage(imageDrawable = resources.getDrawable(R.drawable.truck_rare))
        binding.imgTruckLeftSide.setImage(imageDrawable = resources.getDrawable(R.drawable.truck_rare))
        binding.imgTruckRightSide.setImage(imageDrawable = resources.getDrawable(R.drawable.truck_usot))
        binding.imgTruckPrimary.setImage(imageDrawable = resources.getDrawable(R.drawable.truck_usot))
        binding.imgTruckUSDOTImage.setImage(imageDrawable = resources.getDrawable(R.drawable.truck_usot))

        binding.btnPostATruck.setOnClickListener {
            apiCalled()
        }

        binding.textAck.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    WebView::class.java
                ).putExtra(
                    WebView.URL,
                    "${KeywordsAndConstants.BASE_URL}home/cms/ACK"
                ).putExtra(
                    WebView.TITLE,
                    "Acknowledgement"
                )
            )
        }

        binding.imgTruckFrontImage.registerForOnClick {
            view = 1
            initiateCaptureForProfilePicture()
        }
        binding.imgTruckRare.registerForOnClick {
            view = 2
            initiateCaptureForProfilePicture()

        }
        binding.imgTruckLeftSide.registerForOnClick {
            view = 3
            initiateCaptureForProfilePicture()
        }
        binding.imgTruckRightSide.registerForOnClick {
            view = 4
            initiateCaptureForProfilePicture()
        }
        binding.imgTruckPrimary.registerForOnClick {
            view = 5
            initiateCaptureForProfilePicture()
        }
        binding.imgTruckUSDOTImage.registerForOnClick {
            view = 6
            initiateCaptureForProfilePicture()
        }
    }

    private fun apiCalled() {

        if (imageFilePathTruckFront == null) {
            showMessageInDialog("Please upload Front Image.")
            return
        }

        if (imageFilePathTruckRere == null) {
            showMessageInDialog("Please upload Rear Image.")
            return
        }

        if (imageFilePathTruckLeftSide == null) {
            showMessageInDialog("Please upload Left Side Image .")
            return
        }

        if (imageFilePathTruckRightSide == null) {
            showMessageInDialog("Please upload Right Side Image .")
            return
        }

        if (imageFilePathTruckPrimary == null) {
            showMessageInDialog("Please upload Primary Image.")
            return
        }

        if (imageFilePathTruckDOTS == null) {
            showMessageInDialog("Please upload USDOT Image.")
            return
        }
        if (!binding.checkBoxIAgree.isChecked) {
            showMessageInDialog("Please accept the acknowledgment.")
            return
        }

        showProgress()
        val postATruckRequestAPICalled = PostATruckRequest(
            user_master_id = postTruckRequest.user_master_id,
            truck_weight_metric = postTruckRequest.truck_weight_metric,
            truck_vin_number = postTruckRequest.truck_vin_number,
            truck_type = postTruckRequest.truck_type,
            truck_reg_number = postTruckRequest.truck_reg_number,
            truck_model = postTruckRequest.truck_model,
            truck_make = postTruckRequest.truck_make,
            truck_empty_weight = postTruckRequest.truck_empty_weight,
            truck_color = postTruckRequest.truck_color,
            truck_carring_capacity = postTruckRequest.truck_carring_capacity,
            truck_cabin_type = postTruckRequest.truck_cabin_type,
            truck_body_style = postTruckRequest.truck_body_style,
            usdot = imageFilePathTruckDOTS,
            right = imageFilePathTruckRightSide,
            left = imageFilePathTruckLeftSide,
            back = imageFilePathTruckRere,
            front = imageFilePathTruckFront,
            truck_image = imageFilePathTruckPrimary
        )
        viewModel.addTruck(postATruckRequestAPICalled)
    }

    fun initiateCaptureForProfilePicture() {
        takeProfilePictureWithPermissionCheck()
    }

    @NeedsPermission(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun takeProfilePicture() {
        ImagePickerUtil.pickImage(
            context = this,
            reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
        )
    }

    @OnShowRationale(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun showRationale(request: PermissionRequest) {
        showMessageWithTwoButton(
            message = "We need some permission to take pictures.",
            buttonOneText = "Okay",
            buttonTwoText = "Cancel",
            callback = object : DialogUtil.MultiButtonCallBack {
                override fun buttonOneClicked() {
                    super.buttonOneClicked()
                    request.proceed()
                }

                override fun buttonTwoClicked() {
                    super.buttonTwoClicked()
                    request.cancel()
                    finish()
                }
            }
        )
    }

    @OnPermissionDenied(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun permissionDenied() {
        showMessageWithOneButton(
            "You will not be able to take picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    @OnNeverAskAgain(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun neverAskAgain() {
        showMessageWithOneButton(
            "You will not be able to take picture picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)
        if (requestCode == KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            intentData?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    //Log.i("got image $images")
                    when (view) {
                        1 -> {
                            imageFilePathTruckFront = File(images[0])
                            binding.imgTruckFrontImage.setImage(
                                File(images[0]),
                                isCircularImage = false,
                                needBorderWithCircularImage = true
                            )
                        }
                        2 -> {
                            imageFilePathTruckRere = File(images[0])
                            binding.imgTruckRare.setImage(
                                File(images[0]),
                                isCircularImage = false,
                                needBorderWithCircularImage = true
                            )

                        }
                        3 -> {
                            imageFilePathTruckLeftSide = File(images[0])
                            binding.imgTruckLeftSide.setImage(
                                File(images[0]),
                                isCircularImage = false,
                                needBorderWithCircularImage = true
                            )

                        }
                        4 -> {
                            imageFilePathTruckRightSide = File(images[0])
                            binding.imgTruckRightSide.setImage(
                                File(images[0]),
                                isCircularImage = false,
                                needBorderWithCircularImage = true
                            )

                        }

                        5 -> {
                            imageFilePathTruckPrimary = File(images[0])
                            binding.imgTruckPrimary.setImage(
                                File(images[0]),
                                isCircularImage = false,
                                needBorderWithCircularImage = true
                            )

                        }
                        6 -> {
                            imageFilePathTruckDOTS = File(images[0])
                            binding.imgTruckUSDOTImage.setImage(
                                File(images[0]),
                                isCircularImage = false,
                                needBorderWithCircularImage = true
                            )

                        }


                    }


                }
            }
        }
    }
}