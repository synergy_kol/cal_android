package com.itsc.cal.ui.log_listing

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityLogListingBinding
import com.itsc.cal.model.BannerResponce
import com.itsc.cal.model.LogListResponce
import com.itsc.cal.model.StatusReply
import com.itsc.cal.model.notificationList
import com.itsc.cal.ui.alert.adapter.AlertAdapter
import com.itsc.cal.ui.log_listing.adapter.LogListingAdapter
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.generic.instance

class LogListActivity : CalSuperActivity() {

    private lateinit var binding: ActivityLogListingBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_log_listing)
        enableBackButton()
        setPageTitle("Log Listing")
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)


        showAlertActionbar()
        getLogDetails()
        initView()
        bindToViewModel()
    }

    private fun getLogDetails() {
        showProgress()
        viewModel.getLogDetails()
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveLogListingRead.observe(
            this,
            Observer<Event<ArrayList<LogListResponce>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setAdapter(it.getContent()!!)
                    }

                }
            }
        )
        viewModel.mediatorLiveLogListingError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )

    }


    private fun initView() {


    }

    private fun setAdapter(logListing: ArrayList<LogListResponce>) {
        binding.recycleViewBuilderDetails.apply {
            layoutManager = LinearLayoutManager(this@LogListActivity)
            adapter = LogListingAdapter(this@LogListActivity, logListing)
        }
    }
}