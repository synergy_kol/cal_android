package  com.itsc.cal.ui.sos_message_log

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.repository.Event


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveNotificationResponse: MediatorLiveData<Event<NotificationResponce>> = MediatorLiveData()
    val mediatorLiveNotificationResponseError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()

    val mediatorLiveNotificationListRes: MediatorLiveData<Event<ArrayList<notificationList>>> = MediatorLiveData()
    val  mediatorLiveNotificationListResError: MediatorLiveData<Event<com.itsc.cal.util.model.Result>> =
        MediatorLiveData()


    init {
        mediatorLiveNotificationResponse.addSource(
            repository.mutableLiveDataNotificationDetailsSOS
        ) { t -> mediatorLiveNotificationResponse.postValue(t) }
        mediatorLiveNotificationResponseError.addSource(
            repository.mutableLiveDataNotificationDetailsSOSError
        ) { t -> mediatorLiveNotificationResponseError.postValue(t) }

        mediatorLiveNotificationListRes.addSource(
            repository.mutableLiveDataNotificationsList
        ) { t -> mediatorLiveNotificationListRes.postValue(t) }
        mediatorLiveNotificationListResError.addSource(
            repository.mutableLiveDataNotificationsListError
        ) { t -> mediatorLiveNotificationListResError.postValue(t) }

    }

    fun getNotification(){
        repository.getNotificationSOSMessage()
    }

    fun getNotificationList(type:String){
        repository.getNotificationsList(type)
    }

}
