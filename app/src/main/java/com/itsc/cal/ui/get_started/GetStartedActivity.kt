package com.itsc.cal.ui.get_started

import android.content.Intent
import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants.USER_ROLE
import com.itsc.cal.databinding.ActivityGetStartedBinding
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.login.LoginActivity
import com.itsc.cal.util.UiUtil
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class GetStartedActivity : CalSuperActivity(), KodeinAware {
    private lateinit var binding: ActivityGetStartedBinding
    private val repository: Repository by instance()
    override val kodein: Kodein by kodein()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_get_started)
        binding.context = this
        showAppLogoWithBackGetStartedButton()
        initview()
    }

    private fun initview() {
        val user_role = intent.getStringExtra(USER_ROLE)

        if (repository.getUserData()?.user_role.equals("2") || (user_role?.equals("2") == true)) {
            binding.textMessage.text = "Contractor/Builder account created successfully"
        } else if (repository.getUserData()?.user_role.equals("3") || (user_role?.equals("3") == true)) {
            binding.textMessage.text = "Material Manufacture / Supplier account created successfully"
        } else if (repository.getUserData()?.user_role.equals("4") || (user_role?.equals("4") == true)) {
            binding.textMessage.text = "Independent Truck/Fleet  account created successfully"
        }
    }

    fun clickedGetStarted() {
        repository.clearAllData()
        startActivity(
            UiUtil.clearStackAndStartNewActivity(
                Intent(
                    this,
                    LoginActivity::class.java
                )
            )
        )
        finish()
    }
}