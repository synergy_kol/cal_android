package  com.itsc.cal.ui.builders_details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.itsc.cal.repository.Repository

class ViewModelProvider(
    private val repository: Repository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ViewModel(repository) as T
    }
}