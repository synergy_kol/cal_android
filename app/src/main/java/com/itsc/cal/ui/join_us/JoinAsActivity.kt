package com.itsc.cal.ui.join_us

import android.content.Intent
import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityJoinAsBinding
import com.itsc.cal.ui.signup.SignUpActivity

class JoinAsActivity : CalSuperActivity() {
    private lateinit var binding: ActivityJoinAsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_join_as)
        showAppLogoWithBackButton()
        initView()
    }

    private fun initView() {
        binding.buttonCotractureBuilder.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    SignUpActivity::class.java
                ).putExtra(KeywordsAndConstants.SIGNUP_TYPE, KeywordsAndConstants.CONTRATOR_BUILDER)
            )
        }

        binding.buttonIndependentRegister.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    SignUpActivity::class.java
                ).putExtra(KeywordsAndConstants.SIGNUP_TYPE, KeywordsAndConstants.INDEPENDENT_TRUCK)
            )
        }
        binding.buttonMeterialSupplier.setOnClickListener {

            startActivity(
                Intent(
                    this,
                    SignUpActivity::class.java
                ).putExtra(KeywordsAndConstants.SIGNUP_TYPE, KeywordsAndConstants.MATERIAL_MANUFACTURER)
            )
        }
    }
}