package com.itsc.cal.ui.sos_document_thank_you

import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivitySOSDocumentThankYouBinding

class SOSDocumentThankYouActivity : CalSuperActivity() {
    private lateinit var binding:ActivitySOSDocumentThankYouBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_s_o_s_document_thank_you)
        showASOSDocumentSubmitedActionbar()
    }
}