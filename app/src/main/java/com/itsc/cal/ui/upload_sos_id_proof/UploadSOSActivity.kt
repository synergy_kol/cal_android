package com.itsc.cal.ui.upload_sos_id_proof

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import com.fxn.pix.Pix
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySOSDocumentThankYouBinding
import com.itsc.cal.databinding.ActivityUploadSOSBinding
import com.itsc.cal.model.SOSDocumentResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import permissions.dispatcher.*
import java.io.File

@RuntimePermissions
class UploadSOSActivity : CalSuperActivity(), KodeinAware {
    private lateinit var binding: ActivityUploadSOSBinding
    private var imageFilePath: File? = null
    private var image: String = ""
    private lateinit var sosDocumentResponse: SOSDocumentResponse
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_upload_s_o_s)
        enableBackButton()
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        setPageTitle("")
        showASOSDocumentSubmitedActionbar()
        initView()
        bindToViewModel()
    }

    private fun initView() {
        intent?.let {
            sosDocumentResponse = fromJson(
                it.getStringExtra(KeywordsAndConstants.DATA)
            )
        }
        if (sosDocumentResponse.document_image.equals("")){
            binding.imgProfileId.setImage(imageDrawable = resources.getDrawable(R.drawable.upload_big))
        }else{
            binding.imgProfileId.setImage(imageUrl = "${KeywordsAndConstants.BASE_URL}${sosDocumentResponse.document_image}")
        }

        setPageTitle(sosDocumentResponse.document_title?.replace("*","") ?: "")
        if (sosDocumentResponse.document_status.equals("U")) {
            binding.textMessageTitle.text = "Please upload your ${sosDocumentResponse.document_title?.replace("*","") ?: ""}"
            binding.btnUpload.visibility = View.VISIBLE
            binding.imageClose.visibility = View.GONE
        } else if (sosDocumentResponse.document_status.equals("P")) {
            binding.textMessageTitle.text = "Waiting for Approval"
            binding.btnSubmit.visibility = View.GONE
            binding.btnUpload.visibility = View.VISIBLE
            binding.btnUpload.text ="Retake"
            binding.imageClose.visibility = View.GONE
        } else if (sosDocumentResponse.document_status.equals("A")) {
            binding.textMessageTitle.text = "Successfully verified"
            binding.btnSubmit.visibility = View.GONE
            binding.btnUpload.visibility = View.GONE
            binding.imageClose.visibility = View.GONE
        } else if (sosDocumentResponse.document_status.equals("R")) {
            binding.textMessageTitle.text =
                "Document has been rejected by admin,You can upload again"
            binding.btnUpload.visibility = View.VISIBLE
            binding.btnUpload.text ="Retake"
            binding.imageClose.visibility = View.VISIBLE
        }

        binding.btnUpload.setOnClickListener {
            initiateCaptureForProfilePicture()
        }
        binding.btnSubmit.setOnClickListener {
            if (sosDocumentResponse.document_status.equals("R")){
                updateSOS()
            }else{
                sosSubmit()
            }

        }
    }

    private fun sosSubmit() {
        showProgress()
        viewModel.submitSOS(imageFilePath, sosDocumentResponse.document_type ?: "")
    }
    private fun updateSOS(){
        showProgress()
        viewModel.updateSOS(imageFilePath, sosDocumentResponse.document_id ?: "")
    }

    private fun thankYouDialog() {
        val dialogBinding = ActivitySOSDocumentThankYouBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        val customDialog = Dialog(this)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(dialogBinding.root)
        customDialog.window
            ?.setLayout(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.MATCH_PARENT
            )

        customDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        customDialog.setCancelable(true)
        customDialog.show()

        dialogBinding.imgDone.setOnClickListener {
            customDialog.dismiss()
            finish()
        }

    }

    fun initiateCaptureForProfilePicture() {
        takeProfilePictureWithPermissionCheck()
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataSubmitSOS.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        thankYouDialog()
                        it.readContent()
                    }
                }
            }
        )
        viewModel.mediatorLiveDataSubmitSOSError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataUpdateSOS.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        thankYouDialog()
                        it.readContent()
                    }
                }
            }
        )
        viewModel.mediatorLiveDataUpdateSOSError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

    }


    @NeedsPermission(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun takeProfilePicture() {
        ImagePickerUtil.pickImage(
            context = this,
            reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
        )
    }

    @OnShowRationale(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun showRationale(request: PermissionRequest) {
        showMessageWithTwoButton(
            message = "We need some permission to take pictures.",
            buttonOneText = "Okay",
            buttonTwoText = "Cancel",
            callback = object : DialogUtil.MultiButtonCallBack {
                override fun buttonOneClicked() {
                    super.buttonOneClicked()
                    request.proceed()
                }

                override fun buttonTwoClicked() {
                    super.buttonTwoClicked()
                    request.cancel()
                    finish()
                }
            }
        )
    }

    @OnPermissionDenied(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun permissionDenied() {
        showMessageWithOneButton(
            "You will not be able to take picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    @OnNeverAskAgain(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun neverAskAgain() {
        showMessageWithOneButton(
            "You will not be able to take picture picture. you can again accept the permission by going directly to permission section in settings.",
            object : DialogUtil.CallBack {
                override fun dialogCancelled() {

                }

                override fun buttonClicked() {
                    finish()
                }

            }
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)
        if (requestCode == KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            intentData?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    //Log.i("got image $images")
                    imageFilePath = File(images[0])
                    binding.imgProfileId.setImage(
                        File(images[0]),
                        isCircularImage = false,
                        needBorderWithCircularImage = true
                    )
                    binding.btnUpload.visibility = View.GONE
                    binding.btnSubmit.visibility = View.VISIBLE
                }
            }
        }
    }
}