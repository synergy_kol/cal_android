package com.itsc.cal.ui.sos_section

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityAlertBinding
import com.itsc.cal.databinding.ActivitySosSectionListBinding
import com.itsc.cal.model.StatusReply
import com.itsc.cal.model.notificationList
import com.itsc.cal.ui.alert.adapter.AlertAdapter
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.generic.instance

class SOS_SectionListActivity : CalSuperActivity() {

    private lateinit var binding: ActivitySosSectionListBinding
    private lateinit var notificationList: ArrayList<notificationList>
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var isFromDashBord: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_sos_section_list)
        enableBackButton()
        setPageTitle("SOS Section")
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)


        showAlertActionbar()
        notificationRead()
        initView()
        bindToViewModel()
    }

    private fun notificationRead() {
        showProgress()
        viewModel.notificationRead()
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveNotificationRead.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )
        viewModel.mediatorLiveNotificationReadError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()
                    }

                }
            }
        )

    }


    private fun initView() {
        intent.let {
            notificationList = fromJson(intent.getStringExtra(KeywordsAndConstants.DATA))
            isFromDashBord = intent.getBooleanExtra(KeywordsAndConstants.IS_FORM_DASHBOARD, false)
            setAdapter(notificationList)
            if (isFromDashBord){
                setPageTitle("Alert")
            }else{
                setPageTitle("Msg Log")
            }
        }
    }

    private fun setAdapter(notificationList: ArrayList<notificationList>) {
        binding.recycleViewBuilderDetails.apply {
            layoutManager = LinearLayoutManager(this@SOS_SectionListActivity)
            adapter = AlertAdapter(this@SOS_SectionListActivity, notificationList)
        }
    }
}