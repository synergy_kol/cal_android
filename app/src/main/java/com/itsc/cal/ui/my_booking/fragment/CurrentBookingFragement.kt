package com.itsc.cal.ui.my_booking.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.application.ApplicationClass
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.CurrentBookingFragementBinding

import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse
import com.itsc.cal.ui.job_details.JobDetails
import com.itsc.cal.ui.my_booking.MyBookingActivity
import com.itsc.cal.ui.my_booking.adapter.CurrentBookingAdapter
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class CurrentBookingFragement : Fragment(), KodeinAware {

    private lateinit var binding: CurrentBookingFragementBinding
    private lateinit var mCurrentBookingAdapter: CurrentBookingAdapter
    private lateinit var baseActivity: MyBookingActivity
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var filter = ""
    override lateinit var kodein: Kodein

    companion object {
        fun newInstance(filter: String): CurrentBookingFragement {
            val currentBookingFrag = CurrentBookingFragement()
            val bundle = Bundle()
            bundle.putString("filter", filter)
            currentBookingFrag.arguments = bundle
            return currentBookingFrag
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.current_booking_fragement, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        baseActivity = activity as MyBookingActivity
        kodein = (baseActivity.applicationContext as ApplicationClass).kodein
        viewModel = androidx.lifecycle.ViewModelProvider(
            this,
            viewModelProvider
        )
            .get(
                ViewModel::class.java
            )


        bindToViewModel()

    }

    public fun getJobListing(filter: String) {
        (baseActivity ).showProgress()
        this.filter = filter
        if (filter.equals("4")) {
            viewModel.getJobListCurrent(filter)
        } else {
            viewModel.getJobListPast(filter)
        }
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveJobListingCurrent.observe(
            baseActivity,
            Observer<Event<ArrayList<JobListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        val listToPush: ArrayList<JobListResponse> = ArrayList()
                        it.getContent()!!.forEach { job ->
                            if (job.filter == filter) {
                                listToPush.add(job)
                            }
                        }
                        (baseActivity ).hideProgress()
                        setCurrentBookingAdapter(listToPush)
                    }

                }
            }
        )
        viewModel.mediatorLiveJobListingCurrentError.observe(
            baseActivity,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent() && filter != "") {
                        (baseActivity as MyBookingActivity).hideProgress()
                        if (it.getContent()!!.getErrorCode() == filter.toInt()) {
                            binding.recycleviewCurrentBooking.visibility = View.GONE
                            binding.textNoDataFound.visibility = View.VISIBLE
                        }
                    }

                }
            }


        )

        viewModel.mediatorLiveJobListingPast.observe(
            baseActivity,
            Observer<Event<ArrayList<JobListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        val listToPush: ArrayList<JobListResponse> = ArrayList()
                        it.getContent()!!.forEach { job ->
                            if (job.filter == filter) {
                                listToPush.add(job)
                            }
                        }
                        (baseActivity ).hideProgress()
                        setCurrentBookingAdapter(listToPush)
                    }

                }
            }
        )
        viewModel.mediatorLiveJobListingPastError.observe(
            baseActivity,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent() && filter != "") {
                        (baseActivity ).hideProgress()
                        if (it.getContent()!!.getErrorCode() == filter.toInt()) {
                            binding.recycleviewCurrentBooking.visibility = View.GONE
                            binding.textNoDataFound.visibility = View.VISIBLE
                        }
                    }

                }
            }
        )


    }


    private fun setCurrentBookingAdapter(arrayList: ArrayList<JobListResponse>) {
        if (arrayList.size > 0) {
            binding.textNoDataFound.visibility = View.GONE
            mCurrentBookingAdapter =
                CurrentBookingAdapter(baseActivity, object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        when (view.id) {
                            R.id.cl_view -> {
                                startActivity(
                                    Intent(baseActivity, JobDetails::class.java).putExtra(
                                        KeywordsAndConstants.DATA,
                                        baseActivity.toJson(arrayList.get(position))
                                    ).putExtra(KeywordsAndConstants.isFromBooking,true)
                                )
                            }
                        }
                    }

                }, arrayList)
            val layoutManager =
                LinearLayoutManager(baseActivity, LinearLayoutManager.VERTICAL, false)
            binding.recycleviewCurrentBooking.layoutManager = layoutManager
            binding.recycleviewCurrentBooking.adapter = mCurrentBookingAdapter
        } else {
            binding.textNoDataFound.visibility = View.VISIBLE
        }

    }


}