package com.itsc.cal.ui.my_booking.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildMyBookigListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse
import com.itsc.cal.util.AndroidUtility

class CurrentBookingAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener,val arrayList: ArrayList<JobListResponse>):
    RecyclerView.Adapter<CurrentBookingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(ChildMyBookigListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))


    }

    inner class ViewHolder(val binding: ChildMyBookigListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(jobListResponse:JobListResponse) {

            val desDateTime = AndroidUtility.formatDateFromString(
                "yyyy-MM-dd hh:mm:ss",
                "dd MMM yyyy , hh:mm:ss a",
                jobListResponse.delivery_date+" "+jobListResponse.delivery_time
            )
            binding.textDateTime.text = desDateTime
            binding.textTruckName.text = jobListResponse.truck_name
            binding.textSource.text = jobListResponse.Source
            binding.textDestination.text = jobListResponse.Destination
            binding.btnStartTracking.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            binding.clView.setOnClickListener {
                onItemClickListener.onItemClick(it,adapterPosition)
            }
        }

    }

}