package com.itsc.cal.ui.paymentsettings

import android.content.Intent
import android.os.Bundle
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityPaySettingsBinding
import com.itsc.cal.model.PayoutSettingResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
class PayoutSettingsActivity: CalSuperActivity(), KodeinAware {
    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPaySettingsBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private var mPayoutSettingResponse:PayoutSettingResponse?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_pay_settings)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        enableBackButton()
        showHelpButton()
        setPageTitle("Payment Settings")
        initView()
        bindingViewModel()
    }

    override fun onResume() {
        super.onResume()
        showProgress()
        viewModel.getPayoutSettings()
    }

    private fun initView() {
        binding.apply {
            llBankAccount.setOnClickListener {
                val intent = Intent(this@PayoutSettingsActivity, PayoutSetBankAccountActivity::class.java)
                intent.putExtra("payout_setting",mPayoutSettingResponse)
                startActivity(intent)
            }
            llBankCard.setOnClickListener {
                val intent = Intent(this@PayoutSettingsActivity, PayoutSetCardActivity::class.java)
                intent.putExtra("payout_setting",mPayoutSettingResponse)
                startActivity(intent)
            }
        }
    }
    private fun bindingViewModel() {
        viewModel.mediatorLivePayoutSettings.observe(
            this,
            androidx.lifecycle.Observer<Event<PayoutSettingResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()){
                        hideProgress()
                        showData(it.getContent())
                    }

                }
            }
        )
        viewModel.mediatorLivePayoutSettingsError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun showData(payoutSettingResponse: PayoutSettingResponse?) {
        mPayoutSettingResponse = payoutSettingResponse
        binding.apply {
            if(payoutSettingResponse?.account!=null) {
                tvBankAccountNo.text =
                    "Bank Account - xxxxxxx${payoutSettingResponse?.account?.substring((payoutSettingResponse.account?.length ?: 0) - 4)}"
                tvBankAccountNo2.text =
                    "Bank Account - xxxxxxx${payoutSettingResponse?.account?.substring((payoutSettingResponse.account?.length ?: 0) - 4)}"
            }else{
                tvBankAccountNo.text =
                    "Bank Account - xxxxxxx"
                tvBankAccountNo2.text =
                    "Bank Account - xxxxxxx"
            }
            if(payoutSettingResponse?.card_no!=null) {
                tvBankCard.text =
                    "Card No -  xxxx xxxx xxxx ${payoutSettingResponse?.card_no?.substring((payoutSettingResponse.card_no?.length ?: 0) - 4)}"
            }else{
                tvBankCard.text =
                    "Card No -  xxxx xxxx xxxx"
            }
        }
    }
}