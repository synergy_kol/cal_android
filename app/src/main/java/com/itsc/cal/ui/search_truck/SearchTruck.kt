package com.itsc.cal.ui.search_truck

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivitySearchTrucksBinding
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.search_truck_list.SearchTruckListActivity
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.spinner.Spinner
import com.itsc.cal.widget.spinner.SpinnerData
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SearchTruck : CalSuperActivity(withNav = true), KodeinAware {
    override val kodein: Kodein by kodein()
    private val repository: Repository by instance()
    private lateinit var binding: ActivitySearchTrucksBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var truckDataResponse: TruckDataResponse
    private var fleetType = "F"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_search_trucks)
        showDashboardActionbarWithBackButton()
        enableBackButton()
        showHelpButton()
        setPageTitle("Search Trucks")
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        initView()
        getTruckListData()
        bindToViewModel()
    }

    private fun getTruckListData() {
        showProgress()
        viewModel.getTruckData()
    }

    private fun initView() {

        binding.radioButtonFleet.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                fleetType = "F"
            } else {
                fleetType = "I"
            }
        }
        binding.tvNext.setOnClickListener {
            searchTruckAPICalled()
        }
    }

    private fun searchTruckAPICalled() {
        showProgress()
        viewModel.searchTruckList(
            truck_type = truckDataResponse.truck_type_id,
            fleet_type = fleetType
        )
    }

    private fun bindToViewModel() {

        viewModel.mediatorLiveSearchTruckData.observe(
            this,
            Observer<Event<ArrayList<SearchTruckListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        moveToNext(it.getContent() ?: ArrayList())
                    }
                }
            }
        )
        viewModel.mediatorLiveSearchTruckDataError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )



        viewModel.mediatorLiveTruckData.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<TruckDataResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        val items: ArrayList<SpinnerData<TruckDataResponse>> = ArrayList()
                        it.getContent()!!.forEach { source ->
                            items.add(
                                SpinnerData(
                                    source.name,
                                    source
                                )
                            )
                        }
                        if (items.size > 0)
                            truckDataResponse = items[0].data
                        binding.spinnerTruckType.setBg()
                        binding.spinnerTruckType.addItems(
                            items,
                            withDot = false,
                            listener = object : Spinner.OnItemSelectedListener {
                                override fun <T> selected(item: SpinnerData<T>) {
                                    truckDataResponse = item.data as TruckDataResponse


                                }
                            }
                        )

                    }
                }
            }
        )

        viewModel.mediatorLiveTruckDataError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

    }

    private fun moveToNext(arrayList: ArrayList<SearchTruckListResponse>) {
        startActivity(
            Intent(this, SearchTruckListActivity::class.java).putExtra(
                KeywordsAndConstants.DATA,
                toJson(arrayList)
            )
        )
    }
}