package com.itsc.cal.ui.driver_access_point

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityDriverAccessPointBinding
import com.itsc.cal.model.DriverJob
import com.itsc.cal.ui.driver_job_details.DriverJobDetailsActivity
import com.itsc.cal.widget.edittext.EditTextInputMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class DriverAccessPoint : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()

    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel
    private lateinit var binding: ActivityDriverAccessPointBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_driver_access_point)
        binding.context = this

        showAppLogoWithBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        initView()
        bindToViewModel()
    }
    private fun initView(){
        binding.editVerificationCode.apply {
            inputMode(EditTextInputMode.NUMBER)
        }
    }

    private fun bindToViewModel() {
        viewModel.mediatorLiveDataDriverJobByToken.observe(
            this,
            Observer {
                if (it.shouldReadContent()) {
                    hideProgress()
                    movedToNextPage(it.getContent()!!)
                }
            }
        )
        viewModel.mediatorLiveDataDriverJobByTokenError.observe(
            this,
            Observer {
                if (it.shouldReadContent()) {
                    handleGenericResult(it.getContent()!!)
                }
            }
        )
    }

    fun getJob() {
        if (binding.editVerificationCode.getText().isEmpty()) {
            showMessageInDialog("Please provide code")
            return
        }

        showProgress()
        viewModel.getDriverJobByToken(binding.editVerificationCode.getText())
    }

    private fun movedToNextPage(driverJob: DriverJob) {
        // add job status validration
        startActivity(
            Intent(this, DriverJobDetailsActivity::class.java).putExtra(
                KeywordsAndConstants.DATA,
                toJson(driverJob)
            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP and Intent.FLAG_ACTIVITY_NEW_TASK)
        )
    }
}