package  com.itsc.cal.ui.end_trip

import androidx.lifecycle.MediatorLiveData
import com.itsc.cal.model.*
import com.itsc.cal.repository.Repository
import com.itsc.cal.util.SuperViewModel
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import java.io.File


class ViewModel(private val repository: Repository) : SuperViewModel(repository) {

    val mediatorLiveEndTrip: MediatorLiveData<Event<StatusReply>> =
        MediatorLiveData()
    val mediatorLiveEndTripError: MediatorLiveData<Event<Result>> =
        MediatorLiveData()

    init {
        mediatorLiveEndTrip.addSource(
            repository.mutableLiveDataEndTrip
        ) { t -> mediatorLiveEndTrip.postValue(t) }
        mediatorLiveEndTripError.addSource(
            repository.mutableLiveDataEndTripError
        ) { t -> mediatorLiveEndTripError.postValue(t) }


    }

    fun endTrip(
        receiver_name: String,
        token_id: String,
        driver_job_status: String,
        receiver_nric: String,
        signature:File,
        pod:File?
    ) =
        repository.endTrip(
            receiver_name = receiver_name,
            driver_job_status = driver_job_status,
            token_id = token_id,
            receiver_nric =receiver_nric,
            signature = signature,
            pod = pod
        )


}