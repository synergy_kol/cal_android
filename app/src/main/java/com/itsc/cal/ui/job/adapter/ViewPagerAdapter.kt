package com.itsc.cal.ui.job.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPagerAdapter(fm : FragmentManager, val fragmentList : List<Fragment>) : FragmentStatePagerAdapter(fm) {


    private val titleList = arrayListOf("Upcoming","Assigned","Past","Cancelled")

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }


}