package com.itsc.cal.ui.sos_section.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.ChildAlertBinding
import com.itsc.cal.databinding.ChildSosSectionBinding
import com.itsc.cal.model.notificationList

class SOSsectionAdapter (val mContext: Context, val notificationList:ArrayList<notificationList>):
    RecyclerView.Adapter<SOSsectionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ChildSosSectionBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(notificationList.get(position))
    }

    inner class ViewHolder(val binding: ChildSosSectionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(notificationList:notificationList) {
            binding.textDateTime.text = notificationList.created_ts
            binding.textMessageDetails.text = notificationList.notification_description
            binding.textNotiTitle.text = notificationList.notification_title
            if (notificationList.alert_type.equals("1")){
                binding.cardBg.setBackgroundColor(Color.parseColor("#ff5c5c"))
            }else if (notificationList.alert_type.equals("2")){
                binding.cardBg.setBackgroundColor(Color.parseColor("#ffd152"))
            }else if (notificationList.alert_type.equals("3")){
                binding.cardBg.setBackgroundColor(Color.parseColor("#b8beff"));
            }
        }

    }

}