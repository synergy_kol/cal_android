package com.itsc.cal.ui.invoice_listing

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.databinding.ActivityInvoiceListingBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.ui.invoice_listing.adapter.InvoiceListAdapter

class InvoiceListingActivity : CalSuperActivity(withNav = true) {
    private lateinit var binding:ActivityInvoiceListingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_invoice_listing)
        enableBackButton()
        setPageTitle("Invoices")
        showHelpButton()
        initView()
    }
    private fun initView(){
        binding.recycleviewInvoiceListing.apply {
            layoutManager = LinearLayoutManager(this@InvoiceListingActivity)
            adapter = InvoiceListAdapter(this@InvoiceListingActivity,object :OnItemClickListener{
                override fun onItemClick(view: View, position: Int) {

                }

            })
        }
    }
}