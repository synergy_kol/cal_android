package com.itsc.cal.ui.job.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itsc.cal.databinding.AdapterPostedJobBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.JobListResponse

class PostedJobAdapter (val mContext: Context, val onItemClickListener: OnItemClickListener,val arrayList: ArrayList<JobListResponse>):
    RecyclerView.Adapter<PostedJobAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(AdapterPostedJobBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }
    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(arrayList.get(position))

    }

    inner class ViewHolder(val binding: AdapterPostedJobBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(jobListResponse: JobListResponse) {
            binding.tvSource.text = jobListResponse.Source
            binding.tvJobId.text="#CAL${jobListResponse.job_id}"
            binding.tvDestination.text=jobListResponse.Destination
            binding.tvWeight.text = "${jobListResponse.Weight} QT"
            binding.tvDate.text =jobListResponse.pickup_date

            binding.tvEquipmentText.text =
                if (jobListResponse.Equipment_value == "null" || jobListResponse.Equipment_value=="")
                    "Equipment: No" else
                    "Equipment: ${jobListResponse.Equipment_value}"
            binding.tvEquipment.text = ""
            binding.tvMaterialTypeText.text = "Material: ${jobListResponse.material_name}"
            binding.textTruckName.text = jobListResponse.truck_name
            binding.tvTruckPayInText.text = "Freight Charges: $ ${jobListResponse.FreightCharges}"
            binding.tvTruckPayIn.text = "Estimate Price: $ ${jobListResponse.JobEstimatePrice}"
            binding.cvPostedJob.setOnClickListener{
                onItemClickListener.onItemClick(it,adapterPosition)
            }
            if (jobListResponse.job_status.equals("3")){
                binding.btnReview.visibility = View.VISIBLE
            }else{
                binding.btnReview.visibility = View.GONE
            }
            binding.btnReview.setOnClickListener {
                onItemClickListener.onItemClick(it,adapterPosition)
            }

        }

    }

}