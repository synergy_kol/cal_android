package com.itsc.cal.ui.card_list

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.PopupWindow
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityCardListBinding
import com.itsc.cal.databinding.OptionsPopUpCardListBinding
import com.itsc.cal.interfaces.OnItemClickListener
import com.itsc.cal.model.CardListResponse
import com.itsc.cal.model.StatusReply
import com.itsc.cal.ui.card_list.adapter.CardListAdapter
import com.itsc.cal.ui.payment_method.PaymentMethodActivity
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class CardListActivity : CalSuperActivity(), KodeinAware {
    private lateinit var binding: ActivityCardListBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_card_list)
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        enableBackButton()
        setPageTitle("Manage Card")
        showHelpButton()
        initView()
        bindViewHolder()
    }

    private fun initView() {
        binding.buttonAddNewCard.setOnClickListener {
            startActivity(
                Intent(this, PaymentMethodActivity::class.java).putExtra(
                    KeywordsAndConstants.isEditCard,
                    false
                )
            )
        }
    }

    override fun onResume() {
        super.onResume()
        getCardList()
    }

    private fun getCardList() {
        showProgress()
        viewModel.getCardList()
    }


    private fun bindViewHolder() {

        viewModel.mediatorLiveCardListing.observe(
            this,
            androidx.lifecycle.Observer<Event<ArrayList<CardListResponse>>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        binding.recycleViewCardList.visibility = View.VISIBLE
                        setAdapter(it.getContent() ?: ArrayList())
                        binding.textNoCardFound.visibility = View.GONE
                    }
                }
            }
        )

        viewModel.mediatorLiveCardListingError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                        binding.textNoCardFound.visibility = View.VISIBLE
                        binding.recycleViewCardList.visibility = View.GONE
                    }

                }
            }
        )

        viewModel.mediatorLiveMarkAsDefalut.observe(
            this,
            androidx.lifecycle.Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveMarkAsDefalutError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveDeleteCard.observe(
            this,
            androidx.lifecycle.Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processResponse(it.getContent()!!.message)
                }
            }
        )
        viewModel.mediatorLiveDeleteCardError.observe(
            this,
            androidx.lifecycle.Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = message,
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()
                    getCardList()

                }
            }
        )

    }

    private fun setAdapter(arrayList: ArrayList<CardListResponse>) {
        binding.recycleViewCardList.apply {
            layoutManager = LinearLayoutManager(this@CardListActivity)
            adapter = CardListAdapter(this@CardListActivity, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    when (view.id) {
                        R.id.img_menu -> {
                            showMore(view, arrayList.get(position))
                        }
                        R.id.img_set_default -> {
                            if (arrayList.get(position).is_default.equals("0")){
                                showProgress()
                                viewModel.marksAsDefalut(arrayList.get(position).card_id)
                            }

                        }

                    }
                }

            }, arrayList)
        }
        if (arrayList.size==1 && arrayList.get(0).is_default.equals("0")){
            showProgress()
            viewModel.marksAsDefalut(arrayList.get(0).card_id)
        }
    }

    fun showMore(view: View, cardListResponse: CardListResponse) {
        val popupWindow = PopupWindow(this)
        val popUpBinding = OptionsPopUpCardListBinding.inflate(
            LayoutInflater.from(this),
            null,
            false
        )
        popupWindow.contentView = popUpBinding.root
        popupWindow.isOutsideTouchable = true
        popupWindow.setBackgroundDrawable(
            ColorDrawable(
                ResourcesCompat.getColor(
                    resources,
                    android.R.color.transparent,
                    null
                )
            )
        )

        popUpBinding.textViewDelete.setOnClickListener {

            showMessageWithTwoButton(
                message = "Are you sure to delete Card?",
                buttonOneText = "Yes",
                buttonTwoText = "No",
                callback = object : DialogUtil.MultiButtonCallBack {
                    override fun buttonOneClicked() {
                        popupWindow.dismiss()
                        showProgress()
                        viewModel.deleteCard(cardListResponse.card_id)

                    }

                    override fun buttonTwoClicked() {
                        super.buttonTwoClicked()
                        popupWindow.dismiss()
                    }
                }
            )

        }
        popUpBinding.textViewEdit.setOnClickListener {
            popupWindow.dismiss()
            startActivity(
                Intent(this, PaymentMethodActivity::class.java).putExtra(
                    KeywordsAndConstants.isEditCard,
                    true
                ).putExtra(KeywordsAndConstants.DATA, toJson(cardListResponse))
            )
        }

        popupWindow.showAsDropDown(view)

    }
}