package com.itsc.cal.ui.post_hault_of_job_list.post_export_job_two

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.fxn.pix.Pix
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityPostExportStepTwoBinding
import com.itsc.cal.model.*
import com.itsc.cal.ui.post_hault_of_job_list.post_export_job_three.PostExportJobThreeActivity
import com.itsc.cal.util.DialogUtil
import com.itsc.cal.util.ImagePickerUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.itsc.cal.widget.spinner.Spinner
import com.itsc.cal.widget.spinner.SpinnerData
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import kotlin.collections.ArrayList


class PostExportJobTwoActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private lateinit var binding: ActivityPostExportStepTwoBinding
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var viewModel: ViewModel

    companion object {
        var postATruckPage: Activity? = null
    }

    private lateinit var materialResponse: MaterialResponse
    private var material_option: MaterialOptionResponse = MaterialOptionResponse("", "", "", "")
    private var sizeMeasureMents = "C"
    private var haul_request_id = ""
    private var importExport = ""
    private var specsDocumentFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_post_export_step_two)
        enableBackButton()
        setPageTitle("Step 2")
        showHelpButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)

        postATruckPage = this
        intiView()
        bindViewHolder()
        getMaerialEquipmentList()
    }

    private fun getMaerialEquipmentList() {
        showProgress()
        viewModel.getMaterialEquipmentList()
    }


    private fun bindViewHolder() {


        viewModel.mediatorLiveHaulJobExportStep2.observe(
            this,
            Observer<Event<StatusReply>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        processResponse(it.getContent()!!.message)
                    }

                }
            }
        )

        viewModel.mediatorLiveDataJobExportStep2Error.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

        viewModel.mediatorLiveMaterialEquipementResult.observe(
            this,
            Observer<Event<MaterialEquipmentResponse>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        setEquipmentList(it.getContent())
                    }

                }
            }
        )

        viewModel.mediatorLiveMaterialEquipementError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        it.readContent()

                    }

                }
            }
        )

    }

    private fun setEquipmentList(materialEquipmentResponse: MaterialEquipmentResponse?) {

        if (materialEquipmentResponse?.material_list != null) {

            val items: ArrayList<SpinnerData<MaterialResponse>> = ArrayList()
            (materialEquipmentResponse?.material_list.forEach { source ->
                items.add(
                    SpinnerData(
                        source.name.toString(),
                        source
                    )
                )
            })
            arrayListOf(
                SpinnerData(
                    "Other",
                    "Other"
                )
            )
            items.add(SpinnerData("Other", MaterialResponse("Other", "Other", "", "")))
            if (items.size > 0)
                materialResponse = items[0].data
            binding.SpinnerMaterialType.setBg()
            binding.SpinnerMaterialType.addItems(
                items,
                withDot = false,
                listener = object : Spinner.OnItemSelectedListener {
                    override fun <T> selected(item: SpinnerData<T>) {
                        materialResponse = item.data as MaterialResponse
                        val itemsOption: ArrayList<SpinnerData<MaterialOptionResponse>> =
                            ArrayList()
                        if (materialResponse.material_option?.size ?: 0 > 0) {
                            binding.SpinnerMaterialOption.visibility = View.VISIBLE
                            binding.textMaterialOption.visibility = View.VISIBLE
                            binding.editMaterialOther.visibility = View.GONE
                            (materialResponse.material_option?.forEach { source ->
                                itemsOption.add(
                                    SpinnerData(
                                        source.option_title.toString(),
                                        source
                                    )
                                )
                            })
                            if (itemsOption.size > 0)
                                material_option = itemsOption[0].data

                            binding.SpinnerMaterialOption.setBg()
                            binding.SpinnerMaterialOption.addItems(
                                itemsOption,
                                withDot = false,
                                listener = object : Spinner.OnItemSelectedListener {
                                    override fun <T> selected(item: SpinnerData<T>) {
                                        material_option = item.data as MaterialOptionResponse

                                    }
                                }
                            )
                        } else if (materialResponse.name.equals("Other", true)) {
                            binding.editMaterialOther.visibility = View.VISIBLE
                            binding.textMaterialOption.visibility = View.GONE
                            binding.SpinnerMaterialOption.visibility = View.GONE
                            material_option = MaterialOptionResponse("", "", "", "")
                        } else {
                            binding.SpinnerMaterialOption.visibility = View.GONE
                            binding.textMaterialOption.visibility = View.GONE
                            binding.editMaterialOther.visibility = View.GONE
                        }

                    }
                }
            )

        }

    }

    private fun intiView() {

        intent.let {
            haul_request_id = intent.extras?.getString(KeywordsAndConstants.DATA, "").toString()
            importExport =
                intent.extras?.getString(KeywordsAndConstants.IMPORT_EXPORT, "").toString()
            if (importExport.equals("E")) {
                setPageTitle("Add Haul-OFF Export Job- Step 2")
            } else {
                setPageTitle("Add Haul-OFF Import Job- Step 2")
            }
            Log.i("haul_request_id:", haul_request_id)
        }

        binding.editSizeMesurement.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString() != null && s.toString() != "") {
                    if (sizeMeasureMents.equals("C")) {
                        var tonsFloat = (binding.editSizeMesurement.getText().toString()
                            .trim()).toFloat() * 1.4f
                        var tons = tonsFloat.toString()
                        binding.testCublicTons.setText("(${tons} Tons)")
                    } else {
                        var cublicFloat = (binding.editSizeMesurement.getText().toString()
                            .trim()).toFloat() / 1.4f
                        var cublic = cublicFloat.toString()
                        binding.testCublicTons.setText("(${cublic} Cubic Yards)")
                    }
                }else{
                    binding.testCublicTons.setText("")
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        binding.imgVehicle.setImage(imageDrawable = resources.getDrawable(R.drawable.ic_camera))
        binding.imgVehicle.registerForOnClick {
            takePicture()
        }
        binding.btnPostATruck.setOnClickListener {
            moveToNext()
        }
        binding.radioButtonCubicYards.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                sizeMeasureMents = "C"
                binding.editSizeMesurement.setText("")
                binding.testCublicTons.setText("")

            } else {
                sizeMeasureMents = "T"
                binding.editSizeMesurement.setText("")
                binding.testCublicTons.setText("")

            }
        }
        binding.radioButtonTons.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                sizeMeasureMents = "T"
                binding.editSizeMesurement.setText("")
                binding.testCublicTons.setText("")

            } else {
                sizeMeasureMents = "C"
                binding.editSizeMesurement.setText("")
                binding.testCublicTons.setText("")

            }
        }


        binding.editMaterialOther.apply {
            hint("Enter other material")
            inputMode(EditTextInputMode.INPUT_TEXT)
        }

    }

    private fun moveToNext() {

        if (materialResponse.name.equals("Other") && binding.editMaterialOther.getText().toString().trim() ==""){
            showMessageInDialog("Please provide other Material Type.")
            return
        }

        if (sizeMeasureMents == "") {
            showMessageInDialog("Please select Size / Measurements type.")
            return
        }

        if (binding.editSizeMesurement.getText().toString().trim() == "") {
            showMessageInDialog("Please provide Size / Measurements.")
            return
        }

        if (binding.editSizeMesurement.getText().toString().trim().toFloat() == 0.0f) {
            showMessageInDialog("Please provide valid Size / Measurements.")
            return
        }

        var cublic = "0"
        var tons = "0"
        if (sizeMeasureMents.equals("C")) {
            cublic = binding.editSizeMesurement.getText().toString().trim()
            var tonsFloat =
                (binding.editSizeMesurement.getText().toString().trim()).toFloat() * 1.4f
            tons = tonsFloat.toString()
        } else {
            tons = binding.editSizeMesurement.getText().toString().trim()
            var cublicFloat =
                (binding.editSizeMesurement.getText().toString().trim()).toFloat() / 1.4f
            cublic = cublicFloat.toString()
        }

        if (importExport.equals("E")) {
            showProgress()
            viewModel.postExportJobStep2(
                haul_request_id = haul_request_id,
                MaterialType = materialResponse.material_id,
                MaterialoptionType = material_option.material_id,
                material_other = binding.editMaterialOther.getText().toString().trim(),
                cubic_yards = cublic,
                tons = tons,
                specsDocumentFile = specsDocumentFile
            )
        } else {
            var importJobPostStep2 = ImportJobPostStep2(
                haul_request_id = haul_request_id,
                MaterialType = materialResponse.material_id,
                MaterialoptionType = material_option.material_id,
                material_other = binding.editMaterialOther.getText().toString().trim(),
                cubic_yards = cublic,
                tons = tons,
                additional_notes = "",
                equipment_any = "",
                equipment_other = "",
                equipment_type = "",
                spreading_equipment_other = "",
                spreading_equipment_type = ""
            )

            startActivity(
                Intent(
                    this,
                    PostExportJobThreeActivity::class.java
                ).putExtra(KeywordsAndConstants.isPostJob, true)
                    .putExtra(KeywordsAndConstants.IMPORT_DATA_MODEL, toJson(importJobPostStep2))
                    .putExtra(KeywordsAndConstants.IMPORT_EXPORT, importExport)
                    .putExtra(KeywordsAndConstants.DATA, haul_request_id)
            )
        }

    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            data?.let {
                it.getStringArrayListExtra(Pix.IMAGE_RESULTS)?.let { images ->
                    specsDocumentFile = File(images[0])
                    binding.imgVehicle.setImage(File(images[0]))

                    /*vehicleImage =
                        VehicleImage(
                            images[0],
                            false
                        )*/
                }
            }
        }
    }

    private fun processResponse(message: String) {
        hideProgress()
        showMessageWithOneButton(
            message = "Haul-OFF Export Job- Step-2 added successfully",
            cancellable = true,
            buttonText = "OK",
            callback = object : DialogUtil.CallBack {
                override fun buttonClicked() {
                    super.buttonClicked()

                    startActivity(
                        Intent(
                            this@PostExportJobTwoActivity,
                            PostExportJobThreeActivity::class.java
                        ).putExtra(KeywordsAndConstants.DATA, haul_request_id)
                            .putExtra(KeywordsAndConstants.IMPORT_EXPORT, importExport)
                    )
                }
            }
        )

    }

    @SuppressLint("CheckResult")
    fun takePicture() {
        RxPermissions(this)
            .request(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .subscribe {
                if (!it) {
                    showMessageInDialog("Please accept permissions to take picture.")
                }

                ImagePickerUtil.pickImage(
                    context = this,
                    reqCode = KeywordsAndConstants.REQUEST_CODE_PICK_IMAGE
                )
            }
    }

}