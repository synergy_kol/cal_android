package com.itsc.cal.ui.truck_booking

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.*
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.core.content.edit
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.Projection
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityTruckBookingBinding
import com.itsc.cal.databinding.CustomMarkerDropBinding
import com.itsc.cal.databinding.CustomMarkerPickupBinding
import com.itsc.cal.interfaces.AsyncInterface
import com.itsc.cal.model.TruckStatusResponse
import com.itsc.cal.repository.Repository
import com.itsc.cal.ui.async.CreateRoute
import com.itsc.cal.ui.truck_booking.adapter.TruckBookingTrucksAdapter
import com.itsc.cal.util.WorkaroundMapFragment
import com.sagar.android.logutilmaster.LogUtil
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class TruckBooking : CalSuperActivity(), KodeinAware, AsyncInterface {

    override val kodein: Kodein by kodein()

    companion object {
        const val Job_ID = "jobId"
    }

    enum class SearchPlaceFor {
        FROM,
        TO
    }

    private lateinit var binding: ActivityTruckBookingBinding
    private val logUtil: LogUtil by instance()
    private val repository: Repository by instance()
    private lateinit var searchPlaceFor: SearchPlaceFor
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var map: GoogleMap
    private lateinit var truckStatusResponse: TruckStatusResponse
    private var isPolyLineVisible = false
    private lateinit var polyline: Polyline
    private var jobId = ""
    private lateinit var currentLocation: LocationForFirebase
    private lateinit var sourceLatLng: LatLng
    private lateinit var destinationLatLng: LatLng
    private var currentPositionMarker: Marker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_truck_booking)
        binding.context = this

        showActionbarWithBackButtonAndCall()

        enableBackButton()

        setPageTitle("Truck Booking")

        setUpList()

        getDataFromIntent()
    }

    @SuppressLint("CheckResult", "MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        RxPermissions(this)
            .request(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .subscribe {
                if (!it) {
                    finish()
                }

                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener { locationResult ->
                        locationResult?.let { _ ->
                            startMap()
                        }
                    }
            }
    }

    @SuppressLint("MissingPermission")
    private fun startMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync { googleMap ->
            googleMap?.let {
                map = it

                map.setOnMapLoadedCallback {
                    map.setPadding(0, 500, 0, 0)

                    sourceLatLng = LatLng(
                        truckStatusResponse.source_lat!!.toDouble(),
                        truckStatusResponse.source_long!!.toDouble()
                    )
                    destinationLatLng = LatLng(
                        truckStatusResponse.destination_lat!!.toDouble(),
                        truckStatusResponse.destination_long!!.toDouble()
                    )

                    map.addMarker(
                        MarkerOptions()
                            .title("source")
                            .position(
                                sourceLatLng
                            )
                            .icon(
                                BitmapDescriptorFactory.fromBitmap(
                                    getMarkerBitmapFromView(MarkerFor.PICK_UP)
                                )
                            )
                    )

                    map.addMarker(
                        MarkerOptions()
                            .title("destination")
                            .position(
                                destinationLatLng
                            )
                            .icon(
                                BitmapDescriptorFactory.fromBitmap(
                                    getMarkerBitmapFromView(MarkerFor.DROP)
                                )
                            )
                    )

                    map.animateCamera(
                        CameraUpdateFactory.newLatLngBounds(
                            LatLngBounds.builder()
                                .include(sourceLatLng)
                                .include(destinationLatLng)
                                .build(),
                            120
                        )
                    )

                    val mSupportMapFragment: WorkaroundMapFragment =
                        mapFragment as WorkaroundMapFragment
                    mSupportMapFragment.setListener(object :
                        WorkaroundMapFragment.OnTouchListener {
                        override fun onTouch() {
                            getContainer().requestDisallowInterceptTouchEvent(true);
                        }
                    })

                    CreateRoute(
                        this,
                        sourceLatLng,
                        destinationLatLng,
                        Color.BLUE
                    ).execute()

                    getTruckLiveLocationFromFirebase()
                }
            }
        }
    }

    fun startPlacesSearch(searchPlaceFor: SearchPlaceFor) {
        this.searchPlaceFor = searchPlaceFor

        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        val fields = listOf(Place.Field.ID, Place.Field.NAME)

        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .build(this)
        startActivityForResult(intent, KeywordsAndConstants.AUTOCOMPLETE_PLACE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == KeywordsAndConstants.AUTOCOMPLETE_PLACE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)

                        logUtil.logV(
                            """
                                place selected:
                                ${place.name}
                                ${place.address}
                            """.trimIndent()
                        )

                        when (searchPlaceFor) {
                            SearchPlaceFor.FROM -> {
                                binding.textViewFromText.text = place.name
                            }
                            SearchPlaceFor.TO -> {
                                binding.textViewToText.text = place.name
                            }
                        }
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setUpList() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(
                this@TruckBooking,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = TruckBookingTrucksAdapter(this@TruckBooking)
        }
        PagerSnapHelper().attachToRecyclerView(
            binding.recyclerView
        )
    }

    private enum class MarkerFor {
        PICK_UP,
        DROP
    }

    private fun getMarkerBitmapFromView(markerFor: MarkerFor): Bitmap? {
        when (markerFor) {
            MarkerFor.PICK_UP -> {
                val customMarkerPickupBinding =
                    CustomMarkerPickupBinding.inflate(
                        LayoutInflater.from(
                            this
                        )
                    )
                customMarkerPickupBinding.root.measure(
                    View.MeasureSpec.UNSPECIFIED,
                    View.MeasureSpec.UNSPECIFIED
                )
                customMarkerPickupBinding.root.layout(
                    0,
                    0,
                    customMarkerPickupBinding.root.measuredWidth,
                    customMarkerPickupBinding.root.measuredHeight
                )
                customMarkerPickupBinding.root.buildDrawingCache()
                val returnedBitmap = Bitmap.createBitmap(
                    customMarkerPickupBinding.root.measuredWidth,
                    customMarkerPickupBinding.root.measuredHeight,
                    Bitmap.Config.ARGB_8888
                )
                val canvas = Canvas(returnedBitmap)
                canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
                customMarkerPickupBinding.root.background?.draw(canvas)
                customMarkerPickupBinding.root.draw(canvas)
                return returnedBitmap
            }
            MarkerFor.DROP -> {
                val customMarkerDropBinding =
                    CustomMarkerDropBinding.inflate(
                        LayoutInflater.from(
                            this
                        )
                    )
                customMarkerDropBinding.root.measure(
                    View.MeasureSpec.UNSPECIFIED,
                    View.MeasureSpec.UNSPECIFIED
                )
                customMarkerDropBinding.root.layout(
                    0,
                    0,
                    customMarkerDropBinding.root.measuredWidth,
                    customMarkerDropBinding.root.measuredHeight
                )
                customMarkerDropBinding.root.buildDrawingCache()
                val returnedBitmap = Bitmap.createBitmap(
                    customMarkerDropBinding.root.measuredWidth,
                    customMarkerDropBinding.root.measuredHeight,
                    Bitmap.Config.ARGB_8888
                )
                val canvas = Canvas(returnedBitmap)
                canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
                customMarkerDropBinding.root.background?.draw(canvas)
                customMarkerDropBinding.root.draw(canvas)
                return returnedBitmap
            }
        }
    }

    private fun getDataFromIntent() {
        try {
            intent?.let {
                truckStatusResponse = fromJson(it.getStringExtra(KeywordsAndConstants.DATA)!!)
                jobId = it.getStringExtra(Job_ID)!!
                getLastLocation()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun viewNearestRoute(polylineOptionsList: ArrayList<PolylineOptions>) {
        try {
            if (isPolyLineVisible) {
                map.clear()
            }

            runOnUiThread {
                for (i in 0 until polylineOptionsList.size) {
                    map.addPolyline(polylineOptionsList[i])
                    polyline = map.addPolyline(polylineOptionsList[i])
                    isPolyLineVisible = true
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun viewNearestRouteErrorCallBack() {
        showMessageInDialog("Try again later")
    }

    data class LocationForFirebase(
        var dateTime: String,
        var jobID: String,
        var lat: String,
        var long: String
    ) {
        constructor() : this("", "", "", "")
    }

    private fun getTruckLiveLocationFromFirebase() {
        val truckId = truckStatusResponse.truck_id

        if (truckId == "0" || jobId == "0")
            return

        val key = "${truckId}_$jobId"

        logUtil.logV("key is $key")

        val ref = Firebase.database.reference.child("Location")

        var innerKey = ""

        val cal = Calendar.getInstance()

        ref.addValueEventListener(
            object : com.google.firebase.database.ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(snapshot: com.google.firebase.database.DataSnapshot) {
                    if (snapshot.hasChild(key)) {
                        logUtil.logV("has the child")
                        snapshot.child(key).children.forEach { children ->
                            innerKey = children.key!!
                            logUtil.logV(
                                "inner key is $innerKey"
                            )
                            currentLocation = children.getValue(LocationForFirebase::class.java)!!
                            logUtil.logV(
                                """
                                    location
                                    $currentLocation
                                """.trimIndent()
                            )

                            addCurrentMarker()
                        }
                    }
                }
            }
        )
    }

    private fun addCurrentMarker() {
        if (currentPositionMarker != null) {
            val lastLocation =
                repository.getSharedPref().getString(KeywordsAndConstants.LAST_TRUCK_LOCATION, "")
            if (lastLocation == "")
                return

            val source = LatLng(
                lastLocation!!.split(",")[0].toDouble(),
                lastLocation.split(",")[1].toDouble()
            )
            val dest = LatLng(
                currentLocation.lat.toDouble(),
                currentLocation.long.toDouble()
            )

            val dLon: Double = dest.longitude - source.longitude
            val y = Math.sin(dLon) * Math.cos(dest.latitude)
            val x =
                Math.cos(source.latitude) * Math.sin(dest.latitude) - Math.sin(source.latitude) * Math.cos(
                    dest.latitude
                ) * Math.cos(dLon)
            var brng = Math.toDegrees(Math.atan2(y, x))
            brng = 360 - (brng + 360) % 360

            if (brng === 356.0) brng = 0.0
            val toRotationFinal: Float = brng.toFloat()
            val handler = Handler()
            val start: Long = SystemClock.uptimeMillis()
            val proj: Projection = map.getProjection()
            val startPoint: Point = proj.toScreenLocation(source)
            val startLatLng: LatLng = proj.fromScreenLocation(startPoint)
            val duration: Long = 500

            val startRotation: Float = currentPositionMarker!!.rotation

            val interpolator = LinearInterpolator()

            handler.post(object : Runnable {
                override fun run() {
                    val elapsed: Long = SystemClock.uptimeMillis() - start
                    val t: Float = interpolator.getInterpolation(
                        elapsed.toFloat()
                                / duration
                    )
                    val lng: Double = t * dest.longitude + (1 - t) * startLatLng.longitude
                    val lat: Double = t * dest.latitude + (1 - t) * startLatLng.latitude
                    val rot = t * toRotationFinal + (1 - t) * startRotation
                    val bearing = if (-rot > 180) rot / 2 else rot
                    currentPositionMarker!!.position = LatLng(lat, lng)
                    currentPositionMarker!!.rotation = bearing
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16)
                    }
                }
            })

            return
        }

        currentPositionMarker = map.addMarker(
            MarkerOptions()
                .title("current position")
                .position(
                    sourceLatLng
                )
                .icon(
                    BitmapDescriptorFactory.fromResource(
                        R.drawable.up_arrow_truck_marker
                    )
                )
        )

        repository.getSharedPref()
            .edit {
                putString(
                    KeywordsAndConstants.LAST_TRUCK_LOCATION,
                    "${currentLocation.lat},${currentLocation.long}"
                )
            }
    }
}