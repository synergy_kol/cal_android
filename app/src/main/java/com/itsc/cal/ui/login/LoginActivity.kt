package com.itsc.cal.ui.login

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.util.Patterns
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.itsc.cal.R
import com.itsc.cal.core.CalSuperActivity
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.databinding.ActivityLoginBinding
import com.itsc.cal.model.UserDetails
import com.itsc.cal.ui.add_driver.AddDriverRegistrationActivity
import com.itsc.cal.ui.dashboard.Dashboard
import com.itsc.cal.ui.driver_access_point.DriverAccessPoint
import com.itsc.cal.ui.forgot_password.ForgotActivity
import com.itsc.cal.ui.join_us.JoinAsActivity
import com.itsc.cal.ui.paymentsettings.PayoutSettingsRegistrationActivity
import com.itsc.cal.ui.post_a_truck.PostATruckActivity
import com.itsc.cal.ui.signup.CompleteProfileActivity
import com.itsc.cal.ui.sos_document_submited.SOSDocumentSubmitedActivity
import com.itsc.cal.ui.two_step_verification.TwoStepVerificationActivity
import com.itsc.cal.ui.verification.VerificationActivity
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.widget.edittext.EditTextInputMode
import com.sagar.android.logutilmaster.LogUtil
import com.tbruyelle.rxpermissions2.RxPermissions
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import permissions.dispatcher.RuntimePermissions


class LoginActivity : CalSuperActivity(), KodeinAware {

    override val kodein: Kodein by kodein()
    private val viewModelProvider: ViewModelProvider by instance()
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: ViewModel
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    private val logUtil: LogUtil by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = setLayout(R.layout.activity_login)
        showAppLogoWithOutBackButton()
        viewModel = androidx.lifecycle.ViewModelProvider(this, viewModelProvider)
            .get(ViewModel::class.java)
        initView()
        bindToViewModel()
    }

    private fun initView() {
        getLastLocation()
        binding.editEmailMobile.apply {
            hint("Email")
            inputMode(EditTextInputMode.EMAIL)
        }
        binding.editPassword.apply {
            hint("Password")
            inputMode(EditTextInputMode.PASSWORD)
        }

        binding.buttonLogin.setOnClickListener {
            login()

        }
        binding.textForgotPassword.setOnClickListener {
            startActivity(Intent(this, ForgotActivity::class.java))
        }
        binding.textSignup.setOnClickListener {
            startActivity(Intent(this, JoinAsActivity::class.java))
        }
        binding.buttonDriverAccessPoint.setOnClickListener {
            driverAccessPoint()
        }
    }


    private fun bindToViewModel() {
        viewModel.mediatorLiveDataLogin.observe(
            this,
            Observer<Event<UserDetails>> { t ->
                t?.let {
                    if (it.shouldReadContent())
                        processLoginResponse(it.getContent()!!)
                }
            }
        )
        viewModel.mediatorLiveDataLoginError.observe(
            this,
            Observer<Event<Result>> { t ->
                t?.let {
                    if (it.shouldReadContent()) {
                        hideProgress()
                        handleGenericResult(it.getContent()!!)
                    }

                }
            }
        )

    }

    private fun processLoginResponse(userDetails: UserDetails) {
        hideProgress()
        if (userDetails.verification_status.equals("1")) {
            if (userDetails.is_trusted.equals("1")) {
                //moveToDashBoard()
                moveToScreen()
            } else {
                moveTOTwoStepVerification()
            }

        } else {
            moveToVerificationPage()
        }

    }

    private fun moveTOTwoStepVerification() {
        startActivity( UiUtil.clearStackAndStartNewActivity(Intent(this, TwoStepVerificationActivity::class.java)))
    }

    private fun moveToDashBoard() {
        startActivity(
            UiUtil.clearStackAndStartNewActivity(
                Intent(
                    this,
                    Dashboard::class.java
                )
            )
        )
        finish()
    }

    private fun moveToVerificationPage() {
        startActivity(
            UiUtil.clearStackAndStartNewActivity(
                Intent(
                    this,
                    VerificationActivity::class.java
                ).putExtra(KeywordsAndConstants.isFromForgotPassword, false)
            )
        )
        finish()
    }

    @SuppressLint("CheckResult", "MissingPermission")
    fun login() {
        if (
            binding.editEmailMobile.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide user email.")
            return
        }
        if (
            !Patterns.EMAIL_ADDRESS.matcher(
                binding.editEmailMobile.getText().toString()
            ).matches()
        ) {
            showMessageInDialog("Please provide valid email.")
            return
        }
        if (
            binding.editPassword.getText().toString().isEmpty()
        ) {
            showMessageInDialog("Please provide password.")
            return
        }

        showProgress()

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
                override fun onComplete(task: Task<InstanceIdResult>) {
                    if (!task.isSuccessful) {
                        return
                    }
                    val token = task.result?.token ?: ""
                    Log.v("TOKEN:- ", token)
                    if (this@LoginActivity::lastLocation.isInitialized){
                        viewModel.login(
                            email = binding.editEmailMobile.getText().toString().trim(),
                            password = binding.editPassword.getText().toString(),
                            deviceToken = token,
                            lat = lastLocation.latitude.toString(),
                            lng = lastLocation.longitude.toString()
                        )
                    }else{
                        viewModel.login(
                            email = binding.editEmailMobile.getText().toString().trim(),
                            password = binding.editPassword.getText().toString(),
                            deviceToken = token,
                            lat = "0.0",
                            lng = "0.0"
                        )
                    }

                }
            })
        /*

         RxPermissions(this)
             .request(
                 Manifest.permission.ACCESS_FINE_LOCATION,
                 Manifest.permission.ACCESS_COARSE_LOCATION
             )
             .subscribe {
                 if (!it) {
                     finish()
                 }
                 fusedLocationProviderClient.lastLocation
                     .addOnSuccessListener { locationResult ->
                         locationResult?.let { location ->
                             logUtil.logV(
                                 """
                                     location :
                                     ${location.longitude}
                                     ${location.longitude}
                                 """.trimIndent()
                             )
                             this.lastLocation = location


                         }
                     }
             }*/
    }

    @SuppressLint("CheckResult", "MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        RxPermissions(this)
            .request(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .subscribe {
                if (!it) {
                    finish()
                }

                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener { locationResult ->
                        locationResult?.let { location ->
                            logUtil.logV(
                                """
                                    location :
                                    ${location.latitude}
                                    ${location.longitude}
                                """.trimIndent()
                            )
                            this.lastLocation = location

                            Log.i("LatLong", "" + lastLocation)
                        }
                    }
            }
    }

    private fun driverAccessPoint() {
        startActivity(
            Intent(
                this,
                DriverAccessPoint::class.java
            )
        )
    }

    private fun moveToScreen() {
        val userDetails = viewModel.giveRepository().getUserData()
        when {
            (userDetails?.completed_steps=="1")->{
                startActivity(Intent(this, CompleteProfileActivity::class.java))
                finish()
            }

            (userDetails?.completed_steps=="2")&& (userDetails.user_role=="4")->{
                startActivity(Intent(this, SOSDocumentSubmitedActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="2" && (userDetails.user_role=="2" ||userDetails.user_role=="3") )->{
                startActivity(Intent(this, PayoutSettingsRegistrationActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="3" && userDetails.user_role=="4")->{
                startActivity(Intent(this, PayoutSettingsRegistrationActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="4") &&
                    (userDetails.user_role=="4") &&
                    (userDetails.user_type.equals("I"))->{

                startActivity(Intent(this, PostATruckActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="4") &&
                    (userDetails.user_role=="4") &&
                    (userDetails.user_type.equals("F"))->{

                startActivity(Intent(this, AddDriverRegistrationActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="4") &&
                    (userDetails.user_role=="2" ||userDetails.user_role=="3")->{
                startActivity(Intent(this, Dashboard::class.java))
                finish()
            }
            (userDetails?.completed_steps=="5") &&
                    (userDetails.user_type.equals("F"))->{
                startActivity(Intent(this, PostATruckActivity::class.java))
                finish()
            }
            (userDetails?.completed_steps=="6" || userDetails?.completed_steps=="7")->{
                startActivity(Intent(this, Dashboard::class.java))
                finish()
            }
        }
    }


}