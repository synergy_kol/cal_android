package com.itsc.cal.receiver

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.itsc.cal.core.KeywordsAndConstants

class DownloadReceiver:BroadcastReceiver() {

    override fun onReceive(mContext: Context, intent: Intent?) {
        val referenceId = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
        if(referenceId!=null && referenceId!=0L)
        mContext.getSharedPreferences(
            KeywordsAndConstants.SHARED_PREF_DB,
            Context.MODE_PRIVATE
        ).edit().putString("referenceId","")
    }
}