package com.itsc.cal.`interface`

interface ImageOnItemClickListener {
    fun onItemClick(position: Int )
}