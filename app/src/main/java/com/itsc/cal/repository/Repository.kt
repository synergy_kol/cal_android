package com.itsc.cal.repository

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.util.Log
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.MutableLiveData
import com.google.android.libraries.places.api.Places
import com.itsc.cal.R
import com.itsc.cal.core.ApiInterface
import com.itsc.cal.core.KeywordsAndConstants
import com.itsc.cal.core.KeywordsAndConstants.EMAIL
import com.itsc.cal.core.KeywordsAndConstants.PASSWORD
import com.itsc.cal.enums.Menus
import com.itsc.cal.enums.UserType
import com.itsc.cal.model.*
import com.itsc.cal.service.LocationMonitorForegroundService
import com.itsc.cal.ui.login.LoginActivity
import com.itsc.cal.util.UiUtil
import com.itsc.cal.util.model.Result
import com.itsc.cal.util.repository.Event
import com.itsc.cal.util.repository.SuperRepository
import com.itsc.cal.util.repository.SuperRepositoryCallback
import com.sagar.android.logutilmaster.LogUtil
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

@Suppress("MemberVisibilityCanBePrivate")
class Repository(
    private val context: Application,
    private val logUtil: LogUtil,
    private val apiInterface: ApiInterface,
    private val pref: SharedPreferences

) : SuperRepository() {

    init {
        Places.initialize(
            context,
            context.getString(R.string.place_api_key)
        )
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //private vars
    private var userType: UserType = when (getUserData()?.user_role) {
        "2" -> UserType.BUILDER
        "3" -> UserType.MANUFACTURER
        "4" -> UserType.TRUCK
        else -> UserType.TRUCK
    }

    var signatureBitMap: Bitmap? = null
    ////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //Mutable Live data
    ////////////////////////////////////////////////////////////////////////////////////////////////
    val mutableLiveDataLogin: MutableLiveData<Event<UserDetails>> = MutableLiveData()
    val mutableLiveDataLoginError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSignup: MutableLiveData<Event<UserDetails>> = MutableLiveData()
    val mutableLiveDataSignUpError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataResendOTP: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataResendOTPError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataTwoStepVerification: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataTwoStepVerificationError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSaveCard: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataSaveCardError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataEditCard: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataEditCardError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSubmitReview: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataSubmitReviewError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataGetReview: MutableLiveData<Event<ReviewResponse>> = MutableLiveData()
    val mutableLiveDataGetReviewError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataMarksAsDelay: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataMarksAsDelayError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataMarkAsDefault: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataMarkAsDefaultError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataDeleteCard: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataDeleteCardError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataOTPVerify: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataOTPVerifyError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataOTPVerifyTwoStepVerification: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataOTPVerifyTwoStepVerificationError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataOTPVerifyPasswordToken: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataOTPVerifyPasswordTokenError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataOTPChangePassword: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataOTPChangePasswordError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataUpdatePassword: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataUpdatePasswordError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobConfigration: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataJobConfigrationError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobHaulConfigration: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataJobHaulConfigrationError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobExportStep1: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataJobExportStep1Error: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobImportStep1: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataJobImportStep1Error: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobExportStep2: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataJobExportStep2Error: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobExportStep3: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataJobExportStep3Error: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobImportStep2: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataJobImportStep2Error: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataForgotPassword: MutableLiveData<Event<UserDetails>> = MutableLiveData()
    val mutableLiveDataForgotPasswordError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataForgotPasswordResend: MutableLiveData<Event<UserDetails>> = MutableLiveData()
    val mutableLiveDataForgotPasswordResendError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataTruckData: MutableLiveData<Event<ArrayList<TruckDataResponse>>> =
        MutableLiveData()
    val mutableLiveDataTruckDataError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataTruckDataHault: MutableLiveData<Event<ArrayList<TruckDataResponse>>> =
        MutableLiveData()
    val mutableLiveDataTruckDataErrorHault: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataMeteralData: MutableLiveData<Event<ArrayList<MeterialResponse>>> =
        MutableLiveData()
    val mutableLiveDataMeteralDataError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataLoadData: MutableLiveData<Event<ArrayList<LoadResponse>>> =
        MutableLiveData()
    val mutableLiveDataLoadError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataEquipementData: MutableLiveData<Event<ArrayList<EquipementResponse>>> =
        MutableLiveData()
    val mutableLiveDataEquipementDataError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataImportRequestData: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataImportRequestError: MutableLiveData<Event<Result>> = MutableLiveData()


    val mutableLiveDataExportRequestData: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataExportRequestError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataRequestAcceptedByExpoterData: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataRequestAcceptedByExpoterError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataCancelHultOffSubscriptionData: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataCancelHultOffSubscriptionError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataJobCancelationChargeData: MutableLiveData<Event<JobCancelChargeResponce>> =
        MutableLiveData()
    val mutableLiveDataJobCancelationChargeDataError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataFleetUserDashboardData: MutableLiveData<Event<DashboardStatus>> =
        MutableLiveData()
    val mutableLiveDataFleetUserDashboardDataError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataHaultOfJobDashboardData: MutableLiveData<Event<HaulofSubscriptionStatusResponce>> =
        MutableLiveData()
    val mutableLiveDataHaultOfJobDashboardError: MutableLiveData<Event<Result>> =
        MutableLiveData()


    val mutableLiveDataRequestRejectData: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataRequestRejectError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataBuilderJobListData: MutableLiveData<Event<ArrayList<JobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataBuilderJobListDataError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataPostJob: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataPostJobError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobListing: MutableLiveData<Event<ArrayList<JobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataJobListingError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataExportJob: MutableLiveData<Event<ArrayList<HauljobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataJobExportError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataImportJob: MutableLiveData<Event<ArrayList<HauljobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataJobImportError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataBuilderJobListing: MutableLiveData<Event<ArrayList<JobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataBuilderJobListingError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobListingCurrent: MutableLiveData<Event<ArrayList<JobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataJobListingCurrentError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobListingPast: MutableLiveData<Event<ArrayList<JobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataJobListingPastError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobListingTruckFleet: MutableLiveData<Event<ArrayList<JobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataJobListingTruckFleetError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataJobListingTruckFleetAccept: MutableLiveData<Event<ArrayList<JobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataJobListingTruckFleetAcceptError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataJobListingTruckFleetHide: MutableLiveData<Event<ArrayList<JobListResponse>>> =
        MutableLiveData()
    val mutableLiveDataJobListingTruckFleetHideError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataTruckTypeList: MutableLiveData<Event<ArrayList<TruckTypeResponse>>> =
        MutableLiveData()
    val mutableLiveDataTruckTypeListError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataDriverList: MutableLiveData<Event<ArrayList<DriverListResponse>>> =
        MutableLiveData()
    val mutableLiveDataDriverListError: MutableLiveData<Event<Result>> =
        MutableLiveData()


    val mutableLiveDataSOSDocumentListing: MutableLiveData<Event<SOSResponce>> =
        MutableLiveData()
    val mutableLiveDataSOSDocumentListingError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataCreateHaultRequest: MutableLiveData<Event<CreateHaultOfRequest>> =
        MutableLiveData()
    val mutableLiveDataCreateHaultError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSubmitSOS: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataSubmitSOSError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataUpdateSOS: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataUpdateSOSError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDatAddBuilder: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDatAddBuilderError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataPostATruck: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataPostATruckError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataAddDriver: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataAddDriverError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataEndTrip: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataEndTripError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveStartJob: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveStartJobError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveEmptyTruck: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveEmptyTrucError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataBuilderList: MutableLiveData<Event<ArrayList<BuilderListResponse>>> =
        MutableLiveData()
    val mutableLiveDataBuilderListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSupplierList: MutableLiveData<Event<ArrayList<BuilderListResponse>>> =
        MutableLiveData()
    val mutableLiveDataSupplierListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataStateList: MutableLiveData<Event<ArrayList<StateResponce>>> =
        MutableLiveData()
    val mutableLiveDataStateListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataLogListingList: MutableLiveData<Event<ArrayList<LogListResponce>>> =
        MutableLiveData()
    val mutableLiveDataLogListingtError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataCityList: MutableLiveData<Event<ArrayList<CityResponce>>> =
        MutableLiveData()
    val mutableLiveDataCityListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataCardList: MutableLiveData<Event<ArrayList<CardListResponse>>> =
        MutableLiveData()
    val mutableLiveDataCardListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataNearByConductorList: MutableLiveData<Event<ArrayList<NearByConductorListResponse>>> =
        MutableLiveData()
    val mutableLiveDataNearByConductorListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataTruckStatusList: MutableLiveData<Event<ArrayList<TruckStatusResponse>>> =
        MutableLiveData()
    val mutableLiveDataTruckStatusListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataTruckList: MutableLiveData<Event<ArrayList<TruckListResponse>>> =
        MutableLiveData()
    val mutableLiveDataTruckListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSearchTruckList: MutableLiveData<Event<ArrayList<SearchTruckListResponse>>> =
        MutableLiveData()
    val mutableLiveDataSearchTruckListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataApplyJobs: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataApplyJobsError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataApplyJobsDetailsPage: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataApplyJobsErrorDetailsPage: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataHideJobs: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataHideJobsError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataHideJobsDetails: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataHideJobsErrorDetails: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataUserSubscription: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataUserSubscriptionError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataFleetCirtification: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataFleetCirtificationError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataShowJobs: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataShowJobsError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataShowJobsDetails: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataShowJobsErrorDetails: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataCancelJobs: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataCancelJobsError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataCancelJobsBuilderSupplier: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataCancelJobsErrorBuilderSupplier: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataCancelJobsDetailsPage: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataCancelJobsErrorDetailsPage: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataDefaultCard: MutableLiveData<Event<CardResponse>> = MutableLiveData()
    val mutableLiveDataDefaultCardError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataDefaultCardSubscription: MutableLiveData<Event<CardResponse>> =
        MutableLiveData()
    val mutableLiveDataDefaultCardSubscriptionError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataDefaultCardCancelation: MutableLiveData<Event<CardResponse>> =
        MutableLiveData()
    val mutableLiveDataDefaultCardCancelationError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataDefaultCardHault: MutableLiveData<Event<CardResponse>> = MutableLiveData()
    val mutableLiveDataDefaultCardErrorHault: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataMaterialEquipment: MutableLiveData<Event<MaterialEquipmentResponse>> =
        MutableLiveData()
    val mutableLiveDataMaterialEquiementError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataLoadingEquipment: MutableLiveData<Event<ArrayList<EquipementResponse>>> =
        MutableLiveData()
    val mutableLiveDataLoadingEquiementError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSpreadingEquipment: MutableLiveData<Event<ArrayList<EquipementResponse>>> =
        MutableLiveData()
    val mutableLiveDataSpreadingEquiementError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataTruckDetails: MutableLiveData<Event<TruckDetailsResponse>> =
        MutableLiveData()
    val mutableLiveDataTruckDetailsError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataDriverJobByToken: MutableLiveData<Event<DriverJob>> = MutableLiveData()
    val mutableLiveDataDriverJobByTokenError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSOSReason: MutableLiveData<Event<ArrayList<SOSReason>>> =
        MutableLiveData()
    val mutableLiveDataSOSReasonError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSubmitSOSReason: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataSubmitSOSReasonError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataProfileDetail: MutableLiveData<Event<ProfileDetails>> = MutableLiveData()
    val mutableLiveDataProfileDetailError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataAllProfileDetail: MutableLiveData<Event<ProfileResponse>> = MutableLiveData()
    val mutableLiveDataAllProfileDetailError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataPayoutDetails: MutableLiveData<Event<PayoutResponse>> = MutableLiveData()
    val mutableLiveDataPayoutDetailsError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataImportExportDetails: MutableLiveData<Event<HauljobListResponse>> =
        MutableLiveData()
    val mutableLiveDataImportExportError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSearchImportExportDetails: MutableLiveData<Event<HauljobListResponse>> =
        MutableLiveData()
    val mutableLiveDataSearchImportExportError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataUpdateProfile: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataUpdateProfileError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataNotificationDetails: MutableLiveData<Event<NotificationResponce>> =
        MutableLiveData()
    val mutableLiveDataNotificationDetailsError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataNotificationDetailsSOS: MutableLiveData<Event<NotificationResponce>> =
        MutableLiveData()
    val mutableLiveDataNotificationDetailsSOSError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataNotificationsList: MutableLiveData<Event<ArrayList<notificationList>>> =
        MutableLiveData()
    val mutableLiveDataNotificationsListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataNotificationRead: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataNotificationReadError: MutableLiveData<Event<Result>> = MutableLiveData()


    val mutableLiveDataPaymentPercentage: MutableLiveData<Event<PaymentPecentageResponse>> =
        MutableLiveData()
    val mutableLiveDataPaymentPercentageError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataPaymentPercentageJobPost: MutableLiveData<Event<PaymentPecentageResponse>> =
        MutableLiveData()
    val mutableLiveDataPaymentPercentageJobPostError: MutableLiveData<Event<Result>> =
        MutableLiveData()

    val mutableLiveDataGetBannerListResult: MutableLiveData<Event<ArrayList<BannerResponce>>> =
        MutableLiveData()
    val mutableLiveDataGetBannerListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataSubscriptionListResult: MutableLiveData<Event<ArrayList<SubscriptionListResponce>>> =
        MutableLiveData()
    val mutableLiveDataSubscriptionListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataCirtificationListResult: MutableLiveData<Event<ArrayList<SubscriptionListResponce>>> =
        MutableLiveData()
    val mutableLiveDataCirtificationListError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataLogout: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataLogoutError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataPayoutAddCard: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataPayoutAddCardError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataPayoutAddBank: MutableLiveData<Event<StatusReply>> = MutableLiveData()
    val mutableLiveDataPayoutAddBankError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataPayoutSettings: MutableLiveData<Event<PayoutSettingResponse>> = MutableLiveData()
    val mutableLiveDataPayoutSettingsError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataAllPayoutDetails: MutableLiveData<Event<ArrayList<AllPayoutData>>> = MutableLiveData()
    val mutableLiveDataAllPayoutDetailsError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataUpdCurrLocation: MutableLiveData<Event<StatusReply>> =
        MutableLiveData()
    val mutableLiveDataUpdCurrLocationError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataDriverLocation: MutableLiveData<Event<ArrayList<TruckLocation>>> =
        MutableLiveData()
    val mutableLiveDataDriverLocationError: MutableLiveData<Event<Result>> = MutableLiveData()

    val mutableLiveDataCompleteInfo: MutableLiveData<Event<UserDetails>> =
        MutableLiveData()
    val mutableLiveDataCompleteInfoError: MutableLiveData<Event<Result>> = MutableLiveData()

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //api call
    ////////////////////////////////////////////////////////////////////////////////////////////////
    fun login(
        email: String,
        passowrd: String,
        deviceToken: String = "",
        lat: String = "0.0",
        long: String = "0.0"
    ) {
        getSharedPref()
            .edit()
            .putString(
                EMAIL,
                email
            )
            .apply()
        getSharedPref()
            .edit()
            .putString(
                PASSWORD,
                passowrd
            )
            .apply()
        getSharedPref()
            .edit()
            .putString(
                KeywordsAndConstants.DEVICE_FCM_TOKEN,
                deviceToken
            )
            .apply()
        makeApiCall(
            observable = apiInterface.login(
                source = "MOB",
                email = email,
                password = passowrd,
                device_token = deviceToken,
                device_type = "2",
                lat = lat,
                lng = long
            ),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataLogin,
            errorMutableLiveData = mutableLiveDataLoginError,
            callback = object : SuperRepositoryCallback<UserDetails> {
                override fun success(result: UserDetails) {
                    super.success(result)
                    saveUserDataToPref(result)

                }
            }
        )
    }

    fun signUp(
        email: String,
        passowrd: String,
        deviceToken: String,
        fname: String,
        lname: String,
        mobile: String,
        company_name: String,
        company_registration: String,
        license: String,
        user_role: String,
        user_type: String,
        numberof_truck: String,
        latitude: String,
        longitude: String,
        state: String,
        city: String
    ) {
        getSharedPref()
            .edit()
            .putString(
                EMAIL,
                email
            )
            .apply()
        getSharedPref()
            .edit()
            .putString(
                PASSWORD,
                passowrd
            )
            .apply()
        makeApiCall(
            observable = apiInterface.signUp(
                fname = fname,
                lanme = lname,
                password = passowrd,
                email = email,
                mobile = mobile,
                company_name = company_name,
                company_registration = company_registration,
                license = license,
                registration_source = "MOB",
                user_role = user_role,
                user_type = user_type,
                numberof_truck = numberof_truck,
                latitude = latitude,
                longitude = longitude,
                city = city,
                state = state
            ),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataSignup,
            errorMutableLiveData = mutableLiveDataSignUpError,
            callback = object : SuperRepositoryCallback<UserDetails> {
                override fun success(result: UserDetails) {
                    super.success(result)
                    saveUserDataToPref(result)

                }
            }
        )
    }

    fun completeInfo(
        dob: String,
        companyLegalName: String,
       companyDba: String,
       phone2: String,
        email2: String,
        companyEin: String,
        driverLicense: String,
        driverLicenseExp: String,
        street_address: String,
       zip: String,
       is_billing_address: String,
       bStreetAddress: String,
        city: String,
       state: String,
       b_zip: String
    ){
        makeApiCall(
           apiInterface.completeInformation(
                userId =    getUserData()?.user_master_id?:"",
                dob = dob,
                companyLegalName = companyLegalName,
                companyDba=companyDba,
                phone2=phone2,
                email2=email2,
                companyEin=companyEin,
                driverLicense = driverLicense,
                driverLicenseExp=driverLicenseExp,
                street_address = street_address,
                zip = zip,
                is_billing_address = is_billing_address,
                bStreetAddress = bStreetAddress,
                city = city,
                state = state,
                b_zip = b_zip),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataCompleteInfo,
            errorMutableLiveData = mutableLiveDataCompleteInfoError

        )

    }



    fun resendOTP() {
        makeApiCall(
            apiInterface.reSendOTP(
                user_master_id = getUserData()!!.user_master_id,
                mobile = getUserData()!!.mobile ?: "",
                email = getUserData()!!.email ?: ""
            ),
            successMutableLiveData = mutableLiveDataResendOTP,
            errorMutableLiveData = mutableLiveDataResendOTPError
        )
    }

    fun twoStepOtpVerification(deviceToken: String) {
        makeApiCall(
            apiInterface.sendTwoStepVerificationCode(
                user_master_id = getUserData()!!.user_master_id,
                mobile = getUserData()!!.mobile ?: "",
                email = getUserData()!!.email ?: "",
                device_token = deviceToken
            ),
            successMutableLiveData = mutableLiveDataTwoStepVerification,
            errorMutableLiveData = mutableLiveDataTwoStepVerificationError
        )
    }

    fun verifyOTPTwoStepVerification(otp: String, device_token: String, is_trusted: String) {
        makeApiCall(
            apiInterface.verifyOTPTwoStepVerification(
                user_master_id = getUserData()!!.user_master_id,
                otp = otp,
                device_token = device_token,
                is_trusted = is_trusted
            ),
            successMutableLiveData = mutableLiveDataOTPVerifyTwoStepVerification,
            errorMutableLiveData = mutableLiveDataOTPVerifyTwoStepVerificationError
        )
    }

    fun verifyOTP(otp: String) {
        makeApiCall(
            apiInterface.verifyOTP(
                user_master_id = getUserData()!!.user_master_id,
                otp = otp
            ),
            successMutableLiveData = mutableLiveDataOTPVerify,
            errorMutableLiveData = mutableLiveDataOTPVerifyError
        )
    }

    fun forgotPassword(emailMobile: String) {
        makeApiCall(
            apiInterface.forgotPassword(emailMobile),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataForgotPassword,
            errorMutableLiveData = mutableLiveDataForgotPasswordError,
            callback = object : SuperRepositoryCallback<UserDetails> {
                override fun success(result: UserDetails) {
                    super.success(result)
                    saveUserDataToPref(result)

                }
            }
        )
    }

    fun logout(lat: String = "0.0", long: String = "0.0") {
        makeApiCall(
            apiInterface.logout(
                getUserData()!!.user_master_id,
                lat = lat,
                lng = long,
                device_type = "2"
            ),
            responseJsonKeyword = "",
            successMutableLiveData = mutableLiveDataLogout,
            errorMutableLiveData = mutableLiveDataLogoutError,
            callback = object : SuperRepositoryCallback<StatusReply> {
                override fun success(result: StatusReply) {
                    super.success(result)
                    clearAllData()


                }
            }
        )
    }


    fun forgotPasswordResend(emailMobile: String) {
        makeApiCall(
            apiInterface.forgotPassword(emailMobile),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataForgotPasswordResend,
            errorMutableLiveData = mutableLiveDataForgotPasswordResendError,
            callback = object : SuperRepositoryCallback<UserDetails> {
                override fun success(result: UserDetails) {
                    super.success(result)
                    saveUserDataToPref(result)

                }
            }
        )
    }

    fun verifyOTPPasswordToken(password_token: String) {
        makeApiCall(
            apiInterface.verifyOTPPasswordToken(
                user_master_id = getUserData()!!.user_master_id,
                password_token = password_token
            ),
            successMutableLiveData = mutableLiveDataOTPVerifyPasswordToken,
            errorMutableLiveData = mutableLiveDataOTPVerifyPasswordTokenError
        )
    }


    fun changePassword(password_token: String, password: String) {
        makeApiCall(
            apiInterface.changePassword(
                password_token = password_token,
                password = password
            ),
            successMutableLiveData = mutableLiveDataOTPChangePassword,
            errorMutableLiveData = mutableLiveDataOTPChangePasswordError
        )
    }

    fun updatePassword(password: String, new_password: String) {
        makeApiCall(
            apiInterface.updatePassword(
                user_master_id = getUserData()!!.user_master_id,
                password = password,
                new_password = new_password
            ),
            successMutableLiveData = mutableLiveDataUpdatePassword,
            errorMutableLiveData = mutableLiveDataUpdatePasswordError
        )
    }


    fun getMasterTruckData() {
        makeApiCall(
            apiInterface.getMasterData(
                "truck_type_master"
            ),
            responseJsonKeyword = "userdata",
            callback = object : SuperRepositoryCallback<ArrayList<TruckDataResponse>> {
                override fun success(result: ArrayList<TruckDataResponse>) {
                    super.success(result)

                    mutableLiveDataTruckData.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataTruckDataError
        )
    }

    fun getBannerList() {
        makeApiCall(
            apiInterface.getBanner(getUserData()!!.user_role),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataGetBannerListResult,
            errorMutableLiveData = mutableLiveDataGetBannerListError,
            callback = object : SuperRepositoryCallback<ArrayList<BannerResponce>> {
                override fun success(result: ArrayList<BannerResponce>) {
                    super.success(result)

                }
            }
        )
    }

    fun getSubscriptionListDetails(city_id: String) {
        makeApiCall(
            apiInterface.getSubscriptionListDetails(
                city_id = city_id,
                user_id = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataSubscriptionListResult,
            errorMutableLiveData = mutableLiveDataSubscriptionListError,
            callback = object : SuperRepositoryCallback<ArrayList<SubscriptionListResponce>> {
                override fun success(result: ArrayList<SubscriptionListResponce>) {
                    super.success(result)

                }
            }
        )
    }


    fun getCertificationDetails(city_id: String) {
        makeApiCall(
            apiInterface.getCertificationDetails(
                city_id = city_id,
                user_id = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataCirtificationListResult,
            errorMutableLiveData = mutableLiveDataCirtificationListError,
            callback = object : SuperRepositoryCallback<ArrayList<SubscriptionListResponce>> {
                override fun success(result: ArrayList<SubscriptionListResponce>) {
                    super.success(result)

                }
            }
        )
    }


    fun getMasterTruckDataHaulOff() {
        makeApiCall(
            apiInterface.getMasterData(
                "truck_type_master"
            ),
            responseJsonKeyword = "userdata",
            callback = object : SuperRepositoryCallback<ArrayList<TruckDataResponse>> {
                override fun success(result: ArrayList<TruckDataResponse>) {
                    super.success(result)

                    mutableLiveDataTruckDataHault.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataTruckDataErrorHault
        )
    }

    fun getMasterMeterialData() {
        makeApiCall(
            apiInterface.getMasterData(
                "Material_master"
            ),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataMeteralData,
            errorMutableLiveData = mutableLiveDataMeteralDataError
        )
    }

    fun getLoadData() {
        makeApiCall(
            apiInterface.getMasterData(
                "load_type_master"
            ),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataLoadData,
            errorMutableLiveData = mutableLiveDataLoadError
        )
    }

    fun getMasterEquipementlData() {
        makeApiCall(
            apiInterface.getMasterData(
                "Equipment_master"
            ),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataEquipementData,
            errorMutableLiveData = mutableLiveDataEquipementDataError
        )
    }

    fun getBuilderJobList() {
        makeApiCall(
            apiInterface.getJobListBuilderList(
                builder_id = getUserData()!!.user_master_id,
                supplier_id = "0"

            ),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataBuilderJobListData,
            errorMutableLiveData = mutableLiveDataBuilderJobListDataError
        )
    }

    fun getJobListing(filter: String) {
        makeApiCall(
            apiInterface.getJobList(
                user_master_id = getUserData()!!.user_master_id,
                filter = filter
            ),
            responseJsonKeyword = "jobs",
            callback = object : SuperRepositoryCallback<ArrayList<JobListResponse>> {
                override fun success(result: ArrayList<JobListResponse>) {
                    super.success(result)

                    result.forEach {
                        it.filter = filter
                    }

                    mutableLiveDataJobListing.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }

                override fun error(result: Result) {
                    super.error(result)

                    mutableLiveDataJobListingError.postValue(
                        Event(
                            Result(
                                filter.toInt(),
                                result.getMessageToShow()
                            ),
                            false
                        )
                    )
                }
            }
        )
    }

    fun getJobExportHault(filter: String, tag: String) {
        makeApiCall(
            apiInterface.exportHaultOfJobLising(
                user_master_id = getUserData()!!.user_master_id,
                type = filter
            ),
            responseJsonKeyword = "export_request",
            callback = object : SuperRepositoryCallback<ArrayList<HauljobListResponse>> {
                override fun success(result: ArrayList<HauljobListResponse>) {
                    super.success(result)

                    result.forEach {
                        it.filter = tag
                    }

                    mutableLiveDataExportJob.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }

                override fun error(result: Result) {
                    super.error(result)

                    mutableLiveDataJobExportError.postValue(
                        Event(
                            Result(
                                tag.toInt(),
                                result.getMessageToShow()
                            ),
                            false
                        )
                    )
                }
            }

        )
    }

    fun getJobImportHault(filter: String, tag: String) {
        makeApiCall(
            apiInterface.importHaultOfJobLising(
                user_master_id = getUserData()!!.user_master_id,
                type = filter
            ),
            responseJsonKeyword = "import_request",
            callback = object : SuperRepositoryCallback<ArrayList<HauljobListResponse>> {
                override fun success(result: ArrayList<HauljobListResponse>) {
                    super.success(result)

                    result.forEach {
                        it.filter = tag
                    }

                    mutableLiveDataImportJob.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }

                override fun error(result: Result) {
                    super.error(result)

                    mutableLiveDataJobImportError.postValue(
                        Event(
                            Result(
                                tag.toInt(),
                                result.getMessageToShow()
                            ),
                            false
                        )
                    )
                }
            }
        )
    }

    fun getJobBuilderListingNew(filter: String) {
        makeApiCall(
            apiInterface.getBuilderJobList(
                user_master_id = getUserData()!!.user_master_id,
                filter = filter
            ),
            responseJsonKeyword = "jobs",
            callback = object : SuperRepositoryCallback<ArrayList<JobListResponse>> {
                override fun success(result: ArrayList<JobListResponse>) {
                    super.success(result)

                    result.forEach {
                        it.filter = filter
                    }

                    mutableLiveDataBuilderJobListing.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }

                override fun error(result: Result) {
                    super.error(result)

                    mutableLiveDataBuilderJobListingError.postValue(
                        Event(
                            Result(
                                filter.toInt(),
                                result.getMessageToShow()
                            ),
                            false
                        )
                    )
                }
            }
        )
    }

    fun getMyBookingCurrent(filter: String) {
        makeApiCall(
            apiInterface.getJobListTruckList(
                user_master_id = getUserData()!!.user_master_id,
                job_status = filter
            ),
            responseJsonKeyword = "today_completed_joblist",
            callback = object : SuperRepositoryCallback<ArrayList<JobListResponse>> {
                override fun success(result: ArrayList<JobListResponse>) {
                    super.success(result)

                    result.forEach {
                        it.filter = filter
                    }

                    mutableLiveDataJobListingCurrent.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }

                override fun error(result: Result) {
                    super.error(result)

                    mutableLiveDataJobListingCurrentError.postValue(
                        Event(
                            Result(
                                filter.toInt(),
                                result.getMessageToShow()
                            ),
                            false
                        )
                    )
                }
            }
        )
    }

    fun getMyBookingPast(filter: String) {
        makeApiCall(
            apiInterface.getJobListTruckList(
                user_master_id = getUserData()!!.user_master_id,
                job_status = filter
            ),
            responseJsonKeyword = "past_completed_joblist",
            callback = object : SuperRepositoryCallback<ArrayList<JobListResponse>> {
                override fun success(result: ArrayList<JobListResponse>) {
                    super.success(result)

                    result.forEach {
                        it.filter = filter
                    }

                    mutableLiveDataJobListingPast.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }

                override fun error(result: Result) {
                    super.error(result)

                    mutableLiveDataJobListingPastError.postValue(
                        Event(
                            Result(
                                filter.toInt(),
                                result.getMessageToShow()
                            ),
                            false
                        )
                    )
                }
            }
        )
    }

    fun getJobListingTruckFleet(filter: String) {
        makeApiCall(
            apiInterface.getJobListTruckList(
                user_master_id = getUserData()!!.user_master_id,
                job_status = filter
            ),
            responseJsonKeyword = "new_joblist",
            callback = object : SuperRepositoryCallback<ArrayList<JobListResponse>> {
                override fun success(result: ArrayList<JobListResponse>) {
                    super.success(result)

                    mutableLiveDataJobListingTruckFleet.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataJobListingTruckFleetError
        )
    }

    fun getJobListingTruckFleetAccept(filter: String) {
        makeApiCall(
            apiInterface.getJobListTruckList(
                user_master_id = getUserData()!!.user_master_id,
                job_status = filter
            ),
            responseJsonKeyword = "accepted_joblist",
            callback = object : SuperRepositoryCallback<ArrayList<JobListResponse>> {
                override fun success(result: ArrayList<JobListResponse>) {
                    super.success(result)

                    mutableLiveDataJobListingTruckFleetAccept.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataJobListingTruckFleetAcceptError
        )
    }

    fun getJobListingTruckFleetHide(filter: String) {
        makeApiCall(
            apiInterface.getJobListTruckList(
                user_master_id = getUserData()!!.user_master_id,
                job_status = filter
            ),
            responseJsonKeyword = "hiden_joblist",
            callback = object : SuperRepositoryCallback<ArrayList<JobListResponse>> {
                override fun success(result: ArrayList<JobListResponse>) {
                    super.success(result)

                    mutableLiveDataJobListingTruckFleetHide.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataJobListingTruckFleetHideError
        )
    }

    fun getTruckTypeList() {
        makeApiCall(
            apiInterface.truckTypeList(
                user_master_id = getUserData()!!.user_master_id

            ),
            responseJsonKeyword = "truck_type",
            callback = object : SuperRepositoryCallback<ArrayList<TruckTypeResponse>> {
                override fun success(result: ArrayList<TruckTypeResponse>) {
                    super.success(result)
                    mutableLiveDataTruckTypeList.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataTruckTypeListError
        )
    }

    fun getDriverTypeList() {
        makeApiCall(
            apiInterface.driverList(
                user_master_id = getUserData()!!.user_master_id

            ),
            responseJsonKeyword = "driver_list",
            callback = object : SuperRepositoryCallback<ArrayList<DriverListResponse>> {
                override fun success(result: ArrayList<DriverListResponse>) {
                    super.success(result)
                    mutableLiveDataDriverList.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataDriverListError
        )
    }

    fun postJob(
        source_lat: String,
        source_long: String,
        destination_lat: String,
        destination_long: String,
        delivery_date: String,
        delivery_time: String,
        Destination: String,
        Equipment: String,
        FreightCharges: String,
        JobEstimatePrice: String,
        LodeType: String,
        MaterialType: String,
        NoOfTrucks: String,
        pickup_date: String,
        pickup_time: String,
        priority: String,
        Source: String,
        TruckType: String,
        Weight: String,
        Equipment_value: String,
        builderId: String,
        payment_type: String,
        stripeToken: String,
        card_id: String,
        jobPayableAmount: String
    ) {
        makeApiCall(
            apiInterface.postJob(
                user_master_id = getUserData()?.user_master_id ?: "",
                source_lat = source_lat,
                source_long = source_long,
                destination_lat = destination_lat,
                destination_long = destination_long,
                delivery_date = delivery_date,
                delivery_time = delivery_time,
                Destination = Destination,
                Equipment = Equipment,
                FreightCharges = FreightCharges,
                JobEstimatePrice = JobEstimatePrice,
                LodeType = LodeType,
                MaterialType = MaterialType,
                NoOfTrucks = NoOfTrucks,
                pickup_date = pickup_date,
                pickup_time = pickup_time,
                priority = priority,
                Source = Source,
                TruckType = TruckType,
                Weight = Weight,
                Equipment_value = Equipment_value,
                builder_id = builderId,
                payment_type = payment_type,
                card_id = card_id,
                stripeToken = stripeToken,
                jobPayableAmount = jobPayableAmount

            ),
            successMutableLiveData = mutableLiveDataPostJob,
            errorMutableLiveData = mutableLiveDataPostJobError
        )
    }

    fun getSOSDocumentList() {
        makeApiCall(
            apiInterface.viewDocument(
                user_master_id = getUserData()!!.user_master_id
            ),
            responseNotEnclosedInInnerResponseKeyword = true,
            successMutableLiveData = mutableLiveDataSOSDocumentListing,
            errorMutableLiveData = mutableLiveDataSOSDocumentListingError
        )
    }

    fun createRequestHaultOfJob(request_type: String) {
        makeApiCall(
            apiInterface.createRequestHaultOf(
                user_master_id = getUserData()!!.user_master_id,
                request_type = request_type
            ),
            responseNotEnclosedInInnerResponseKeyword = true,
            successMutableLiveData = mutableLiveDataCreateHaultRequest,
            errorMutableLiveData = mutableLiveDataCreateHaultError
        )
    }

    fun submitSOS(
        imageFile: File?, sos_document_type: String
    ) {
        var ext = imageFile?.name?.split(".")?.get(1)
        var file = (imageFile?.asRequestBody("image/$ext".toMediaType())?.let {
            MultipartBody.Part.createFormData(
                "sos_document_file",
                imageFile?.name,
                it
            )
        })

        makeApiCall(
            apiInterface.submitSOS(
                user_master_id = getUserData()!!.user_master_id.toString()
                    .toRequestBody(MultipartBody.FORM),
                sos_document_type = sos_document_type.toString().toRequestBody(MultipartBody.FORM),
                sos_document_file = file
            ),
            successMutableLiveData = mutableLiveDataSubmitSOS,
            errorMutableLiveData = mutableLiveDataSubmitSOSError

        )
    }

    fun updateSOS(
        imageFile: File?, document_id: String
    ) {
        var ext = imageFile?.name?.split(".")?.get(1)
        var file = (imageFile?.asRequestBody("image/$ext".toMediaType())?.let {
            MultipartBody.Part.createFormData(
                "sos_document_file",
                imageFile?.name,
                it
            )
        })

        makeApiCall(
            apiInterface.updateSOS(
                user_master_id = getUserData()!!.user_master_id.toString()
                    .toRequestBody(MultipartBody.FORM),
                document_id = document_id.toString().toRequestBody(MultipartBody.FORM),
                sos_document_file = file
            ),
            successMutableLiveData = mutableLiveDataUpdateSOS,
            errorMutableLiveData = mutableLiveDataUpdateSOSError

        )
    }

    fun getBuilderList() {
        makeApiCall(
            apiInterface.getBuilderListing(
                getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "builder_list",
            callback = object : SuperRepositoryCallback<ArrayList<BuilderListResponse>> {
                override fun success(result: ArrayList<BuilderListResponse>) {
                    super.success(result)
                    mutableLiveDataBuilderList.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataBuilderListError
        )
    }


    fun getSupplierList() {
        makeApiCall(
            apiInterface.getSupplierList(
                getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "supplier_list",
            callback = object : SuperRepositoryCallback<ArrayList<BuilderListResponse>> {
                override fun success(result: ArrayList<BuilderListResponse>) {
                    super.success(result)
                    mutableLiveDataSupplierList.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataSupplierListError
        )
    }

    fun getStateList() {
        makeApiCall(
            apiInterface.getStateList(
                "MOB"
            ),
            responseJsonKeyword = "data",
            callback = object : SuperRepositoryCallback<ArrayList<StateResponce>> {
                override fun success(result: ArrayList<StateResponce>) {
                    super.success(result)
                    mutableLiveDataStateList.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataStateListError
        )
    }

    fun getLogDetails() {
        makeApiCall(
            apiInterface.getLogDetails(
                getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "data",
            callback = object : SuperRepositoryCallback<ArrayList<LogListResponce>> {
                override fun success(result: ArrayList<LogListResponce>) {
                    super.success(result)
                    mutableLiveDataLogListingList.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataLogListingtError
        )
    }

    fun getCityList(stateId: String) {
        makeApiCall(
            apiInterface.getCityList(
                stateId
            ),
            responseJsonKeyword = "data",
            callback = object : SuperRepositoryCallback<ArrayList<CityResponce>> {
                override fun success(result: ArrayList<CityResponce>) {
                    super.success(result)
                    mutableLiveDataCityList.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataCityListError
        )
    }

    fun addBuilder(
        imageFile: File?,
        email: String,
        company_name: String,
        address: String,
        first_name: String,
        last_name: String,
        latitute: String,
        longitute: String,
        phone: String
    ) {
        var ext = imageFile?.name?.split(".")?.get(1)
        var file = (imageFile?.asRequestBody("image/$ext".toMediaType())?.let {
            MultipartBody.Part.createFormData(
                "builder_photo",
                imageFile?.name,
                it
            )
        })

        makeApiCall(
            apiInterface.addBuilder(
                user_master_id = getUserData()!!.user_master_id.toString()
                    .toRequestBody(MultipartBody.FORM),
                email = email.toString().toRequestBody(MultipartBody.FORM),
                builder_photo = file,
                user_role = getUserData()!!.user_role.toString()
                    .toRequestBody(MultipartBody.FORM),
                company_name = company_name.toString().toRequestBody(MultipartBody.FORM),
                address = address.toString().toRequestBody(MultipartBody.FORM),
                first_name = first_name.toString().toRequestBody(MultipartBody.FORM),
                last_name = last_name.toString().toRequestBody(MultipartBody.FORM),
                latitute = latitute.toString().toRequestBody(MultipartBody.FORM),
                longitute = longitute.toString().toRequestBody(MultipartBody.FORM),
                phone = phone.toString().toRequestBody(MultipartBody.FORM)
            ),

            successMutableLiveData = mutableLiveDatAddBuilder,
            errorMutableLiveData = mutableLiveDatAddBuilderError

        )
    }

    fun addTruck(
        postATruckRequest: PostATruckRequest
    ) {
        var extTruckImage = postATruckRequest.truck_image?.name?.split(".")?.get(1)
        var fileTruckImage =
            (postATruckRequest.truck_image?.asRequestBody("image/$extTruckImage".toMediaType())
                ?.let {
                    MultipartBody.Part.createFormData(
                        "truck_image",
                        postATruckRequest.truck_image?.name,
                        it
                    )
                })


        var fontImage = postATruckRequest.front?.name?.split(".")?.get(1)
        var fileFontImage =
            (postATruckRequest.front?.asRequestBody("image/$fontImage".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "front[]",
                    postATruckRequest.front?.name,
                    it
                )
            })

        var backImage = postATruckRequest.back?.name?.split(".")?.get(1)
        var filebackImage =
            (postATruckRequest.back?.asRequestBody("image/$backImage".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "front[]",
                    postATruckRequest.back?.name,
                    it
                )
            })

        var leftImage = postATruckRequest.left?.name?.split(".")?.get(1)
        var fileLeftImage =
            (postATruckRequest.left?.asRequestBody("image/$leftImage".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "left[]",
                    postATruckRequest.left?.name,
                    it
                )
            })

        var rightImage = postATruckRequest.right?.name?.split(".")?.get(1)
        var fileRightImage =
            (postATruckRequest.right?.asRequestBody("image/$rightImage".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "right[]",
                    postATruckRequest.right?.name,
                    it
                )
            })

        var usdotImage = postATruckRequest.usdot?.name?.split(".")?.get(1)
        var fileUsdotImage =
            (postATruckRequest.usdot?.asRequestBody("image/$usdotImage".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "usdot_image[]",
                    postATruckRequest.usdot?.name,
                    it
                )
            })


        makeApiCall(
            apiInterface.addTruck(
                user_master_id = getUserData()!!.user_master_id.toString()
                    .toRequestBody(MultipartBody.FORM),
                truck_image = fileTruckImage,
                truck_body_style = postATruckRequest.truck_body_style.toRequestBody(MultipartBody.FORM),
                truck_cabin_type = postATruckRequest.truck_cabin_type.toRequestBody(MultipartBody.FORM),
                truck_carring_capacity = postATruckRequest.truck_carring_capacity.toRequestBody(
                    MultipartBody.FORM
                ),
                truck_color = postATruckRequest.truck_color.toRequestBody(MultipartBody.FORM),
                truck_empty_weight = postATruckRequest.truck_empty_weight.toRequestBody(
                    MultipartBody.FORM
                ),
                truck_make = postATruckRequest.truck_make.toRequestBody(MultipartBody.FORM),
                truck_model = postATruckRequest.truck_model.toRequestBody(MultipartBody.FORM),
                truck_reg_number = postATruckRequest.truck_reg_number.toRequestBody(MultipartBody.FORM),
                truck_type = postATruckRequest.truck_type.toRequestBody(MultipartBody.FORM),
                truck_vin_number = postATruckRequest.truck_vin_number.toRequestBody(MultipartBody.FORM),
                truck_weight_metric = postATruckRequest.truck_weight_metric.toRequestBody(
                    MultipartBody.FORM
                ),
                front = fileFontImage,
                back = filebackImage,
                left = fileLeftImage,
                right = fileRightImage,
                usdot = fileUsdotImage
            ),

            successMutableLiveData = mutableLiveDataPostATruck,
            errorMutableLiveData = mutableLiveDataPostATruckError

        )
    }

    fun applyJobs(job_id: String) {
        makeApiCall(
            apiInterface.applyJobs(
                user_master_id = getUserData()!!.user_master_id,
                job_id = job_id
            ),
            successMutableLiveData = mutableLiveDataApplyJobs,
            errorMutableLiveData = mutableLiveDataApplyJobsError
        )
    }

    fun applyJobsDetailsPage(job_id: String) {
        makeApiCall(
            apiInterface.applyJobs(
                user_master_id = getUserData()!!.user_master_id,
                job_id = job_id
            ),
            successMutableLiveData = mutableLiveDataApplyJobsDetailsPage,
            errorMutableLiveData = mutableLiveDataApplyJobsErrorDetailsPage
        )
    }

    fun hideJobs(job_id: String) {
        makeApiCall(
            apiInterface.hideJobs(
                user_master_id = getUserData()!!.user_master_id,
                job_id = job_id
            ),
            successMutableLiveData = mutableLiveDataHideJobs,
            errorMutableLiveData = mutableLiveDataHideJobsError
        )
    }

    fun hideJobsDetails(job_id: String) {
        makeApiCall(
            apiInterface.hideJobs(
                user_master_id = getUserData()!!.user_master_id,
                job_id = job_id
            ),
            successMutableLiveData = mutableLiveDataHideJobsDetails,
            errorMutableLiveData = mutableLiveDataHideJobsErrorDetails
        )
    }

    fun userSubscription(stripeToken: String, subscribe_id: String) {
        makeApiCall(
            apiInterface.userSubscription(
                user_master_id = getUserData()!!.user_master_id,
                city_id = getUserData()!!.city ?: "",
                stripeToken = stripeToken,
                subscribe_id = subscribe_id
            ),
            successMutableLiveData = mutableLiveDataUserSubscription,
            errorMutableLiveData = mutableLiveDataUserSubscriptionError
        )
    }

    fun fleetCertification(stripeToken: String, certification_id: String) {
        makeApiCall(
            apiInterface.fleetCertification(
                user_master_id = getUserData()!!.user_master_id,
                city_id = getUserData()!!.city ?: "",
                stripeToken = stripeToken,
                certification_id = certification_id
            ),
            successMutableLiveData = mutableLiveDataFleetCirtification,
            errorMutableLiveData = mutableLiveDataFleetCirtificationError
        )
    }

    fun showJobs(job_id: String) {
        makeApiCall(
            apiInterface.showJobs(
                user_master_id = getUserData()!!.user_master_id,
                job_id = job_id
            ),
            successMutableLiveData = mutableLiveDataShowJobs,
            errorMutableLiveData = mutableLiveDataShowJobsError
        )
    }

    fun showJobsDetails(job_id: String) {
        makeApiCall(
            apiInterface.showJobs(
                user_master_id = getUserData()!!.user_master_id,
                job_id = job_id
            ),
            successMutableLiveData = mutableLiveDataShowJobsDetails,
            errorMutableLiveData = mutableLiveDataShowJobsErrorDetails
        )
    }

    fun cancelJobs(job_id: String, cancel_reason: String, amount: String) {
        makeApiCall(
            apiInterface.cancelJobs(
                user_master_id = getUserData()!!.user_master_id,
                job_id = job_id,
                cancel_reason = cancel_reason,
                amount = amount
            ),
            successMutableLiveData = mutableLiveDataCancelJobs,
            errorMutableLiveData = mutableLiveDataCancelJobsError
        )
    }

    fun cancelJobsBuilderSupplier(
        job_id: String,
        cancel_reason: String,
        amount: String,
        stripeToken: String
    ) {
        makeApiCall(
            apiInterface.cancelJobsBuilderSupplier(
                user_master_id = getUserData()!!.user_master_id,
                job_id = job_id,
                cancel_reason = cancel_reason,
                amount = amount,
                role_id = getUserData()!!.user_role,
                stripeToken = stripeToken
            ),
            successMutableLiveData = mutableLiveDataCancelJobsBuilderSupplier,
            errorMutableLiveData = mutableLiveDataCancelJobsErrorBuilderSupplier
        )
    }

    fun cancelJobsDeatils(job_id: String, cancel_reason: String, amount: String) {
        makeApiCall(
            apiInterface.cancelJobs(
                user_master_id = getUserData()!!.user_master_id,
                job_id = job_id,
                cancel_reason = cancel_reason,
                amount = amount
            ),
            successMutableLiveData = mutableLiveDataCancelJobsDetailsPage,
            errorMutableLiveData = mutableLiveDataCancelJobsErrorDetailsPage
        )
    }

    fun getTruckList() {
        makeApiCall(
            apiInterface.truckList(
                getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "truck_list",
            callback = object : SuperRepositoryCallback<ArrayList<TruckListResponse>> {
                override fun success(result: ArrayList<TruckListResponse>) {
                    super.success(result)
                    mutableLiveDataTruckList.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataTruckListError
        )
    }

    fun jobConfiguration(
        job_id: String, truckId: ArrayList<String>, driverId: ArrayList<String>
    ) {
        val truckIdBody = ArrayList<RequestBody>()
        val driverIdBody = ArrayList<RequestBody>()
        for (i in 0 until truckId.size) {
            truckIdBody.add(truckId.get(i).toString().toRequestBody(MultipartBody.FORM))
        }
        for (i in 0 until driverId.size) {
            driverIdBody.add(driverId.get(i).toString().toRequestBody(MultipartBody.FORM))
        }

        makeApiCall(
            apiInterface.jobConfiguration(
                user_master_id = getUserData()!!.user_master_id.toRequestBody(MultipartBody.FORM),
                job_id = job_id.toRequestBody(MultipartBody.FORM),
                driver_id = driverIdBody,
                truck_id = truckIdBody
            ),
            successMutableLiveData = mutableLiveDataJobConfigration,
            errorMutableLiveData = mutableLiveDataJobConfigrationError
        )
    }

    fun truckDetails(truckId: String) {
        makeApiCall(
            apiInterface.truckDetails(
                truckId
            ),
            responseJsonKeyword = "truck_details",
            successMutableLiveData = mutableLiveDataTruckDetails,
            errorMutableLiveData = mutableLiveDataTruckDetailsError
        )
    }

    fun getDriverJobByToken(token: String) {
        makeApiCall(
            apiInterface.getJobByToken(
                token
            ),
            responseJsonKeyword = "jobs",
            successMutableLiveData = mutableLiveDataDriverJobByToken,
            errorMutableLiveData = mutableLiveDataDriverJobByTokenError
        )
    }

    fun addDriver(
        driverName: String,
        driverNumber: String,
        diverLicense: String,
        driverLicenseFile: File?,
        diverPhoto: File?,
        email: String
    ) {


        var extLicense = driverLicenseFile?.name?.split(".")?.get(1)
        var fileLicense =
            (driverLicenseFile?.asRequestBody("image/$extLicense".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "driver_licence_photo",
                    driverLicenseFile?.name,
                    it
                )
            })

        var extPhoto = diverPhoto?.name?.split(".")?.get(1)
        var filePhoto = (diverPhoto?.asRequestBody("image/$extPhoto".toMediaType())?.let {
            MultipartBody.Part.createFormData(
                "driver_photo",
                diverPhoto?.name,
                it
            )
        })

        makeApiCall(
            apiInterface.addDriver(
                user_master_id = getUserData()!!.user_master_id.toString()
                    .toRequestBody(MultipartBody.FORM),
                driver_licence_photo = fileLicense,
                driver_photo = filePhoto,
                driver_name = driverName.toRequestBody(MultipartBody.FORM),
                driver_licence = diverLicense.toRequestBody(MultipartBody.FORM),
                driver_number = driverNumber.toRequestBody(MultipartBody.FORM),
                email = email.toRequestBody(MultipartBody.FORM)

            ),

            successMutableLiveData = mutableLiveDataAddDriver,
            errorMutableLiveData = mutableLiveDataAddDriverError

        )
    }

    fun endTrip(
        receiver_name: String = "",
        token_id: String,
        receiver_nric: String = "",
        signature: File? = null,
        pod: File? = null,
        driver_job_status: String
    ) {


        var extLicense = signature?.name?.split(".")?.get(1)
        var signature =
            (signature?.asRequestBody("image/$extLicense".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "signature",
                    signature?.name,
                    it
                )
            })

        var extPhoto = pod?.name?.split(".")?.get(1)
        var pod = (pod?.asRequestBody("image/$extPhoto".toMediaType())?.let {
            MultipartBody.Part.createFormData(
                "final_receipt",
                pod?.name,
                it
            )
        })

        makeApiCall(
            apiInterface.endTrip(
                signature = signature,
                final_receipt = pod,
                receiver_name = receiver_name.toRequestBody(MultipartBody.FORM),
                receiver_nric = receiver_nric.toRequestBody(MultipartBody.FORM),
                token_id = token_id.toRequestBody(MultipartBody.FORM),
                driver_job_status = driver_job_status.toRequestBody(MultipartBody.FORM)

            ),
            successMutableLiveData = mutableLiveDataEndTrip,
            errorMutableLiveData = mutableLiveDataEndTripError

        )
    }

    fun startJob(
        challan: File?,
        truckImage: File?,
        truckWeight: String,
        token: String
    ) {
        var extLicense = challan?.name?.split(".")?.get(1)
        var fileLicense =
            (challan?.asRequestBody("image/$extLicense".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "challan",
                    challan?.name,
                    it
                )
            })

        var extLicenseWeight = truckImage?.name?.split(".")?.get(1)
        var fileLicenseWeight =
            (truckImage?.asRequestBody("image/$extLicenseWeight".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "truck_image",
                    truckImage?.name,
                    it
                )
            })

        makeApiCall(
            apiInterface.startJob(
                challan = fileLicense,
                token_id = token.toRequestBody(MultipartBody.FORM),
                truck_image = fileLicenseWeight,
                truck_weight = truckWeight.toRequestBody(MultipartBody.FORM)
            ),

            successMutableLiveData = mutableLiveStartJob,
            errorMutableLiveData = mutableLiveStartJobError

        )
    }

    fun emptyTruckImage(
        challan: File?,
        token: String,
        truck_weight: String
    ) {
        var extLicense = challan?.name?.split(".")?.get(1)
        var fileLicense =
            (challan?.asRequestBody("image/$extLicense".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "truck_image",
                    challan?.name,
                    it
                )
            })

        makeApiCall(
            apiInterface.emptyTruckAPI(
                truck_image = fileLicense,
                token_id = token.toRequestBody(MultipartBody.FORM),
                truck_weight = truck_weight.toRequestBody(MultipartBody.FORM)
            ),

            successMutableLiveData = mutableLiveEmptyTruck,
            errorMutableLiveData = mutableLiveEmptyTrucError

        )
    }

    fun addCard(
        card_name: String,
        expiry_month: String,
        expiry_year: String,
        number: String,
        cvv: String
    ) {
        makeApiCall(
            apiInterface.saveCard(
                user_master_id = getUserData()!!.user_master_id,
                card_name = card_name,
                expiry_month = expiry_month,
                expiry_year = expiry_year,
                number = number,
                cvv_number = cvv
            ),
            successMutableLiveData = mutableLiveDataSaveCard,
            errorMutableLiveData = mutableLiveDataSaveCardError
        )
    }

    fun editCard(
        card_name: String,
        expiry_month: String,
        expiry_year: String,
        number: String,
        cvv: String,
        card_id: String
    ) {
        makeApiCall(
            apiInterface.editCard(
                user_master_id = getUserData()!!.user_master_id,
                card_name = card_name,
                expiry_month = expiry_month,
                expiry_year = expiry_year,
                number = number,
                card_id = card_id,
                cvv_number = cvv
            ),
            successMutableLiveData = mutableLiveDataEditCard,
            errorMutableLiveData = mutableLiveDataEditCardError
        )
    }

    fun payoutAddCard(
        number: String,
        expiry_month: String,
        expiry_year: String,
        cvv: String
    ) {
        makeApiCall(
            apiInterface.payoutAddCard(
                user_master_id = getUserData()?.user_master_id?:"",
                connect_type = "1",
                cardnumber = number,
                exp_m = expiry_month,
                exp_y = expiry_year,
                cvc = cvv
            ),
            successMutableLiveData = mutableLiveDataPayoutAddCard,
            errorMutableLiveData = mutableLiveDataPayoutAddCardError
        )
    }

    fun payoutAddBank(
        number: String,
        routing_number:String,
        account_holder_name:String
    ) {
        makeApiCall(
            apiInterface.payoutAddBankAccount(
                user_master_id = getUserData()?.user_master_id?:"",
                connect_type = "2",
                account_number = number,
                routing_number = routing_number,
                account_holder_type = "individual",
                account_holder_name = account_holder_name,
                currency="usd",
                country="US"
            ),
            successMutableLiveData = mutableLiveDataPayoutAddBank,
            errorMutableLiveData = mutableLiveDataPayoutAddBankError
        )
    }

    fun getPayoutSettings() {
        makeApiCall(
            apiInterface.getPaymentSettings(
                user_master_id = getUserData()?.user_master_id?:""),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataPayoutSettings,
            errorMutableLiveData = mutableLiveDataPayoutSettingsError
        )
    }

    fun getAllPayoutDetails() {
        makeApiCall(
            apiInterface.getAllPayoutDetails(
                user_master_id = getUserData()?.user_master_id?:""),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataAllPayoutDetails,
            errorMutableLiveData = mutableLiveDataAllPayoutDetailsError
        )
    }

    fun marksAsDelay(
        token: String,
        delayReason: String,
        delayTime: String

    ) {
        makeApiCall(
            apiInterface.marksAsDelay(
                token_id = token,
                delay_reason = delayReason,
                delay_time = delayTime

            ),
            successMutableLiveData = mutableLiveDataMarksAsDelay,
            errorMutableLiveData = mutableLiveDataMarksAsDelayError
        )
    }

    fun getCardList() {
        makeApiCall(
            apiInterface.getCard(
                getUserData()?.user_master_id?:""
            ),
            responseJsonKeyword = "card_list",
            successMutableLiveData = mutableLiveDataCardList,
            errorMutableLiveData = mutableLiveDataCardListError
        )
    }

    fun setAsDefault(card_id: String) {
        makeApiCall(
            apiInterface.markAsDefault(
                user_master_id = getUserData()!!.user_master_id,
                card_id = card_id

            ),
            successMutableLiveData = mutableLiveDataMarkAsDefault,
            errorMutableLiveData = mutableLiveDataMarkAsDefaultError
        )
    }

    fun deleteCard(card_id: String) {
        makeApiCall(
            apiInterface.deleteCard(
                card_id = card_id

            ),
            successMutableLiveData = mutableLiveDataDeleteCard,
            errorMutableLiveData = mutableLiveDataDeleteCardError
        )
    }

    fun getNearByConductorList(lat: String, long: String, distance: String) {
        makeApiCall(
            apiInterface.getNearByConductor(
                lat = lat,
                long = long,
                distance = distance
            ),
            responseJsonKeyword = "userdata",
            successMutableLiveData = mutableLiveDataNearByConductorList,
            errorMutableLiveData = mutableLiveDataNearByConductorListError
        )
    }

    fun getTruckStatus(job_id: String, builder_id: String) {
        if (getUserData()!!.user_role.equals("2")) {
            makeApiCall(
                apiInterface.getTruckStatus(
                    job_id = job_id,
                    builder_id = builder_id,
                    supplier_id = "",
                    fleet_id = ""
                ),
                responseJsonKeyword = "jobs",
                successMutableLiveData = mutableLiveDataTruckStatusList,
                errorMutableLiveData = mutableLiveDataTruckStatusListError
            )
        } else if (getUserData()!!.user_role.equals("3")) {
            makeApiCall(
                apiInterface.getTruckStatus(
                    job_id = job_id,
                    builder_id = "",
                    supplier_id = builder_id,
                    fleet_id = ""
                ),
                responseJsonKeyword = "jobs",
                successMutableLiveData = mutableLiveDataTruckStatusList,
                errorMutableLiveData = mutableLiveDataTruckStatusListError
            )
        } else {
            makeApiCall(
                apiInterface.getTruckStatus(
                    job_id = job_id,
                    builder_id = "",
                    supplier_id = "",
                    fleet_id = builder_id
                ),
                responseJsonKeyword = "jobs",
                successMutableLiveData = mutableLiveDataTruckStatusList,
                errorMutableLiveData = mutableLiveDataTruckStatusListError
            )
        }

    }

    fun getDefaultCard() {
        makeApiCall(
            apiInterface.getDefaultCard(
                user_master_id = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "card_list",
            successMutableLiveData = mutableLiveDataDefaultCard,
            errorMutableLiveData = mutableLiveDataDefaultCardError
        )
    }

    fun getDefaultCardSubscription() {
        makeApiCall(
            apiInterface.getDefaultCard(
                user_master_id = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "card_list",
            successMutableLiveData = mutableLiveDataDefaultCardSubscription,
            errorMutableLiveData = mutableLiveDataDefaultCardSubscriptionError
        )
    }

    fun getDefaultCardCancelation() {
        makeApiCall(
            apiInterface.getDefaultCard(
                user_master_id = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "card_list",
            successMutableLiveData = mutableLiveDataDefaultCardCancelation,
            errorMutableLiveData = mutableLiveDataDefaultCardCancelationError
        )
    }

    fun getDefaultCardHaul() {
        makeApiCall(
            apiInterface.getDefaultCard(
                user_master_id = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "card_list",
            successMutableLiveData = mutableLiveDataDefaultCardHault,
            errorMutableLiveData = mutableLiveDataDefaultCardErrorHault
        )
    }

    fun getMaterialEquipmentList() {
        makeApiCall(
            apiInterface.getMeterialEquipmentList(getUserData()?.user_master_id ?: ""),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataMaterialEquipment,
            errorMutableLiveData = mutableLiveDataMaterialEquiementError
        )
    }

    fun getLoadingEquipmentOptionList() {
        makeApiCall(
            apiInterface.getEquipementOption("L"),
            responseJsonKeyword = "loading_equipment_list",
            successMutableLiveData = mutableLiveDataLoadingEquipment,
            errorMutableLiveData = mutableLiveDataLoadingEquiementError
        )
    }

    fun getSpreadingEquipmentOptionList() {
        makeApiCall(
            apiInterface.getEquipementOption("S"),
            responseJsonKeyword = "spreading_equipment_list",
            successMutableLiveData = mutableLiveDataSpreadingEquipment,
            errorMutableLiveData = mutableLiveDataSpreadingEquiementError
        )
    }


    fun submitReview(
        comment: String,
        job_id: String,
        rating: String,
        receiver_id: String,
        sender_id: String

    ) {
        makeApiCall(
            apiInterface.submitReview(
                comment = comment,
                job_id = job_id,
                rating = rating,
                receiver_id = receiver_id,
                sender_id = sender_id
            ),
            successMutableLiveData = mutableLiveDataSubmitReview,
            errorMutableLiveData = mutableLiveDataSubmitReviewError
        )
    }

    fun getReView(
        job_id: String,
        receiver_id: String,
        sender_id: String

    ) {
        makeApiCall(
            apiInterface.getReview(
                job_id = job_id,
                receiver_id = receiver_id,
                sender_id = sender_id
            ),
            responseJsonKeyword = "Reviews",
            successMutableLiveData = mutableLiveDataGetReview,
            errorMutableLiveData = mutableLiveDataGetReviewError
        )
    }

    fun submitSOSReason(
        reason: String,
        token_id: String,
        driver_id: String
    ) {
        makeApiCall(
            apiInterface.submitSOSReason(
                reason = reason,
                token_id = token_id,
                driver_id = driver_id
            ),
            successMutableLiveData = mutableLiveDataSubmitSOSReason,
            errorMutableLiveData = mutableLiveDataSubmitSOSReasonError
        )
    }

    fun getSOSReason() {
        makeApiCall(
            apiInterface.getSOSReason("driver"),
            responseJsonKeyword = "sos_reasons",
            successMutableLiveData = mutableLiveDataSOSReason,
            errorMutableLiveData = mutableLiveDataSOSReasonError
        )
    }

    fun getProfileData(userId: String) {
        makeApiCall(
            apiInterface.getProfileData(
                userId
            ),
            responseJsonKeyword = "profile",
            callback = object : SuperRepositoryCallback<ProfileDetails> {
                override fun success(result: ProfileDetails) {
                    super.success(result)
                    mutableLiveDataProfileDetail.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataProfileDetailError
        )
    }

    fun getProfileDataNew(userId: String) {
        makeApiCall(
            apiInterface.getProfileData(
                userId
            ),
            responseNotEnclosedInInnerResponseKeyword=true,
            callback = object : SuperRepositoryCallback<ProfileResponse> {
                override fun success(result: ProfileResponse) {
                    super.success(result)
                    mutableLiveDataAllProfileDetail.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataAllProfileDetailError
        )
    }

    fun updateProfile(
        firstName: String? = null,
        lastName: String? = null,
        mobile: String? = null,
        address: String? = null,
        country: String? = null,
        state: String? = null,
        pinCode: String? = null,
        city: String? = null,
        dob: String? = null,
        calRegistration: String? = null,
        userId: String,
        companyName: String? = null,
        companyRegistration: String? = null,
        imageFile: File? = null
    ) {
        var ext = imageFile?.name?.split(".")?.get(1)
        var file = (imageFile?.asRequestBody("image/$ext".toMediaType())?.let {
            MultipartBody.Part.createFormData(
                "profile_image",
                imageFile?.name,
                it
            )
        })

        makeApiCall(
            apiInterface.updateProfile(
                firstName = firstName?.toRequestBody(MultipartBody.FORM),
                lastName = lastName?.toRequestBody(MultipartBody.FORM),
                mobile = mobile?.toRequestBody(MultipartBody.FORM),
                address = address?.toRequestBody(MultipartBody.FORM),
                country = country?.toRequestBody(MultipartBody.FORM),
                state = state?.toRequestBody(MultipartBody.FORM),
                pinCode = pinCode?.toRequestBody(MultipartBody.FORM),
                dob = dob?.toRequestBody(MultipartBody.FORM),
                calRegistration = calRegistration?.toRequestBody(MultipartBody.FORM),
                userId = userId.toRequestBody(MultipartBody.FORM),
                companyName = companyName?.toRequestBody(MultipartBody.FORM),
                companyRegistration = companyRegistration?.toRequestBody(MultipartBody.FORM),
                profile_image = file
            ),
            successMutableLiveData = mutableLiveDataUpdateProfile,
            errorMutableLiveData = mutableLiveDataUpdateProfileError

        )
    }


    fun searchTruck(truck_type: String, fleet_type: String) {
        makeApiCall(
            apiInterface.searchTruck(
                fleet_type = fleet_type,
                truck_type = truck_type

            ),
            responseJsonKeyword = "trucks",
            callback = object : SuperRepositoryCallback<ArrayList<SearchTruckListResponse>> {
                override fun success(result: ArrayList<SearchTruckListResponse>) {
                    super.success(result)
                    mutableLiveDataSearchTruckList.postValue(
                        Event(
                            result,
                            false
                        )
                    )
                }
            },
            errorMutableLiveData = mutableLiveDataSearchTruckListError
        )
    }

    fun getPayoutDetails(job_id: String) {
        makeApiCall(
            apiInterface.payoutDetails(
                job_id = job_id
            ),
            responseJsonKeyword = "payout_details",
            successMutableLiveData = mutableLiveDataPayoutDetails,
            errorMutableLiveData = mutableLiveDataPayoutDetailsError
        )
    }

    fun getImportExportJobDetails(haul_request_id: String) {
        makeApiCall(
            apiInterface.importExportJobDetails(
                haul_request_id = haul_request_id,
                userId = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "haul_details",
            successMutableLiveData = mutableLiveDataImportExportDetails,
            errorMutableLiveData = mutableLiveDataImportExportError
        )
    }

    fun matchesJobDetails(haul_request_id: String) {
        makeApiCall(
            apiInterface.importExportJobDetails(
                haul_request_id = haul_request_id,
                userId = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "haul_details",
            successMutableLiveData = mutableLiveDataSearchImportExportDetails,
            errorMutableLiveData = mutableLiveDataSearchImportExportError
        )
    }

    fun getNotification() {
        makeApiCall(
            apiInterface.getNotification(
                userId = getUserData()!!.user_master_id
            ),
            successMutableLiveData = mutableLiveDataNotificationDetails,
            responseNotEnclosedInInnerResponseKeyword = true,
            errorMutableLiveData = mutableLiveDataNotificationDetailsError
        )
    }

    fun getNotificationsList(type:String) {
        makeApiCall(
            apiInterface.getNotificationList(
               getUserData()?.user_master_id?:"",
                type
            ),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataNotificationsList,
            errorMutableLiveData = mutableLiveDataNotificationsListError
        )
    }

    fun getNotificationSOSMessage() {
        makeApiCall(
            apiInterface.getNotification(
                userId = getUserData()!!.user_master_id
            ),
            successMutableLiveData = mutableLiveDataNotificationDetailsSOS,
            responseNotEnclosedInInnerResponseKeyword = true,
            errorMutableLiveData = mutableLiveDataNotificationDetailsSOSError
        )
    }

    fun notificationRead() {
        makeApiCall(
            apiInterface.notificationRead(
                user_master_id = getUserData()!!.user_master_id,
                read_status = "1"
            ),
            successMutableLiveData = mutableLiveDataNotificationRead,
            errorMutableLiveData = mutableLiveDataNotificationReadError
        )
    }

    fun jobPostHaulOff(
        materialType: String,
        MaterialoptionType: String,
        spec_sheet: String,
        lib_information: String,
        material_cont: String,
        Source: String,
        Destination: String,
        haul_paymemt_amt: String,
        payment_type: String,
        stripeToken: String,
        materials_available: String,
        source_lat: String,
        source_long: String,
        destination_lat: String,
        destination_long: String,
        TruckType: String,
        NoOfTrucks: String,
        pickup_date: String,
        pickup_time: String,
        delivery_time: String,
        delivery_date: String,
        equipmentId: String,
        equipmentOptionId: String,
        export_request_id: String,
        exporter_id: String,
        haul_request_mapid: String,
        import_request_id: String,
        specs_sheet: String,
        importer_id: String
    ) {


        makeApiCall(
            apiInterface.postHaulOffJob(
                user_master_id = getUserData()!!.user_master_id.toRequestBody(MultipartBody.FORM),
                MaterialType = materialType.toRequestBody(MultipartBody.FORM),
                MaterialoptionType = MaterialoptionType.toRequestBody(MultipartBody.FORM),
                spec_sheet = spec_sheet.toRequestBody(MultipartBody.FORM),
                lib_information = lib_information.toRequestBody(MultipartBody.FORM),
                material_cont = material_cont.toRequestBody(MultipartBody.FORM),
                materials_available = materials_available.toRequestBody(MultipartBody.FORM),
                Source = Source.toRequestBody(MultipartBody.FORM),
                Destination = Destination.toRequestBody(MultipartBody.FORM),
                haul_paymemt_amt = haul_paymemt_amt.toRequestBody(MultipartBody.FORM),
                payment_type = payment_type.toRequestBody(MultipartBody.FORM),
                stripeToken = stripeToken.toRequestBody(MultipartBody.FORM),
                source_lat = source_lat.toRequestBody(MultipartBody.FORM),
                source_long = source_long.toRequestBody(MultipartBody.FORM),
                destination_lat = destination_lat.toRequestBody(MultipartBody.FORM),
                destination_long = destination_long.toRequestBody(MultipartBody.FORM),
                TruckType = TruckType.toRequestBody(MultipartBody.FORM),
                NoOfTrucks = NoOfTrucks.toRequestBody(MultipartBody.FORM),
                pickup_date = pickup_date.toRequestBody(MultipartBody.FORM),
                pickup_time = pickup_time.toRequestBody(MultipartBody.FORM),
                delivery_time = delivery_time.toRequestBody(MultipartBody.FORM),
                delivery_date = delivery_date.toRequestBody(MultipartBody.FORM),
                Equipment = equipmentId.toRequestBody(MultipartBody.FORM),
                export_request_id = export_request_id.toRequestBody(MultipartBody.FORM),
                exporter_id = exporter_id.toRequestBody(MultipartBody.FORM),
                haul_request_mapid = haul_request_mapid.toRequestBody(MultipartBody.FORM),
                import_request_id = import_request_id.toRequestBody(MultipartBody.FORM),
                importer_id = importer_id.toRequestBody(MultipartBody.FORM),
                specs_sheet = specs_sheet.toRequestBody(MultipartBody.FORM)
            ),
            successMutableLiveData = mutableLiveDataJobHaulConfigration,
            errorMutableLiveData = mutableLiveDataJobHaulConfigrationError
        )
    }


    fun postExportStep1(
        haul_request_id: String,
        mapLat: String,
        mapLng: String,
        address: String,
        point_of_contact: String,
        main_number: String,
        email: String,
        materialImageList: ArrayList<File>

    ) {
        val materialImage = ArrayList<MultipartBody.Part?>()

        for (i in 0 until materialImageList.size) {
            var ext = materialImageList.get(i)?.name?.split(".")?.get(1)
            var file = (materialImageList.get(i)?.asRequestBody("image/$ext".toMediaType())?.let {
                MultipartBody.Part.createFormData(
                    "haul_request_image[${i}]",
                    materialImageList.get(i)?.name,
                    it
                )
            })
            materialImage.add(file)
        }
        makeApiCall(
            apiInterface.postExportStep1(
                haul_request_id = haul_request_id.toRequestBody(MultipartBody.FORM),
                mapLat = mapLat.toRequestBody(MultipartBody.FORM),
                mapLng = mapLng.toRequestBody(MultipartBody.FORM),
                address = address.toRequestBody(MultipartBody.FORM),
                point_of_contact = point_of_contact.toRequestBody(MultipartBody.FORM),
                main_number = main_number.toRequestBody(MultipartBody.FORM),
                email = email.toRequestBody(MultipartBody.FORM),
                materialImage = materialImage
            ),
            successMutableLiveData = mutableLiveDataJobExportStep1,
            errorMutableLiveData = mutableLiveDataJobExportStep1Error
        )
    }


    fun postImportStep1(
        haul_request_id: String,
        mapLat: String,
        mapLng: String,
        address: String,
        point_of_contact: String,
        main_number: String,
        email: String


    ) {

        makeApiCall(
            apiInterface.postImportStep1(
                haul_request_id = haul_request_id.toRequestBody(MultipartBody.FORM),
                mapLat = mapLat.toRequestBody(MultipartBody.FORM),
                mapLng = mapLng.toRequestBody(MultipartBody.FORM),
                address = address.toRequestBody(MultipartBody.FORM),
                point_of_contact = point_of_contact.toRequestBody(MultipartBody.FORM),
                main_number = main_number.toRequestBody(MultipartBody.FORM),
                email = email.toRequestBody(MultipartBody.FORM)

            ),
            successMutableLiveData = mutableLiveDataJobImportStep1,
            errorMutableLiveData = mutableLiveDataJobImportStep1Error
        )
    }

    fun postExportStep2(
        haul_request_id: String,
        MaterialType: String,
        material_other: String,
        MaterialoptionType: String,
        cubic_yards: String,
        tons: String,
        specsDocumentFile: File?

    ) {

        var ext = specsDocumentFile?.name?.split(".")?.get(1)
        var file = (specsDocumentFile?.asRequestBody("image/$ext".toMediaType())?.let {
            MultipartBody.Part.createFormData(
                "specs_sheet",
                specsDocumentFile?.name,
                it
            )
        })
        makeApiCall(
            apiInterface.postExportStep2(
                haul_request_id = haul_request_id.toRequestBody(MultipartBody.FORM),
                MaterialType = MaterialType.toRequestBody(MultipartBody.FORM),
                material_other = material_other.toRequestBody(MultipartBody.FORM),
                MaterialoptionType = MaterialoptionType.toRequestBody(MultipartBody.FORM),
                cubic_yards = cubic_yards.toRequestBody(MultipartBody.FORM),
                tons = tons.toRequestBody(MultipartBody.FORM),
                specsDocuments = file
            ),
            successMutableLiveData = mutableLiveDataJobExportStep2,
            errorMutableLiveData = mutableLiveDataJobExportStep2Error
        )
    }

    fun postExportStep3(
        haul_request_id: String,
        equipment_type: String,
        equipment_other: String,
        equipment_any: String,
        additional_notes: String
    ) {

        makeApiCall(
            apiInterface.postExportStep3(
                haul_request_id = haul_request_id.toRequestBody(MultipartBody.FORM),
                equipment_type = equipment_type.toRequestBody(MultipartBody.FORM),
                equipment_other = equipment_other.toRequestBody(MultipartBody.FORM),
                equipment_any = equipment_any.toRequestBody(MultipartBody.FORM),
                additional_notes = additional_notes.toRequestBody(MultipartBody.FORM)

            ),
            successMutableLiveData = mutableLiveDataJobExportStep3,
            errorMutableLiveData = mutableLiveDataJobExportStep3Error
        )
    }


    fun postImporStep2(
        haul_request_id: String,
        MaterialType: String,
        material_other: String,
        MaterialoptionType: String,
        cubic_yards: String,
        tons: String,
        equipment_type: String,
        equipment_other: String,
        equipment_any: String,
        spreading_equipment_type: String,
        spreading_equipment_other: String,
        additional_notes: String
    ) {

        makeApiCall(
            apiInterface.postImportStep2(
                haul_request_id = haul_request_id.toRequestBody(MultipartBody.FORM),
                MaterialType = MaterialType.toRequestBody(MultipartBody.FORM),
                material_other = material_other.toRequestBody(MultipartBody.FORM),
                MaterialoptionType = MaterialoptionType.toRequestBody(MultipartBody.FORM),
                cubic_yards = cubic_yards.toRequestBody(MultipartBody.FORM),
                tons = tons.toRequestBody(MultipartBody.FORM),
                equipment_type = equipment_type.toRequestBody(MultipartBody.FORM),
                equipment_other = equipment_other.toRequestBody(MultipartBody.FORM),
                equipment_any = equipment_any.toRequestBody(MultipartBody.FORM),
                spreading_equipment_type = spreading_equipment_type.toRequestBody(MultipartBody.FORM),
                spreading_equipment_other = spreading_equipment_other.toRequestBody(MultipartBody.FORM),
                additional_notes = additional_notes.toRequestBody(MultipartBody.FORM)

            ),
            successMutableLiveData = mutableLiveDataJobImportStep2,
            errorMutableLiveData = mutableLiveDataJobImportStep2Error
        )
    }

    fun getPaymentPercentage() {
        makeApiCall(
            apiInterface.getPaymentPercentage(
                user_master_id = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataPaymentPercentage,
            errorMutableLiveData = mutableLiveDataPaymentPercentageError
        )
    }

    fun getPaymentPercentagePostJob() {
        makeApiCall(
            apiInterface.getPaymentPercentage(
                user_master_id = getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataPaymentPercentageJobPost,
            errorMutableLiveData = mutableLiveDataPaymentPercentageJobPostError
        )
    }

    fun sendImportRequest(importer_request_id: String, exporter_request_id: String) {
        makeApiCall(
            apiInterface.sendImportRequest(
                importer_request_id = importer_request_id,
                exporter_request_id = exporter_request_id
            ),
            responseJsonKeyword = "",
            successMutableLiveData = mutableLiveDataImportRequestData,
            errorMutableLiveData = mutableLiveDataImportRequestError
        )
    }

    fun sendExportRequest(importer_request_id: String, exporter_request_id: String) {
        makeApiCall(
            apiInterface.sendExportRequest(
                importer_request_id = importer_request_id,
                exporter_request_id = exporter_request_id
            ),
            responseJsonKeyword = "",
            successMutableLiveData = mutableLiveDataExportRequestData,
            errorMutableLiveData = mutableLiveDataExportRequestError
        )
    }

    fun requestAcceptedByExporter(request_map_id: String) {
        makeApiCall(
            apiInterface.requestAcceptByExporter(
                request_map_id = request_map_id
            ),
            responseJsonKeyword = "",
            successMutableLiveData = mutableLiveDataRequestAcceptedByExpoterData,
            errorMutableLiveData = mutableLiveDataRequestAcceptedByExpoterError
        )
    }

    fun cancelHaulofSubscription() {
        makeApiCall(
            apiInterface.cancelHaulofSubscription(
                getUserData()!!.user_master_id,
                getUserData()!!.user_role
            ),
            responseJsonKeyword = "",
            successMutableLiveData = mutableLiveDataCancelHultOffSubscriptionData,
            errorMutableLiveData = mutableLiveDataCancelHultOffSubscriptionError
        )
    }

    fun getCancelJobDetails(job_id: String, user_type: String) {
        makeApiCall(
            apiInterface.getCancelJobDetails(
                job_id = job_id,
                user_id = getUserData()!!.user_master_id,
                city_id = getUserData()!!.city ?: "",
                user_type = user_type
            ),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataJobCancelationChargeData,
            errorMutableLiveData = mutableLiveDataJobCancelationChargeDataError
        )
    }

    fun fleetcheckDashboardStatus() {
        makeApiCall(
            apiInterface.fleetcheckDashboardStatus(
                getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataFleetUserDashboardData,
            errorMutableLiveData = mutableLiveDataFleetUserDashboardDataError
        )
    }

    fun getHaulofSubscriptionStatus() {
        makeApiCall(
            apiInterface.getHaulofSubscriptionStatus(
                getUserData()!!.user_master_id
            ),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataHaultOfJobDashboardData,
            errorMutableLiveData = mutableLiveDataHaultOfJobDashboardError
        )
    }

    fun requestCancel(request_map_id: String) {
        makeApiCall(
            apiInterface.requestReject(
                request_map_id = request_map_id
            ),
            responseJsonKeyword = "",
            successMutableLiveData = mutableLiveDataRequestRejectData,
            errorMutableLiveData = mutableLiveDataRequestRejectError
        )
    }

    fun updateDriverCurrentLocation(job_id:String, lat:String, lng:String) {
        makeApiCall(
            apiInterface.updateDriverCurrentLocation(
                job_id = job_id,
                lat = lat,
                lng = lng
            ),
            responseJsonKeyword = "",
            successMutableLiveData = mutableLiveDataUpdCurrLocation,
            errorMutableLiveData = mutableLiveDataUpdCurrLocationError
        )
    }

    fun getDriverLocation(job_id:String) {
        makeApiCall(
            apiInterface.getDriverCurrentLocation(
                job_id = job_id
            ),
            responseJsonKeyword = "data",
            successMutableLiveData = mutableLiveDataDriverLocation,
            errorMutableLiveData = mutableLiveDataDriverLocationError
        )
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //public Methods
    fun getUserType(): UserType {
        if (getUserData()!!.user_role.equals("2")) {
            return UserType.BUILDER
        } else if (getUserData()!!.user_role.equals("3")) {
            return UserType.MANUFACTURER
        } else if (getUserData()!!.user_role.equals("4")) {
            return UserType.TRUCK
        } else {
            return UserType.TRUCK
        }
    }

    fun getSharedPref(): SharedPreferences {
        return context.getSharedPreferences(
            KeywordsAndConstants.SHARED_PREF_DB,
            Context.MODE_PRIVATE
        )
    }

    fun getDashboardMenuItems(): ArrayList<DashboardGridItem> {
        val list: ArrayList<DashboardGridItem> = ArrayList()

        when (getUserType()) {
            UserType.TRUCK -> {
                list.addAll(
                    arrayListOf(
                        DashboardGridItem(
                            "Certified Document",
                            R.drawable.sos_additional_requests,
                            menu = Menus.SOS_ADDITIONAL_REQUEST
                        ),
                        DashboardGridItem(
                            image = R.drawable.certified_document,
                            name = "SOS",
                            menu = Menus.SOS_MESSAGE_LOG
                        ),
                        DashboardGridItem(
                            "Manage Truck",
                            R.drawable.truck_with_window,
                            menu = Menus.MANAGE_TRUCK
                        ),
                        DashboardGridItem(
                            "My Bookings",
                            R.drawable.my_bookings,
                            menu = Menus.MY_BOOKINGS
                        ),
                        DashboardGridItem(
                            "Jobs List",
                            R.drawable.job,
                            menu = Menus.JOB_LIST_TRUCK_FLEET
                        ),
                        /*  DashboardGridItem(
                              "Manage Account",
                              R.drawable.account,
                              menu = Menus.MANAGE_ACCOUNT
                          ),*/
                        DashboardGridItem(
                            "Track Status",
                            R.drawable.track_status,
                            menu = Menus.TRACK_STATUS
                        ),
                        DashboardGridItem(
                            "Manage Driver",
                            R.drawable.manager_driver,
                            menu = Menus.MANAGE_DRIVER
                        ),
                        DashboardGridItem(
                            "Alerts",
                            R.drawable.alerts,
                            menu = Menus.ALERTS
                        ),
                        DashboardGridItem(
                            "Logout",
                            R.drawable.logout,
                            menu = Menus.LOGOUT
                        )
                    )
                )
            }
            UserType.MANUFACTURER -> {
                list.addAll(
                    arrayListOf(

                        DashboardGridItem(
                            "Near By Independent Truck/Fleet",
                            R.drawable.account,
                            menu = Menus.NEAR_BY_CONTRACTOR
                        ),
                        DashboardGridItem(
                            "Search Contractor",
                            R.drawable.search_contractor,
                            menu = Menus.SEARCH_CONTRACTOR
                        ),
                        DashboardGridItem(
                            "Post A Job",
                            R.drawable.job,
                            menu = Menus.POST_A_JOB
                        ),
                        DashboardGridItem(
                            "My Posted Jobs",
                            R.drawable.job,
                            menu = Menus.MY_JOBS
                        ),
                        DashboardGridItem(
                            "Search Truck",
                            R.drawable.search_truck,
                            menu = Menus.SEARCH_TRUCK
                        ),
                        DashboardGridItem(
                            "Track Deliveries",
                            R.drawable.tracking_truck_live,
                            menu = Menus.TRACKING_TRUCKS_LIVE
                        ),
                        DashboardGridItem(
                            "Alerts",
                            R.drawable.alerts,
                            menu = Menus.ALERTS
                        ),
                        DashboardGridItem(
                            "Haul-OFF",
                            R.drawable.haul_off,
                            menu = Menus.HAUL_OFF
                        ),
                        DashboardGridItem(
                            "Track Status",
                            R.drawable.track_status,
                            menu = Menus.TRACK_STATUS
                        ),
                        DashboardGridItem(
                            "Logout",
                            R.drawable.logout,
                            menu = Menus.LOGOUT
                        )
                    )
                )
            }
            UserType.BUILDER -> {
                list.addAll(
                    arrayListOf(
                        DashboardGridItem(
                            "Near By Independent Truck/Fleet",
                            R.drawable.account,
                            menu = Menus.NEAR_BY_CONTRACTOR
                        ),
                        DashboardGridItem(
                            image = R.drawable.search_contractor,
                            name = "Search Material/Supplier",
                            menu = Menus.SEARCH_MATERIAL_SUPPLIER
                        ),
                        DashboardGridItem(
                            "Post A Job",
                            R.drawable.job,
                            menu = Menus.POST_A_JOB
                        ),
                        DashboardGridItem(
                            "My Posted Jobs",
                            R.drawable.job,
                            menu = Menus.MY_JOBS
                        ),
                        DashboardGridItem(
                            "Search Truck",
                            R.drawable.search_truck,
                            menu = Menus.SEARCH_TRUCK
                        ),
                        DashboardGridItem(
                            "Track Deliveries",
                            R.drawable.tracking_truck_live,
                            menu = Menus.TRACKING_TRUCKS_LIVE
                        ),
                        DashboardGridItem(
                            "Alerts",
                            R.drawable.alerts,
                            menu = Menus.ALERTS
                        ),
                        DashboardGridItem(
                            "Haul-OFF",
                            R.drawable.haul_off,
                            menu = Menus.HAUL_OFF
                        ),
                        DashboardGridItem(
                            "Track Status",
                            R.drawable.track_status,
                            menu = Menus.TRACK_STATUS
                        ),
                        DashboardGridItem(
                            "Logout",
                            R.drawable.logout,
                            menu = Menus.LOGOUT
                        )
                    )
                )
            }
        }

        return list
    }

    fun saveUserDataToPref(userData: UserDetails) {
        getSharedPref()
            .edit()
            .putString(
                KeywordsAndConstants.USER_DATA,
                toJson(userData)
            )
            .apply()
    }


    fun getNavMenuItems(): ArrayList<NavMenuItem> {
        val list: ArrayList<NavMenuItem> = ArrayList()

        when (getUserType()) {
            UserType.TRUCK -> {
                list.addAll(
                    arrayListOf(
                        NavMenuItem(
                            R.drawable.home,
                            "Home",
                            menu = Menus.HOME
                        ),
                        NavMenuItem(
                            R.drawable.sos_additional_requests,
                            "Certified Document",
                            menu = Menus.SOS_ADDITIONAL_REQUEST
                        ),
                        NavMenuItem(
                            R.drawable.certified_document,
                            "SOS",
                            menu = Menus.SOS_MESSAGE_LOG
                        ),
                        NavMenuItem(
                            R.drawable.truck,
                            "Manage Truck",
                            menu = Menus.MANAGE_TRUCK
                        ),
                        NavMenuItem(
                            R.drawable.my_bookings,
                            "My Bookings",
                            menu = Menus.MY_BOOKINGS
                        ),
                        NavMenuItem(
                            R.drawable.job,
                            "Search for Jobs",
                            menu = Menus.SEARCH_FOR_JOBS
                        ),
                           NavMenuItem(
                               R.drawable.account,
                               "Payment Settings",
                               menu = Menus.PAYOUT_SETTINGS
                           ),
                        NavMenuItem(
                            R.drawable.account,
                            "Payout Details",
                            menu = Menus.PAYOUT_DETAILS
                        ),
                        NavMenuItem(
                            R.drawable.track_status,
                            "Truck Status",
                            menu = Menus.TRACKING_TRUCKS_LIVE
                        ),
                        NavMenuItem(
                            R.drawable.ic_subscribe,
                            "Certification Free",
                            menu = Menus.SUBSCRIPTION_LIST
                        ),
                        NavMenuItem(
                            R.drawable.alerts,
                            "Alerts",
                            menu = Menus.ALERTS
                        ),
                        NavMenuItem(
                            R.drawable.communicate,
                            "Communicate",
                            menu = Menus.COMMUNICATE,
                            innerItems = arrayListOf(
                                NavMenuInnerItem(
                                    "About Us",
                                    menu = Menus.ABOUT_US
                                ),
                                NavMenuInnerItem(
                                    "Contact Us",
                                    menu = Menus.CONTACT_US
                                ),
                                NavMenuInnerItem(
                                    "Logout",
                                    menu = Menus.LOGOUT
                                )
                            )
                        )
                    )
                )
            }
            UserType.MANUFACTURER -> {
                list.addAll(
                    arrayListOf(
                        NavMenuItem(
                            R.drawable.home,
                            "Home",
                            menu = Menus.HOME
                        ),
                        NavMenuItem(
                            R.drawable.account,
                            "Near By Contractor",
                            menu = Menus.NEAR_BY_CONTRACTOR
                        ),
                        NavMenuItem(
                            R.drawable.search_contractor,
                            "Search Contractor",
                            menu = Menus.SEARCH_CONTRACTOR
                        ),
                        NavMenuItem(
                            R.drawable.job,
                            "Post A Job",
                            menu = Menus.POST_A_JOB
                        ),
                        NavMenuItem(
                            R.drawable.job,
                            "My Jobs",
                            menu = Menus.MY_JOBS
                        ),
                        NavMenuItem(
                            R.drawable.tracking_truck_live,
                            "Track Deliveries",
                            menu = Menus.TRACKING_TRUCKS_LIVE
                        ),
                        NavMenuItem(
                            R.drawable.search_truck,
                            "Search Trucks",
                            menu = Menus.SEARCH_TRUCK
                        ),
                        NavMenuItem(
                            R.drawable.ic_manage_card,
                            "Manage Card",
                            menu = Menus.MANAGE_CARD
                        ),
                        NavMenuItem(
                            R.drawable.ic_subscribe,
                            "Subscription",
                            menu = Menus.SUBSCRIPTION_LIST
                        ),
                        NavMenuItem(
                            R.drawable.warning,
                            "Alerts",
                            menu = Menus.ALERTS
                        ),
                        NavMenuItem(
                            R.drawable.haul_off,
                            "Haul-OFF",
                            menu = Menus.HAUL_OFF
                        ),
                        NavMenuItem(
                            R.drawable.track_status,
                            "Track Status",
                            menu = Menus.TRACK_STATUS
                        ),

                        NavMenuItem(
                            R.drawable.communicate,
                            "Communicate",
                            menu = Menus.COMMUNICATE,
                            innerItems = arrayListOf(
                                NavMenuInnerItem(
                                    "About Us",
                                    menu = Menus.ABOUT_US
                                ),
                                NavMenuInnerItem(
                                    "Contact Us",
                                    menu = Menus.CONTACT_US
                                ),
                                NavMenuInnerItem(
                                    "Logout",
                                    menu = Menus.LOGOUT
                                )
                            )
                        )
                    )
                )
            }
            UserType.BUILDER -> {
                list.addAll(
                    arrayListOf(
                        NavMenuItem(
                            R.drawable.home,
                            "Home",
                            menu = Menus.HOME
                        ),
                        NavMenuItem(
                            name = "Near By Independent Truck/Fleet",
                            image = R.drawable.account,
                            menu = Menus.NEAR_BY_CONTRACTOR
                        ),
                        NavMenuItem(
                            R.drawable.search_contractor,
                            "Search Material/Supplier",
                            menu = Menus.SEARCH_MATERIAL_SUPPLIER
                        ),
                        NavMenuItem(
                            R.drawable.job,
                            "Post A Job",
                            menu = Menus.POST_A_JOB
                        ),
                        NavMenuItem(
                            R.drawable.job,
                            "My Jobs",
                            menu = Menus.MY_JOBS
                        ),
                        NavMenuItem(
                            R.drawable.tracking_truck_live,
                            "Track Deliveries",
                            menu = Menus.TRACKING_TRUCKS_LIVE
                        ),
                        NavMenuItem(
                            R.drawable.search_truck,
                            "Search Trucks",
                            menu = Menus.SEARCH_TRUCK
                        ),
                        NavMenuItem(
                            R.drawable.ic_manage_card,
                            "Manage Card",
                            menu = Menus.MANAGE_CARD
                        ),
                        NavMenuItem(
                            R.drawable.ic_subscribe,
                            "Subscription",
                            menu = Menus.SUBSCRIPTION_LIST
                        ),
                        NavMenuItem(
                            R.drawable.warning,
                            "Alerts",
                            menu = Menus.ALERTS
                        ),
                        NavMenuItem(
                            R.drawable.haul_off,
                            "Haul-OFF",
                            menu = Menus.HAUL_OFF
                        ),
                        NavMenuItem(
                            R.drawable.track_status,
                            "Track Status",
                            menu = Menus.TRACK_STATUS
                        ),
                        NavMenuItem(
                            R.drawable.communicate,
                            "Communicate",
                            menu = Menus.COMMUNICATE,
                            innerItems = arrayListOf(
                                NavMenuInnerItem(
                                    "About Us",
                                    menu = Menus.ABOUT_US
                                ),
                                NavMenuInnerItem(
                                    "Contact Us",
                                    menu = Menus.CONTACT_US
                                ),
                                NavMenuInnerItem(
                                    "Logout",
                                    menu = Menus.LOGOUT
                                )
                            )
                        )
                    )
                )
            }
        }

        return list
    }

    fun clearAllData() {
        getSharedPref().edit().clear().apply()
    }


    fun getUserData(): UserDetails? {
        if (
            getSharedPref().getString(KeywordsAndConstants.USER_DATA, "").equals("")
        )
            return null
        return fromJson<UserDetails>(
            getSharedPref().getString(
                KeywordsAndConstants.USER_DATA,
                ""
            )!!
        )
    }

    fun startMonitoringLocation() {
        context.startService(
            Intent(
                context,
                LocationMonitorForegroundService::class.java
            )
        )
    }

    fun stopMonitoringLocation() {
        context.stopService(
            Intent(
                context,
                LocationMonitorForegroundService::class.java
            )
        )
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //private Methods
    ////////////////////////////////////////////////////////////////////////////////////////////////
}
